package com.apptmyz.wms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.Response;
import com.apptmyz.wms.data.SubProjectMasterModel;
import com.apptmyz.wms.data.SubprojectResponse;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PickListSubprojectActivity extends BaseActivity {

    TextView titleTv;
    RecyclerView customersRv;
    LinearLayoutManager layoutManager;
    Context context;
    ArrayList<SubProjectMasterModel> subprojects = new ArrayList<>();
    CustomerAdapter customerAdapter;
    DataSource dataSource;
    EditText searchView;
    ImageView refreshIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_pick_list_subproject, frameLayout);

        context = PickListSubprojectActivity.this;
        dataSource = new DataSource(context);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Pick List");

        searchView = (EditText) findViewById(R.id.searchView);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setFilteredData(editable.toString());
            }
        });

        customersRv = (RecyclerView) findViewById(R.id.rv_subprojects);
        layoutManager = new LinearLayoutManager(context);
        customersRv.setLayoutManager(layoutManager);

        subprojects = dataSource.subprojects.getSubprojectsData();
        if (Utils.isValidArrayList(subprojects)) {
            customerAdapter = new CustomerAdapter(subprojects);
            customersRv.setAdapter(customerAdapter);
        } else {
            if (Utils.getConnectivityStatus(context))
                new SubprojectsTask().execute();
        }
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_refresh:
                    if (Utils.getConnectivityStatus(context)) {
                        searchView.setText(null);
                        searchView.clearFocus();
                        new SubprojectsTask().execute();
                    }
                    break;
            }
        }
    };

    private void setFilteredData(String query) {
        ArrayList<SubProjectMasterModel> tempList = new ArrayList<>();

        for (SubProjectMasterModel model : subprojects) {
            if (model.getName().toLowerCase().contains(query.toLowerCase())) {
                tempList.add(model);
            }
        }
        customerAdapter = new CustomerAdapter(tempList);
        customersRv.setAdapter(customerAdapter);
        customerAdapter.notifyDataSetChanged();
    }

    ProgressDialog subprojectsDialog;

    class SubprojectsTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                subprojectsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getPickListSubprojectsUrl();

                Utils.logD("Log 1");
                SubprojectResponse response = (SubprojectResponse) HttpRequest
                        .getInputStreamFromUrl(url, SubprojectResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<SubProjectMasterModel> data = (ArrayList<SubProjectMasterModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            subprojects = data;
                            dataSource.subprojects.insertSubprojectData(subprojects);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && subprojectsDialog != null && subprojectsDialog.isShowing()) {
                subprojectsDialog.dismiss();
            }
            if (showErrorDialog()) {
                setData();
            }
            super.onPostExecute(result);
        }

    }

    private void setData() {
        customerAdapter = new CustomerAdapter(subprojects);
        customersRv.setAdapter(customerAdapter);
        customerAdapter.notifyDataSetChanged();
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PickListSubprojectActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.DataObjectHolder> {
        private ArrayList<SubProjectMasterModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView customerTv;
            RelativeLayout layout;

            public DataObjectHolder(View itemView) {
                super(itemView);

                layout = (RelativeLayout) itemView.findViewById(R.id.customer_layout);
                customerTv = (TextView) itemView.findViewById(R.id.tv_customer);
                layout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    intent = new Intent(PickListSubprojectActivity.this, PickListCustomerActivity.class);
                    intent.putExtra("subprojectId", list.get(pos).getSubProjectId());
                    intent.putExtra("subProjectName", list.get(pos).getName());

                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            }
        }

        public CustomerAdapter(ArrayList<SubProjectMasterModel> myDataset) {
            list = myDataset;
        }

        @Override
        public CustomerAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_customer_item, parent, false);
            return new CustomerAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final CustomerAdapter.DataObjectHolder holder, final int position) {
            SubProjectMasterModel model = list.get(position);

            if (model != null) {
                holder.customerTv.setText(model.getName());
            }

            holder.customerTv.setTag(position);
            holder.layout.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<SubProjectMasterModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(SubProjectMasterModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

}
