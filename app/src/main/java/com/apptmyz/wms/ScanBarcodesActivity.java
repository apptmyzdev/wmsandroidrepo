package com.apptmyz.wms;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.CreateGrnInput;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.PutAwayData;
import com.apptmyz.wms.data.WmsBarcodeScanModel;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ScanBarcodesActivity extends BaseActivity {
    TextInputEditText barcode1Et, barcode2Et;
    TextView statusTv;
    Button resetBtn, checkBtn;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private String barcode1Text, barcode2Text;
    private Context context;
    private DataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_scan_barcodes, frameLayout);
        context = ScanBarcodesActivity.this;
        dataSource = new DataSource(context);
        titleTv.setText("Scan Barcodes");

        barcode1Et = (TextInputEditText) findViewById(R.id.input_barcode1);
        barcode1Et.addTextChangedListener(barcode1Watcher);
        barcode2Et = (TextInputEditText) findViewById(R.id.input_barcode2);
        resetBtn = (Button) findViewById(R.id.btn_reset);
        checkBtn = (Button) findViewById(R.id.btn_check);
        barcode2Et.addTextChangedListener(barcode2Watcher);
        statusTv = (TextView) findViewById(R.id.tv_status);
        resetBtn.setOnClickListener(listener);
        checkBtn.setOnClickListener(listener);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_reset:
                    reset();
                    break;
                case R.id.btn_check:
                    checkAndUpdateStatus();
                    break;
                default:
                    break;
            }
        }
    };

    private void reset() {
        barcode1Et.setText(null);
        barcode2Et.setText(null);
        barcode1Et.requestFocus();
    }

    private void checkAndUpdateStatus() {
        barcode1Text = barcode1Et.getText().toString();
        barcode2Text = barcode2Et.getText().toString();
        String status = "";
        if (barcode1Text.equals(barcode2Text)) {
            status = "good";
           Utils.showSimpleGreenAlert(context, "Status", getResources().getString(R.string.good_to_go));
        } else {
            status = "mismatch";
            Utils.showSimpleRedAlert(context, "Status", getResources().getString(R.string.barcode_mismatch));
        }
        if(Utils.getConnectivityStatus(context)) {
            WmsBarcodeScanModel input = new WmsBarcodeScanModel(barcode1Text, barcode2Text, status);
            new SubmitScanLog(input).execute();
        }
        reset();
    }

    private TextWatcher barcode1Watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = barcode1Et
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    barcode1Text = scannedValue;
                                    barcode2Text = barcode2Et.getText().toString();
                                    barcode2Et.requestFocus();
                                    if (Utils.isValidString(barcode2Text))
                                        checkAndUpdateStatus();
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };
    private TextWatcher barcode2Watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = barcode2Et
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    barcode2Text = scannedValue;
                                    barcode1Text = barcode1Et.getText().toString();
                                    if (Utils.isValidString(barcode1Text))
                                        checkAndUpdateStatus();
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    ProgressDialog submitDialog;

    class SubmitScanLog extends AsyncTask<String, String, Object> {
        WmsBarcodeScanModel input;
        GeneralResponse response;

        SubmitScanLog(WmsBarcodeScanModel input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSendBarcodesUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input, WmsBarcodeScanModel.class);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    Utils.showToast(context, response.getMessage());
                }
                barcode1Et.setText("");
                barcode2Et.setText("");
                barcode1Et.requestFocus();
            }
            super.onPostExecute(result);
        }

    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(ScanBarcodesActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }
}
