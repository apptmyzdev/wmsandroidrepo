package com.apptmyz.wms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.CustomerMasterModelNew;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.VehicleMasterModel;
import com.apptmyz.wms.data.VendorMasterModel;
import com.apptmyz.wms.data.WmsLoadingTcModel;
import com.apptmyz.wms.data.WmsTcsResponse;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LoadingScreenLatest extends BaseActivity {
    TextView customerLabelTv, vehicleLabelTv, vendorLabelTv;
    AutoCompleteTextView customerSp, vendorSp;
    Spinner vehicleSp;
    AutoCompleteTextView vehicleAutoTv;
    Button goBtn;
    RadioGroup loadByRg;
    private Context context;
    private DataSource dataSource;
    private String type;
    private int customerId, vehicleId, vendorId, data;
    private Gson gson = new Gson();
    private ArrayList<WmsLoadingTcModel> tcsList = new ArrayList<>();
    private List<WmsLoadingTcModel> selectedtcsList = new ArrayList<>();
    private TcsAdapter tcsAdapter;
    private RecyclerView tcsRv;
    private TextView titleTv, noDataTv,list_size;
    private EditText searchView;
    private SparseBooleanArray mChecked = new SparseBooleanArray();
    private CheckBox selectAllCb;
    private RelativeLayout selectAllLayout;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_loading_screen_latest, frameLayout);
        context = LoadingScreenLatest.this;
        dataSource = new DataSource(context);
        list_size=(TextView)findViewById(R.id.tv_listsize);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Loading");
        noDataTv = (TextView) findViewById(R.id.tv_no_data);

        loadByRg = (RadioGroup) findViewById(R.id.radioGroup);
        customerLabelTv = (TextView) findViewById(R.id.tvSelectCustLabel);
        vehicleLabelTv = (TextView) findViewById(R.id.tvSelectVehicleLabel);
        vendorLabelTv = (TextView) findViewById(R.id.tvSelectVendorLabel);
        customerSp = (AutoCompleteTextView) findViewById(R.id.customerSp);
        vehicleSp = (Spinner) findViewById(R.id.vehicleSp);
        vendorSp = (AutoCompleteTextView) findViewById(R.id.vendorSp);
        goBtn = (Button) findViewById(R.id.btn_go);
        goBtn.setOnClickListener(listener);
        vehicleAutoTv = (AutoCompleteTextView) findViewById(R.id.vehicleAutoTv);
        vehicleAutoTv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                VehicleMasterModel vmm = ((VehicleMasterModel) adapterView.getItemAtPosition(i));
                vehicleId = vmm.getVehicleId();
            }
        });
        tcsRv = (RecyclerView) findViewById(R.id.rv_tcs);
        tcsAdapter = new TcsAdapter(tcsList);
        tcsRv.setNestedScrollingEnabled(false);
        tcsRv.setLayoutManager(new LinearLayoutManager(this));
        tcsRv.setAdapter(tcsAdapter);
        searchView = (EditText) findViewById(R.id.searchView);

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setFilteredData(editable.toString());
            }
        });

        selectedtcsList.clear();
        selectAllLayout = findViewById(R.id.rl_selectall);
        selectAllCb = findViewById(R.id.cb_select_all);
        selectAllCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tcsAdapter == null || tcsList == null) return;
                if(selectAllCb.isChecked()) {
                    selectedtcsList.addAll(tcsList);
                } else {
                    selectedtcsList.clear();
                }

                for (int i = 0; i < tcsList.size(); i++) {
                    WmsLoadingTcModel model = tcsList.get(i);
                    mChecked.put(i, selectAllCb.isChecked());
                }

                if(tcsAdapter != null) {
                    tcsAdapter.notifyDataSetChanged();
                }
            }
        });
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedtcsList != null && selectedtcsList.isEmpty()) {
                    Utils.showSimpleAlert(context, "", "Select atleast one TC");
                    return;
                }
                new SubmitTcsTask().execute();
            }
        });

        ArrayList<CustomerMasterModelNew> customers = dataSource.customers.getCustomers();
        if (Utils.isValidArrayList(customers)) {
            ArrayAdapter<CustomerMasterModelNew> customerAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, customers);
            customerSp.setAdapter(customerAdapter);
        }

        ArrayList<VendorMasterModel> vendors = (ArrayList<VendorMasterModel>) dataSource.vendorMaster.getAll();
        if (Utils.isValidArrayList(vendors)) {
            ArrayAdapter<VendorMasterModel> vendorAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, vendors);
            vendorSp.setAdapter(vendorAdapter);
        }

        ArrayList<VehicleMasterModel> vehicles = dataSource.vehicles.getVehicles();
        if (Utils.isValidArrayList(vehicles)) {
            ArrayAdapter<VehicleMasterModel> vehicleMasterModelArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, vehicles);
            vehicleSp.setAdapter(vehicleMasterModelArrayAdapter);
            vehicleAutoTv.setAdapter(vehicleMasterModelArrayAdapter);
        }

        vehicleAutoTv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    vehicleAutoTv.showDropDown();

            }
        });
        vehicleAutoTv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                vehicleAutoTv.showDropDown();
                return false;
            }
        });

        customerSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!isFinishing() && hasFocus)
                    customerSp.showDropDown();

            }
        });

        customerSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!isFinishing())
                    customerSp.showDropDown();
                return false;
            }
        });

        vendorSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!isFinishing() && hasFocus)
                    vendorSp.showDropDown();

            }
        });

        vendorSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!isFinishing())
                    vendorSp.showDropDown();
                return false;
            }
        });

        loadByRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_customer) {
                    customerLabelTv.setVisibility(View.VISIBLE);
                    customerSp.setVisibility(View.VISIBLE);
                    vehicleLabelTv.setVisibility(View.GONE);
                    vendorLabelTv.setVisibility(View.GONE);
                    vendorSp.setVisibility(View.GONE);
                    vehicleAutoTv.setVisibility(View.GONE);
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_vehicle) {
                    customerLabelTv.setVisibility(View.GONE);
                    customerSp.setVisibility(View.GONE);
                    vehicleLabelTv.setVisibility(View.VISIBLE);
                    vendorLabelTv.setVisibility(View.GONE);
                    vendorSp.setVisibility(View.GONE);
                    vehicleAutoTv.setVisibility(View.VISIBLE);
                } else {
                    customerLabelTv.setVisibility(View.GONE);
                    customerSp.setVisibility(View.GONE);
                    vehicleLabelTv.setVisibility(View.GONE);
                    vendorLabelTv.setVisibility(View.VISIBLE);
                    vendorSp.setVisibility(View.VISIBLE);
                    vehicleAutoTv.setVisibility(View.GONE);
                }
            }
        });

        customerSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                customerId = ((CustomerMasterModelNew) adapterView.getItemAtPosition(i)).getId();
            }
        });

        vehicleSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                vehicleId = vehicles.get(i).getVehicleId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        vendorSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                vendorId = ((VendorMasterModel) adapterView.getItemAtPosition(i)).getVendorId();
            }
        });
    }

    private void setFilteredData(String query) {
        ArrayList<WmsLoadingTcModel> tempList = new ArrayList<>();

        for (WmsLoadingTcModel model : tcsList) {
            if (model.getTcNo().toLowerCase().contains(query.toLowerCase())) {
                tempList.add(model);
            }
        }
        if (Utils.isValidArrayList(tempList)) {
            noDataTv.setVisibility(View.GONE);
            selectAllLayout.setVisibility(View.VISIBLE);
            tcsAdapter = new TcsAdapter(tempList);
            tcsRv.setAdapter(tcsAdapter);
            tcsAdapter.notifyDataSetChanged();
        } else {
            selectAllLayout.setVisibility(View.GONE);
            tcsRv.setAdapter(null);
            noDataTv.setVisibility(View.VISIBLE);
        }

    }


    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_go:
                    validateAndProceed();
                    break;
                default:
                    break;
            }
        }
    };

    private void validateAndProceed() {
        if (loadByRg.getCheckedRadioButtonId() == R.id.rb_customer) {
            type = "C";
            data = customerId;
        } else if (loadByRg.getCheckedRadioButtonId() == R.id.rb_vehicle) {
            type = "V";
            data = vehicleId;
        } else {
            type = "VE";
            data = vendorId;
        }
        if (Utils.getConnectivityStatus(context)) {
            new TcsTask().execute();
        }
    }

    ProgressDialog dialog;

    class TcsTask extends AsyncTask<String, String, Object> {
        ArrayList<WmsLoadingTcModel> models = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                dialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getTcsListUrl(type, data);
                Utils.logD("Log 1");
                WmsTcsResponse response = (WmsTcsResponse) HttpRequest
                        .getInputStreamFromUrl(url, WmsTcsResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        tcsList = (ArrayList<WmsLoadingTcModel>) response.getData();
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            if (showErrorDialog()) {
                setData();
            }
            super.onPostExecute(result);
        }

    }

    private void setData() {
       list_size.setText(tcsList.size()+"");
        tcsAdapter = new TcsAdapter(tcsList);
        tcsRv.setLayoutManager(new LinearLayoutManager(this));
        tcsRv.setAdapter(tcsAdapter);
        tcsAdapter.notifyDataSetChanged();
        selectAllLayout.setVisibility(View.VISIBLE);
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(LoadingScreenLatest.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    class TcsAdapter extends RecyclerView.Adapter<TcsAdapter.DataObjectHolder> {
        private ArrayList<WmsLoadingTcModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView tcNumTv, subprojectTv;
            CheckBox checkBox;
            RelativeLayout tcLayout;

            public DataObjectHolder(View itemView) {
                super(itemView);

                tcNumTv = (TextView) itemView.findViewById(R.id.tv_tc_number);
                subprojectTv = (TextView) itemView.findViewById(R.id.tv_subproject);
                checkBox = itemView.findViewById(R.id.checkBox);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
                tcLayout = itemView.findViewById(R.id.tc_layout);
                tcLayout.setOnClickListener(null);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int position = (int) view.getTag();
                        Intent intent = new Intent(context, LoadingScanningLatestScreen.class);
                        intent.putExtra("tc", gson.toJson(list.get(position), WmsLoadingTcModel.class));
                        startActivity(intent);
                    }
                });
            }
        }

        public TcsAdapter(ArrayList<WmsLoadingTcModel> myDataset) {
            list = myDataset;
        }

        @Override
        public TcsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_tc_item, parent, false);
            return new TcsAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final TcsAdapter.DataObjectHolder holder, final int position) {
            WmsLoadingTcModel model = list.get(position);

            if (model != null) {
                holder.tcNumTv.setText(model.getTcNo());
                holder.subprojectTv.setText(model.getSubproject());
                holder.subprojectTv.setText("Rane-sp / WH-Ser");
            }

            holder.tcNumTv.setTag(position);
            holder.subprojectTv.setTag(position);
            holder.itemView.setTag(position);
            holder.checkBox.setTag(position);
            holder.checkBox.setChecked(mChecked.get(position));

            holder.tcLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = (int) view.getTag();
                    Intent intent = new Intent(context, LoadingScanningLatestScreen.class);
                    intent.putExtra("tc", gson.toJson(list.get(position), WmsLoadingTcModel.class));
                    startActivity(intent);
                }
            });
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.getTag() != null) {
                        int pos = (int) buttonView.getTag();

                        if (isChecked) {
                            mChecked.put(pos, isChecked);
                            selectedtcsList.add(list.get(pos));
                            if(isAllTCSSelected()) {
                                selectAllCb.setChecked(isChecked);
                            }
                        } else {
                            selectedtcsList.remove(list.get(pos));
                            mChecked.delete(pos);
                            if(!isAllTCSSelected()) {
                                selectAllCb.setChecked(isChecked);
                            }
                        }
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<WmsLoadingTcModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(WmsLoadingTcModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    protected boolean isAllTCSSelected() {
        for (int i = 0; i < tcsList.size(); i++) {
            if(tcsAdapter != null && tcsList != null) {
                WmsLoadingTcModel model = tcsList.get(i);
                if (!mChecked.get(i)) {
                    return false;
                }
            }
        }
        return true;
    }

    class SubmitTcsTask extends AsyncTask<String, String, Object> {
        ArrayList<WmsLoadingTcModel> models = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                dialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitTCsUrl().replace(" ", "%20");;
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String json = gson.toJson(selectedtcsList);
                GeneralResponse response = (GeneralResponse) HttpRequest
                        .postData(url, json, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        selectedtcsList.clear();
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            Utils.showSimpleAlert(context, null, "Submitted Succesfully");
            new TcsTask().execute();
            super.onPostExecute(result);
        }
    }
}