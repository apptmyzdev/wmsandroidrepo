package com.apptmyz.wms.util;

import android.graphics.Bitmap;

import com.apptmyz.wms.data.Bin;
import com.apptmyz.wms.data.DestinationDataModel;
import com.apptmyz.wms.data.InventoryData;
import com.apptmyz.wms.data.LoadUnload;
import com.apptmyz.wms.data.LoadingResponse;
import com.apptmyz.wms.data.LoadingResponseLatest;
import com.apptmyz.wms.data.Part;
import com.apptmyz.wms.data.RouteData;
import com.apptmyz.wms.data.SheetDetailsResponse;
import com.apptmyz.wms.data.UnloadingResponse;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Globals {
    public static String BLUETOOTH_STATE;

    public static String lastErrMsg;
    public static int screenWidth;
    public static int screenHeight;
    public static String username;
    public static String empId;
    public static String scCode;
    public static String baseUrl;
    public static String tcNumber;

    public static String loadingDataType;

    public static int barcodeLength;
    public static int barcodeMinLength;
    public static int barcodeMaxLength;

    public static int loaidngBarcodeLength;
    public static int loaidngBarcodeMinLength;
    public static int loaidngBarcodeMaxLength;

    public static boolean isPrint = false;
    public static boolean isPause = false;
    public static boolean selectedPrint = false;

    public static LoadUnload selectedSheet;

    public static String sheetNo;

    public static SheetDetailsResponse sheetDetailsResponse;

    public static String serverTime;
    public static long timeDiff;

    public static boolean isDataSubmitted;

    public static String conNumber;

    public static boolean xdEnabled;
    public static RouteData selectedRoute;
    public static String conNum;

    public static Bitmap bitmap;

    public static String groupId;
    public static String bayNo;
    public static HashMap<String, DestinationDataModel> destMap;
    public static Boolean isLoading = false;
    public static String vehicleNo;
    public static String inwardNo;
    public static String partNo;
    public static String vendorId;
    public static int partID;
    public static ArrayList<Bin> bins = new ArrayList<>();
    public static UnloadingResponse unloadingResponse;
    public static LoadingResponseLatest loadingResponse;


    public static ArrayList<Bin> populateDummyList() {
        bins = new ArrayList<>();

        Bin bin1 = new Bin();
        bin1.setBinNumber("B1A123");
        bin1.setProductNumber("P1");
        bin1.setNoOfBoxesToScan(5);
        bin1.setTotalNumOfBoxes(5);
        bin1.setPicked(false);

        Bin bin2 = new Bin();
        bin2.setBinNumber("B2A456");
        bin2.setProductNumber("P2");
        bin2.setNoOfBoxesToScan(3);
        bin2.setTotalNumOfBoxes(4);
        bin2.setPicked(false);

        Bin bin3 = new Bin();
        bin3.setBinNumber("B3A789");
        bin3.setProductNumber("P3");
        bin3.setNoOfBoxesToScan(0);
        bin3.setTotalNumOfBoxes(3);
        bin3.setPicked(true);

        bins.add(bin1);
        bins.add(bin2);
        bins.add(bin3);

        return bins;
    }

    public static ArrayList<Bin> binsData = new ArrayList<>();

    public static void populateBinsData() {
        Bin bin1 = new Bin();
        bin1.setBinNumber("B123");
        bin1.setNoOfBoxesToScan(5);
        bin1.setPutAwayDate("22 Oct 2020");

        List<Part> parts = new ArrayList<>();
        Part part1 = new Part();
        part1.setProductNumber("P1");
        part1.setQuantity(2);
        part1.setPutAwayDate("22 Oct 2020");

        Part part2 = new Part();
        part2.setProductNumber("P2");
        part2.setQuantity(3);
        part2.setPutAwayDate("22 Oct 2020");

        parts.add(part1);
        parts.add(part2);
        bin1.setParts(parts);

        Bin bin2 = new Bin();
        bin2.setBinNumber("B456");
        bin2.setNoOfBoxesToScan(5);
        bin2.setPutAwayDate("22 Oct 2020");

        List<Part> parts2 = new ArrayList<>();
        Part part3 = new Part();
        part3.setProductNumber("P1");
        part3.setQuantity(3);
        part3.setPutAwayDate("22 Oct 2020");

        Part part4 = new Part();
        part4.setProductNumber("P4");
        part4.setQuantity(2);
        part4.setPutAwayDate("22 Oct 2020");

        parts2.add(part3);
        parts2.add(part4);
        bin2.setParts(parts2);

        binsData.add(bin1);
        binsData.add(bin2);
    }

    public static ArrayList<Part> partsData = new ArrayList<>();

    public static void populatePartsData() {
        Part part1 = new Part();
        part1.setProductNumber("P123");
        part1.setQuantity(5);
        part1.setPutAwayDate("22 Oct 2020");

        List<Bin> bins1 = new ArrayList<>();
        Bin bin1 = new Bin();
        bin1.setBinNumber("B1");
        bin1.setNoOfBoxesToScan(2);
        bin1.setPutAwayDate("22 Oct 2020");

        Bin bin2 = new Bin();
        bin2.setBinNumber("B2");
        bin2.setNoOfBoxesToScan(3);
        bin2.setPutAwayDate("22 Oct 2020");

        bins1.add(bin1);
        bins1.add(bin2);
        part1.setBins(bins1);

        Part part2 = new Part();
        part2.setProductNumber("P456");
        part2.setQuantity(5);
        part2.setPutAwayDate("22 Oct 2020");

        List<Bin> bins2 = new ArrayList<>();
        Bin bin3 = new Bin();
        bin3.setBinNumber("B1");
        bin3.setNoOfBoxesToScan(1);
        bin3.setPutAwayDate("22 Oct 2020");

        Bin bin4 = new Bin();
        bin4.setBinNumber("B4");
        bin4.setNoOfBoxesToScan(4);
        bin4.setPutAwayDate("22 Oct 2020");

        bins2.add(bin3);
        bins2.add(bin4);
        part2.setBins(bins2);

        partsData.add(part1);
        partsData.add(part2);
    }
}
