package com.apptmyz.wms.util;

public class WmsUtils {
   // public static final String BASE_URL = "https://espl.spoton.co.in/erts/api/wms/";
    public static final String BASE_URL = "https://erts.spoton.co.in/erts/api/wms/";
    //"https://erts.spoton.co.in/erts/api/wms/" //- Current Production URL
    //"https://espl.spoton.co.in/erts/api/wms/"; //Current UAT URL

    // "http://172.0.1.103:8080/erts/api/wms/";
    // "http://104.237.4.37:8080/erts/api/wms/";
    public static final String loginTag = "login";
    public static final String putAwayTag = "putaway";
    public static final String dataTag = "data";
    public static final String slashTag = "/";
    public static final String vehicleNumTag = "vehicleNo";
    public static final String questionMarkTag = "?";
    public static final String equalsTag = "=";
    public static final String ampersandTag = "&";
    public static final String completeTag = "complete";
    public static final String generateTag = "generate";
    public static final String grnTag = "grn";
    public static final String vendorMasterTag = "vendor";
    public static final String submitTag = "submit";
    public static final String tokenTag = "token";

    public static final String subprojectsTag = "subproject";
    public static final String customerTag = "customer";
    public static final String nodeTag = "node";
    public static final String picklistTag = "picklist";
    public static final String picklistV2Tag = "picklistV2";
    public static final String binTag = "bin";
    public static final String allTag = "all";
    public static final String skipBinTag = "skipbin";
    public static final String reasonTag = "reason";
    public static final String pickingTag = "picking";
    public static final String inventoryTag = "inventory";
    public static String unloadingUrlTag = "unloading/data?vehicleNo=";
    public static String unloadingSubmitUrlTag = "unloading/submit";
    public static String routeMasterUrlTag = "vendor/route?vendor=";
    public static String vehicleMasterUrlTag = "route/vehicle?route=";
    public static String loadingDataUrlTag = "loading/data?vendor=";
    public static String loadingSubmitDataUrlTag = "loading/submit?vendor=";
    public static String imageSubmitUrlTag = "save/vehicle/img";

    public static final String searchTag = "search";
    public static final String partTag = "part";
    public static final String exceptionTag = "exception";
    public static final String typeTag = "type";
    public static final String transferTag = "transfer";
    public static final String vehicleTag = "vehicle";
    public static final String flagTag = "flag";
    public static final String scanBarocdesTag = "barcode/scan";
    public static final String submitAllTag = "submit/all";
    public static final String loadingTag = "loading";

    public static String getLoginUrl() {
        return BASE_URL + loginTag;
    }

    public static String getPutAwayUrl() {
        return BASE_URL + putAwayTag + slashTag + dataTag;
    }

    public static String getPutAwayCompletedUrl() {
        return BASE_URL + putAwayTag + slashTag + completeTag + slashTag + dataTag;
    }

    public static String getCreateGrnUrl() {
        return BASE_URL + generateTag + slashTag + grnTag;
    }

    public static String getUnloadingUrlTag(String vehicleNo) {
        return BASE_URL + unloadingUrlTag + vehicleNo;
    }

    public static String getUnloadingSubmitUrlTag() {
        return BASE_URL + unloadingSubmitUrlTag;
    }

    public static String getSubmitPutAwayUrl() {
        return BASE_URL + putAwayTag + slashTag + submitTag;
    }

    public static String getTokenUrl() {
        return BASE_URL + tokenTag;
    }


    public static String getPickListSubprojectsUrl() {
        return BASE_URL + subprojectsTag;
    }

    public static String getPickListCustomersUrl(int subprojectId) {
        return BASE_URL + subprojectsTag + slashTag + customerTag + questionMarkTag + subprojectsTag + equalsTag + subprojectId;
    }

    public static String getPickListUrl(String flag, String data) {
        return BASE_URL + picklistTag + slashTag + dataTag + questionMarkTag + flagTag + equalsTag + flag
                + ampersandTag + dataTag + equalsTag + data;
    }

    public static String getSubmitPickListUrl(int subprojectId, int nodeId) {
        return BASE_URL + picklistTag + slashTag + submitTag + questionMarkTag + subprojectsTag + equalsTag + subprojectId
                + ampersandTag + nodeTag + equalsTag + nodeId;
    }

    public static String getBinMastersUrl() {
        return BASE_URL + binTag + slashTag + allTag;
    }

    public static String getSkipBinReasonsUrl() {
        return BASE_URL + skipBinTag + slashTag + reasonTag;
    }

    public static String getSkipBinUrl() {
        return BASE_URL + pickingTag + slashTag + skipBinTag;
    }

    public static String getInventoryDataUrl(String type) {
        return BASE_URL + inventoryTag + slashTag + dataTag + slashTag + type;
    }

    public static String getVendorMasterTag() {
        return BASE_URL + slashTag + vendorMasterTag;
    }

    public static String getRouteMasterUrlTag(String vendor) {
        return BASE_URL + routeMasterUrlTag + vendor;
    }

    public static String getVehicleMasterUrlTag(String route) {
        return BASE_URL + vehicleMasterUrlTag + route;
    }

    public static String getLoadingDataUrlTag(String vendorCode, String vehno) {
        return BASE_URL + loadingDataUrlTag + vendorCode + "&vehicleNo=" + vehno;
    }

    public static String getLoadingSubmitDataUrlTag(String vendorId, String vehNo) {
        return BASE_URL + loadingSubmitDataUrlTag + vendorId + "&vehicleNo=" + vehNo;
    }

    public static String getExceptionTypesUrl() {
        return BASE_URL + inventoryTag + slashTag + exceptionTag + slashTag + reasonTag;
    }

    public static String getSubmitExceptionUrl() {
        return BASE_URL + inventoryTag + slashTag + exceptionTag;
    }

    public static String getImageSubmitUrlTag() {
        return BASE_URL + imageSubmitUrlTag;
    }


    public static String getSearchBinOrPartUrl(String data, String type) {
        return BASE_URL + searchTag + slashTag + binTag + slashTag + partTag + questionMarkTag + typeTag + equalsTag + type
                + ampersandTag + dataTag + equalsTag + data;
    }

    public static String getPartMastersUrl() {
        return BASE_URL + partTag + slashTag + allTag;
    }

    public static String getBinTransferUrl() {
        return BASE_URL + binTag + slashTag + transferTag;
    }

    public static String getSubmitInventoryUrl() {
        return BASE_URL + inventoryTag + slashTag + submitTag;
    }

    public static String getCustomersUrl() {
        return BASE_URL + nodeTag;
    }

    public static String getVehiclesUrl() {
        return BASE_URL + vehicleTag;
    }

    public static String getTcsListUrl(String flag, int data) {
        return BASE_URL + "loading/tc" + questionMarkTag + flagTag + equalsTag + flag + ampersandTag + dataTag + equalsTag + data;
    }

    public static String getLoadingDataLatestUrlTag() {
        return BASE_URL + "loading/data";
    }

    public static String getInspectionUrl(String vehicleNum, String inwardNum) {
        return BASE_URL + "inspection/data?vehicleNo=" + vehicleNum + ampersandTag + "inwardNo=" + inwardNum;
    }

    public static String getSubmitInspectionUrl() {
        return BASE_URL + "inspection/submit";
    }

    public static String getUnpackingDataUrl(String vehicleNum, String inwardNum, String invoiceNum) {
        return BASE_URL + "unpacking/data?vehicleNo=" + vehicleNum + "&inwardNo=" + inwardNum + "&invoiceNo=" + invoiceNum;
    }

    public static String getSubmitUnpackingUrl() {
        return BASE_URL + "unpacking/submit";
    }

    public static String getInvoicesUrl(int subprojectId) {
        return BASE_URL + "packing/orderlist?subproject=" + subprojectId;
    }

    public static String getPackingDataUrl(int subprojectId, String orderNum) {
        return BASE_URL + "packing/data?subproject=" + subprojectId + "&orderNo=" + orderNum;
    }

    public static String getPackingSubmitUrl() {
        return BASE_URL + "packing/submit";
    }

    public static String getInwardNumsUrl(String vehicleNum, int flag) {
        return BASE_URL + "vehicle/inwardNo?vehicleNo=" + vehicleNum + "&flag=" + flag;
    }

    public static String getDocumentHandoverUrl(String tcNumber) {
        return BASE_URL + "doumentHandover/tc/invoice?tcNo=" + tcNumber;
    }

    public static String getDocumentHandoverByVendorOrVehicleUrl(int number, String flag) {
        return BASE_URL + "doumentHandover/invoice?data=" + number + "&flag=" + flag;
    }

    public static String getSubmitHandoverUrl() {
        return BASE_URL + "documentHandover/submit";
    }

    public static String getSendBarcodesUrl() {
        return BASE_URL + scanBarocdesTag;
    }

    public static String getPickListIdsUrl() {
        return BASE_URL + "picklist";
    }

    public static String getPickListIdsUrlV2() {
        return BASE_URL + picklistV2Tag;
    }

    public static String getSubmitPickListIds() {
        return BASE_URL + picklistTag + slashTag + submitAllTag;
    }

    public static String getPickingExcepReasonsUrl() {
        return BASE_URL + "picklist/exception/reason";
    }

    public static String getSubmitTCsUrl() {
        return BASE_URL + loadingTag + slashTag + submitAllTag;
    }
}
