package com.apptmyz.wms;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.apptmyz.wms.custom.ADRadioGroup;
import com.apptmyz.wms.custom.CounterView;
import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.CreateGrnInput;
import com.apptmyz.wms.data.CreateGrnPartJson;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.PutAwayData;
import com.apptmyz.wms.data.PutAwayResponse;
import com.apptmyz.wms.data.WmsPutawayBinModel;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.Constants;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PutAwayActivity extends BaseActivity {
    private static final int BOXES_MAX_COUNT = 10;

    Context context;
    TextView titleTv;
    Button addPutAwayDataBtn, closeBtn, createGrnBtn;
    TextInputEditText binNumberEt, productNumberEt, vehicleNumEt;
    ImageView scanPutAwayDataIv, scanProductIv, vehicleNumProceedIv, refreshIv;
    CounterView counterView;
    CompletedPutAwayDataAdapter adapter;
    LinearLayout addPutAwayDataLayout, pendingLayout, completedLayout;
    SparseBooleanArray mChecked = new SparseBooleanArray();
    CheckBox selectAllCb;
    RecyclerView binsRv, completedPutAwayDatasRv;
    BinAdapter binAdapter;
    private ArrayList<PutAwayData> bins = new ArrayList<>();
    LinearLayoutManager layoutManager, completedLayoutManager;
    int addedBoxesCount;
    PutAwayData bin = null;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private TextView noProductsTv;
    private String binNumber, productNumber;
    Gson gson = new Gson();
    ADRadioGroup radioGroup;
    int count;
    private List<PutAwayData> selectedList = new ArrayList<>();
    private ArrayList<PutAwayData> binListUi = new ArrayList<>();
    private ArrayList<PutAwayData> groupData = new ArrayList<>();
    private ArrayList<ArrayList<WmsPutawayBinModel>> childData = new ArrayList<>();
    private DataSource dataSource;

    private ExpandableListView expandableListView;
    private ExpandableListAdapter expandableListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_put_away, frameLayout);

        context = PutAwayActivity.this;
        dataSource = new DataSource(context);

        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Put Away");
        radioGroup = (ADRadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(checkListener);
        noProductsTv = (TextView) findViewById(R.id.tv_no_products);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);

        expandableListView = (ExpandableListView) findViewById(R.id.explv_putaway);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            expandableListView.setNestedScrollingEnabled(false);
        }

        addPutAwayDataBtn = (Button) findViewById(R.id.btn_add_bin);
        closeBtn = (Button) findViewById(R.id.btn_close);
        createGrnBtn = (Button) findViewById(R.id.btn_create_grn);
        createGrnBtn.setOnClickListener(listener);

        selectAllCb = (CheckBox) findViewById(R.id.cb_select_all);
        selectAllCb.setOnClickListener(clickListener);

        pendingLayout = (LinearLayout) findViewById(R.id.ll_pending);
        completedLayout = (LinearLayout) findViewById(R.id.ll_completed);

        addPutAwayDataBtn.setOnClickListener(listener);
        closeBtn.setOnClickListener(listener);

        vehicleNumProceedIv = (ImageView) findViewById(R.id.iv_proceed);
        vehicleNumProceedIv.setOnClickListener(listener);

        binsRv = (RecyclerView) findViewById(R.id.rv_bins);
        binAdapter = new BinAdapter((ArrayList<PutAwayData>) bins);
        layoutManager = new LinearLayoutManager(this);
        binsRv.setLayoutManager(layoutManager);
        binsRv.setAdapter(binAdapter);

        completedPutAwayDatasRv = (RecyclerView) findViewById(R.id.rv_completed_bins);
        adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
        completedLayoutManager = new LinearLayoutManager(this);
        completedPutAwayDatasRv.setLayoutManager(completedLayoutManager);
        completedPutAwayDatasRv.setAdapter(adapter);

        productNumberEt = (TextInputEditText) findViewById(R.id.input_product_number);
        productNumberEt.addTextChangedListener(productNumWatcher);
        vehicleNumEt = (TextInputEditText) findViewById(R.id.input_tc_number);

        scanPutAwayDataIv = (ImageView) findViewById(R.id.iv_scan_bin);
        scanProductIv = (ImageView) findViewById(R.id.iv_scan_product);

        scanProductIv.setOnClickListener(listener);

        counterView = (CounterView) findViewById(R.id.counterView);

        addPutAwayDataLayout = (LinearLayout) findViewById(R.id.addbin_layout);
        setInitialData();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CheckBox chk = (CheckBox) v;
            int itemCount = count;
            for (int i = 0; i < itemCount; i++) {
                mChecked.put(i, chk.isChecked());
            }
            if (expandableListAdapter != null)
                expandableListAdapter.notifyDataSetChanged();
        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_create_grn:
                    selectedList.clear();
                    selectedList = new ArrayList<>();
                    if (mChecked.size() > 0) {
                        boolean createGrn = true;
                        for (int i = 0; i < groupData.size(); i++) {
                            if (mChecked.get(i)) {
                                if(groupData.get(i).getGrnBoxes() <=0 || groupData.get(i).getGrnBoxes() > groupData.get(i).getTotalBoxes()) {
                                    Utils.showAlertDialog(context, "Alert", "Please Enter a value between 1 and " + groupData.get(i).getTotalBoxes());
                                    createGrn = false;
                                } else {
                                    selectedList.add(binListUi.get(i));
                                }
                            }
                        }
                        if (Utils.isValidArrayList((ArrayList<?>) selectedList))
                            Utils.logD(selectedList.toString());
                        if(createGrn) {
                            goToCreateGRN();
                        }
                    } else {
                        Utils.showToast(context, "Please select atleast one order");
                    }
                    break;
                case R.id.iv_refresh:
                    if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
                        if (Utils.getConnectivityStatus(context)) {
                            new PutAwayDataTask().execute();
                        }
                    } else {
                        if (Utils.getConnectivityStatus(context)) {
                            new PutAwayCompletedDataTask().execute();
                        }
                    }
                    break;
                case R.id.iv_proceed:
                    if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
                        bins = dataSource.putAway.getPutAwayData(0);
                        if (Utils.isValidArrayList(bins)) {
                            binAdapter = new BinAdapter(bins);
                            binsRv.setAdapter(binAdapter);
                            binAdapter.notifyDataSetChanged();
                            pendingLayout.setVisibility(View.VISIBLE);
                            noProductsTv.setVisibility(View.GONE);
                        } else {
                            if (Utils.getConnectivityStatus(context)) {
                                new PutAwayDataTask().execute();
                            }
                        }
                    } else {
                        binListUi = dataSource.putAway.getPutAwayData(1);
                        if (Utils.isValidArrayList((ArrayList<?>) binListUi)) {
                            adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
                            completedPutAwayDatasRv.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            completedLayout.setVisibility(View.VISIBLE);
                            noProductsTv.setVisibility(View.GONE);
                        } else {
                            if (Utils.getConnectivityStatus(context)) {
                                new PutAwayCompletedDataTask().execute();
                            }
                        }
                    }
                    break;
                case R.id.btn_add_bin:
                    binNumber = "";
                    productNumber = "";
                    addPutAwayDataLayout.setVisibility(View.VISIBLE);
                    counterView.setValue(BOXES_MAX_COUNT - addedBoxesCount);
                    break;
                case R.id.btn_close:
                    if (!Utils.isValidArrayList(bins)) {
                        if (addedBoxesCount != BOXES_MAX_COUNT) {
                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Boxes Count did not match");
                        } else {
                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Done");
                        }
                    }
                    break;
                case R.id.iv_scan_bin:
                    binNumberEt.clearComposingText();
                    binNumberEt.requestFocus();
                    break;
                case R.id.iv_scan_product:
                    productNumberEt.clearComposingText();
                    productNumberEt.requestFocus();
                    break;
                default:
                    break;
            }
        }
    };

    private void goToCreateGRN() {
        if (Utils.isValidArrayList((ArrayList<?>) selectedList)) {
            if (Utils.getConnectivityStatus(context)) {
                CreateGrnInput input = new CreateGrnInput();
                List<CreateGrnPartJson> partList = new ArrayList<>();
                for (PutAwayData data : selectedList) {
                    CreateGrnPartJson model = new CreateGrnPartJson(data.getId(), data.getPartNo(), data.getPartId(), data.getTotalBoxes(),
                            data.getBinNumber(), data.getScanTime(), data.getVehNo(), data.getInwardNo(), data.getDataType(), data.getDaNo(),data.getGrnBoxes());
                    partList.add(model);
                }
                input.setPartList(partList);
                new CreateGrnTask(input).execute();
            }
        }
    }

    ProgressDialog createGrnDialog;

    class CreateGrnTask extends AsyncTask<String, String, Object> {
        CreateGrnInput input;
        GeneralResponse response;

        CreateGrnTask(CreateGrnInput input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                createGrnDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getCreateGrnUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input, CreateGrnInput.class);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        for (CreateGrnPartJson model : input.getPartList()) {
                            dataSource.putAway.delete(model.getId());
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && createGrnDialog != null && createGrnDialog.isShowing()) {
                createGrnDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null)
                    Utils.showToast(context, response.getMessage());
                openHomeScreen();
            }
            super.onPostExecute(result);
        }

    }

    private void openHomeScreen() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
    }

    ProgressDialog putAwayDialog, putAwayCompletedDialog;

    class PutAwayDataTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                putAwayDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPutAwayUrl();

                Utils.logD("Log 1");
                PutAwayResponse response = (PutAwayResponse) HttpRequest
                        .getInputStreamFromUrl(url, PutAwayResponse.class,
                                context);

                if (response != null) {
                    dataSource.putAway.deletePendingData();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<PutAwayData> data = (ArrayList<PutAwayData>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            bins = data;
                            dataSource.putAway.insertPutAwayData(bins, 0);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && putAwayDialog != null && putAwayDialog.isShowing()) {
                putAwayDialog.dismiss();
            }
            showErrorDialog();
            setData();
            super.onPostExecute(result);
        }
    }

    class PutAwayCompletedDataTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            groupData.clear();
            if (!isFinishing()) {
                putAwayCompletedDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPutAwayCompletedUrl();

                Utils.logD("Log 1");
                PutAwayResponse response = (PutAwayResponse) HttpRequest
                        .getInputStreamFromUrl(url, PutAwayResponse.class,
                                context);

                if (response != null) {
                    dataSource.putAway.deleteCompletedData();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<PutAwayData> data = (ArrayList<PutAwayData>) response.getData();
                        if (Utils.isValidArrayList(data)) {
                            groupData = data;
                            childData.clear();
                            for (PutAwayData model : data) {
                                childData.add((ArrayList<WmsPutawayBinModel>) model.getBinList());
                            }
                            binListUi = data;
                            dataSource.putAway.insertPutAwayData(binListUi, 1);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && putAwayCompletedDialog != null && putAwayCompletedDialog.isShowing()) {
                putAwayCompletedDialog.dismiss();
            }
            showErrorDialog();
            setCompletedData();
            super.onPostExecute(result);
        }
    }

    private void setCompletedData() {
        pendingLayout.setVisibility(View.GONE);
        if (Utils.isValidArrayList(groupData)) {
            completedLayout.setVisibility(View.VISIBLE);
            noProductsTv.setVisibility(View.GONE);
            expandableListAdapter = new ExpandableListAdapter(context);
            expandableListView.setAdapter(expandableListAdapter);
            for (int i = 0; i < expandableListView.getExpandableListAdapter().getGroupCount(); i++) {
                expandableListView.expandGroup(i);
            }
            expandableListAdapter.notifyDataSetChanged();
        } else {
            completedLayout.setVisibility(View.GONE);
            noProductsTv.setVisibility(View.VISIBLE);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PutAwayActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void setInitialData() {
        if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
            bins = dataSource.putAway.getPutAwayData(0);
            binAdapter = new BinAdapter(bins);
            binsRv.setAdapter(binAdapter);
            binAdapter.notifyDataSetChanged();
            if (Utils.isValidArrayList(bins)) {
                pendingLayout.setVisibility(View.VISIBLE);
                noProductsTv.setVisibility(View.GONE);
            } else {
                if (Utils.getConnectivityStatus(context))
                    new PutAwayDataTask().execute();
            }
        } else {
            binListUi = dataSource.putAway.getPutAwayData(1);
            adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
            completedPutAwayDatasRv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            if (Utils.isValidArrayList(binListUi)) {
                completedLayout.setVisibility(View.VISIBLE);
                noProductsTv.setVisibility(View.GONE);
            } else {
                if (Utils.getConnectivityStatus(context))
                    new PutAwayCompletedDataTask().execute();
            }
        }
    }


    private void setData() {
        if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
            bins = dataSource.putAway.getPutAwayData(0);
            binAdapter = new BinAdapter(bins);
            binsRv.setAdapter(binAdapter);
            binAdapter.notifyDataSetChanged();
            if (Utils.isValidArrayList(bins)) {
                pendingLayout.setVisibility(View.VISIBLE);
                noProductsTv.setVisibility(View.GONE);
            } else {
                pendingLayout.setVisibility(View.GONE);
                noProductsTv.setVisibility(View.VISIBLE);
            }
        } else {
            if (Utils.getConnectivityStatus(context))
                new PutAwayCompletedDataTask().execute();
        }
    }

    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binNumberEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() > 0) {
                                        binNumber = scannedValue;
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private TextWatcher productNumWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = productNumberEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
                                        if (Utils.isValidString(scannedValue)) {
                                            ArrayList<PutAwayData> tempPutAwayDatas = new ArrayList<>();
                                            for (PutAwayData bin : bins) {
                                                if ((bin.getDataType().equals("P") && bin.getPartNo().toLowerCase().contains(scannedValue.toLowerCase()))
                                                        || (bin.getDataType().equals("DA") && bin.getDaNo().toLowerCase().contains(scannedValue.toLowerCase())))
                                                    tempPutAwayDatas.add(bin);
                                            }
                                            if (Utils.isValidArrayList(tempPutAwayDatas)) {
                                                binAdapter = new BinAdapter(tempPutAwayDatas);
                                                binsRv.setAdapter(binAdapter);
                                                binAdapter.notifyDataSetChanged();
                                                pendingLayout.setVisibility(View.VISIBLE);
                                                noProductsTv.setVisibility(View.GONE);
                                            } else {
                                                binsRv.setAdapter(null);
                                                binAdapter.notifyDataSetChanged();
                                                pendingLayout.setVisibility(View.GONE);
                                                noProductsTv.setVisibility(View.VISIBLE);
                                            }
                                        } else {
                                            if (Utils.isValidArrayList(bins)) {
                                                binAdapter = new BinAdapter(bins);
                                                binsRv.setAdapter(binAdapter);
                                                binAdapter.notifyDataSetChanged();
                                                pendingLayout.setVisibility(View.VISIBLE);
                                                noProductsTv.setVisibility(View.GONE);
                                            } else {
                                                binsRv.setAdapter(null);
                                                binAdapter.notifyDataSetChanged();
                                                pendingLayout.setVisibility(View.GONE);
                                                noProductsTv.setVisibility(View.VISIBLE);
                                            }
                                        }

                                        if (scannedValue.length() >= 2) {
                                            productNumber = scannedValue;
                                            for (PutAwayData bin : bins) {
                                                if (bin.getPartNo().equals(productNumber)) {
                                                    Intent intent = new Intent(context, PutAwayDetailActivity.class);
                                                    String binStr = gson.toJson(bin);
                                                    intent.putExtra("bin", binStr);
                                                    intent.putExtra("vehicleNum", bin.getVehNo());
                                                    intent.putExtra("inwardNum", bin.getInwardNo());
                                                    startActivity(intent);
                                                }
                                            }
                                        }
                                    } else {
                                        if (Utils.isValidString(scannedValue)) {
                                            ArrayList<PutAwayData> tempPutAwayDatas = new ArrayList<>();
                                            for (PutAwayData bin : binListUi) {
                                                if ((bin.getDataType().equals("P") && bin.getPartNo().toLowerCase().contains(scannedValue.toLowerCase()))
                                                        || (bin.getDataType().equals("DA") && bin.getDaNo().toLowerCase().contains(scannedValue.toLowerCase())))
                                                    tempPutAwayDatas.add(bin);
                                            }
                                            if (Utils.isValidArrayList(tempPutAwayDatas)) {
                                                adapter = new CompletedPutAwayDataAdapter(tempPutAwayDatas);
                                                completedPutAwayDatasRv.setAdapter(adapter);
                                                adapter.notifyDataSetChanged();
                                                completedLayout.setVisibility(View.VISIBLE);
                                                noProductsTv.setVisibility(View.GONE);
                                            } else {
                                                completedPutAwayDatasRv.setAdapter(null);
                                                adapter.notifyDataSetChanged();
                                                completedLayout.setVisibility(View.GONE);
                                                noProductsTv.setVisibility(View.VISIBLE);
                                            }
                                        } else {
                                            if (Utils.isValidArrayList(binListUi)) {
                                                adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
                                                completedPutAwayDatasRv.setAdapter(adapter);
                                                adapter.notifyDataSetChanged();
                                                completedLayout.setVisibility(View.VISIBLE);
                                                noProductsTv.setVisibility(View.GONE);
                                            } else {
                                                completedPutAwayDatasRv.setAdapter(null);
                                                adapter.notifyDataSetChanged();
                                                completedLayout.setVisibility(View.GONE);
                                                noProductsTv.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private RadioGroup.OnCheckedChangeListener checkListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

            switch (checkedId) {
                case R.id.rb_pending:
                    completedLayout.setVisibility(View.GONE);
                    bins = dataSource.putAway.getPutAwayData(0);
                    if (Utils.isValidArrayList(bins)) {
                        binAdapter = new BinAdapter(bins);
                        binsRv.setAdapter(binAdapter);
                        binAdapter.notifyDataSetChanged();
                        pendingLayout.setVisibility(View.VISIBLE);
                        noProductsTv.setVisibility(View.GONE);
                    } else {
                        if (Utils.getConnectivityStatus(context)) {
                            new PutAwayDataTask().execute();
                        }
                    }

                    break;

                case R.id.rb_completed:
                    pendingLayout.setVisibility(View.GONE);
                    binListUi = dataSource.putAway.getPutAwayData(1);
                    if (Utils.getConnectivityStatus(context)) {
                        new PutAwayCompletedDataTask().execute();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    class BinAdapter extends RecyclerView.Adapter<BinAdapter.DataObjectHolder> {
        private ArrayList<PutAwayData> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView binNumTv, productNumTv, noOfBoxesTv, vehicleNumTv, inwardNumTv, custCodeTv;
            LinearLayout putAwayLayout;

            public DataObjectHolder(View itemView) {
                super(itemView);

                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_boxes_count);
                putAwayLayout = (LinearLayout) itemView.findViewById(R.id.putaway_layout);

                vehicleNumTv = (TextView) itemView.findViewById(R.id.tv_vehicle_number);
                inwardNumTv = (TextView) itemView.findViewById(R.id.tv_inward_number);
                custCodeTv = (TextView) itemView.findViewById(R.id.tv_customer_code);
                putAwayLayout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    intent = new Intent(PutAwayActivity.this, PutAwayDetailActivity.class);
                    String binStr = gson.toJson(list.get(pos));
                    intent.putExtra("bin", binStr);
                    intent.putExtra("vehicleNum", list.get(pos).getVehNo());
                    intent.putExtra("inwardNum", list.get(pos).getInwardNo());
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            }
        }

        public BinAdapter(ArrayList<PutAwayData> myDataset) {
            list = myDataset;
        }

        @Override
        public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_putaway_item, parent, false);
            return new DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final DataObjectHolder holder, final int position) {
            PutAwayData model = list.get(position);
            if (model != null) {
                if (model.getBinNumber() != null) {
                    holder.binNumTv.setText(model.getBinNumber());
                } else {
                    holder.binNumTv.setText("--");
                }

                if (model.getDataType().equals("P")) {
                    holder.productNumTv.setText(model.getPartNo());
                    if (Utils.isValidString(model.getBatchNo())) {
                        holder.custCodeTv.setVisibility(View.VISIBLE);
                        holder.custCodeTv.setText(model.getBatchNo());
                    } else {
                        holder.custCodeTv.setVisibility(View.INVISIBLE);
                    }

                    holder.inwardNumTv.setText(model.getTotalQty()+"");
                } else {
                    holder.productNumTv.setText(model.getDaNo());
                    if (Utils.isValidString(model.getCustomerCode())) {
                        holder.custCodeTv.setVisibility(View.VISIBLE);
                        holder.custCodeTv.setText(model.getCustomerCode());
                    } else {
                        holder.custCodeTv.setVisibility(View.INVISIBLE);
                    }
                    holder.inwardNumTv.setText(model.getSubproject()+"");
                }

                int scannedBoxes = model.getTotalBoxes() - model.getNoOfBoxesToScan();
                holder.noOfBoxesTv.setText(scannedBoxes + "/"
                        + model.getTotalBoxes());

                holder.vehicleNumTv.setText(model.getVehNo());


                holder.vehicleNumTv.setTag(position);
                holder.inwardNumTv.setTag(position);
                holder.custCodeTv.setTag(position);
                holder.binNumTv.setTag(position);
                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.putAwayLayout.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<PutAwayData> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PutAwayData dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    class CompletedPutAwayDataAdapter extends RecyclerView.Adapter<CompletedPutAwayDataAdapter.DataObjectHolder> {
        private ArrayList<PutAwayData> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView binNumTv, productNumTv, noOfBoxesTv, vehicleNumTv, inwardNumTv, custCodeTv;
            CheckBox checkBox;

            public DataObjectHolder(View itemView) {
                super(itemView);

                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                vehicleNumTv = (TextView) itemView.findViewById(R.id.tv_vehicle_number);
                inwardNumTv = (TextView) itemView.findViewById(R.id.tv_inward_number);
                custCodeTv = (TextView) itemView.findViewById(R.id.tv_customer_code);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (buttonView.getTag() != null) {
                            int pos = (int) buttonView.getTag();

                            if (isChecked) {
                                mChecked.put(pos, isChecked);
                                if (isAllValuesChecked()) {
                                    selectAllCb.setChecked(isChecked);
                                }
                            } else {
                                mChecked.delete(pos);
                                selectAllCb.setChecked(isChecked);

                            }
                        }
                    }
                });
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    intent = new Intent(PutAwayActivity.this, PutAwayDetailActivity.class);
                    String binStr = gson.toJson(list.get(pos));
                    intent.putExtra("bin", binStr);
                    intent.putExtra("vehicleNum", list.get(pos).getVehNo());
                    intent.putExtra("inwardNum", list.get(pos).getInwardNo());
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            }
        }

        public CompletedPutAwayDataAdapter(ArrayList<PutAwayData> myDataset) {
            list = myDataset;
        }

        @Override
        public CompletedPutAwayDataAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                               int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_grn_item, parent, false);
            return new CompletedPutAwayDataAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final CompletedPutAwayDataAdapter.DataObjectHolder holder, final int position) {
            PutAwayData model = list.get(position);
            if (model != null) {
                holder.binNumTv.setText(model.getBinNumber());
                if (model.getDataType().equals("P")) {
                    holder.productNumTv.setText(model.getPartNo());
                } else {
                    holder.productNumTv.setText(model.getDaNo());
                }
                holder.noOfBoxesTv.setText(String.valueOf(model.getTotalBoxes()));
                holder.checkBox.setChecked(mChecked.get(position));
                holder.vehicleNumTv.setText(model.getVehNo());
                holder.inwardNumTv.setText(model.getInwardNo());
                holder.custCodeTv.setText(model.getCustomerCode());

                holder.vehicleNumTv.setTag(position);
                holder.inwardNumTv.setTag(position);
                holder.custCodeTv.setTag(position);
                holder.binNumTv.setTag(position);
                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.checkBox.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            count = list.size();
            return list.size();
        }

        public void update(ArrayList<PutAwayData> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PutAwayData dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    protected boolean isAllValuesChecked() {
        for (int i = 0; i < groupData.size(); i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater inflater;

        class GroupViewholder {
            TextView binNumTv, productNumTv, vehicleNumTv, inwardNumTv, totalNoOfBoxesTv;
            CheckBox checkBox;
            EditText noOfBoxesTv;
            TextWatcher textWatcher;
        }
        public ExpandableListAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public WmsPutawayBinModel getChild(int groupPosition, int childPosition) {
            return childData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.child_item_put_away, parent, false);

            v.setTag(R.string.group_pos, groupPosition);
            v.setTag(R.string.child_pos, childPosition);

            final WmsPutawayBinModel part = (WmsPutawayBinModel) getChild(groupPosition, childPosition);
            TextView binNumberTv = (TextView) v.findViewById(R.id.tv_bin_number);
            TextView batchNoTv = (TextView) v.findViewById(R.id.tv_batch_number);
            TextView qtyTv = (TextView) v.findViewById(R.id.tv_qty);

            binNumberTv.setText(part.getBinNo());
            batchNoTv.setText(part.getBatchNo());
            qtyTv.setText("Qty: " + part.getNoBoxes());
            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childData.get(groupPosition).size();
        }

        @Override
        public PutAwayData getGroup(int groupPosition) {
            return groupData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            count = groupData.size();
            return groupData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            GroupViewholder gvHolder = null;
            if (convertView != null)
                gvHolder = (GroupViewholder)convertView.getTag();
            else {
                convertView = inflater.inflate(R.layout.layout_grn_item, parent, false);
                gvHolder = new GroupViewholder();
                gvHolder.binNumTv = (TextView) convertView.findViewById(R.id.tv_bin_number);
                gvHolder.productNumTv = (TextView) convertView.findViewById(R.id.tv_product_number);
                gvHolder.noOfBoxesTv = (EditText) convertView.findViewById(R.id.grn_quantity);
                gvHolder.totalNoOfBoxesTv = (TextView) convertView.findViewById(R.id.tv_quantity);
                gvHolder.vehicleNumTv = (TextView) convertView.findViewById(R.id.tv_vehicle_number);
                gvHolder.inwardNumTv = (TextView) convertView.findViewById(R.id.tv_inward_number);
                gvHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
                gvHolder.textWatcher = null;
                convertView.setTag(gvHolder);

            }

            if(gvHolder.textWatcher != null){
                gvHolder.noOfBoxesTv.removeTextChangedListener(gvHolder.textWatcher);
            }
            gvHolder.checkBox.setOnCheckedChangeListener(null);

            final PutAwayData model = (PutAwayData) getGroup(groupPosition);

            if (model != null) {
                gvHolder.binNumTv.setText(model.getBinNumber());
                if (model.getDataType().equals("P")) {
                    gvHolder.productNumTv.setText(model.getPartNo());
                } else {
                    gvHolder.productNumTv.setText(model.getDaNo());
                }

                if(model.getGrnBoxes() == 0) {
                    model.setGrnBoxes(model.getTotalBoxes());
                    gvHolder.noOfBoxesTv.setText(String.valueOf(model.getTotalBoxes()));
                } else {
                    gvHolder.noOfBoxesTv.setText(String.valueOf(model.getGrnBoxes()));
                    gvHolder.noOfBoxesTv.setSelection(gvHolder.noOfBoxesTv.getText().length());
                }

                gvHolder.totalNoOfBoxesTv.setText("/  " + model.getTotalBoxes());
                gvHolder.checkBox.setChecked(mChecked.get(groupPosition));
                gvHolder.vehicleNumTv.setText(model.getVehNo());
                gvHolder.inwardNumTv.setText(model.getInwardNo());
                gvHolder.vehicleNumTv.setTag(groupPosition);
                gvHolder.inwardNumTv.setTag(groupPosition);
                gvHolder.binNumTv.setTag(groupPosition);
                gvHolder.productNumTv.setTag(groupPosition);
                gvHolder.noOfBoxesTv.setTag(groupPosition);
                gvHolder.checkBox.setTag(groupPosition);
            }

            TextWatcher textWatcher = new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!s.toString().isEmpty()) {
                        model.setGrnBoxes(Integer.parseInt(s.toString()));
                    }
                }
            };

            gvHolder.noOfBoxesTv.addTextChangedListener(textWatcher);
            gvHolder.textWatcher = textWatcher;

            gvHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.getTag() != null) {
                        int pos = (int) buttonView.getTag();

                        if (isChecked) {
                            mChecked.put(pos, isChecked);
                            if (isAllValuesChecked()) {
                                selectAllCb.setChecked(isChecked);
                            }
                        } else {
                            mChecked.delete(pos);
                            selectAllCb.setChecked(isChecked);

                        }
                    }
                }
            });
            return convertView;
        }

        private View.OnClickListener groupClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int groupPosition = (Integer) v.getTag();
                Intent intent = new Intent(PutAwayActivity.this, PutAwayDetailActivity.class);
                String binStr = gson.toJson(groupData.get(groupPosition));
                intent.putExtra("bin", binStr);
                intent.putExtra("vehicleNum", groupData.get(groupPosition).getVehNo());
                intent.putExtra("inwardNum", groupData.get(groupPosition).getInwardNo());
                if (intent != null) {
                    startActivity(intent);
                }
            }
        };

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }

    }
}
