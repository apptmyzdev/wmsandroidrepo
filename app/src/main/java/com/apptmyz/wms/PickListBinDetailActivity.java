package com.apptmyz.wms;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.wms.custom.CounterListener;
import com.apptmyz.wms.custom.CounterView;
import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.custom.ImageProcessor;
import com.apptmyz.wms.data.BinMasterModel;
import com.apptmyz.wms.data.CreateGrnInput;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.SkipBinReasonModel;
import com.apptmyz.wms.data.SkipBinRequest;
import com.apptmyz.wms.data.SubmitPickListInput;
import com.apptmyz.wms.data.WmsPartInvoiceModel;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.fileutils.ImageLoader;
import com.apptmyz.wms.util.CameraUtils;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.apptmyz.wms.util.Utils;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class PickListBinDetailActivity extends BaseActivity {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3, MY_PERMISSIONS_CAMERA = 4;
    private static final int CAPTURE_IMG = 1;
    private static final int CROP_IMG = 2;
    private ArrayList<String> imgBase64s;
    private ArrayList<String> imgFileNames;
    private ConstraintLayout imagesView;
    private ImageLoader imageLoader;
    private boolean isHideViewBtn = false;
    private RecyclerView imagesRv;
    private ImagesAdapter imagesAdapter;
    private TextInputEditText binEt, daEt;
    private TextInputLayout binLayout;
    private String binNum;
    private int binId;
    private boolean isAlternateDA;
    private String alternateDaNo, excepRemarks;
    private Integer excepReasonId;
    private Spinner reasonSp;
    private EditText remarksEt;
    private CounterView cView;
    private RadioGroup pickFromRg;
    private Button captureImageBtn;

    private int imagePosition = -1;
    TextView titleTv, binNumTv, productNumTv, qtyTv;
    private String imageStoragePath, capBase64Filename = "", capBase64 = "";
    TextInputEditText productNumEt;
    ImageView scanProductIv;
    Button confirmBtn, saveBtn, skipBinBtn, captureNewBtn, cancelImgsBtn;
    TextView toPickStatusTv, scanCompleteStatusTv, pickInstructionTv;
    CounterView counterView;
    Context context;
    WmsPartInvoiceModel bin;
    Gson gson = new Gson();
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition, barcodeLength, barcodeMinLength, barcodeMaxLength;
    private String data;
    private int timeScan, scannedBoxes, subprojectId, nodeId;
    private TimerTask timerScanTask;
    private String binNumber, productNumber, type;
    private DataSource dataSource;
    private boolean isPartialPicking;
    private CheckBox damagedCb;
    private EditText exceptionRemarksEt;
    private LinearLayout remarksLayout;
    private Button captureExceptionImgBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_pick_list_bin_detail, frameLayout);

        context = PickListBinDetailActivity.this;
        dataSource = new DataSource(context);

        damagedCb = (CheckBox) findViewById(R.id.cb_damaged);
        exceptionRemarksEt = (EditText) findViewById(R.id.exception_remarks_et);
        remarksLayout = (LinearLayout) findViewById(R.id.exception_remarks_ll);
        captureExceptionImgBtn = (Button) findViewById(R.id.btn_exception_captureimage);
        captureExceptionImgBtn.setOnClickListener(listener);

        damagedCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    remarksLayout.setVisibility(View.VISIBLE);
                    captureExceptionImgBtn.setVisibility(View.VISIBLE);
                } else {
                    remarksLayout.setVisibility(View.GONE);
                    if (Utils.isValidArrayList(imgBase64s)) {
                        imgBase64s.clear();
                        imgFileNames.clear();
                    }
                    captureExceptionImgBtn.setVisibility(View.GONE);
                }
            }
        });

        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Pick List");
        saveBtn = (Button) findViewById(R.id.btn_save);
        saveBtn.setOnClickListener(listener);
        counterView = (CounterView) findViewById(R.id.counterView);
        toPickStatusTv = (TextView) findViewById(R.id.tv_boxes_tobepicked);
        scanCompleteStatusTv = (TextView) findViewById(R.id.tv_boxes_scanned);
        imagesView = (ConstraintLayout) findViewById(R.id.layout_image_capture);

        if (getIntent().getExtras() != null) {
            type = getIntent().getStringExtra("type");
            data = getIntent().getStringExtra("data");

            String binStr = getIntent().getStringExtra("bin");
            subprojectId = getIntent().getIntExtra("subprojectId", 0);
            nodeId = getIntent().getIntExtra("nodeId", 0);
            bin = gson.fromJson(binStr, WmsPartInvoiceModel.class);
            barcodeLength = bin.getBarcodeLength();
            barcodeMaxLength = bin.getBarcodeMaxLength();
            barcodeMinLength = bin.getBarcodeMinLength();
            counterView.setmMaxLimit(bin.getTotalBoxes());
            toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes()));
        }

        skipBinBtn = (Button) findViewById(R.id.btn_skip_bin);
        skipBinBtn.setOnClickListener(listener);

        if (bin != null && bin.getDataType().equals("DA")) {
            skipBinBtn.setVisibility(View.VISIBLE);
        } else {
            skipBinBtn.setVisibility(View.GONE);
        }

        scanCompleteStatusTv.setText(String.valueOf(0));

        binNumTv = (TextView) findViewById(R.id.tv_bin_number);
        binNumTv.setText(bin.getBinNo());
        productNumTv = (TextView) findViewById(R.id.tv_product_number);
        productNumTv.setText(bin.getDataType().equals("P") ? bin.getPartNo() : bin.getDaNo());
        qtyTv = (TextView) findViewById(R.id.tv_qty);
        qtyTv.setText(String.valueOf(bin.getTotalBoxes()));

        pickInstructionTv = (TextView) findViewById(R.id.tv_pick_instruction);
        pickInstructionTv.setText("Pick " + bin.getTotalBoxes() + " Boxes");

        productNumEt = (TextInputEditText) findViewById(R.id.input_product_number);
        productNumEt.addTextChangedListener(productNumWatcher);

        scanProductIv = (ImageView) findViewById(R.id.iv_scan_product);
        scanProductIv.setOnClickListener(listener);

        confirmBtn = (Button) findViewById(R.id.btn_confirm);
        confirmBtn.setOnClickListener(listener);

        counterView.setCounterListener(new CounterListener() {
            @Override
            public void onIncrementClick(String value) {
                toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes() - counterView.getValue()));
                scanCompleteStatusTv.setText(String.valueOf(counterView.getValue()));
            }

            @Override
            public void onDecrementClick(String value) {
                toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes() - counterView.getValue()));
                scanCompleteStatusTv.setText(String.valueOf(counterView.getValue()));
            }

            @Override
            public void onEditValue(String value) {
                toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes() - counterView.getValue()));
                scanCompleteStatusTv.setText(String.valueOf(counterView.getValue()));
            }
        });

    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_exception_captureimage:
                    imageCapture();
                    break;
                case R.id.btn_skip_bin:
                    showExceptionDialog();
                    break;
                case R.id.btn_save:
                    toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes() - counterView.getValue()));
                    scanCompleteStatusTv.setText(String.valueOf(counterView.getValue()));
                    break;
                case R.id.iv_scan_product:
                    productNumEt.requestFocus();
                    productNumEt.setText("");
                    break;
                case R.id.btn_confirm:
                    scannedBoxes = counterView.getValue();
                    String remarks = exceptionRemarksEt.getText().toString().trim();
                    if (!damagedCb.isChecked() || Utils.isValidString(remarks)) {
                        if (scannedBoxes != 0) {
                            reason = "";
                            remarks = "";
                            if (scannedBoxes > bin.getTotalBoxes()) {
                                Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Scan the Correct Qty of Boxes");
                                return;
                            }
                            if (scannedBoxes < bin.getTotalBoxes()) {
                                isPartialPicking = true;
                                showReasonPopUp();
                            } else {
                                isPartialPicking = false;
                                if (Utils.getConnectivityStatus(context)) {
                                    SubmitPickListInput input = new SubmitPickListInput();
                                    submit(getSubmitData());
                                }
                            }
                        } else {
                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Scan the Boxes");
                        }
                    } else {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Enter the Remarks");
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void submit(SubmitPickListInput input) {
        List<SubmitPickListInput> list = new ArrayList<>();
        list.add(input);
        new SubmitPickList(list).execute();
    }

    private TextWatcher productNumWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = productNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() > 0) {
                                        if (scannedValue.length() >= barcodeMaxLength - barcodeMinLength) {
                                            productNumber = scannedValue;
                                            if (scannedValue.length() >= barcodeLength && scannedValue.length() >= barcodeMaxLength && counterView.getValue() < counterView.getmMaxLimit()) {
                                                productNumber = scannedValue.substring(barcodeMinLength - 1, barcodeMaxLength);
                                            }
                                        }

                                        if (productNumber != null && !(bin.getDataType().equals("P") && productNumber.equalsIgnoreCase(bin.getBatchNo()!=null ? bin.getBatchNo():bin.getPartNo()) ||
                                                bin.getDataType().equals("DA") && productNumber.equalsIgnoreCase(bin.getDaNo()))) {
                                            Utils.showSimpleAlert(context, getString(R.string.alert), "Scanned Value does not Match");
                                            productNumEt.setText("");
                                            return;
                                        } else {
                                            counterView.setValue(counterView.getValue() + 1);
                                        }
                                        timeScan = 0;
                                        if (timerScan != null)
                                            timerScan.cancel();
                                        timerScan = null;
                                    }
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }
                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    String reason = "", remarks = "";

    private void showReasonPopUp() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage("You have Scanned less than the Actual No. of Boxes. Pick a Reason: ");
        alert.setTitle("Select Reason");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_pick_list_reason, null);
        alert.setView(view);
        Spinner spinner = view.findViewById(R.id.sp_reason);
        EditText remarksEt = view.findViewById(R.id.et_remarks);
        String[] s = getResources().getStringArray(R.array.reasons);
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(PickListBinDetailActivity.this,
                android.R.layout.simple_dropdown_item_1line, s);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(16, 16, 16, 16);
        spinner.setPadding(5, 5, 5, 5);
        spinner.setLayoutParams(lp);
        spinner.setAdapter(adp);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reason = s[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                reason = s[0];
            }
        });

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                remarks = remarksEt.getText().toString().trim();
                submit(getSubmitData());
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alert.show();
    }

    private SubmitPickListInput getSubmitData() {
        SubmitPickListInput input = new SubmitPickListInput();
        input.setBinId(bin.getBinId());
        input.setOrderNo(bin.getOrderNo());
        input.setPartId(bin.getPartId());
        input.setPartNo(bin.getPartNo());
        input.setTotalBoxes(counterView.getValue());
        input.setId(bin.getId());
        input.setReasonForShortage(reason);
        input.setRemarks(remarks);
        input.setPartialPicking(isPartialPicking);

        if (damagedCb.isChecked()) {
            input.setExcepType(Constants.DAMAGED_EXCEP_TYPE);
            String remarks = exceptionRemarksEt.getText().toString().trim();
            input.setExcepRemarks(remarks);
            input.setImages(imgBase64s);
        }

        input.setScanTime(Utils.getTime(new Date(), getResources().getString(R.string.server_date_format)));
        if (isAlternateDA) {
            setExceptionData(input);
        }
        return input;
    }

    private void setExceptionData(SubmitPickListInput input) {
        input.setExcepType(Constants.PICKLIST_EXCEPTION_TYPE);
        input.setImages(imgBase64s);
        input.setAlternateBin(binId);
        input.setTotalBoxes(cView.getValue());
        input.setAlternateDaNo(alternateDaNo);
        input.setExcepRemarks(excepRemarks);
        input.setReasonId(excepReasonId);
    }

    ProgressDialog submitDialog;

    class SubmitPickList extends AsyncTask<String, String, Object> {
        List<SubmitPickListInput> input;
        GeneralResponse response;

        SubmitPickList(List<SubmitPickListInput> input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitPickListUrl(subprojectId, nodeId);
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        int remainingCount = bin.getTotalBoxes() - counterView.getValue();
                        if (remainingCount == 0) {
                            dataSource.pickListParts.updateAsSubmitted(type, data, bin.getId());
                        } else {
                            dataSource.pickListParts.updateBoxCount(remainingCount, type, data, bin.getId());
                        }
                        Intent intent = new Intent(context, PickListActivity.class);
                        intent.putExtra("isRefresh", true);
                        intent.putExtra("type", type);
                        intent.putExtra("data", data);
                        intent.putExtra("subprojectId", subprojectId);
                        intent.putExtra("nodeId", nodeId);
                        startActivity(intent);
                    }
                    Utils.showToast(context, response.getMessage());
                }
                finish();
            }
            super.onPostExecute(result);
        }

    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PickListBinDetailActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    int reasonId;

    private void showSkipBinReasonPopUp() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        final Spinner spinner = new Spinner(context);
        alert.setMessage("Select a Reason for Skipping the BIN : ");
        alert.setTitle("Select Reason");
        String[] s = getResources().getStringArray(R.array.reasons);
        List<SkipBinReasonModel> reasons = dataSource.skipBinReasons.getSkipBinReasons();
        final ArrayAdapter<SkipBinReasonModel> adp = new ArrayAdapter<SkipBinReasonModel>(PickListBinDetailActivity.this,
                android.R.layout.simple_dropdown_item_1line, reasons);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(16, 16, 16, 16);
        spinner.setPadding(5, 5, 5, 5);
        spinner.setLayoutParams(lp);
        spinner.setAdapter(adp);
        alert.setView(spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reasonId = reasons.get(i).getReasonId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                if (Utils.getConnectivityStatus(context)) {
                    SkipBinRequest input = new SkipBinRequest();
                    input.setBinId(bin.getBinId());
                    input.setBinNo(bin.getBinNo());
                    input.setExcepType(reasonId);
                    input.setPartId(bin.getPartId());
                    input.setPartNo(bin.getPartNo());
                    input.setTotalBoxes(counterView.getValue());
                    input.setScanTime(Utils.getTime(new Date(), getResources().getString(R.string.server_date_format)));
                    new SkipBinTask(input).execute();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alert.show();
    }

    ProgressDialog skipBinDialog;

    class SkipBinTask extends AsyncTask<String, String, Object> {
        SkipBinRequest input;
        GeneralResponse response;

        SkipBinTask(SkipBinRequest input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                skipBinDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSkipBinUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && skipBinDialog != null && skipBinDialog.isShowing()) {
                skipBinDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        Intent intent = new Intent(context, PickListActivity.class);
                        intent.putExtra("type", type);
                        intent.putExtra("data", data);
                        intent.putExtra("subprojectId", subprojectId);
                        intent.putExtra("nodeId", nodeId);
                        intent.putExtra("isRefresh", true);
                        startActivity(intent);
                    }
                    Utils.showToast(context, response.getMessage());
                }
                finish();
            }
            super.onPostExecute(result);
        }
    }

    private android.app.AlertDialog alert, imagesAlert;

    private void showExceptionDialog() {
        isAlternateDA = true;
        damagedCb.setChecked(false);
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_picklist_exception, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        alert = alertDialog.show();
        binLayout = (TextInputLayout) view.findViewById(R.id.layout_bin);
        binEt = (TextInputEditText) view.findViewById(R.id.input_bin_number);
        daEt = (TextInputEditText) view.findViewById(R.id.input_da_number);
        TextView originalDaTv, partNumTv, qtyTv, toPickTv, scanCompleteTv;
        originalDaTv = (TextView) view.findViewById(R.id.tv_original_da);
        partNumTv = (TextView) view.findViewById(R.id.tv_part_no);
        qtyTv = (TextView) view.findViewById(R.id.tv_qty);

        reasonSp = (Spinner) view.findViewById(R.id.sp_reason);
        remarksEt = (EditText) view.findViewById(R.id.remarks_et);
        captureImageBtn = (Button) view.findViewById(R.id.btn_captureimage);
        cView = (CounterView) view.findViewById(R.id.exceptionCv);
        Button confirmExceptionBtn = (Button) view.findViewById(R.id.btn_confirm_alternate_da);
        Button cancelExceptionBtn = (Button) view.findViewById(R.id.btn_cancel_exception);
        originalDaTv.setText(bin.getDaNo());
        partNumTv.setText(bin.getPartNo());
        qtyTv.setText(bin.getTotalBoxes() + "");
        List<SkipBinReasonModel> reasons = dataSource.pickingExcepReasons.getReasons();
        ArrayAdapter<SkipBinReasonModel> reasonsAdapter = new ArrayAdapter<SkipBinReasonModel>(PickListBinDetailActivity.this,
                android.R.layout.simple_dropdown_item_1line, reasons);
        reasonSp.setAdapter(reasonsAdapter);
        reasonSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                excepReasonId = reasons.get(i).getReasonId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        pickFromRg = (RadioGroup) view.findViewById(R.id.radioGroup);
        RadioGroup.OnCheckedChangeListener changeListener = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (i) {
                    case R.id.rb_same_bin:
                        binEt.setText(null);
                        binLayout.setVisibility(View.GONE);
                        break;

                    case R.id.rb_diff_bin:
                        binLayout.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
            }
        };
        pickFromRg.setOnCheckedChangeListener(changeListener);
        RadioButton sameBinRb = (RadioButton) view.findViewById(R.id.rb_same_bin);
        RadioButton diffBinRb = (RadioButton) view.findViewById(R.id.rb_diff_bin);
        toPickTv = (TextView) view.findViewById(R.id.tv_topick);
        scanCompleteTv = (TextView) view.findViewById(R.id.tv_scan_complete);
        cView.setmMaxLimit(bin.getTotalBoxes());
        toPickTv.setText(String.valueOf(bin.getTotalBoxes()));
        scanCompleteTv.setText(String.valueOf(0));
        cView.setCounterListener(new CounterListener() {
            @Override
            public void onIncrementClick(String value) {
                toPickTv.setText(String.valueOf(bin.getTotalBoxes() - cView.getValue()));
                scanCompleteTv.setText(String.valueOf(counterView.getValue()));
            }

            @Override
            public void onDecrementClick(String value) {
                toPickTv.setText(String.valueOf(bin.getTotalBoxes() - cView.getValue()));
                scanCompleteTv.setText(String.valueOf(counterView.getValue()));
            }

            @Override
            public void onEditValue(String value) {
                toPickTv.setText(String.valueOf(bin.getTotalBoxes() - cView.getValue()));
                scanCompleteTv.setText(String.valueOf(counterView.getValue()));
            }
        });

        binEt.addTextChangedListener(binWatcher);
//        daEt.addTextChangedListener(daWatcher);

        confirmExceptionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAndCreateException();
            }
        });

        cancelExceptionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAlternateDA = false;
//                cView.setValue(0);
                if (Utils.isValidArrayList(imgBase64s)) {
                    imgBase64s.clear();
                    imgFileNames.clear();
                }
                alert.dismiss();
            }
        });
        captureImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageCapture();
            }
        });
    }

    private void imageCapture() {
        isHideViewBtn = false;
        capBase64Filename = "IMAGE_Exception";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                return;
            }
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_CAMERA);

                return;
            }
        }
        if (!Utils.isValidArrayList(imgBase64s)) {
            imagePosition = -1;
            captureImage(CAPTURE_IMG, capBase64Filename + "_1"); //opens Camera Activity
        } else {
            showImagesDialog(imgBase64s);
        }
    }

    private void validateAndCreateException() {
        scannedBoxes = cView.getValue();
        excepRemarks = remarksEt.getText().toString().trim();
        alternateDaNo = daEt.getText().toString().trim();

        if (pickFromRg.getCheckedRadioButtonId() == R.id.rb_same_bin || (binId != 0)) {
            if (Utils.isValidString(alternateDaNo)) {
                if (excepReasonId != null && excepReasonId != 0) {
                    if (Utils.isValidString(excepRemarks)) {
                        if (scannedBoxes != 0) {
                            reason = "";
                            remarks = "";
                            if (scannedBoxes > bin.getTotalBoxes()) {
                                Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Scan the Correct Qty of Boxes");
                                return;
                            }
                            isPartialPicking = false;
                            if (Utils.getConnectivityStatus(context)) {
                                submit(getSubmitData());
                            }
                        } else {
                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Scan the Boxes");
                        }
                    } else {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Enter the Remarks");
                    }
                } else {
                    Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Select the Reason");
                }
            } else {
                Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Select valid Alternate DA number");
            }
        } else {
            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Scan the BIN");
        }
    }

    private TextWatcher binWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() >= 2) {
                                        binNum = scannedValue;
                                        binId = 0;
                                        int matchCount = dataSource.bins.matchesData(scannedValue);
                                        if (matchCount == 0) {
                                            binNum = "";
                                            Utils.showSimpleAlert(context, "Alert", "BIN Number doesn't exist");
                                        } else {
                                            if (dataSource.bins.exists(binNum) != -1) {
                                                BinMasterModel model = dataSource.bins.getBin(binNum);
                                                binId = model.getBinId();
                                            }
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private void showImagesDialog(ArrayList<String> imageBase64s) {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_capture_image, null, false);
        view.setVisibility(View.VISIBLE);
        imageLoader = Utils.getImageLoader(context, "IMAGES");
        imagesRv = (RecyclerView) view.findViewById(R.id.rv_exception_images);
        if (Utils.isValidArrayList(imgBase64s)) {
            imagesAdapter = new ImagesAdapter(imgBase64s);
            imagesRv.setAdapter(imagesAdapter);
        }
        captureNewBtn = (Button) view.findViewById(R.id.btn_captue_new);
        cancelImgsBtn = (Button) view.findViewById(R.id.btn_cancel_imgs);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));
        if (Utils.isValidArrayList(imageBase64s)) {
            imagesAdapter = new ImagesAdapter(imageBase64s);
            imagesRv.setAdapter(imagesAdapter);
        }
        cancelImgsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagesAlert.dismiss();
                imagesView.setVisibility(View.GONE);
            }
        });
        captureNewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isValidArrayList(imageBase64s) && imageBase64s.size() == 3) {
                    Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                } else {
                    imagePosition = -1;
                    captureImage(CAPTURE_IMG, capBase64Filename + "_" + imagePosition);
                }
            }
        });
        alertDialog.setView(view);
        imagesAlert = alertDialog.show();
    }

    private void captureImage(int requestCode, String imageFileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE, imageFileName);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            fileUri = Uri.fromFile(file);
        } else {
            fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, requestCode);
    }

    class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.DataObjectHolder> {
        private ArrayList<String> bitmaps;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            ImageView imageView, deleteIv;
            Button viewBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.iv_image);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                viewBtn = (Button) itemView.findViewById(R.id.btn_view);
                viewBtn.setOnClickListener(this);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (v.getId()) {
                    case R.id.btn_view:
                        int pos = (int) v.getTag();
                        showImagePreview(getBitmap(imgBase64s.get(pos)), pos);
                        break;
                    case R.id.iv_delete:
                        imgBase64s.remove((int) v.getTag());
                        notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        }

        public ImagesAdapter(ArrayList<String> myDataset) {
            bitmaps = myDataset;
        }

        @Override
        public ImagesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_image_item, parent, false);
            return new ImagesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ImagesAdapter.DataObjectHolder holder, final int position) {
            String base64 = bitmaps.get(position);
            if (base64 != null) {
                holder.imageView.setImageBitmap(getBitmap(base64));
                if (isHideViewBtn) {
                    holder.viewBtn.setVisibility(View.GONE);
                    holder.deleteIv.setVisibility(View.GONE);
                } else {
                    holder.viewBtn.setVisibility(View.VISIBLE);
                    holder.deleteIv.setVisibility(View.VISIBLE);
                }

                holder.imageView.setTag(position);
                holder.deleteIv.setTag(position);
                holder.viewBtn.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            int count = bitmaps.size();
            return bitmaps.size();
        }

        public void update(ArrayList<String> data) {
            bitmaps.clear();
            bitmaps.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(String dataObj, int index) {
            bitmaps.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            bitmaps.remove(index);
            notifyItemRemoved(index);
        }

    }

    private void showImagePreview(Bitmap bitmap, int position) {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle("Image Preview");
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_pod, null);
        ImageView docketIv;
        Button cancelBtn, reCaptureBtn;
        docketIv = (ImageView) view.findViewById(R.id.iv_pod);
        cancelBtn = (Button) view.findViewById(R.id.btn_crop);
        cancelBtn.setText("Cancel");
        reCaptureBtn = (Button) view.findViewById(R.id.btn_recapture);

        alertDialog.setView(view);
        byte[] decodedString = Base64.decode(getBase64(bitmap), Base64.NO_WRAP);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        decodedByte.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        docketIv.setImageBitmap(decodedByte);
        final android.app.AlertDialog alert = alertDialog.show();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        reCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                imagePosition = position;
                captureImage(CAPTURE_IMG, capBase64Filename + "_" + position);
            }
        });
    }

    private String getBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70,
                byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        encoded = encoded.replace("\n", "");
        return encoded;
    }

    private Bitmap getBitmap(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_IMG:
                    try {
                        bitmap = getFullBitmap(imageStoragePath);
                        if (bitmap != null) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] imageBytes = baos.toByteArray();
                            capBase64 = capBase64Filename;
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);

                            if (Utils.isValidString(capBase64)) {
                                if (damagedCb.isChecked()) {
                                    captureExceptionImgBtn.setBackgroundResource(R.drawable.btn_bg_green);
                                } else
                                    captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);
                            }

                            Date date = new Date();
                            long d = date.getTime();
                            Bitmap resizedBitmap = null;
                            if (bitmap != null) {
                                int w = bitmap.getWidth();
                                int h = bitmap.getHeight();
                                Matrix mat = new Matrix();
                                if (w > h)
                                    mat.postRotate(90);

                                resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                                ByteArrayOutputStream baosResized = new ByteArrayOutputStream();
                                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baosResized);
                                byte[] resizedImageBytes = baosResized.toByteArray();
                            }
                        }
                        Date date = new Date();
                        long d = date.getTime();
                        capBase64Filename = Utils.getImageTime(d);

                        if (!Utils.isValidArrayList(imgBase64s)) {
                            imgBase64s = new ArrayList<>();
                            imgFileNames = new ArrayList<>();
                        }

                        if (imagePosition == -1) {
                            imgBase64s.add(getBase64(bitmap));
                            imgFileNames.add(capBase64Filename);
                        } else {
                            imgBase64s.set(imagePosition, getBase64(bitmap));
                            imgFileNames.set(imagePosition, capBase64Filename);
                        }

                        isHideViewBtn = false;
                        imagesAdapter = new ImagesAdapter(imgBase64s);
                        imagesRv.setAdapter(imagesAdapter);
                        imagesAdapter.notifyDataSetChanged();

                        if (imagesAlert == null || !imagesAlert.isShowing())
                            showImagesDialog(imgBase64s);

                        addImage(bitmap, capBase64Filename, false, CROP_IMG);//////
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.logE("CAPTURE ERROR : " + e.toString());
                    }
                    break;

                case CROP_IMG:
                    try {
                        bitmap = ImageProcessor.getInstance().getBitmap();
                        if (bitmap != null) {
                            capBase64 = "IMAGE_Exception";
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);
                            if (Utils.isValidString(capBase64)) {
                                if (damagedCb.isChecked()) {
                                    captureExceptionImgBtn.setBackgroundResource(R.drawable.btn_bg_green);
                                } else
                                    captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);
                            }

                            capBase64Filename = ImageProcessor.getInstance()
                                    .getImageFileName();
                            addImage(bitmap, capBase64Filename, false, CROP_IMG);
                        }
                    } catch (Exception e) {
                        Utils.logE(e.toString());
                    }
                    break;
                default:
                    break;
            }
        } else {
            switch (requestCode) {
                case CROP_IMG:
                    try {
                        bitmap = ImageProcessor.getInstance().getBitmap();
                        if (bitmap != null) {
                            capBase64 = "IMAGE_Exception";
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);
                            if (Utils.isValidString(capBase64)) {
                                if (damagedCb.isChecked()) {
                                    captureExceptionImgBtn.setBackgroundResource(R.drawable.btn_bg_green);
                                } else
                                    captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);
                            }

                            capBase64Filename = ImageProcessor.getInstance()
                                    .getImageFileName();
                            addImage(bitmap, capBase64Filename, false, CROP_IMG);
                        }
                    } catch (Exception e) {
                        Utils.logE(e.toString());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop, int cropRequestCode) {
        if (bitmap != null) {
            ImageLoader imageLoader = new ImageLoader(context, "POD");
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                if (!isCrop) {
                } else {
                    doCrop(bitmap, imageFileName, cropRequestCode);
                }
            }
        }
    }

    private void doCrop(Bitmap bitmap, String imageFileName, int cropRequestCode) {
        ImageProcessor.getInstance().setData(context, bitmap, imageFileName);
        Intent intent = new Intent(context, ADCropActivity.class);
        ((Activity) context).startActivityForResult(intent, cropRequestCode);
    }

    private Bitmap getFullBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            int IMAGE_MAX_SIZE = 0;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                IMAGE_MAX_SIZE = 500000; //0.5MP
            } else {
                IMAGE_MAX_SIZE = 1200000; // 1.2MP
            }
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);

            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            Matrix mat = new Matrix();
            mat.postRotate(angle);

            b = Bitmap.createBitmap(b, 0, 0,
                    b.getWidth(), b.getHeight(), mat, true);

            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("", e.getMessage(), e);
            return null;
        }
    }
}
