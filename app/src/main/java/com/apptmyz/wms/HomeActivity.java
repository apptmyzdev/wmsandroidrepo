package com.apptmyz.wms;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.BinMasterModel;
import com.apptmyz.wms.data.BinMasterResponse;
import com.apptmyz.wms.data.CustomerMasterModel;
import com.apptmyz.wms.data.CustomerMasterModelNew;
import com.apptmyz.wms.data.CustomersResponse;
import com.apptmyz.wms.data.ExceptionTypeResponse;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.PartMasterModel;
import com.apptmyz.wms.data.PartMasterResponse;
import com.apptmyz.wms.data.SkipBinReasonModel;
import com.apptmyz.wms.data.SkipBinReasonsResponse;
import com.apptmyz.wms.data.VehicleMasterModel;
import com.apptmyz.wms.data.VehiclesResponse;
import com.apptmyz.wms.data.VendorMasterResponse;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.example.tscdll.TSCActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import static com.apptmyz.wms.util.Constants.DEVICE;

public class HomeActivity extends BaseActivity {
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 100;
    GridView grid;
    List<String> list;
    CustomGridViewAdapter customGridAdapter;
    TextView titleTv;
    Context context;
    DataSource dataSource;
    private boolean isManualSync;
    private android.app.AlertDialog alert = null;
    private String selectedDevice;
    private static final String OTG = "OTG";
    private static final String TSC = "TSC";
    public static Intent findDeviceList = null;
    private BluetoothAdapter bluetoothAdapter;

    private Timer timer;
    private Handler handler;
    private TimerTask timerTask;
    private ProgressDialog progressDialog;
    private int time;
    public static TSCActivity TscDll = new TSCActivity();
    private boolean isPrinting, tscConnected, shouldEstablishConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_home, frameLayout);

        context = HomeActivity.this;
        dataSource = new DataSource(context);
        homeIv.setVisibility(View.GONE);

        if (!Utils.isUpdatesCalledToday(context, Constants.CHECK_TOKEN_PREF)) {
            if (Utils.getConnectivityStatus(context)) { //Checking whether the token is still valid. Get a fresh token if it's not.
                new GetTokenTask().execute();
            } else {
                openLoginScreen();
                finish();
            }
        } else {
            setData();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                return;
            }
            default:
                break;
        }
    }

    ProgressDialog tokenDialog;

    class GetTokenTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getTokenUrl();

                Utils.logD("Log 1");
                GeneralResponse response = (GeneralResponse) HttpRequest
                        .getInputStreamFromUrl(url, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                tokenDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && tokenDialog != null && tokenDialog.isShowing()) {
                tokenDialog.dismiss();
            }
            Utils.updateTimeStamp(context, Constants.CHECK_TOKEN_PREF);

            if (showErrorDialog()) {
                isManualSync = false;
                syncMasters();
            } else {
                dataSource.pickListParts.clearAll();
                openLoginScreen();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog binsDialog;

    class BinMastersTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getBinMastersUrl();

                Utils.logD("Log 1");
                BinMasterResponse response = (BinMasterResponse) HttpRequest
                        .getInputStreamFromUrl(url, BinMasterResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<BinMasterModel> bins = (ArrayList<BinMasterModel>) response.getData();
                        if (Utils.isValidArrayList(bins))
                            dataSource.bins.saveBins(bins);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                binsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && binsDialog != null && binsDialog.isShowing()) {
                binsDialog.dismiss();
            }

            if (showErrorDialog())
                Utils.updateTimeStamp(context, Constants.CHECK_BIN_MASTER_PREF);
            super.onPostExecute(result);
        }
    }

    ProgressDialog partsDialog;

    class PartMastersTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPartMastersUrl();

                Utils.logD("Log 1");
                PartMasterResponse response = (PartMasterResponse) HttpRequest
                        .getInputStreamFromUrl(url, PartMasterResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<PartMasterModel> parts = (ArrayList<PartMasterModel>) response.getData();
                        if (Utils.isValidArrayList(parts))
                            dataSource.parts.saveParts(parts);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                partsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && partsDialog != null && partsDialog.isShowing()) {
                partsDialog.dismiss();
            }

            if (showErrorDialog())
                Utils.updateTimeStamp(context, Constants.CHECK_PART_MASTER_PREF);
            super.onPostExecute(result);
        }
    }

    ProgressDialog customersDialog;

    class CustomersTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getCustomersUrl();

                Utils.logD("Log 1");
                CustomersResponse response = (CustomersResponse) HttpRequest
                        .getInputStreamFromUrl(url, CustomersResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<CustomerMasterModelNew> customers = (ArrayList<CustomerMasterModelNew>) response.getData();
                        if (Utils.isValidArrayList(customers))
                            dataSource.customers.saveCustomers(customers);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                customersDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && customersDialog != null && customersDialog.isShowing()) {
                customersDialog.dismiss();
            }
            if (showErrorDialog())
                Utils.updateTimeStamp(context, Constants.CHECK_CUSTOMERS_MASTER_PREF);
            super.onPostExecute(result);
        }
    }

    ProgressDialog vehiclesDialog;

    class VechiclesTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getVehiclesUrl();

                Utils.logD("Log 1");
                VehiclesResponse response = (VehiclesResponse) HttpRequest
                        .getInputStreamFromUrl(url, VehiclesResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<VehicleMasterModel> vehicleMasterModels = (ArrayList<VehicleMasterModel>) response.getData();
                        if (Utils.isValidArrayList(vehicleMasterModels))
                            dataSource.vehicles.saveVehicles(vehicleMasterModels);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                vehiclesDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && vehiclesDialog != null && vehiclesDialog.isShowing()) {
                vehiclesDialog.dismiss();
            }
            if (showErrorDialog())
                Utils.updateTimeStamp(context, Constants.CHECK_VEHICLES_MASTER_PREF);
            super.onPostExecute(result);
        }
    }

    ProgressDialog skipBinDialog;

    class SkipBinReasonsTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getSkipBinReasonsUrl();

                Utils.logD("Log 1");
                SkipBinReasonsResponse response = (SkipBinReasonsResponse) HttpRequest
                        .getInputStreamFromUrl(url, SkipBinReasonsResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<SkipBinReasonModel> bins = (ArrayList<SkipBinReasonModel>) response.getData();
                        if (Utils.isValidArrayList(bins))
                            dataSource.skipBinReasons.saveReasons(bins);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                skipBinDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && skipBinDialog != null && skipBinDialog.isShowing()) {
                skipBinDialog.dismiss();
            }
            if (showErrorDialog())
                Utils.updateTimeStamp(context, Constants.CHECK_SKIPBINREASONS_MASTER_PREF);
            super.onPostExecute(result);
        }
    }

    ProgressDialog vendorDialog;

    class GetVendorMasterTask extends AsyncTask {
        VendorMasterResponse response = null;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                vendorDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getVendorMasterTag();

                response = (VendorMasterResponse) HttpRequest
                        .getInputStreamFromUrl(url, VendorMasterResponse.class,
                                context);

                if (response != null) {
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && vendorDialog != null && vendorDialog.isShowing()) {
                vendorDialog.dismiss();
            }

            if (showErrorDialog()) {
                Utils.updateTimeStamp(context, Constants.CHECK_VENDOR_MASTER_PREF);

                if (Utils.isValidArrayList((ArrayList<?>) response.getData()))
                    dataSource.vendorMaster.saveVendorMaster(response.getData());
                setData();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog exceptionTypeDialog;

    class ExceptionTypesTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getExceptionTypesUrl();

                Utils.logD("Log 1");
                ExceptionTypeResponse response = (ExceptionTypeResponse) HttpRequest
                        .getInputStreamFromUrl(url, ExceptionTypeResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<SkipBinReasonModel> exceptions = (ArrayList<SkipBinReasonModel>) response.getData();
                        if (Utils.isValidArrayList(exceptions))
                            dataSource.exceptionTypes.saveExceptionTypes(exceptions);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                exceptionTypeDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && exceptionTypeDialog != null && exceptionTypeDialog.isShowing()) {
                exceptionTypeDialog.dismiss();
            }

            if (showErrorDialog()) {
                Utils.updateTimeStamp(context, Constants.CHECK_EXCEPTIONTYPE_MASTER_PREF);
            }

            super.onPostExecute(result);
        }
    }


    ProgressDialog pickingDialog;

    class PickingExcepReasonsTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPickingExcepReasonsUrl();

                Utils.logD("Log 1");
                ExceptionTypeResponse response = (ExceptionTypeResponse) HttpRequest
                        .getInputStreamFromUrl(url, ExceptionTypeResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<SkipBinReasonModel> exceptions = (ArrayList<SkipBinReasonModel>) response.getData();
                        if (Utils.isValidArrayList(exceptions))
                            dataSource.pickingExcepReasons.saveReasons(exceptions);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                pickingDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && pickingDialog != null && pickingDialog.isShowing()) {
                pickingDialog.dismiss();
            }

            if (showErrorDialog()) {
                Utils.updateTimeStamp(context, Constants.CHECK_PICKING_EXCEP_REASONS_MASTER_PREF);
            }

            super.onPostExecute(result);
        }
    }

    private void openLoginScreen() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivity(intent);
    }

    private void setData() {
        list = new ArrayList<String>();
        grid = (GridView) findViewById(R.id.gv_menu);
        titleTv = (TextView) findViewById(R.id.tv_top_title);

        list.add("Unloading");
        list.add("Inbound Inspection");
        list.add("Unpacking");
        list.add("Put Away");
        list.add("Pick List");
        list.add("Packing");
        list.add("Loading");
        list.add("Document Handover");
        list.add("Normal Inventory");
        list.add("Perpetual Inventory");
        list.add("BIN to BIN Transfer");
        list.add("Search BIN/Part");
        list.add("Sync Masters Data");
        list.add("Scan Barcodes");
        list.add("Setup Printer");
        list.add("Test Printer");
        list.add("Logout");
        list.add("Database");
//        list.add("Alerts");
        customGridAdapter = new CustomGridViewAdapter(this, R.layout.layout_home_menu_item, (ArrayList<String>) list);
        grid.setAdapter(customGridAdapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                openActivity(position);
            }
        });

        String imeiStr = dataSource.sharedPreferences.getValue(Constants.IMEI);
        if (!Utils.isValidString(imeiStr)) {
            String imei = "";
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                imei = Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            } else {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                    }
                    return;
                }
                imei = telephonyManager.getDeviceId();

                if (!Utils.isValidString(imei))
                    imei = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
            }
            if (Utils.isValidString(imei)) {
                dataSource.sharedPreferences.set(Constants.IMEI, imei);
            }
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(HomeActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }

    private void openActivity(int position) {
        Intent intent = null;
        switch (position) {
            case 0:
                intent = new Intent(context, UnloadingScreen.class);
                break;
            case 1:
                intent = new Intent(context, InboundInspectionActivity.class);
                break;
            case 2:
                intent = new Intent(context, UnpackingActivity.class);
                break;
            case 3:
                intent = new Intent(context, PutAwayActivity.class);
                break;
            case 4:
                intent = new Intent(context, PickByCustomerOrPartActivity.class);
                break;
            case 5:
                intent = new Intent(context, PackingActivity.class);
                break;
            case 6:
                intent = new Intent(context, LoadingScreenLatest.class);
                break;
            case 7:
                intent = new Intent(context, DocumentHandoverActivity.class);
                break;
            case 8:
                intent = new Intent(context, NormalInventoryActivity.class);
                break;
            case 9:
                intent = new Intent(context, NormalInventoryActivity.class);
                intent.putExtra("perpetual", true);
                break;
            case 10:
                intent = new Intent(context, BinToBinTransferActivity.class);
                break;
            case 11:
                intent = new Intent(context, SearchActivity.class);
                break;
            case 12:
                isManualSync = true;
                syncMasters();
                break;
            case 13:
                intent = new Intent(context, ScanBarcodesActivity.class);
                break;
            case 14:
                setupPrinter();
                break;
            case 15:
                testPrint();
                break;
            case 16:
                showLogoutAlert();
                break;
            case 17:
                intent = new Intent(context, AndroidDatabaseManager.class);
                break;
            default:
                break;
        }
        if (intent != null)
            startActivity(intent);
    }

    private void testPrint() {
        tscConnected = false;
        isPrinting = true;
        String device = dataSource.sharedPreferences.getValue(DEVICE);
        if (Utils.isValidString(device)) {
            selectedDevice = device;
            checkBTState();
        } else {
            Utils.showToast(context, "Select a printer");
        }
    }

    public void checkBTState() {
        String state = getResources().getString(R.string.connecting_to_printer);
        Globals.BLUETOOTH_STATE = state;
        shouldEstablishConnection = true;
        if (isBluetoothEnabled()) {
            if (!isBluetoothDiscovering()) {
                progressDialog = ProgressDialog.show(context, "",
                        "Connecting To The Printer");

                timer = new Timer();
                handler = new Handler() {
                };
                time = Constants.connAttemptsTym;
                timerTask = new TimerTask() {

                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                    boolean deviceConnected = isDeviceConnected();
                                    Log.d("deviceConnected", "" + deviceConnected);
                                    if (deviceConnected) {
                                        tscConnected = true;
                                        Globals.selectedPrint = true;
                                        Globals.isPrint = true;
                                        Globals.isPause = true;
                                        if (isPrinting) {
                                            shouldEstablishConnection = false;
                                            handler.removeCallbacks(this);
                                            printTSCLabel();
                                            isPrinting = false;
                                            timerTask.cancel();
                                            timer.cancel();
                                            timer.purge();
                                            if (progressDialog != null) {
                                                progressDialog.dismiss();
                                                progressDialog = null;
                                            }
                                            return;
                                        }
                                        timerTask.cancel();
                                        timer.cancel();
                                        timer.purge();
                                        time += Constants.maxConnAttemptsTym;
                                    }
                                    if (time > Constants.maxConnAttemptsTym) {
                                        if (progressDialog != null) {
                                            progressDialog.dismiss();
                                            progressDialog = null;
                                        }
                                        timer.cancel();
                                    } else
                                        time = time * 2;
                            }
                        });
                    }

                };
                timer.schedule(timerTask, Constants.PRINT_SLEEP_TIME, time);
                Globals.BLUETOOTH_STATE = context.getResources()
                        .getString(R.string.connect_devices);
            }
        }
    }

    private boolean isDeviceConnected() {
        if (!tscConnected) {
            String openport = TscDll.openport(selectedDevice.toString());
            Log.d("openport", openport);
            if (openport.equals("-1")) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public void printTSCLabel() {
        TscDll.clearbuffer();
        TscDll.sendcommandUTF8(PrinterLabels.getTscBluetoothTestSample(selectedDevice));
        String closeport = TscDll.closeport();
        Log.d("closeport", closeport);
    }

    private void setupPrinter() {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(getResources().getString(R.string.whichPrinterTitel));
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.selected_device, null);
        TextView deviceName, deviceMac;
        final Button chooseDevice;

        deviceName = (TextView) view.findViewById(R.id.devicename);
        deviceMac = (TextView) view.findViewById(R.id.devicemac);
        chooseDevice = (Button) view.findViewById(R.id.choosedevice);
        selectedDevice = dataSource.sharedPreferences.getValue(DEVICE);
        if (selectedDevice != null && selectedDevice.length() != 0) {
            deviceMac.setText(selectedDevice);
        } else if (Boolean.parseBoolean(dataSource.sharedPreferences.getValue(OTG))) {
            String name = "";
            name += TSC;
            deviceName.setText(name + "-" + OTG);
        }
        alertDialog.setView(view);
        chooseDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                startPrinting();
            }
        });
        alert = alertDialog.show();
    }

    public void startPrinting() {
        dataSource.sharedPreferences.set(Constants.isPrinterAvailble, "Y");
        Globals.BLUETOOTH_STATE = "";
        checkBluetoothState();
    }

    public void checkBluetoothState() {
        String state = getResources().getString(R.string.connecting_to_printer);
        Globals.BLUETOOTH_STATE = state;
        if (isBluetoothEnabled()) {
            if (!isBluetoothDiscovering()) {
                startDeviceList();
                Globals.BLUETOOTH_STATE = context.getResources()
                        .getString(R.string.connect_devices);
            }
        }
    }

    public boolean isBluetoothEnabled() {
        Utils.logD("isBluetoothEnabled");
        boolean isEnabled = false;

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        setSelectedDevice(bluetoothAdapter);

        if (bluetoothAdapter == null) {
            Toast.makeText(context,
                    getResources().getString(R.string.bt_not_support),
                    Toast.LENGTH_LONG).show();
        } else if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
        } else
            isEnabled = true;

        return isEnabled;
    }

    public boolean isBluetoothDiscovering() {
        boolean isDiscovering = false;

        if (bluetoothAdapter.isDiscovering()) {
            Toast.makeText(context,
                    getResources().getString(R.string.bt_enabled),
                    Toast.LENGTH_SHORT).show();
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(enableBtIntent,
                    Constants.REQUEST_DISCOVERABLE_BT);
            isDiscovering = true;
        }

        return isDiscovering;
    }

    public void startDeviceList() {
        findDeviceList = new Intent(context, DeviceList.class);
        startActivityForResult(findDeviceList,
                Constants.REQUEST_PAIRED_DEVICE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_ENABLE_BT:
                    checkBluetoothState();
                    break;
                case Constants.REQUEST_PAIRED_DEVICE:
                    String device = data.getStringExtra("device");
                    break;
                case Constants.REQUEST_DISCOVERABLE_BT:
                    checkBluetoothState();
                    break;
                default:
                    break;
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setSelectedDevice(BluetoothAdapter mBtAdapter) {
        if (selectedDevice == null) {
            String dev = "Zebra";
            Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device != null) {
                        if (device.getAddress().equals(dev)) {
                            selectedDevice = device.toString();
                            dataSource.sharedPreferences.set(DEVICE, selectedDevice);
                        }
                    }
                }
            }
        }
    }

    private void syncMasters() {
        if (Utils.getConnectivityStatus(context) && (isManualSync || !Utils.checkIfCalledToday(context, Constants.CHECK_BIN_MASTER_PREF)))
            new BinMastersTask().execute();

        if (Utils.getConnectivityStatus(context) && (isManualSync || !Utils.checkIfCalledToday(context, Constants.CHECK_PART_MASTER_PREF)))
            new PartMastersTask().execute();

        if (Utils.getConnectivityStatus(context) && (isManualSync || !Utils.checkIfCalledToday(context, Constants.CHECK_CUSTOMERS_MASTER_PREF))) {
            new CustomersTask().execute();
        }

        if (Utils.getConnectivityStatus(context) && (isManualSync || !Utils.checkIfCalledToday(context, Constants.CHECK_VEHICLES_MASTER_PREF))) {
            new VechiclesTask().execute();
        }

        if (Utils.getConnectivityStatus(context) && (isManualSync || !Utils.checkIfCalledToday(context, Constants.CHECK_SKIPBINREASONS_MASTER_PREF))) {
            new SkipBinReasonsTask().execute();
        }

        if (Utils.getConnectivityStatus(context) && (isManualSync || !Utils.checkIfCalledToday(context, Constants.CHECK_VENDOR_MASTER_PREF))) {
            new GetVendorMasterTask().execute();
        }

        if (Utils.getConnectivityStatus(context) && (isManualSync || !Utils.checkIfCalledToday(context, Constants.CHECK_EXCEPTIONTYPE_MASTER_PREF)))
            new ExceptionTypesTask().execute();

        if (Utils.getConnectivityStatus(context) && (isManualSync || !Utils.checkIfCalledToday(context, Constants.CHECK_PICKING_EXCEP_REASONS_MASTER_PREF)))
            new PickingExcepReasonsTask().execute();
    }

    private void showLogoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.alert_dialog_title));
        builder.setMessage(getResources().getString(R.string.logout_confirmation));
        builder.setCancelable(false);

        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        logout();
                    }
                });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }

    private void logout() {
        clearLoginPrefs();
        Intent intent = new Intent(context, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void clearLoginPrefs() {
        dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);
        dataSource.dropTables();
    }

    public class CustomGridViewAdapter extends ArrayAdapter<String> {
        Context context;
        int layoutResourceId;
        ArrayList<String> data = new ArrayList<String>();

        public CustomGridViewAdapter(Context context, int layoutResourceId,
                                     ArrayList<String> data) {
            super(context, layoutResourceId, data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            RecordHolder holder = null;

            if (row == null) {
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new RecordHolder();
                holder.txtTitle = (TextView) row.findViewById(R.id.tv_menu_title);
                row.setTag(holder);
            } else {
                holder = (RecordHolder) row.getTag();
            }

            holder.txtTitle.setText(data.get(position));
            return row;

        }

        class RecordHolder {
            TextView txtTitle;
        }
    }
}
