package com.apptmyz.wms;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.apptmyz.wms.custom.ADRadioGroup;
import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.WmsPartInvoiceModel;
import com.apptmyz.wms.data.PickListResponse;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.apptmyz.wms.util.Utils;

public class PickListActivity extends BaseActivity {
    TextView titleTv, noProductsTv;
    ArrayList<WmsPartInvoiceModel> bins = new ArrayList<>();
    RecyclerView binsRv;
    LinearLayoutManager layoutManager;
    BinAdapter binAdapter;
    TextInputEditText binNumEt;
    ImageView scanBinIv;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private String binNumber, customerName;
    Context context;
    Gson gson = new Gson();
    ADRadioGroup radioGroup;
    boolean isBinScanned = false;
    int customerId, subprojectId;
    DataSource dataSource;
    ImageView refreshIv;
    String type;
    String data;
    private boolean isRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_pick_list, frameLayout);

        context = PickListActivity.this;
        dataSource = new DataSource(context);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);
        noProductsTv = (TextView) findViewById(R.id.tv_no_products);

        titleTv = (TextView) findViewById(R.id.tv_top_title);

        if (getIntent().getExtras() != null) {
            isRefresh = getIntent().getBooleanExtra("isRefresh", false);
            type = getIntent().getStringExtra("type");
            data = getIntent().getStringExtra("data");

            subprojectId = getIntent().getIntExtra("subprojectId", 0);
            customerName = getIntent().getStringExtra("nodeName");
            customerId = getIntent().getIntExtra("nodeId", 0);
        }

        titleTv.setText(customerName != null ? customerName : "Pick List");

        radioGroup = (ADRadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(checkListener);

        scanBinIv = (ImageView) findViewById(R.id.iv_scan_bin);
        scanBinIv.setOnClickListener(listener);
        binNumEt = (TextInputEditText) findViewById(R.id.input_bin_number);
        binNumEt.addTextChangedListener(watcher);

        binsRv = (RecyclerView) findViewById(R.id.rv_bins);
        binsRv.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(this);
        binsRv.setLayoutManager(layoutManager);

        if (isRefresh) {
            refresh();
        } else {
            setInitialData(0);
        }
    }

    private void setInitialData(int status) {
        bins = new ArrayList<>();
        bins = dataSource.pickListParts.getPickListData(type, data, status);
        binAdapter = new BinAdapter(bins);
        binsRv.setAdapter(binAdapter);
        binAdapter.notifyDataSetChanged();
        if (Utils.isValidArrayList(bins)) {
            noProductsTv.setVisibility(View.GONE);
        } else {
            refresh();
        }
    }

    ProgressDialog subprojectsDialog;

    class PartsTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                subprojectsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getPickListUrl(type, data).replace(" ", "%20");
                bins = null;
                Utils.logD("Log 1");
                PickListResponse response = (PickListResponse) HttpRequest
                        .getInputStreamFromUrl(url, PickListResponse.class,
                                context);

                if (response != null) {
                    dataSource.pickListParts.delete(type, data);
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<WmsPartInvoiceModel> models = (ArrayList<WmsPartInvoiceModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) models)) {
                            dataSource.pickListParts.insertPickListData(models, type, data);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && subprojectsDialog != null && subprojectsDialog.isShowing()) {
                subprojectsDialog.dismiss();
            }
            if (binAdapter != null)
                binAdapter.notifyDataSetChanged();
            if (showErrorDialog()) {
                setData(radioGroup.getCheckedRadioButtonId() == R.id.rb_pending ? 0 : 1);
            }
            super.onPostExecute(result);
        }
    }

    private void setData(int status) {
        bins = new ArrayList<>();
        bins = dataSource.pickListParts.getPickListData(type, data, status);
        binAdapter = new BinAdapter(bins);
        binsRv.setAdapter(binAdapter);
        binAdapter.notifyDataSetChanged();
        if (Utils.isValidArrayList(bins)) {
            noProductsTv.setVisibility(View.GONE);
        } else {
            noProductsTv.setVisibility(View.VISIBLE);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PickListActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private RadioGroup.OnCheckedChangeListener checkListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
            switch (checkedId) {
                case R.id.rb_pending:
                    setData(0);
                    break;
                case R.id.rb_completed:
                    setData(1);
                    break;
                default:
                    break;
            }
        }
    };

    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            isBinScanned = false;
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (Utils.isValidString(scannedValue)) {
                                        ArrayList<WmsPartInvoiceModel> tempBins = new ArrayList<>();
                                        for (WmsPartInvoiceModel bin : bins) {
                                            if (bin.getBinNo().contains(scannedValue)
                                                    || bin.getPartNo().contains(scannedValue))
                                                tempBins.add(bin);
                                        }
                                        binAdapter = new BinAdapter(tempBins);
                                        binsRv.setAdapter(binAdapter);
                                        binAdapter.notifyDataSetChanged();
                                    } else {
                                        binAdapter = new BinAdapter(bins);
                                        binsRv.setAdapter(binAdapter);
                                        binAdapter.notifyDataSetChanged();
                                    }

                                    if (scannedValue.length() >= 2) { //change this later
                                        binNumber = scannedValue;
                                        int matchCount = dataSource.bins.matchesData(scannedValue);
                                        if (matchCount == 0) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN number doesn't exist");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        }
                                        boolean isMatch = false;
                                        for (WmsPartInvoiceModel bin : bins) {
                                            if (binNumber.equals(bin.getBinNo())) {
                                                isMatch = true;
                                                isBinScanned = true;

                                                Intent intent = new Intent(context, PickListBinDetailActivity.class);
                                                intent.putExtra("type", type);
                                                intent.putExtra("data", data);
                                                String binStr = gson.toJson(bin);
                                                intent.putExtra("subprojectId", subprojectId);
                                                intent.putExtra("nodeId", customerId);
                                                intent.putExtra("bin", binStr);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                        if (!isMatch) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN Number Doesn't match any existing bins");
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_scan_bin:
                    binNumEt.requestFocus();
                    binNumEt.setText("");
                    break;
                case R.id.iv_refresh:
                    refresh();
                    break;
                default:
                    break;
            }
        }
    };

    private void refresh() {
        binNumEt.setText(null);
        binNumEt.clearFocus();
        if (Utils.getConnectivityStatus(context))
            new PartsTask().execute();
    }

    class BinAdapter extends RecyclerView.Adapter<BinAdapter.DataObjectHolder> {
        private ArrayList<WmsPartInvoiceModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView binNumTv, numberTv, noOfBoxesTv, orderNumTv, partNoTv, custPartNumTv, custCodeTv;
            LinearLayout layout;

            public DataObjectHolder(View itemView) {
                super(itemView);

                layout = (LinearLayout) itemView.findViewById(R.id.putaway_layout);
                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                numberTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_boxes_count);
                orderNumTv = (TextView) itemView.findViewById(R.id.tv_order_number);
                partNoTv = (TextView) itemView.findViewById(R.id.tv_part_no);
                custCodeTv = (TextView) itemView.findViewById(R.id.tv_cust_code);
                custPartNumTv = (TextView) itemView.findViewById(R.id.tv_cust_part_no);
                layout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    intent = new Intent(PickListActivity.this, PickListBinDetailActivity.class);
                    String binStr = gson.toJson(list.get(pos));
                    intent.putExtra("type", type);
                    intent.putExtra("data", data);
                    intent.putExtra("bin", binStr);
                    intent.putExtra("subprojectId", subprojectId);
                    intent.putExtra("nodeId", customerId);
                    if (intent != null) {
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }

        public BinAdapter(ArrayList<WmsPartInvoiceModel> myDataset) {
            list = myDataset;
        }

        @Override
        public BinAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_picklist_item, parent, false);
            return new BinAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final BinAdapter.DataObjectHolder holder, final int position) {
            WmsPartInvoiceModel model = list.get(position);

            if (model != null) {
                holder.binNumTv.setText(model.getBinNo());
                if( model.getDataType().equals("P")) {
                    holder.numberTv.setText( model.getPartNo());
                    holder.partNoTv.setText(model.getBatchNo());
                    holder.custPartNumTv.setText(model.getCustCode());
                    holder.custCodeTv.setText(model.getTotalQty());
                }else {
                    holder.numberTv.setText(model.getDaNo());
                    holder.partNoTv.setText(model.getPartNo());
                    holder.custPartNumTv.setText(model.getCustPartNo());
                    holder.custCodeTv.setText(model.getCustCode());
                }

                holder.noOfBoxesTv.setText(String.valueOf(model.getTotalBoxes()));

                holder.orderNumTv.setText(model.getOrderNo());



            }

            holder.binNumTv.setTag(position);
            holder.numberTv.setTag(position);
            holder.noOfBoxesTv.setTag(position);
            holder.orderNumTv.setTag(position);
            holder.partNoTv.setTag(position);
            holder.custPartNumTv.setTag(position);
            holder.custCodeTv.setTag(position);
            holder.layout.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<WmsPartInvoiceModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(WmsPartInvoiceModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }
}
