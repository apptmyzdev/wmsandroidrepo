package com.apptmyz.wms.data;

import com.apptmyz.wms.data.WmsLoadingTcModel;

import java.util.List;

public class WmsTcsResponse {
    private boolean status;
    private String message;
    private List<WmsLoadingTcModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WmsLoadingTcModel> getData() {
        return data;
    }

    public void setData(List<WmsLoadingTcModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "WmsTcsResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
