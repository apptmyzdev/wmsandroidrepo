package com.apptmyz.wms.data;

public class PickListIdsModel {
    private String picklistNo;
    private boolean pickallFlag;

    public String getPicklistNo() {
        return picklistNo;
    }

    public void setPicklistNo(String picklistNo) {
        this.picklistNo = picklistNo;
    }

    public boolean isPickallFlag() {
        return pickallFlag;
    }

    public void setPickallFlag(boolean pickallFlag) {
        this.pickallFlag = pickallFlag;
    }
    @Override
    public String toString() {
        return "PickListIdsResponse{" +
                "picklistNo=" + picklistNo +
                ", pickallFlag='" + pickallFlag + '\'' +
                '}';
    }
}
