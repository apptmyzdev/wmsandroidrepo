package com.apptmyz.wms.data;

import java.util.List;

public class LoadingSubmitModel {
    private boolean finalSubmission;
    private Integer subprojectId;
    private String dataType;
    private List<ProductModel> partList;
    private String tcNo;
    private String vehicleNo;

    public LoadingSubmitModel(boolean finalSubmission, Integer subprojectId, String dataType, List<ProductModel> partList, String tcNo, String vehicleNo) {
        this.finalSubmission = finalSubmission;
        this.subprojectId = subprojectId;
        this.dataType = dataType;
        this.partList = partList;
        this.tcNo = tcNo;
        this.vehicleNo = vehicleNo;
    }

    public boolean isFinalSubmission() {
        return finalSubmission;
    }

    public void setFinalSubmission(boolean finalSubmission) {
        this.finalSubmission = finalSubmission;
    }

    public Integer getSubprojectId() {
        return subprojectId;
    }

    public void setSubprojectId(Integer subprojectId) {
        this.subprojectId = subprojectId;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public List<ProductModel> getPartList() {
        return partList;
    }

    public void setPartList(List<ProductModel> partList) {
        this.partList = partList;
    }

    public String getTcNo() {
        return tcNo;
    }

    public void setTcNo(String tcNo) {
        this.tcNo = tcNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    @Override
    public String toString() {
        return "LoadingSubmitModel{" +
                "finalSubmission=" + finalSubmission +
                ", subprojectId=" + subprojectId +
                ", dataType='" + dataType + '\'' +
                ", partList=" + partList +
                ", tcNo='" + tcNo + '\'' +
                ", vehicleNo='" + vehicleNo + '\'' +
                '}';
    }
}
