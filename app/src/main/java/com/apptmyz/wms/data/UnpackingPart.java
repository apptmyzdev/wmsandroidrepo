package com.apptmyz.wms.data;

import java.util.List;

public class UnpackingPart {
    private int boxId;
    private int partId;
    private String partNo;
    private int noOfPrimaryCartons;
    private int noOfExceptions;
    private int noOfPrimaryCartonsContained;
    private int status;
    private int qty;
    private String vehicleNumber;
    private String inwardNumber;
    private int isActedUpon;
    private String masterBoxNum;
    private int isSelected;
    private List<ExceptionModel> exceptionList;
    private String invoiceNo;
    private String flag;

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getNoOfPrimaryCartons() {
        return qty;
    }

    public void setNoOfPrimaryCartons(int qty) {
        this.qty = qty;
    }

    public int getNoOfExceptions() {
        return noOfExceptions;
    }

    public void setNoOfExceptions(int noOfExceptions) {
        this.noOfExceptions = noOfExceptions;
    }

    public int getNoOfPrimaryCartonsContained() {
        return noOfPrimaryCartonsContained;
    }

    public void setNoOfPrimaryCartonsContained(int noOfPrimaryCartonsContained) {
        this.noOfPrimaryCartonsContained = noOfPrimaryCartonsContained;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getInwardNumber() {
        return inwardNumber;
    }

    public void setInwardNumber(String inwardNumber) {
        this.inwardNumber = inwardNumber;
    }

    public int getIsActedUpon() {
        return isActedUpon;
    }

    public void setIsActedUpon(int isActedUpon) {
        this.isActedUpon = isActedUpon;
    }

    public String getMasterBoxNum() {
        return masterBoxNum;
    }

    public void setMasterBoxNum(String masterBoxNum) {
        this.masterBoxNum = masterBoxNum;
    }

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    public List<ExceptionModel> getExceptionList() {
        return exceptionList;
    }

    public void setExceptionList(List<ExceptionModel> exceptionList) {
        this.exceptionList = exceptionList;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "UnpackingPart{" +
                "boxId=" + boxId +
                ", partId=" + partId +
                ", partNo='" + partNo + '\'' +
                ", noOfPrimaryCartons=" + noOfPrimaryCartons +
                ", noOfExceptions=" + noOfExceptions +
                ", noOfPrimaryCartonsContained=" + noOfPrimaryCartonsContained +
                ", status=" + status +
                ", qty=" + qty +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", inwardNumber='" + inwardNumber + '\'' +
                ", isActedUpon=" + isActedUpon +
                ", masterBoxNum='" + masterBoxNum + '\'' +
                ", isSelected=" + isSelected +
                ", exceptionList=" + exceptionList +
                ", invoiceNo='" + invoiceNo + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }
}