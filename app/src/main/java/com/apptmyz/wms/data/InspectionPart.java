package com.apptmyz.wms.data;

import java.util.List;

public class InspectionPart {
    private int partId;
    private String partNo;
    private String daNo;
    private String invoiceNo;
    private String masterBoxNo;
    private int noOfMasterCartons;
    private int noOfExceptions;
    private int status;
    private int qty;
    private String vehicleNumber;
    private String inwardNumber;
    private int goodBoxes;
    private List<ExceptionModel> exceptionList;
    private int noInvoice;
    private int noMasterBox;
    private String dataType;

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getNoOfMasterCartons() {
        return noOfMasterCartons;
    }

    public void setNoOfMasterCartons(int noOfMasterCartons) {
        this.noOfMasterCartons = noOfMasterCartons;
    }

    public int getNoOfExceptions() {
        return noOfExceptions;
    }

    public void setNoOfExceptions(int noOfExceptions) {
        this.noOfExceptions = noOfExceptions;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getInwardNumber() {
        return inwardNumber;
    }

    public void setInwardNumber(String inwardNumber) {
        this.inwardNumber = inwardNumber;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getGoodBoxes() {
        return goodBoxes;
    }

    public void setGoodBoxes(int goodBoxes) {
        this.goodBoxes = goodBoxes;
    }

    public List<ExceptionModel> getExceptionList() {
        return exceptionList;
    }

    public void setExceptionList(List<ExceptionModel> exceptionList) {
        this.exceptionList = exceptionList;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDaNo() {
        return daNo;
    }

    public void setDaNo(String daNo) {
        this.daNo = daNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getMasterBoxNo() {
        return masterBoxNo;
    }

    public void setMasterBoxNo(String masterBoxNo) {
        this.masterBoxNo = masterBoxNo;
    }

    public int getNoInvoice() {
        return noInvoice;
    }

    public void setNoInvoice(int noInvoice) {
        this.noInvoice = noInvoice;
    }

    public int getNoMasterBox() {
        return noMasterBox;
    }

    public void setNoMasterBox(int noMasterBox) {
        this.noMasterBox = noMasterBox;
    }

    @Override
    public String toString() {
        return "InspectionPart{" +
                "partId=" + partId +
                ", partNo='" + partNo + '\'' +
                ", daNo='" + daNo + '\'' +
                ", invoiceNo='" + invoiceNo + '\'' +
                ", masterBoxNo='" + masterBoxNo + '\'' +
                ", noOfMasterCartons=" + noOfMasterCartons +
                ", noOfExceptions=" + noOfExceptions +
                ", status=" + status +
                ", qty=" + qty +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", inwardNumber='" + inwardNumber + '\'' +
                ", goodBoxes=" + goodBoxes +
                ", exceptionList=" + exceptionList +
                ", noInvoice=" + noInvoice +
                ", noMasterBox=" + noMasterBox +
                ", dataType='" + dataType + '\'' +
                '}';
    }
}
