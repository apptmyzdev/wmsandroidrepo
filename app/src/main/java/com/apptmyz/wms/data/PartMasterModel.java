package com.apptmyz.wms.data;

public class PartMasterModel {
    private int partId;
    private String partNo;

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    @Override
    public String toString() {
        return partNo;
    }
}
