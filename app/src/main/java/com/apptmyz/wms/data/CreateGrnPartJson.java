package com.apptmyz.wms.data;

public class CreateGrnPartJson {
    private Integer id;
    private String partNo;
    private int partId;
    private int totalBoxes;
    private String binNo;
    private String scanTime;

    private String vehNo;
    private String inwardNo;
    private String dataType;
    private String daNo;
    private int grnBoxes;

    public CreateGrnPartJson(Integer id, String partNo, int partId, int totalBoxes,
                             String binNo, String scanTime, String vehicleNo, String inwardNum,
                             String dataType, String daNo, int grnBoxes) {
        this.id = id;
        this.partNo = partNo;
        this.partId = partId;
        this.totalBoxes = totalBoxes;
        this.binNo = binNo;
        this.scanTime = scanTime;
        this.vehNo = vehicleNo;
        this.inwardNo = inwardNum;
        this.dataType = dataType;
        this.daNo = daNo;
        this.grnBoxes = grnBoxes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getVehicleNo() {
        return vehNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehNo = vehicleNo;
    }

    public String getInwardNum() {
        return inwardNo;
    }

    public void setInwardNum(String inwardNum) {
        this.inwardNo = inwardNum;
    }

    public String getVehNo() {
        return vehNo;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    public String getInwardNo() {
        return inwardNo;
    }

    public void setInwardNo(String inwardNo) {
        this.inwardNo = inwardNo;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDaNo() {
        return daNo;
    }

    public void setDaNo(String daNo) {
        this.daNo = daNo;
    }

    public int getGrnBoxes() {
        return grnBoxes;
    }

    public void setGrnBoxes(int grnBoxes) {
        this.grnBoxes = grnBoxes;
    }

    @Override
    public String toString() {
        return "CreateGrnPartJson{" +
                "id=" + id +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", binNo='" + binNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", vehNo='" + vehNo + '\'' +
                ", inwardNo='" + inwardNo + '\'' +
                ", dataType='" + dataType + '\'' +
                ", daNo='" + daNo + '\'' +
                ", grnBoxes='" + grnBoxes + '\'' +
                '}';
    }
}
