package com.apptmyz.wms.data;

public class WmsInventoryDetailModel {
	private String data;
	private Integer dataId;
	private Integer noBoxes;
    private Integer colorFlag; //1 - orange, 2 - green, 3 - red

    //local params
    private boolean isConfirmed;
    private boolean isQtyCorrect;
    private int actualQty;

	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Integer getNoBoxes() {
		return noBoxes;
	}
	public void setNoBoxes(Integer noBoxes) {
		this.noBoxes = noBoxes;
	}

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public boolean isQtyCorrect() {
        return isQtyCorrect;
    }

    public void setQtyCorrect(boolean qtyCorrect) {
        isQtyCorrect = qtyCorrect;
    }

    public int getActualQty() {
        return actualQty;
    }

    public void setActualQty(int actualQty) {
        this.actualQty = actualQty;
    }

    public Integer getColorFlag() {
        return colorFlag;
    }

    public void setColorFlag(Integer colorFlag) {
        this.colorFlag = colorFlag;
    }

    @Override
    public String toString() {
        return "WmsInventoryDetailModel{" +
                "data='" + data + '\'' +
                ", dataId=" + dataId +
                ", noBoxes=" + noBoxes +
                ", colorFlag=" + colorFlag +
                ", isConfirmed=" + isConfirmed +
                ", isQtyCorrect=" + isQtyCorrect +
                ", actualQty=" + actualQty +
                '}';
    }
}
