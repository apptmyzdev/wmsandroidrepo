package com.apptmyz.wms.data;

import java.util.List;

public class UnpackingData {
    private List<UnpackingBox> boxList;
    private List<UnpackingPart> partList;
    private List<UnpackingPart> completedPartList;

    public List<UnpackingBox> getBoxList() {
        return boxList;
    }

    public void setBoxList(List<UnpackingBox> boxList) {
        this.boxList = boxList;
    }

    public List<UnpackingPart> getPartList() {
        return partList;
    }

    public void setPartList(List<UnpackingPart> partList) {
        this.partList = partList;
    }

    public List<UnpackingPart> getCompletedPartList() {
        return completedPartList;
    }

    public void setCompletedPartList(List<UnpackingPart> completedPartList) {
        this.completedPartList = completedPartList;
    }

    @Override
    public String toString() {
        return "UnpackingData{" +
                "boxList=" + boxList +
                ", partList=" + partList +
                ", completedPartList=" + completedPartList +
                '}';
    }
}
