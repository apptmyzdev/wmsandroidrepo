package com.apptmyz.wms.data;

public class VendorMasterModel {

    private int vendorId;
    private String vendorCode;
    private String vendorName;
    private String vendorAdd1;
    private String vendorAdd2;
    private String vendorAdd3;
    private String city;
    private String stateCode;
    private String state;
    private int pinCode;
    private String vendorGstin;
    private String vendorType;
    private String createdBy;
    private String createdTimestamp;
    private String updatedBy;
    private String updatedTimestamp;
    private String choosen;
    private String image;
    private String vendorSubprojectModels;
    private String servicablePincodes;
    private boolean activeFlag;
    private String mobileNo;
    private String taxDetails;
    private String fuelChargeList;

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorAdd1() {
        return vendorAdd1;
    }

    public void setVendorAdd1(String vendorAdd1) {
        this.vendorAdd1 = vendorAdd1;
    }

    public String getVendorAdd2() {
        return vendorAdd2;
    }

    public void setVendorAdd2(String vendorAdd2) {
        this.vendorAdd2 = vendorAdd2;
    }

    public String getVendorAdd3() {
        return vendorAdd3;
    }

    public void setVendorAdd3(String vendorAdd3) {
        this.vendorAdd3 = vendorAdd3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public String getVendorGstin() {
        return vendorGstin;
    }

    public void setVendorGstin(String vendorGstin) {
        this.vendorGstin = vendorGstin;
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(String createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(String updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    public String getChoosen() {
        return choosen;
    }

    public void setChoosen(String choosen) {
        this.choosen = choosen;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVendorSubprojectModels() {
        return vendorSubprojectModels;
    }

    public void setVendorSubprojectModels(String vendorSubprojectModels) {
        this.vendorSubprojectModels = vendorSubprojectModels;
    }

    public String getServicablePincodes() {
        return servicablePincodes;
    }

    public void setServicablePincodes(String servicablePincodes) {
        this.servicablePincodes = servicablePincodes;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(String taxDetails) {
        this.taxDetails = taxDetails;
    }

    public String getFuelChargeList() {
        return fuelChargeList;
    }

    public void setFuelChargeList(String fuelChargeList) {
        this.fuelChargeList = fuelChargeList;
    }

    @Override
    public String toString() {
        return vendorName;
    }
}
