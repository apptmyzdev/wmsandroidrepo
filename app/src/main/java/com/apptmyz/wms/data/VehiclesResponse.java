package com.apptmyz.wms.data;

import java.util.List;

public class VehiclesResponse {
    private boolean status;
    private String message;
    private List<VehicleMasterModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VehicleMasterModel> getData() {
        return data;
    }

    public void setData(List<VehicleMasterModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "VehiclesResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
