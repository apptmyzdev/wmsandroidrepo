package com.apptmyz.wms.data;

import java.util.List;

public class UnloadingModel {
    private boolean imgFlag;
    private String dataType;
    private Integer barcodeLength;
    private Integer barcodeMinLength;
    private Integer barcodeMaxLength;
    private String inwardNo;
    private List<ProductModel> partList;

    public boolean isImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(boolean imgFlag) {
        this.imgFlag = imgFlag;
    }

    public List<ProductModel> getPartList() {
        return partList;
    }

    public void setPartList(List<ProductModel> partList) {
        this.partList = partList;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Integer getBarcodeLength() {
        return barcodeLength;
    }

    public void setBarcodeLength(Integer barcodeLength) {
        this.barcodeLength = barcodeLength;
    }

    public Integer getBarcodeMinLength() {
        return barcodeMinLength;
    }

    public void setBarcodeMinLength(Integer barcodeMinLength) {
        this.barcodeMinLength = barcodeMinLength;
    }

    public Integer getBarcodeMaxLength() {
        return barcodeMaxLength;
    }

    public void setBarcodeMaxLength(Integer barcodeMaxLength) {
        this.barcodeMaxLength = barcodeMaxLength;
    }

    public String getInwardNo() {
        return inwardNo;
    }

    public void setInwardNo(String inwardNo) {
        this.inwardNo = inwardNo;
    }

    @Override
    public String toString() {
        return "UnloadingModel{" +
                "imgFlag=" + imgFlag +
                ", dataType='" + dataType + '\'' +
                ", barcodeLength=" + barcodeLength +
                ", barcodeMinLength=" + barcodeMinLength +
                ", barcodeMaxLength=" + barcodeMaxLength +
                ", inwardNo='" + inwardNo + '\'' +
                ", partList=" + partList +
                '}';
    }
}
