package com.apptmyz.wms.data;

public class WmsLoadingModel {
	private String partNo;
	private Integer partId;
	private int totalBoxes;
	private int scannedBoxes;
	private String scanTime;
	private int pickingComplete;

	private String tcNumber;
	private int status;

	public String getPartNo() {
		return partNo;
	}
	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}
	public Integer getPartId() {
		return partId;
	}
	public void setPartId(Integer partId) {
		this.partId = partId;
	}
	public int getTotalBoxes() {
		return totalBoxes;
	}
	public void setTotalBoxes(int totalBoxes) {
		this.totalBoxes = totalBoxes;
	}
	public int getScannedBoxes() {
		return scannedBoxes;
	}
	public void setScannedBoxes(int scannedBoxes) {
		this.scannedBoxes = scannedBoxes;
	}
	public String getScanTime() {
		return scanTime;
	}
	public void setScanTime(String scanTime) {
		this.scanTime = scanTime;
	}
	public int getPickingComplete() {
		return pickingComplete;
	}
	public void setPickingComplete(int pickingComplete) {
		this.pickingComplete = pickingComplete;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTcNumber() {
		return tcNumber;
	}

	public void setTcNumber(String tcNumber) {
		this.tcNumber = tcNumber;
	}

	@Override
	public String toString() {
		return "WmsLoadingModel{" +
				"partNo='" + partNo + '\'' +
				", partId=" + partId +
				", totalBoxes=" + totalBoxes +
				", scannedBoxes=" + scannedBoxes +
				", scanTime='" + scanTime + '\'' +
				", pickingComplete=" + pickingComplete +
				", tcNumber='" + tcNumber + '\'' +
				", status=" + status +
				'}';
	}
}
