package com.apptmyz.wms.data;

public class InvoiceModel {
    private String invoiceNo;
    private int noOfBoxes;
    private int totalQty;
    private String barcode;
    private int scannedBoxes;
    private String custCode;
    private String partNo;
    private String custPartNo;

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    private String batchNo;
    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public int getNoOfBoxes() {
        return noOfBoxes;
    }

    public void setNoOfBoxes(int noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getScannedBoxes() {
        return scannedBoxes;
    }

    public void setScannedBoxes(int scannedBoxes) {
        this.scannedBoxes = scannedBoxes;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getCustPartNo() {
        return custPartNo;
    }

    public void setCustPartNo(String custPartNo) {
        this.custPartNo = custPartNo;
    }

    @Override
    public String toString() {
        return "InvoiceModel{" +
                "invoiceNo='" + invoiceNo + '\'' +
                ", noOfBoxes=" + noOfBoxes +
                ", totalQty=" + totalQty +
                ", barcode='" + barcode + '\'' +
                ", scannedBoxes=" + scannedBoxes +
                ", custCode='" + custCode + '\'' +
                ", partNo='" + partNo + '\'' +
                ", custPartNo='" + custPartNo + '\'' +
                '}';
    }
}
