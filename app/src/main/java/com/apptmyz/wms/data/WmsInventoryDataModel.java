package com.apptmyz.wms.data;

import java.util.List;

public class WmsInventoryDataModel {
	private String data;
	private Integer dataId;
	private List<WmsInventoryDetailModel> dataList;

	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public List<WmsInventoryDetailModel> getDataList() {
		return dataList;
	}
	public void setDataList(List<WmsInventoryDetailModel> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "WmsInventoryDataModel{" +
				"data='" + data + '\'' +
				", dataId=" + dataId +
				", dataList=" + dataList +
				'}';
	}
}
