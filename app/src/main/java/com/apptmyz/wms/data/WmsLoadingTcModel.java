package com.apptmyz.wms.data;

public class WmsLoadingTcModel {
	private String tcNo;
	private String subproject;
	private int subprojectId;
	private int routeId;
	private String vehNo;
	private String secLrNo;
	private int vendorId;

	public String getTcNo() {
		return tcNo;
	}
	public void setTcNo(String tcNo) {
		this.tcNo = tcNo;
	}
	public String getSubproject() {
		return subproject;
	}
	public void setSubproject(String subproject) {
		this.subproject = subproject;
	}
	public int getSubprojectId() {
		return subprojectId;
	}
	public void setSubprojectId(int subprojectId) {
		this.subprojectId = subprojectId;
	}

	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	public String getVehNo() {
		return vehNo;
	}

	public void setVehNo(String vehNo) {
		this.vehNo = vehNo;
	}

	public String getSecLrNo() {
		return secLrNo;
	}

	public void setSecLrNo(String secLrNo) {
		this.secLrNo = secLrNo;
	}

	public int getVendorId() {
		return vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

	@Override
	public String toString() {
		return "WmsLoadingTcModel{" +
				"tcNo='" + tcNo + '\'' +
				", subproject='" + subproject + '\'' +
				", subprojectId=" + subprojectId +
				", routeId=" + routeId +
				", vehNo='" + vehNo + '\'' +
				", secLrNo='" + secLrNo + '\'' +
				", vendorId=" + vendorId +
				'}';
	}
}
