package com.apptmyz.wms.data;

public class BinMasterModel {
    private int binId;
    private String binNo;
    private String aisle;

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getAisle() {
        return aisle;
    }

    public void setAisle(String aisle) {
        this.aisle = aisle;
    }

    @Override
    public String toString() {
        return "BinMasterModel{" +
                "binId=" + binId +
                ", binNo='" + binNo + '\'' +
                ", aisle='" + aisle + '\'' +
                '}';
    }
}
