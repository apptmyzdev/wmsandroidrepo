package com.apptmyz.wms.data;

import java.util.List;

public class ExceptionModel {
    private int excepType;
    private String excepDesc;
    private List<String> imgBase64s;
    private int exceptionCount;
    private String vehicleNum;
    private String partNo;
    private String scanTime;
    private String dataType;

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public int getExcepType() {
        return excepType;
    }

    public void setExcepType(int excepType) {
        this.excepType = excepType;
    }

    public String getExcepDesc() {
        return excepDesc;
    }

    public void setExcepDesc(String excepDesc) {
        this.excepDesc = excepDesc;
    }

    public List<String> getImgBase64s() {
        return imgBase64s;
    }

    public void setImgBase64s(List<String> imgBase64s) {
        this.imgBase64s = imgBase64s;
    }

    public int getExceptionCount() {
        return exceptionCount;
    }

    public void setExceptionCount(int exceptionCount) {
        this.exceptionCount = exceptionCount;
    }

    public String getVehicleNum() {
        return vehicleNum;
    }

    public void setVehicleNum(String vehicleNum) {
        this.vehicleNum = vehicleNum;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    @Override
    public String toString() {
        return "ExceptionModel{" +
                "excepType=" + excepType +
                ", excepDesc='" + excepDesc + '\'' +
                ", imgBase64s=" + imgBase64s +
                ", exceptionCount=" + exceptionCount +
                ", vehicleNum='" + vehicleNum + '\'' +
                ", partNo='" + partNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", dataType='" + dataType + '\'' +
                '}';
    }
}
