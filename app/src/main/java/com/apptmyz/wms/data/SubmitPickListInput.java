package com.apptmyz.wms.data;

import java.util.List;

public class SubmitPickListInput {
    private String orderNo;
    private String partNo;
    private int partId;
    private int totalBoxes;
    private int binId;
    private String scanTime;
    private String reasonForShortage;
    private String remarks;
    private int id;
    private boolean isPartialPicking;

    //exception
    private int excepType;
    private List<String> images;
    private String excepRemarks;
    private String alternateDaNo;
    private Integer alternateBin;
    private Integer reasonId;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getReasonForShortage() {
        return reasonForShortage;
    }

    public void setReasonForShortage(String reasonForShortage) {
        this.reasonForShortage = reasonForShortage;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPartialPicking() {
        return isPartialPicking;
    }

    public void setPartialPicking(boolean partialPicking) {
        isPartialPicking = partialPicking;
    }

    public int getExcepType() {
        return excepType;
    }

    public void setExcepType(int excepType) {
        this.excepType = excepType;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getExcepRemarks() {
        return excepRemarks;
    }

    public void setExcepRemarks(String excepRemarks) {
        this.excepRemarks = excepRemarks;
    }

    public String getAlternateDaNo() {
        return alternateDaNo;
    }

    public void setAlternateDaNo(String alternateDaNo) {
        this.alternateDaNo = alternateDaNo;
    }

    public Integer getAlternateBin() {
        return alternateBin;
    }

    public void setAlternateBin(Integer alternateBin) {
        this.alternateBin = alternateBin;
    }

    public Integer getReasonId() {
        return reasonId;
    }

    public void setReasonId(Integer reasonId) {
        this.reasonId = reasonId;
    }

    @Override
    public String toString() {
        return "SubmitPickListInput{" +
                "orderNo='" + orderNo + '\'' +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", binId=" + binId +
                ", scanTime='" + scanTime + '\'' +
                ", reasonForShortage='" + reasonForShortage + '\'' +
                ", remarks='" + remarks + '\'' +
                ", id=" + id +
                ", isPartialPicking=" + isPartialPicking +
                ", excepType=" + excepType +
                ", images=" + images +
                ", excepRemarks='" + excepRemarks + '\'' +
                ", alternateDaNo='" + alternateDaNo + '\'' +
                ", alternateBin=" + alternateBin +
                ", reasonId=" + reasonId +
                '}';
    }
}
