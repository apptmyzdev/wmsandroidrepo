package com.apptmyz.wms.data;

import java.util.List;
import java.util.Objects;

import androidx.annotation.Nullable;

public class InventoryData {
    private int binId;
    private String binNo;
    private List<WmsPartInvoiceModel> partList;

    public String getBinNumber() {
        return binNo;
    }

    public void setBinNumber(String binNumber) {
        this.binNo = binNumber;
    }

    public List<WmsPartInvoiceModel> getPartList() {
        return partList;
    }

    public void setPartList(List<WmsPartInvoiceModel> partList) {
        this.partList = partList;
    }

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    @Override
    public String toString() {
        return "InventoryData{" +
                "binId=" + binId +
                ", binNumber='" + binNo + '\'' +
                ", partList=" + partList +
                '}';
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj instanceof InventoryData) {
            InventoryData data = (InventoryData) obj;
            return data.getBinNumber().equals(this.binNo);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(binNo);
    }
}
