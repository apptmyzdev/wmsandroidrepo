package com.apptmyz.wms.data;

public class WmsBarcodeScanModel {
	private String barcode1;
	private String barcode2;
	private String status;

	public WmsBarcodeScanModel(String barcode1, String barcode2, String status) {
		this.barcode1 = barcode1;
		this.barcode2 = barcode2;
		this.status = status;
	}

	public String getBarcode1() {
		return barcode1;
	}
	public void setBarcode1(String barcode1) {
		this.barcode1 = barcode1;
	}
	public String getBarcode2() {
		return barcode2;
	}
	public void setBarcode2(String barcode2) {
		this.barcode2 = barcode2;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "WmsBarcodeScanModel{" +
				"barcode1='" + barcode1 + '\'' +
				", barcode2='" + barcode2 + '\'' +
				", status='" + status + '\'' +
				'}';
	}
}
