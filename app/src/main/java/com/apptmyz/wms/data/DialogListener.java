package com.apptmyz.wms.data;

public interface DialogListener {
    void onPositiveButtonClick();
    void onNegativeButtonClick();
}
