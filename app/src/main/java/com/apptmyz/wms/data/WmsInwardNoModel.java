package com.apptmyz.wms.data;

import java.util.List;

public class WmsInwardNoModel {
	private String inwardNo;
	private List<String> invoiceList;
	public String getInwardNo() {
		return inwardNo;
	}
	public void setInwardNo(String inwardNo) {
		this.inwardNo = inwardNo;
	}
	public List<String> getInvoiceList() {
		return invoiceList;
	}
	public void setInvoiceList(List<String> invoiceList) {
		this.invoiceList = invoiceList;
	}

	@Override
	public String toString() {
		return inwardNo;
	}
}
