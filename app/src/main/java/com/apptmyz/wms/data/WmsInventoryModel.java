package com.apptmyz.wms.data;

import java.util.List;

public class WmsInventoryModel {
	private String dataType;
	private Integer cycleId;
	private List<WmsInventoryDataModel> dataList;

	private Integer bin;
	private Integer part;
	private Integer goodBoxes;
	private List<ExceptionModel> exceptionList;

	/*
	private Integer binId;
	private String binNo;
	private List<WmsPartInvoiceModel> partList;
	public Integer getBinId() {
		return binId;
	}
	public void setBinId(Integer binId) {
		this.binId = binId;
	}
	public String getBinNo() {
		return binNo;
	}
	public void setBinNo(String binNo) {
		this.binNo = binNo;
	}
	public List<WmsPartInvoiceModel> getPartList() {
		return partList;
	}
	public void setPartList(List<WmsPartInvoiceModel> partList) {
		this.partList = partList;
	}
*/
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Integer getCycleId() {
		return cycleId;
	}
	public void setCycleId(Integer cycleId) {
		this.cycleId = cycleId;
	}
	public List<WmsInventoryDataModel> getDataList() {
		return dataList;
	}
	public void setDataList(List<WmsInventoryDataModel> dataList) {
		this.dataList = dataList;
	}

	public Integer getBin() {
		return bin;
	}

	public void setBin(Integer bin) {
		this.bin = bin;
	}

	public Integer getPart() {
		return part;
	}

	public void setPart(Integer part) {
		this.part = part;
	}

	public Integer getGoodBoxes() {
		return goodBoxes;
	}

	public void setGoodBoxes(Integer goodBoxes) {
		this.goodBoxes = goodBoxes;
	}

	public List<ExceptionModel> getExceptionList() {
		return exceptionList;
	}

	public void setExceptionList(List<ExceptionModel> exceptionList) {
		this.exceptionList = exceptionList;
	}

	@Override
	public String toString() {
		return "WmsInventoryModel{" +
				"dataType='" + dataType + '\'' +
				", cycleId=" + cycleId +
				", dataList=" + dataList +
				", bin=" + bin +
				", part=" + part +
				", goodBoxes=" + goodBoxes +
				", exceptionList=" + exceptionList +
				'}';
	}
}
