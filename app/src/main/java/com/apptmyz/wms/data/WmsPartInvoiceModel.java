package com.apptmyz.wms.data;

import java.util.List;

public class WmsPartInvoiceModel {
    private Integer id;
    private String partNo;
    private Integer partId;
    private int totalBoxes;
    private int totalQty;
    private String binNo;
    private String scanTime;
    private int xdFlag;
    private String nextDest;
    private int excepType;
    private String excepDesc;
    private String excepImage;
    private List<InvoiceModel> invoiceList;
    private int isPicked;
    private String orderNo;
    private int binId;

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    private String batchNo;
    private int subprojectId;
    private int nodeId;

    //local params
    private boolean isConfirmed;
    private boolean isQtyCorrect;
    private int actualQty;

    private String dataType;
    private String daNo;
    private String custPartNo;
    private String supplier;
    private String custCode;

    private Integer barcodeLength;
    private Integer barcodeMinLength;
    private Integer barcodeMaxLength;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getXdFlag() {
        return xdFlag;
    }

    public void setXdFlag(int xdFlag) {
        this.xdFlag = xdFlag;
    }

    public String getNextDest() {
        return nextDest;
    }

    public void setNextDest(String nextDest) {
        this.nextDest = nextDest;
    }

    public int getExcepType() {
        return excepType;
    }

    public void setExcepType(int excepType) {
        this.excepType = excepType;
    }

    public String getExcepDesc() {
        return excepDesc;
    }

    public void setExcepDesc(String excepDesc) {
        this.excepDesc = excepDesc;
    }

    public String getExcepImage() {
        return excepImage;
    }

    public void setExcepImage(String excepImage) {
        this.excepImage = excepImage;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public Integer getPartId() {
        return partId;
    }

    public void setPartId(Integer partId) {
        this.partId = partId;
    }

    public List<InvoiceModel> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<InvoiceModel> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public int getIsPicked() {
        return isPicked;
    }

    public void setIsPicked(int isPicked) {
        this.isPicked = isPicked;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public int getSubprojectId() {
        return subprojectId;
    }

    public void setSubprojectId(int subprojectId) {
        this.subprojectId = subprojectId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public boolean isQtyCorrect() {
        return isQtyCorrect;
    }

    public void setQtyCorrect(boolean qtyCorrect) {
        isQtyCorrect = qtyCorrect;
    }

    public int getActualQty() {
        return actualQty;
    }

    public void setActualQty(int actualQty) {
        this.actualQty = actualQty;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDaNo() {
        return daNo;
    }

    public void setDaNo(String daNo) {
        this.daNo = daNo;
    }

    public String getCustPartNo() {
        return custPartNo;
    }

    public void setCustPartNo(String custPartNo) {
        this.custPartNo = custPartNo;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public Integer getBarcodeLength() {
        return barcodeLength;
    }

    public void setBarcodeLength(Integer barcodeLength) {
        this.barcodeLength = barcodeLength;
    }

    public Integer getBarcodeMinLength() {
        return barcodeMinLength;
    }

    public void setBarcodeMinLength(Integer barcodeMinLength) {
        this.barcodeMinLength = barcodeMinLength;
    }

    public Integer getBarcodeMaxLength() {
        return barcodeMaxLength;
    }

    public void setBarcodeMaxLength(Integer barcodeMaxLength) {
        this.barcodeMaxLength = barcodeMaxLength;
    }

    @Override
    public String toString() {
        return "WmsPartInvoiceModel{" +
                "id=" + id +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", totalQty=" + totalQty +
                ", binNo='" + binNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", xdFlag=" + xdFlag +
                ", nextDest='" + nextDest + '\'' +
                ", excepType=" + excepType +
                ", excepDesc='" + excepDesc + '\'' +
                ", excepImage='" + excepImage + '\'' +
                ", invoiceList=" + invoiceList +
                ", isPicked=" + isPicked +
                ", orderNo='" + orderNo + '\'' +
                ", binId=" + binId +
                ", subprojectId=" + subprojectId +
                ", nodeId=" + nodeId +
                ", isConfirmed=" + isConfirmed +
                ", isQtyCorrect=" + isQtyCorrect +
                ", actualQty=" + actualQty +
                ", dataType='" + dataType + '\'' +
                ", daNo='" + daNo + '\'' +
                ", custPartNo='" + custPartNo + '\'' +
                ", supplier='" + supplier + '\'' +
                ", custCode='" + custCode + '\'' +
                ", barcodeLength=" + barcodeLength +
                ", barcodeMinLength=" + barcodeMinLength +
                ", barcodeMaxLength=" + barcodeMaxLength +
                '}';
    }
}
