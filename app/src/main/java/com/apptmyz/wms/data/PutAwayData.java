package com.apptmyz.wms.data;

import java.util.List;

public class PutAwayData {
    private Integer id;
    private String partNo;
    private int partId;
    private List<InvoiceModel> invoiceList;
    private int totalBoxes;
    private int totalQty;
    private int noOfBoxesToScan;

    private String binNo;
    private String scanTime;

    private String vehNo;
    private int status;

    private String inwardNo;
    private String dataType;
    private String daNo;
    private List<WmsPutawayBinModel> binList;
    private String customerCode;
    private String batchNo;

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    private String subproject;

    private int grnBoxes = 0;

    public int getGrnBoxes() {
        return grnBoxes;
    }

    public void setGrnBoxes(int grnBoxes) {
        this.grnBoxes = grnBoxes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public List<InvoiceModel> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<InvoiceModel> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getNoOfBoxesToScan() {
        return noOfBoxesToScan;
    }

    public void setNoOfBoxesToScan(int noOfBoxesToScan) {
        this.noOfBoxesToScan = noOfBoxesToScan;
    }

    public String getBinNumber() {
        return binNo;
    }

    public void setBinNumber(String binNumber) {
        this.binNo = binNumber;
    }

    public String getVehNo() {
        return vehNo;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getInwardNo() {
        return inwardNo;
    }

    public void setInwardNo(String inwardNo) {
        this.inwardNo = inwardNo;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDaNo() {
        return daNo;
    }

    public void setDaNo(String daNo) {
        this.daNo = daNo;
    }

    public List<WmsPutawayBinModel> getBinList() {
        return binList;
    }

    public void setBinList(List<WmsPutawayBinModel> binList) {
        this.binList = binList;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getSubproject() {
        return subproject;
    }

    public void setSubproject(String subproject) {
        this.subproject = subproject;
    }

    @Override
    public String toString() {
        return "PutAwayData{" +
                "id=" + id +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", invoiceList=" + invoiceList +
                ", totalBoxes=" + totalBoxes +
                ", totalQty=" + totalQty +
                ", noOfBoxesToScan=" + noOfBoxesToScan +
                ", binNo='" + binNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", vehNo='" + vehNo + '\'' +
                ", status=" + status +
                ", inwardNo='" + inwardNo + '\'' +
                ", dataType='" + dataType + '\'' +
                ", daNo='" + daNo + '\'' +
                ", binList=" + binList +
                ", customerCode='" + customerCode + '\'' +
                ", subproject='" + subproject + '\'' +
                '}';
    }
}
