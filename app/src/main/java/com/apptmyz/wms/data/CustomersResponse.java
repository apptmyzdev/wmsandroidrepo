package com.apptmyz.wms.data;

import java.util.List;

public class CustomersResponse {
    private boolean status;
    private String message;
    private List<CustomerMasterModelNew> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CustomerMasterModelNew> getData() {
        return data;
    }

    public void setData(List<CustomerMasterModelNew> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CustomersResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
