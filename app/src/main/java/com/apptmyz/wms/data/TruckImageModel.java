package com.apptmyz.wms.data;

import java.util.List;

public class TruckImageModel {
    private int warehouseId;
    private String vehicleNo;
    private String tcNo;
    private int loadUnloadFlag;
    private String vehTxnDate;
    private String bay;
    private List<String> images;
    private Integer subprojectId;
    private Integer routeId;

    public String getTcNo() {
        return tcNo;
    }

    public void setTcNo(String tcNo) {
        this.tcNo = tcNo;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public int getLoadUnloadFlag() {
        return loadUnloadFlag;
    }

    public void setLoadUnloadFlag(int loadUnloadFlag) {
        this.loadUnloadFlag = loadUnloadFlag;
    }

    public String getVehTxnDate() {
        return vehTxnDate;
    }

    public void setVehTxnDate(String vehTxnDate) {
        this.vehTxnDate = vehTxnDate;
    }

    public String getBay() {
        return bay;
    }

    public void setBay(String bay) {
        this.bay = bay;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Integer getSubprojectId() {
        return subprojectId;
    }

    public void setSubprojectId(Integer subprojectId) {
        this.subprojectId = subprojectId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    @Override
    public String toString() {
        return "TruckImageModel{" +
                "warehouseId=" + warehouseId +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", tcNo='" + tcNo + '\'' +
                ", loadUnloadFlag=" + loadUnloadFlag +
                ", vehTxnDate='" + vehTxnDate + '\'' +
                ", bay='" + bay + '\'' +
                ", images=" + images +
                ", subprojectId=" + subprojectId +
                ", routeId=" + routeId +
                '}';
    }
}
