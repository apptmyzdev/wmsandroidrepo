package com.apptmyz.wms.data;

import java.util.List;

public class WmsDocumentHandoverInvoiceModel {
	private String invoiceNo;
	private List<String> tcNos;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public List<String> getTcNos() {
		return tcNos;
	}

	public void setTcNos(List<String> tcNos) {
		this.tcNos = tcNos;
	}

	@Override
	public String toString() {
		return "WmsDocumentHandoverInvoiceModel{" +
				"invoiceNo='" + invoiceNo + '\'' +
				'}';
	}
}
