package com.apptmyz.wms.data;

public class WmsPutawayBinModel {
	private String binNo;
	private Integer noBoxes;
	private String batchNo;

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getBinNo() {
		return binNo;
	}
	public void setBinNo(String binNo) {
		this.binNo = binNo;
	}
	public Integer getNoBoxes() {
		return noBoxes;
	}
	public void setNoBoxes(Integer noBoxes) {
		this.noBoxes = noBoxes;
	}

	public WmsPutawayBinModel(String binNo, Integer noBoxes) {
		this.binNo = binNo;
		this.noBoxes = noBoxes;
	}

	@Override
	public String toString() {
		return "WmsPutawayBinModel{" +
				"binNo='" + binNo + '\'' +
				", noBoxes=" + noBoxes +
				", batchNo=" + batchNo +
				'}';
	}
}
