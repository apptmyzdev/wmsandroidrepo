package com.apptmyz.wms.data;

import java.util.List;

public class InwardNumsResponse {
    private boolean status;
    private String message;
    private List<WmsInwardNoModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WmsInwardNoModel> getData() {
        return data;
    }

    public void setData(List<WmsInwardNoModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "InwardNumsResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
