package com.apptmyz.wms;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.wms.custom.BitmapScalingUtil;
import com.apptmyz.wms.custom.CameraActivity;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.LoadingSubmitModel;
import com.apptmyz.wms.data.LoginResponse;
import com.apptmyz.wms.data.NewLoadingPartsModel;
import com.apptmyz.wms.data.NewProductModel;
import com.apptmyz.wms.data.PartsListModel;
import com.apptmyz.wms.data.ProductModel;
import com.apptmyz.wms.data.TruckImageModel;
import com.apptmyz.wms.data.WmsLoadingModel;
import com.apptmyz.wms.data.WmsLoadingTcModel;
import com.apptmyz.wms.data.WmsPartInvoiceModel;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.fileutils.ImageLoader;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class LoadingReconcileScreen extends Activity {
    ProgressDialog submitDialog;
    private Context context;
    private DataSource dataSource;
    private TextView vehicleNoTv, totalPartsTv, totalBoxesTv, partsScannedTv, partsNotScannedTv;
    private Button submitBtn, rescanBtn;
    private List<NewProductModel> partsList;
    private AlertDialog errDlg;
    private String vehNo, vendorId;
    private RelativeLayout imageLayout;
    private Button captureBtn, sendBtn;
    private TextView msgTv;
    public Gson gson = new Gson();
    public final int TAKE_PHOTO = 1;
    private ImageView truckIv;
    public final int CROP_IMAGE = 2;
    private String truckImageFileName;
    private String tcStr, dataType, vehicleNo;
    private boolean isImageSubmitted;
    private boolean isFinalSubmit;
    private int subProjectId, routeId;
    private String tcNumber = "";
    private boolean isScanCompleted;

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.btn_submit:
                    isScanCompleted = true;
                    for (NewProductModel model : partsList) {
                        String number = Utils.getNumber(model);
                        if (model.getTotalBoxes() > model.getScannedBoxes()) {
                            isScanCompleted = false;
                            break;
                        }
                    }

                    if (validatePicking()) {
                        if (isImageSubmitted || !isFinalSubmit) {
                            if(isScanCompleted) {
                                isFinalSubmit = true;
                                imageLayout.setVisibility(View.VISIBLE);
                            } else {
                                new Submit().execute();
                            }
                        } else {
                            imageLayout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Utils.showSimpleAlert(context, "Alert", "Please Pick All the Parts Before Loading");
                    }
                    break;

                case R.id.btn_cancel_submit:
                    goBack();
                    break;

                case R.id.btn_capture_image:
                    truckImageFileName = "";
                    takePicture(context);
                    break;
                case R.id.btn_send_image:
                    if (Utils.isValidString(truckImageFileName)) {
                        Utils.logE("in LT image");

                        String text = "";

                        String truck = Globals.vehicleNo;
                        if (Utils.isValidString(truck)) {
                            text = text + "\n" + "Veh#  : " + truck;
                        }

                        String s = dataSource.sharedPreferences
                                .getValue(Constants.IMEI);
                        if (Utils.isValidString(s))
                            text = text + "\n" + "T-ID# : " + s;

                        String user = dataSource.sharedPreferences
                                .getValue(Constants.USERNAME_PREF);
                        if (Utils.isValidString(user))
                            text = text + "\n" + "U-ID# : " + user;

                        text = text + "\n" + "I-DT# : "
                                + Utils.getITime(Long.valueOf(truckImageFileName));

                        imageLayout.setVisibility(View.GONE);
                        TruckImageModel model = new TruckImageModel();

                        model.setVehTxnDate(Utils.getScanTimeLoading());
                        model.setLoadUnloadFlag(Constants.LOADING_COMPLETED);

                        model.setVehicleNo(vehicleNo);
                        model.setTcNo(tcNumber);
                        model.setSubprojectId(subProjectId);
                        model.setRouteId(routeId);
                        ImageLoader loader = new ImageLoader(context, Constants.TRUCK);
                        try {
                            String base64 = loader.fileCache.getBase64StringToName(
                                    truckImageFileName, text, context);
                            List<String> list = new ArrayList<String>();
                            if (Utils.isValidString(base64)) {
                                list.add(base64);
                                model.setImages(list);
                                Utils.logD(model.toString());
                                new SendStackingImage().execute(model);
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utils.logE("in else ");


                        Utils.showSimpleAlert(
                                context,
                                getString(R.string.alert_dialog_title),
                                "Please capture stacking image");

                    }

                    break;
            }

        }
    };
    private ImageView home_iv;
    private List<ProductModel> partSumbitList;
    private LoadingSubmitModel outputModel;
    private CheckBox finalSubmitCb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_reconcile_screen);

        context = LoadingReconcileScreen.this;

        dataSource = new DataSource(context);
        finalSubmitCb = (CheckBox) findViewById(R.id.cb_day_end);

        vehicleNoTv = (TextView) findViewById(R.id.tv_summary_tally_number);
        totalPartsTv = (TextView) findViewById(R.id.tv_total_dockets);
        totalBoxesTv = (TextView) findViewById(R.id.tv_total_packets);
        partsScannedTv = (TextView) findViewById(R.id.tv_packets_not_exception);
        partsNotScannedTv = (TextView) findViewById(R.id.tv_not_scanned_packets);

        submitBtn = (Button) findViewById(R.id.btn_submit);
        submitBtn.setOnClickListener(onClickListener);

        rescanBtn = (Button) findViewById(R.id.btn_cancel_submit);
        rescanBtn.setOnClickListener(onClickListener);

        home_iv = (ImageView) findViewById(R.id.home_iv);
        home_iv.setVisibility(View.GONE);

        Intent intent = getIntent();
        vehNo = intent.getStringExtra(Constants.VEHICLE_NUMBER);
        vendorId = String.valueOf(intent.getIntExtra(Constants.VENDOR_ID, 0));


        imageLayout = (RelativeLayout) findViewById(R.id.layout_image_popup);

        captureBtn = (Button) findViewById(R.id.btn_capture_image);
        captureBtn.setOnClickListener(onClickListener);

        sendBtn = (Button) findViewById(R.id.btn_send_image);
        sendBtn.setOnClickListener(onClickListener);

        truckIv = (ImageView) findViewById(R.id.iv_truck);
        truckIv.setOnClickListener(onClickListener);

        tcStr = getIntent().getStringExtra("tc");
        dataType = getIntent().getStringExtra("dataType");
        vehicleNo = getIntent().getStringExtra(Constants.VEHICLE_NUMBER);
        subProjectId = getIntent().getIntExtra("subProjectId", 0);
        routeId = getIntent().getIntExtra("routeId", 0);

        msgTv = (TextView) findViewById(R.id.tv_msg);
        msgTv.setText("After loading the last package and before Final Submission of Sheet, Take Picture with Doors in open position and then Submit the Sheet");
        finalSubmitCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isFinalSubmit = isChecked;
            }
        });
        setData();
    }

    private boolean validatePicking() {
        boolean isPickingCompleted = true;
        for (NewProductModel part : partsList) {
            if (part.getPickingComplete() != 1)
                isPickingCompleted = false;
        }
        return isPickingCompleted;
    }

    private void setData() {
        partSumbitList = new ArrayList<>();

        int totScannedCount = 0;
        int totUnscannedCount = 0;
        int totCount = 0;
        int totBox = 0;
        int totParts = 0;

        if (Utils.isValidString(tcStr)) {
            WmsLoadingTcModel tcModel = gson.fromJson(tcStr, WmsLoadingTcModel.class);
            if (tcModel != null) {
                tcNumber = tcModel.getTcNo();
            }
        }

        if (Utils.isValidString(Globals.tcNumber)) {
            vehicleNoTv.setText(Globals.tcNumber);
        }

        partsList = dataSource.loadingData.getScannedParts();
        totParts = partsList.size();
        if (Utils.isValidArrayList((ArrayList<?>) partsList)) {
            for (NewProductModel productModel : partsList) {
                if (productModel != null) {
                    if (productModel.getTotalBoxes() > productModel.getScannedBoxes()) {
                        isScanCompleted = false;
                    }

                    totScannedCount = totScannedCount + productModel.getScannedBoxes();
                    totCount = totCount + productModel.getTotalBoxes();
                    totBox = totBox + productModel.getTotalBoxes();

                    ProductModel partsListModel = new ProductModel();
                    partsListModel.setPartId(productModel.getPartId());
                    partsListModel.setBarcode(productModel.getBarcode());
                    partsListModel.setMasterBoxNo(productModel.getMasterBoxNo());
                    partsListModel.setDaNo(productModel.getDaNo());
                    partsListModel.setInvoiceNo(productModel.getInvoiceNo());
                    partsListModel.setPickingComplete(validatePicking() ? 1 : 0);
                    partsListModel.setTotalQty(productModel.getTotalQty());
                    partsListModel.setScannedBoxes(productModel.getScannedBoxes());
                    partsListModel.setPartNo(productModel.getPartNo());
                    partsListModel.setScanTime(productModel.getScanTime());
                    partsListModel.setTotalBoxes(productModel.getScannedBoxes());
                    partsListModel.setScanComplete(productModel.getTotalBoxes() == productModel.getScannedBoxes() ? 1 : 0);
                    partSumbitList.add(partsListModel);
                }
            }
            totUnscannedCount = totCount - totScannedCount;
            totalPartsTv.setText(Integer.toString(totParts));
            totalBoxesTv.setText(Integer.toString(totBox));
            partsScannedTv.setText(Integer.toString(totScannedCount));
            partsNotScannedTv.setText(Integer.toString(totUnscannedCount));
        }
    }

    private void goBack() {
        Intent intent = new Intent(context, LoadingScanningLatestScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("tc", tcStr);
        intent.putExtra(Constants.VEHICLE_NUMBER, vehNo);
        intent.putExtra(Constants.VENDOR_ID, vendorId);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return true;
        }
        return true;
    }

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (!isFinishing()) {
            if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
                errDlg = new AlertDialog.Builder(LoadingReconcileScreen.this).create();
                errDlg.setTitle(getString(R.string.alert_dialog_title));
                errDlg.setCancelable(false);
                errDlg.setMessage(Globals.lastErrMsg);

                Globals.lastErrMsg = "";
                errDlg.setButton(getString(R.string.ok_btn),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                errDlg.dismiss();
                            }
                        });

                if (submitDialog != null && submitDialog.isShowing())
                    submitDialog.dismiss();

                Utils.dismissProgressDialog();
                isNotErr = false;
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void goToHome() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    class Submit extends AsyncTask<Object, Object, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            submitDialog = Utils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(Object... params) {
            try {
                Gson gson = new Gson();

                outputModel = new LoadingSubmitModel(isFinalSubmit, subProjectId, dataType, partSumbitList, tcNumber, vehicleNo);
                String data = gson.toJson(outputModel);
                Utils.logD("Data:" + data);

                String url = WmsUtils.getLoadingSubmitDataUrlTag(vendorId, vehicleNo);

                if (Utils.isValidString(data)) {
                    LoginResponse response = (LoginResponse) HttpRequest
                            .postData(url, data, LoginResponse.class,
                                    context);
                    if (response != null) {
                        Utils.logD(response.toString());
                        if (!response.isStatus()) {
                            Globals.lastErrMsg = response.getMessage();
                        }
                    } else {
                        Globals.lastErrMsg = "Server Response is null";
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                Utils.logE(e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            Globals.isDataSubmitted = false;
            if (!isFinishing() && submitDialog != null && !submitDialog.isShowing())
                submitDialog.dismiss();
            if (showErrorDialog()) {
                if (Utils.isValidString(Globals.vehicleNo)) {
                    dataSource.loadingData.deleteLoadingData(vehicleNo);
                }

                Utils.showToast(context, "Successful");
                if (isFinalSubmit)
                    goToHome();
                else
                    goBack();
            }
            super.onPostExecute(result);
        }
    }

    class SendStackingImage extends AsyncTask<TruckImageModel, Object, Object> {
        GeneralResponse response;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            Utils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(TruckImageModel... params) {

            try {

                Globals.lastErrMsg = "";

                TruckImageModel model = params[0];

                String url = WmsUtils.getImageSubmitUrlTag();

                String data = gson.toJson(model);

                response = (GeneralResponse) HttpRequest
                        .postData(url, data, GeneralResponse.class, context);
                if (response != null) {
                    if (response.isStatus()) {
                        Utils.logD(response.toString());


                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Object result) {
            Utils.dismissProgressDialog();
            if (showErrorDialog()) {
                if (Globals.unloadingResponse != null && Utils.isValidArrayList((ArrayList<?>) Globals.unloadingResponse.getData().getPartList())) {
                    dataSource.unloadingParts.saveData(context, Globals.unloadingResponse, vehNo);
                }
                if (response != null && response.isStatus())
                    isImageSubmitted = true;
                new Submit().execute();
            }
            super.onPostExecute(result);
        }
    }

    private void takePicture(Context context) {
        Utils.logD("*");
        Intent intent = new Intent(context, CameraActivity.class);
        startActivityForResult(intent, TAKE_PHOTO);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        switch (requestCode) {
            case TAKE_PHOTO:
                try {
                    Utils.logD("**");

                    Uri uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
                    String imageUri = uri.toString();
                    bitmap = BitmapScalingUtil.bitmapFromUri(context,
                            Uri.parse(imageUri));
                    if (bitmap != null) {
                        int w = bitmap.getWidth();
                        int h = bitmap.getHeight();
                        Matrix mat = new Matrix();
                        if (w > h)
                            mat.postRotate(90);

                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                        Globals.bitmap = bitmap;

                        String diff = dataSource.sharedPreferences
                                .getValue(Constants.TIME_DIFF);
                        long d = 0;
                        if (Utils.isValidString(diff))
                            d = Long.valueOf(diff);

                        addImage(bitmap, Utils.getImageTime(d), false);
                    }
                } catch (Exception e) {

                }

                break;
        }

    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop) {
        Utils.logD("***");

        if (truckIv != null && bitmap != null) {
            Utils.logD("*****");

            ImageLoader imageLoader = new ImageLoader(context, Constants.TRUCK);
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                if (truckIv.getVisibility() == View.GONE || truckIv.getVisibility() == View.INVISIBLE)
                    truckIv.setVisibility(View.VISIBLE);
                Utils.enableView(sendBtn);
                truckImageFileName = imageFileName;
                truckIv.setImageBitmap(bitmap);
            }
        }
    }


}
