package com.apptmyz.wms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptmyz.wms.data.LoadUnload;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.Utils;

public class UnloadingScreen extends Activity {

    boolean isQc;
    private TextView titleTv;
    private Button okBtn;
    private EditText vehNoEt;
    private Context context;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_go:
                    String vehNo = vehNoEt.getText().toString();
                    if (isQc) {

                    } else {
                        if (Utils.isValidString(vehNo) && Utils.isValidVehNo(vehNo)) {
                            Globals.vehicleNo = vehNo;
                            Intent intent = new Intent(context, PartsScanningScreen.class);
                            intent.putExtra(Constants.CLASS_NAME, getClass().getName());
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else
                            Utils.showToast(context, getString(R.string.invalidVehNo));
                    }
                    break;

                case R.id.home_iv:
                    goBack();
                    break;

            }
        }

    };
    private ImageView home_iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unloading_screen);

        context = UnloadingScreen.this;

        titleTv = (TextView) findViewById(R.id.title_tv);
        titleTv.setText(getString(R.string.unloading_sheet));

        okBtn = (Button) findViewById(R.id.btn_go);
        okBtn.setOnClickListener(onClickListener);

        vehNoEt = (EditText) findViewById(R.id.et_vehicle_no);
        home_iv = (ImageView) findViewById(R.id.home_iv);
        home_iv.setOnClickListener(onClickListener);

        Intent intent = getIntent();
        isQc = intent.getBooleanExtra("QC", false);

        setData();
    }

    private void goBack() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void setData() {
        Globals.selectedSheet = new LoadUnload();


        Globals.selectedSheet.setSheetNo("WLABOHX0000037");
        Globals.selectedSheet.setCarrier("VEHICLE");
        Globals.selectedSheet.setTripNo("");
        Globals.selectedSheet.setTxnDate("21-Oct-2020 00:00:00");
        Globals.selectedSheet.setSheetType("QC");
        Globals.selectedSheet.setSheetDate("21-Oct-2020 00:00:00");
        Globals.selectedSheet.setBranchCode("BLRH");
        Globals.selectedSheet.setBranchType("HUB");
        Globals.selectedSheet.setPartiallyScanned(0);
        Globals.selectedSheet.setConCount(2);
        Globals.selectedSheet.setPcsCount(3);
        Globals.selectedSheet.setToBranchCode("");
        Globals.selectedSheet.setLtutFlag("LT");
        Globals.selectedSheet.setLockMacId(null);
        Globals.selectedSheet.setIotNo(null);
        Globals.selectedSheet.setIsValidIOT(null);
        Globals.selectedSheet.setLoadPercent(0);

        Utils.logD(Globals.selectedSheet.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPostResume() {
       vehNoEt.setText("");
        super.onPostResume();
    }
}
