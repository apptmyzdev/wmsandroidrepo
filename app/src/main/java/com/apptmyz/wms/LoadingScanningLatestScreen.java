package com.apptmyz.wms;

import androidx.annotation.IdRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.wms.custom.BitmapScalingUtil;
import com.apptmyz.wms.custom.CameraActivity;
import com.apptmyz.wms.custom.CounterView;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.InvoiceModel;
import com.apptmyz.wms.data.LoadingResponseLatest;
import com.apptmyz.wms.data.NewProductModel;
import com.apptmyz.wms.data.TruckImageModel;
import com.apptmyz.wms.data.WmsLoadingTcModel;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.fileutils.ImageLoader;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.ScanUtils;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.apptmyz.wms.wmsfragments.LoadingScanInProgress;
import com.apptmyz.wms.wmsfragments.LoadingToBeScanned;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class LoadingScanningLatestScreen extends BaseActivity {
    public final int TAKE_PHOTO = 1;
    public final int TAKE_STACKING_PHOTO = 2;
    public LoadingToBeScanned toBeScanned;
    public LoadingScanInProgress scanInProgress;
    private Context context;
    private DataSource dataSource;
    private List<NewProductModel> newProductModelList;
    private RadioGroup radioGroup;
    private FragmentManager fm;
    private Button vehicleNoBtn;
    private TextView weightTv, percentTv;
    private String vehNo, vendorId, tcNumber, stackingImgFileName;
    private String truckImageFileName;
    private ImageView home_iv;
    private RelativeLayout boxCountPopup;
    private EditText boxCountEt;
    private TextView productNoTv, scannedCountTv;
    private Button boxSubmitBtn, addBtn, subtractBtn, reconcileBtn;
    private RelativeLayout imageLayout;
    private Button captureBtn, sendBtn;
    private TextView msgTv;
    public Gson gson = new Gson();
    private ImageView truckIv;
    public final int CROP_IMAGE = 2;
    private CounterView counterView;

    private int maxCount = -1,
            minCount = -1;
    private String tcStr = null;
    TextView titleTv;
    private boolean isStackingImg = true;
    private RecyclerView stackingImagesRv;
    private ImagesAdapter stackingImagesAdapter;
    private ArrayList<Bitmap> stackingImgBitmaps = new ArrayList<>();
    private ArrayList<String> stackingImgFileNames;
    private ArrayList<Bitmap> imgBitmaps = new ArrayList<>();
    private ArrayList<String> imgFileNames;
    private int stackingImagePosition, imagePosition;
    private EditText scannedBoxEt;

    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private int isManual, subProjectId, routeId;
    private TimerTask timerScanTask;
    private ImageView proceedIv;
    private String barcodeNum, requiredBarcodeText;
    private RelativeLayout reasonLayout;
    private EditText extraReasonEt;
    private Button submitReasonBtn;
    private ImageView refreshIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_loading_scanning_latest_screen, frameLayout);

        context = LoadingScanningLatestScreen.this;

        refreshIv = (ImageView) findViewById(R.id.iv_refresh);

        dataSource = new DataSource(context);
        reasonLayout = (RelativeLayout) findViewById(R.id.layout_reason_extra);
        extraReasonEt = (EditText) findViewById(R.id.reason_extra_et);
        submitReasonBtn = (Button) findViewById(R.id.btn_submit_reason);
        submitReasonBtn.setOnClickListener(listener);

        fm = getFragmentManager();
        titleTv = (TextView) findViewById(R.id.tv_top_title);

        scannedBoxEt = (EditText) findViewById(R.id.scanned_pkg);
        scannedBoxEt.addTextChangedListener(scanWatcher);
        proceedIv = (ImageView) findViewById(R.id.iv_proceed);
        proceedIv.setOnClickListener(listener);

        toBeScannedLayout = (LinearLayout) findViewById(R.id.tobe_scanned_layout);
        scanInProgressLayout = (LinearLayout) findViewById(R.id.sca_inprogress_layout);
        scanCompletedLayout = (LinearLayout) findViewById(R.id.scan_completed_layout);
        exceptionLayout = (LinearLayout) findViewById(R.id.exception_layout);

        vehicleNoBtn = (Button) findViewById(R.id.btn_sheet);

        radioGroup = (RadioGroup) findViewById(R.id.rg_display_type);
        radioGroup.setOnCheckedChangeListener(changeListener);

        weightTv = (TextView) findViewById(R.id.tv_weight);
        percentTv = (TextView) findViewById(R.id.tv_percentage);

        boxCountPopup = (RelativeLayout) findViewById(R.id.box_count_popup);

        boxCountEt = (EditText) findViewById(R.id.et_box_count);
        productNoTv = (TextView) findViewById(R.id.tv_product_number);
        scannedCountTv = (TextView) findViewById(R.id.tv_scanned_count);

        addBtn = (Button) findViewById(R.id.add_btn);
        addBtn.setOnClickListener(listener);

        subtractBtn = (Button) findViewById(R.id.substract_btn);
        subtractBtn.setOnClickListener(listener);

        boxSubmitBtn = (Button) findViewById(R.id.btn_go);
        boxSubmitBtn.setOnClickListener(listener);

        reconcileBtn = (Button) findViewById(R.id.btn_reconcile);
        reconcileBtn.setOnClickListener(listener);

        home_iv = (ImageView) findViewById(R.id.home_iv);
        home_iv.setOnClickListener(listener);

        imageLayout = (RelativeLayout) findViewById(R.id.layout_image_popup);

        captureBtn = (Button) findViewById(R.id.btn_capture_image);
        captureBtn.setOnClickListener(listener);

        sendBtn = (Button) findViewById(R.id.btn_send_image);
        sendBtn.setOnClickListener(listener);

        truckIv = (ImageView) findViewById(R.id.iv_truck);
        truckIv.setOnClickListener(listener);

        stackingImagesRv = (RecyclerView) findViewById(R.id.rv_images);
        stackingImagesRv.setLayoutManager(new LinearLayoutManager(context));

        if (Utils.isValidArrayList(stackingImgBitmaps)) {
            stackingImagesAdapter = new ImagesAdapter(stackingImgBitmaps);
            stackingImagesRv.setAdapter(stackingImagesAdapter);
        }

        msgTv = (TextView) findViewById(R.id.tv_msg);
        msgTv.setText("Before Starting Loading, Take Picture of shipment stacked inside vehicle, Keeping back doors Open");

        counterView = (CounterView) findViewById(R.id.counterViewCount);

        toBeScanned = (LoadingToBeScanned) fm
                .findFragmentById(R.id.tobe_scanned_fragment);
        scanInProgress = (LoadingScanInProgress) fm
                .findFragmentById(R.id.scan_inprogress_fragment);


        Intent intent = getIntent();
        tcStr = intent.getStringExtra("tc");
        if (Utils.isValidString(tcStr)) {
            WmsLoadingTcModel tcModel = gson.fromJson(tcStr, WmsLoadingTcModel.class);
            if (tcModel != null) {
                tcNumber = tcModel.getTcNo();
                vehNo = tcModel.getVehNo();
                vehicleNoBtn.setText("Vehicle Number: " + tcModel.getVehNo());
                subProjectId = tcModel.getSubprojectId();
                routeId = tcModel.getRouteId();
                titleTv.setText("Loading - " + tcNumber);
            }
        }

        if (Utils.isValidString(tcNumber)) {
            Globals.tcNumber = tcNumber;
            if (dataSource.loadingData.getPartsData(tcNumber))
                resetAllFragmentData();
            else
                new LoadingTask().execute();
        }

        refreshIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LoadingTask().execute();
            }
        });
    }

    private TextWatcher scanWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = scannedBoxEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue + " len: " + scannedValue.length());
                                    if (isManual == 0 && scannedValue.length() > 0) {
                                        barcodeNum = scannedValue;
                                        updateBoxScan();//it's auto scan
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }
                            }
                        });
                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }
        }
    };

    private String selectedDataType;

    public void showBoxCountPopup(int scannedCount, String dataType, String productNo, int totPackets) {
        scannedCountTv.setText(Integer.toString(scannedCount));
        boxCountPopup.setVisibility(View.VISIBLE);
        productNoTv.setText(productNo);
        boxCountEt.setText("");
        selectedDataType = dataType;
        maxCount = totPackets;
        minCount = scannedCount;
        counterView.setmMaxLimit(totPackets);
        counterView.setmMinLimit(scannedCount);
        counterView.setValue(scannedCount);
    }

    class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.DataObjectHolder> {
        private ArrayList<Bitmap> bitmaps;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            ImageView imageView, deleteIv;
            Button viewBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.iv_image);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                viewBtn = (Button) itemView.findViewById(R.id.btn_view);
                viewBtn.setOnClickListener(this);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (v.getId()) {
                    case R.id.btn_view:
                        int pos = (int) v.getTag();
                        if (isStackingImg) {
                            showImagePreview(stackingImgBitmaps.get(pos), pos);
                        } else {
                            showImagePreview(imgBitmaps.get(pos), pos);
                        }
                        break;
                    case R.id.iv_delete:
                        if (isStackingImg) {
                            stackingImgBitmaps.remove((int) v.getTag());
                        } else {
                            imgBitmaps.remove((int) v.getTag());
                        }
                        notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        }

        public ImagesAdapter(ArrayList<Bitmap> myDataset) {
            bitmaps = myDataset;
        }

        @Override
        public ImagesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_image_item, parent, false);
            return new ImagesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ImagesAdapter.DataObjectHolder holder, final int position) {
            Bitmap bitmap = bitmaps.get(position);
            if (bitmap != null) {
                holder.imageView.setImageBitmap(bitmap);

                holder.imageView.setTag(position);
                holder.deleteIv.setTag(position);
                holder.viewBtn.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            int count = bitmaps.size();
            return bitmaps.size();
        }

        public void update(ArrayList<Bitmap> data) {
            bitmaps.clear();
            bitmaps.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(Bitmap dataObj, int index) {
            bitmaps.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            bitmaps.remove(index);
            notifyItemRemoved(index);
        }
    }

    public void addStackingImage(Bitmap bitmap, String imageFileName, boolean isCrop) {
        Utils.logD("***");

        if (truckIv != null && bitmap != null) {
            Utils.logD("*****");
            ImageLoader imageLoader = new ImageLoader(context, Constants.TRUCK);
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                stackingImgFileName = imageFileName;
                if (!Utils.isValidArrayList(stackingImgBitmaps)) {
                    stackingImgBitmaps = new ArrayList<>();
                    stackingImgFileNames = new ArrayList<>();
                    Utils.enableView(sendBtn);
                }
                if (stackingImagePosition == -1) {
                    stackingImgBitmaps.add(bitmap);
                    stackingImgFileNames.add(imageFileName);
                } else {
                    stackingImgBitmaps.set(stackingImagePosition, bitmap);
                    stackingImgFileNames.set(stackingImagePosition, imageFileName);
                }
                isStackingImg = true;
                stackingImagesAdapter = new ImagesAdapter(stackingImgBitmaps);
                stackingImagesRv.setAdapter(stackingImagesAdapter);
                stackingImagesAdapter.notifyDataSetChanged();
            }
        }
    }

    private void updateBoxScan() {
        if (Utils.isValidString(barcodeNum) && barcodeNum.length() >= Globals.loaidngBarcodeLength && barcodeNum.length() >= Globals.loaidngBarcodeMaxLength) {
            requiredBarcodeText = barcodeNum.substring(Globals.loaidngBarcodeMinLength - 1, Globals.loaidngBarcodeMaxLength);
            NewProductModel model = dataSource.loadingData.getLoadingData(requiredBarcodeText);
            if (model != null) {
                if(model.getScannedBoxes() < model.getTotalBoxes()) {
                    Globals.partNo = Utils.getNumber(model);
                    int invoiceScannedQty = 0;
                    if (Utils.isValidArrayList((ArrayList<?>) model.getInvoiceList())) {
                        InvoiceModel invoice = model.getInvoiceList().get(0);
                        if (invoice != null) {
                            if (invoice.getNoOfBoxes() <= invoice.getScannedBoxes() || model.getScannedBoxes() >= model.getTotalBoxes()) {
                                Utils.showSimpleAlert(context, getString(R.string.alert), "You Cannot Add More than the Total Boxes");
                                return;
                            }
                            invoiceScannedQty = invoice.getScannedBoxes() + 1;
                        }
                    } else {
                        if (model.getScannedCount() >= model.getTotalBoxes()) {
                            Utils.showSimpleAlert(context, getString(R.string.alert), "You Cannot Add More than the Total Boxes");
                            return;
                        }
                    }
                    dataSource.loadingData.updateScanCount(context, requiredBarcodeText, model.getScannedBoxes() + 1, invoiceScannedQty);
                } else {
                    Utils.showSimpleAlert(context, getString(R.string.alert), "You Cannot Add More than the Total Boxes");
                }
            }
            scannedBoxEt.setText(null);
            scannedBoxEt.clearFocus();
        }
    }

    IsExtraDialog isExtraDialog = null;

    public class IsExtraDialog extends Dialog {
        private TextView messageTv;
        private Button yesBtn, noBtn;
        private String msg;

        public IsExtraDialog(String message) {
            super(LoadingScanningLatestScreen.this, R.style.CustomAlertDialog);
            msg = message;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_alert_dialog_2);

            messageTv = (TextView) findViewById(R.id.tv_error_message);
            messageTv.setText(msg);

            yesBtn = (Button) findViewById(R.id.btn_go);
            noBtn = (Button) findViewById(R.id.btn_cancel);
            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isExtraDialog.dismiss();
                    reasonLayout.setVisibility(View.VISIBLE);
                }
            });
            noBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
        }
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_submit_reason:
                    String reasonExtra = extraReasonEt.getText().toString().trim();
                    if (Utils.isValidString(reasonExtra)) {
                        reasonLayout.setVisibility(View.GONE);
                    } else {
                        Utils.showSimpleAlert(context, "Alert", "Please Enter Valid Reason");
                    }
                    break;
                case R.id.iv_proceed:
                    barcodeNum = scannedBoxEt.getText().toString().trim();
                    updateBoxScan();
                    break;
                case R.id.substract_btn:
                    String count1 = boxCountEt.getText().toString();
                    if (Utils.isValidString(count1)) {

                        int i1 = Integer.parseInt(count1) - 1;
                        if (i1 >= minCount) {
                            boxCountEt.setText(Integer.toString(i1));
                        }
                    }
                    break;

                case R.id.add_btn:
                    String count2 = boxCountEt.getText().toString();
                    if (Utils.isValidString(count2)) {
                        int i2 = Integer.parseInt(count2) + 1;
                        if (maxCount > 0 && i2 <= maxCount) {
                            boxCountEt.setText(Integer.toString(i2));
                        }
                    }
                    break;

                case R.id.btn_go:

                    int count3 = counterView.getValue();

                    if (count3 != 0) {
                        if (count3 >= minCount && count3 <= maxCount) {
                            String partNo = productNoTv.getText().toString();
                            dataSource.loadingData.updatePartScan(context, Globals.tcNumber, (count3), selectedDataType, partNo);
                            boxCountPopup.setVisibility(View.GONE);
                            Utils.dissmissKeyboard(boxCountEt);
                        } else {
                            Utils.showToast(context, getString(R.string.alert_box));
                        }
                    } else {
                        Utils.showToast(context, getString(R.string.alert_box));
                    }
                    break;

                case R.id.btn_reconcile:
                    Intent intent = new Intent(context, LoadingReconcileScreen.class);
                    intent.putExtra("tc", tcStr);
                    intent.putExtra(Constants.VEHICLE_NUMBER, vehNo);
                    intent.putExtra(Constants.VENDOR_ID, vendorId);
                    intent.putExtra("subProjectId", subProjectId);
                    intent.putExtra("routeId", routeId);
                    intent.putExtra("dataType", Globals.loadingDataType);
                    startActivity(intent);
                    finish();

                    break;

                case R.id.btn_capture_image:
                    stackingImgFileName = "";
                    stackingImagePosition = -1;
                    if (Utils.isValidArrayList(stackingImgFileNames) && stackingImgFileNames.size() == 3) {
                        Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                    } else {
                        takePicture(context, TAKE_STACKING_PHOTO);
                    }
                    break;
                case R.id.btn_send_image:
                    if (Utils.isValidArrayList(stackingImgFileNames)) {
                        Utils.logE("in LT image");

                        String text = "";

                        String truck = Globals.vehicleNo;
                        if (Utils.isValidString(truck)) {
                            text = text + "\n" + "Veh#  : " + truck;
                        }

                        String s = dataSource.sharedPreferences
                                .getValue(Constants.IMEI);
                        if (Utils.isValidString(s))
                            text = text + "\n" + "T-ID# : " + s;

                        String user = dataSource.sharedPreferences
                                .getValue(Constants.USERNAME_PREF);
                        if (Utils.isValidString(user))
                            text = text + "\n" + "U-ID# : " + user;

                        text = text + "\n" + "I-DT# : "
                                + Utils.getITime(Long.valueOf(stackingImgFileName));

                        imageLayout.setVisibility(View.GONE);
                        TruckImageModel model = new TruckImageModel();

                        model.setVehTxnDate(Utils.getScanTimeLoading());
                        model.setLoadUnloadFlag(Constants.LOADING_STARTED);
                        model.setTcNo(tcNumber);
                        model.setVehicleNo(vehNo);
                        model.setSubprojectId(subProjectId);
                        model.setRouteId(routeId);
                        ImageLoader loader = new ImageLoader(context, Constants.TRUCK);
                        try {
                            String base64 = loader.fileCache.getBase64StringToName(
                                    stackingImgFileName, text, context);
                            if (Utils.isValidArrayList(stackingImgBitmaps)) {
                                List<String> list = getBase64s(stackingImgBitmaps);
                                model.setImages(list);
                                Utils.logD(model.toString());
                                new SendStackingImage().execute(model);
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utils.logE("in else ");
                        Utils.showSimpleAlert(
                                context,
                                getString(R.string.alert_dialog_title),
                                "Please capture stacking image");
                    }
                    break;
            }
        }
    };

    private List<String> getBase64s(List<Bitmap> bitmaps) {
        List<String> list = new ArrayList<>();
        for (Bitmap bitmap : bitmaps) {
            String base64 = getBase64(bitmap);
            list.add(base64);
        }
        return list;
    }

    private String getBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70,
                byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        encoded = encoded.replace("\n", "");
        return encoded;
    }

    private void takePicture(Context context, int requestCode) {
        Utils.logD("*");
        Intent intent = new Intent(context, CameraActivity.class);
        startActivityForResult(intent, requestCode);
    }

    public void resetAllFragmentData() {
        Utils.dismissProgressDialog();
        int id = radioGroup.getCheckedRadioButtonId();
        int tag = -1;

        switch (id) {
            case R.id.rb_tobescanned:
                tag = Constants.toBeScannedTag;
                break;

            case R.id.rb_recentscanned:
                tag = Constants.scanInProgressTag;
                break;

            case R.id.rb_scancompleted:
                tag = Constants.scanCompletedTag;
                break;

            case R.id.rb_exception:
                tag = Constants.exceptionTag;
                break;

            default:
                break;
        }

        dataSource.loadingData.getScanConsList(context, tag, false);
        dataSource.loadingData.getScannedQty(Globals.tcNumber);
    }

    private LinearLayout toBeScannedLayout, scanInProgressLayout,
            scanCompletedLayout, exceptionLayout, topFrags;

    private RadioGroup.OnCheckedChangeListener changeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
            resetAllFragmentData();
            switch (i) {
                case R.id.rb_tobescanned:
                    showView(toBeScannedLayout);
                    hideView(scanInProgressLayout);
                    hideView(scanCompletedLayout);
                    hideView(exceptionLayout);
                    break;

                case R.id.rb_recentscanned:
                    hideView(toBeScannedLayout);
                    showView(scanInProgressLayout);
                    hideView(scanCompletedLayout);
                    hideView(exceptionLayout);
                    break;

                case R.id.rb_scancompleted:
                    hideView(toBeScannedLayout);
                    hideView(scanInProgressLayout);
                    showView(scanCompletedLayout);
                    hideView(exceptionLayout);
                    break;

                case R.id.rb_exception:
                    hideView(toBeScannedLayout);
                    hideView(scanInProgressLayout);
                    hideView(scanCompletedLayout);
                    showView(exceptionLayout);
                    break;

                default:
                    break;
            }
        }
    };

    private void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    private void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    ProgressDialog unloadingDialog;
    AlertDialog errDlg;

    class LoadingTask extends AsyncTask<String, Object, Object> {
        LoadingResponseLatest loadingResponse;

        @Override
        protected void onPreExecute() {
            unloadingDialog = Utils.getProgressDialog(context);
            Globals.lastErrMsg = "";
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";
                dataSource.loadingData.deleteLoadingData(tcNumber);
                String url = WmsUtils.getLoadingDataLatestUrlTag();

                loadingResponse = (LoadingResponseLatest) HttpRequest
                        .postData(url, tcStr, LoadingResponseLatest.class, context);
                if (loadingResponse != null) {
                    Utils.logD(loadingResponse.toString());
                    if (loadingResponse.getStatus()) {
                        Globals.loadingResponse = loadingResponse;
                        if (loadingResponse.getData() == null)
                            Globals.lastErrMsg = Constants.NO_DATA_AVAILABLE;
                    } else {
                        Globals.lastErrMsg = loadingResponse.getMessage();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && unloadingDialog != null && unloadingDialog.isShowing())
                unloadingDialog.dismiss();
            if (showErrorDialog()) {
                if (loadingResponse.getData().isImgFlag()) {
                    imageLayout.setVisibility(View.VISIBLE);
                } else {
                    if (loadingResponse.getData() != null) {
                        Globals.loadingResponse = loadingResponse;
                        Globals.loadingDataType = loadingResponse.getData().getDataType();
                        dataSource.sharedPreferences.set(Constants.LOADING_DATATYPE, loadingResponse.getData().getDataType());
                        dataSource.loadingData.saveData(context, loadingResponse, tcNumber);
                    }
                }
            }
            super.onPostExecute(result);
        }
    }

    class SendStackingImage extends AsyncTask<TruckImageModel, Object, Object> {
        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            Utils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(TruckImageModel... params) {

            try {
                Globals.lastErrMsg = "";

                TruckImageModel model = params[0];
                String url = WmsUtils.getImageSubmitUrlTag();

                String data = gson.toJson(model);

                GeneralResponse response = (GeneralResponse) HttpRequest
                        .postData(url, data, GeneralResponse.class, context);
                if (response != null) {
                    if (response.isStatus()) {
                        Utils.logD(response.toString());
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Object result) {
            Utils.dismissProgressDialog();
            if (showErrorDialog()) {
                if (Globals.loadingResponse != null && Utils.isValidArrayList((ArrayList<?>) Globals.loadingResponse.getData().getPartList())) {
                    dataSource.loadingData.saveData(context, Globals.loadingResponse, Globals.tcNumber);
                }
            }
            super.onPostExecute(result);
        }
    }

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
        if (isLogout)
            dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

        try {
            if (!isFinishing()) {
                if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {

                    errDlg = new AlertDialog.Builder(context).create();
                    errDlg.setTitle(getString(R.string.alert_dialog_title));
                    errDlg.setCancelable(false);
                    errDlg.setMessage(Globals.lastErrMsg);
                    Globals.lastErrMsg = "";
                    errDlg.setButton(getString(R.string.ok_btn),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {


                                    errDlg.dismiss();
                                    finish();
                                }
                            });


                    Utils.dismissProgressDialog();
                    isNotErr = false;
                    errDlg.show();
                }
            }
        } catch (Exception e) {
            Utils.logE(e.toString());
        }
        return isNotErr;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        switch (requestCode) {
            case TAKE_STACKING_PHOTO:
                try {
                    Utils.logD("**");
                    Uri uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
                    String imageUri = uri.toString();
                    bitmap = BitmapScalingUtil.bitmapFromUri(context,
                            Uri.parse(imageUri));
                    if (bitmap != null) {
                        int w = bitmap.getWidth();
                        int h = bitmap.getHeight();
                        Matrix mat = new Matrix();
                        if (w > h)
                            mat.postRotate(90);

                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                        Globals.bitmap = bitmap;

                        String diff = dataSource.sharedPreferences
                                .getValue(Constants.TIME_DIFF);
                        long d = 0;
                        if (Utils.isValidString(diff))
                            d = Long.valueOf(diff);

                        addStackingImage(bitmap, Utils.getImageTime(d), false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void showImagePreview(Bitmap bitmap, int position) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Image Preview");
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_pod, null);
        ImageView docketIv;
        Button cancelBtn, reCaptureBtn;
        docketIv = (ImageView) view.findViewById(R.id.iv_pod);
        cancelBtn = (Button) view.findViewById(R.id.btn_crop);
        cancelBtn.setText("Cancel");
        reCaptureBtn = (Button) view.findViewById(R.id.btn_recapture);

        alertDialog.setView(view);
        byte[] decodedString = Base64.decode(getBase64(bitmap), Base64.NO_WRAP);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        decodedByte.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        docketIv.setImageBitmap(decodedByte);
        final AlertDialog alert = alertDialog.show();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        reCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                if (isStackingImg) {
                    stackingImagePosition = position;
                    takePicture(context, TAKE_STACKING_PHOTO);
                } else {
                    imagePosition = position;
                    takePicture(context, TAKE_PHOTO);
                }
            }
        });
    }

}
