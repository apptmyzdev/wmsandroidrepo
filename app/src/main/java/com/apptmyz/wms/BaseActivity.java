package com.apptmyz.wms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseActivity extends AppCompatActivity {
    protected FrameLayout frameLayout;
    protected ImageView homeIv;
    protected TextView titleTv, versionTv;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        context = BaseActivity.this;

        frameLayout = (FrameLayout) findViewById(R.id.content_frame);
        homeIv = (ImageView) findViewById(R.id.home_iv);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        versionTv = (TextView) findViewById(R.id.tv_version_name);
        String versionName = "";
        try {
            versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionTv.setText("Ver: " + versionName + " - " + BuildConfig.RELEASE_DATE);
        homeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
    }

    private void goBack() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
