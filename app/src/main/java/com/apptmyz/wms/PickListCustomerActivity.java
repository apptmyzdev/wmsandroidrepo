package com.apptmyz.wms;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.Bin;
import com.apptmyz.wms.data.CustomerResponse;
import com.apptmyz.wms.data.NodeMasterModel;
import com.apptmyz.wms.data.Response;
import com.apptmyz.wms.data.SubProjectMasterModel;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;


public class PickListCustomerActivity extends BaseActivity {

    TextView titleTv;
    RecyclerView customersRv;
    LinearLayoutManager layoutManager;
    Context context;
    ArrayList<NodeMasterModel> customers = new ArrayList<>();
    CustomerAdapter customerAdapter;
    int subprojectId;
    DataSource dataSource;
    String subprojectName;
    EditText searchView;
    ImageView refreshIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_pick_list_customer, frameLayout);

        context = PickListCustomerActivity.this;
        dataSource = new DataSource(context);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        searchView = (EditText) findViewById(R.id.searchView);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setFilteredData(editable.toString());
            }
        });

        if (getIntent().getExtras() != null) {
            subprojectId = getIntent().getIntExtra("subprojectId", 0);
            subprojectName = getIntent().getStringExtra("subProjectName");
        }

        titleTv.setText(subprojectName != null ? subprojectName : "Pick List");

        customersRv = (RecyclerView) findViewById(R.id.rv_customers);
        layoutManager = new LinearLayoutManager(context);
        customersRv.setLayoutManager(layoutManager);

        customers = dataSource.nodes.getNodesData(subprojectId);
        if (Utils.isValidArrayList(customers)) {
            customerAdapter = new CustomerAdapter(customers);
            customersRv.setAdapter(customerAdapter);
        } else {
            if (Utils.getConnectivityStatus(context))
                new CustomersTask().execute();
        }
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_refresh:
                    if (Utils.getConnectivityStatus(context)) {
                        searchView.setText(null);
                        searchView.clearFocus();
                        new CustomersTask().execute();
                    }
                    break;
            }
        }
    };

    private void setFilteredData(String query) {
        ArrayList<NodeMasterModel> tempList = new ArrayList<>();

        for (NodeMasterModel model : customers) {
            if (model.getNodeName().toLowerCase().contains(query.toLowerCase())) {
                tempList.add(model);
            }
        }
        customerAdapter = new CustomerAdapter(tempList);
        customersRv.setAdapter(customerAdapter);
        customerAdapter.notifyDataSetChanged();
    }

    ProgressDialog subprojectsDialog;

    class CustomersTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                subprojectsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPickListCustomersUrl(subprojectId);

                Utils.logD("Log 1");
                CustomerResponse response = (CustomerResponse) HttpRequest
                        .getInputStreamFromUrl(url, CustomerResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<NodeMasterModel> data = (ArrayList<NodeMasterModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            dataSource.nodes.insertNodeData(data, subprojectId);
                            customers = data;
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && subprojectsDialog != null && subprojectsDialog.isShowing()) {
                subprojectsDialog.dismiss();
            }
            if (showErrorDialog()) {
                setData();
            }
            super.onPostExecute(result);
        }

    }

    private void setData() {
        customerAdapter = new CustomerAdapter(customers);
        customersRv.setAdapter(customerAdapter);
        customerAdapter.notifyDataSetChanged();
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PickListCustomerActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.DataObjectHolder> {
        private ArrayList<NodeMasterModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView customerTv;
            RelativeLayout layout;

            public DataObjectHolder(View itemView) {
                super(itemView);

                layout = (RelativeLayout) itemView.findViewById(R.id.customer_layout);
                customerTv = (TextView) itemView.findViewById(R.id.tv_customer);
                layout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    intent = new Intent(PickListCustomerActivity.this, PickListActivity.class);
                    intent.putExtra("subprojectId", subprojectId);
                    intent.putExtra("nodeId", list.get(pos).getNodeId());
                    intent.putExtra("nodeName", list.get(pos).getNodeName());

                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            }
        }

        public CustomerAdapter(ArrayList<NodeMasterModel> myDataset) {
            list = myDataset;
        }

        @Override
        public CustomerAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_customer_item, parent, false);
            return new CustomerAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final CustomerAdapter.DataObjectHolder holder, final int position) {
            NodeMasterModel model = list.get(position);

            if (model != null) {
                holder.customerTv.setText(model.getNodeName());
            }

            holder.customerTv.setTag(position);
            holder.layout.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<NodeMasterModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(NodeMasterModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

}
