package com.apptmyz.wms;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.VehicleMasterModel;
import com.apptmyz.wms.data.VendorMasterModel;
import com.apptmyz.wms.data.WmsDocumentHandoverInvoiceModel;
import com.apptmyz.wms.data.WmsDocumentHandoverRequestModel;
import com.apptmyz.wms.data.WmsDocumentHandoverResponse;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class DocumentHandoverActivity extends BaseActivity {
    private Context context;
    private DataSource dataSource;
    private Button goBtn, submitBtn, supervisorSignatureBtn, driverSignatureBtn;
//    private TextInputEditText tcNumEt;
    private AutoCompleteTextView vehicleAutoTv, vendorAutoTv;
    private RecyclerView invoicesRv;
    private InvoicesAdapter invoicesAdapter;
    private int count;
    private SparseBooleanArray mChecked = new SparseBooleanArray();
    private CheckBox selectAllCb, manifestCb, ewayBillCb;
    private String supervisorSignBase64, tcNumber, driverSignBase64;
    private ArrayList<WmsDocumentHandoverInvoiceModel> invoicesList = new ArrayList<>();
    private RadioGroup loadByRg;
    private String type;
    private int vehicleId, vendorId, data = -1;
    private TextView  vendorLabelTv, vehicleLabelTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_document_handover, frameLayout);
        initializeViews();
    }

    private void initializeViews() {
        context = DocumentHandoverActivity.this;
        dataSource = new DataSource(context);
        titleTv.setText("Document Handover");

        selectAllCb = (CheckBox) findViewById(R.id.cb_select_all);
        selectAllCb.setOnClickListener(clickListener);
        manifestCb = (CheckBox) findViewById(R.id.cb_manifest);
        ewayBillCb = (CheckBox) findViewById(R.id.cb_ewaybill);
        invoicesRv = (RecyclerView) findViewById(R.id.rv_invoices);
//        tcNumEt = (TextInputEditText) findViewById(R.id.input_tc_number);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        invoicesRv.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(invoicesRv.getContext(),
                layoutManager.getOrientation());
        invoicesRv.addItemDecoration(dividerItemDecoration);
        invoicesAdapter = new InvoicesAdapter((ArrayList<WmsDocumentHandoverInvoiceModel>) invoicesList);
        goBtn = (Button) findViewById(R.id.btn_go);
        supervisorSignatureBtn = (Button) findViewById(R.id.btn_supervisor_signature);
        driverSignatureBtn = (Button) findViewById(R.id.btn_driver_signature);
        submitBtn = (Button) findViewById(R.id.btn_submit);
        goBtn.setOnClickListener(listener);
        submitBtn.setOnClickListener(listener);
        supervisorSignatureBtn.setOnClickListener(listener);
        driverSignatureBtn.setOnClickListener(listener);

        vendorAutoTv = (AutoCompleteTextView) findViewById(R.id.vendorAutoTv);
        vehicleAutoTv = (AutoCompleteTextView) findViewById(R.id.vehicleAutoTv);
        loadByRg = findViewById(R.id.radioGroup);
        vendorLabelTv = findViewById(R.id.tvSelectVendorLabel);
        vehicleLabelTv = findViewById(R.id.tvSelectVehicleLabel);

        loadByRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_vehicle) {
                    vehicleAutoTv.setVisibility(View.VISIBLE);
                    vendorAutoTv.setVisibility(View.GONE);
                    vehicleLabelTv.setVisibility(View.VISIBLE);
                    vendorLabelTv.setVisibility(View.GONE);
                } else if(radioGroup.getCheckedRadioButtonId() == R.id.rb_vendor) {
                    vehicleAutoTv.setVisibility(View.GONE);
                    vendorAutoTv.setVisibility(View.VISIBLE);
                    vehicleLabelTv.setVisibility(View.GONE);
                    vendorLabelTv.setVisibility(View.VISIBLE);
                }
            }
        });

        ArrayList<VendorMasterModel> vendors = (ArrayList<VendorMasterModel>) dataSource.vendorMaster.getAll();
        if (Utils.isValidArrayList(vendors)) {
            ArrayAdapter<VendorMasterModel> vendorAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, vendors);
            vendorAutoTv.setAdapter(vendorAdapter);
        }

        vendorAutoTv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!isFinishing() && hasFocus)
                    vendorAutoTv.showDropDown();
            }
        });

        vendorAutoTv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!isFinishing())
                    vendorAutoTv.showDropDown();
                return false;
            }
        });

        vendorAutoTv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                vendorId = ((VendorMasterModel) adapterView.getItemAtPosition(i)).getVendorId();
            }
        });

        ArrayList<VehicleMasterModel> vehicles = dataSource.vehicles.getVehicles();
        if (Utils.isValidArrayList(vehicles)) {
            ArrayAdapter<VehicleMasterModel> vehicleMasterModelArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, vehicles);
            vehicleAutoTv.setAdapter(vehicleMasterModelArrayAdapter);
        }

        vehicleAutoTv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    vehicleAutoTv.showDropDown();

            }
        });
        vehicleAutoTv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                vehicleAutoTv.showDropDown();
                return false;
            }
        });

        vehicleAutoTv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                VehicleMasterModel vmm = ((VehicleMasterModel) adapterView.getItemAtPosition(i));
                vehicleId = vmm.getVehicleId();
            }
        });
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int itemCount = count;
            for (int i = 0; i < itemCount; i++) {
                mChecked.put(i, selectAllCb.isChecked());
            }
            invoicesAdapter.notifyDataSetChanged();
        }
    };


    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_go:
                    validateAndGetData();
                    break;
                case R.id.btn_submit:
                    validateAndSubmit();
                    break;
                case R.id.btn_supervisor_signature:
                    showSupervisorSignature();
                    break;
                case R.id.btn_driver_signature:
                    showDriverSignature();
                    break;
                default:
                    break;
            }
        }
    };

    private void validateAndSubmit() {
        if (Utils.isValidArrayList(invoicesList)) {
            if (Utils.isValidString(supervisorSignBase64)) {
                if (Utils.isValidString(driverSignBase64)) {
                    if (isAllValuesChecked()) {
                        if (manifestCb.isChecked()) {
                            if (ewayBillCb.isChecked()) {
                                if (Utils.getConnectivityStatus(context)) {
                                    new SubmitHandoverTask(getSubmitData()).execute();
                                }
                            } else {
                                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select Ewaybill");
                            }
                        } else {
                            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select Manifest");
                        }
                    } else {
                        Utils.showSimpleAlert(context, getString(R.string.alert), getString(R.string.select_all_invoices_alert));
                    }
                } else {
                    Utils.showSimpleAlert(context, getString(R.string.alert), "Please Capture Driver Signature");
                }
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Capture Supervisor Signature");
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "No Data Selected For Submission");
        }
    }

    private WmsDocumentHandoverRequestModel getSubmitData() {
        WmsDocumentHandoverRequestModel input = new WmsDocumentHandoverRequestModel();
        input.setTcNo(tcNumber);
        input.setInvoiceList(invoicesList);
        input.setReceivedEwayBill(ewayBillCb.isChecked() ? 1 : 0);
        input.setReceivedManifest(manifestCb.isChecked() ? 1 : 0);
        List<String> signatures = new ArrayList<>();
        signatures.add(supervisorSignBase64);
        signatures.add(driverSignBase64);
        input.setSignature(signatures);
        return input;
    }

    ProgressDialog submitDialog;

    class SubmitHandoverTask extends AsyncTask<String, String, Object> {
        WmsDocumentHandoverRequestModel input;
        GeneralResponse response;

        SubmitHandoverTask(WmsDocumentHandoverRequestModel input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitHandoverUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        finish();
                    }
                    Utils.showToast(context, response.getMessage());
                }
            }
            super.onPostExecute(result);
        }
    }

    private void validateAndGetData() {
    if (loadByRg.getCheckedRadioButtonId() == R.id.rb_vehicle) {
            type = "V";
            data = vehicleId;
        } else {
            type = "VE";
            data = vendorId;
        }

        if(type.equalsIgnoreCase("V") && data == -1) {
            Utils.showSimpleAlert(context, getString(R.string.alert), getResources().getString(R.string.valid_vehicle_number));
            return;
        } else if(type.equalsIgnoreCase("VE") && data == -1) {
            Utils.showSimpleAlert(context, getString(R.string.alert), getResources().getString(R.string.valid_vendor_number));
            return;
        }
        if (Utils.getConnectivityStatus(context)) {
            new GetInvoicesTask(tcNumber).execute();
        }
    }

    ProgressDialog invoicesDialog;

    class GetInvoicesTask extends AsyncTask<String, String, Object> {
        private String tcNumber;

        GetInvoicesTask(String tcNumber) {
            this.tcNumber = tcNumber;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                invoicesDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getDocumentHandoverByVendorOrVehicleUrl(data, type);

                Utils.logD("Log 1");
                WmsDocumentHandoverResponse response = (WmsDocumentHandoverResponse) HttpRequest
                        .getInputStreamFromUrl(url, WmsDocumentHandoverResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        invoicesList = (ArrayList<WmsDocumentHandoverInvoiceModel>) response.getData();
                    } else {
                        invoicesList.clear();
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && invoicesDialog != null && invoicesDialog.isShowing()) {
                invoicesDialog.dismiss();
            }
            showErrorDialog();
            setData();
            super.onPostExecute(result);
        }
    }

    private void setData() {
        invoicesAdapter = new InvoicesAdapter((ArrayList<WmsDocumentHandoverInvoiceModel>) invoicesList);
        invoicesRv.setAdapter(invoicesAdapter);
        invoicesAdapter.notifyDataSetChanged();
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(DocumentHandoverActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void showSupervisorSignature() {
        Intent intent = new Intent(context, MyFingerPaint.class);
        startActivityForResult(intent, Constants.SUPERVISOR_SIGNATURE_CODE);
    }

    private void showDriverSignature() {
        Intent intent = new Intent(context, MyFingerPaint.class);
        startActivityForResult(intent, Constants.DRIVER_SIGNATURE_CODE);
    }

    class InvoicesAdapter extends RecyclerView.Adapter<InvoicesAdapter.DataObjectHolder> {
        private ArrayList<WmsDocumentHandoverInvoiceModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView partNumTv;
            CheckBox checkBox;

            public DataObjectHolder(View itemView) {
                super(itemView);

                partNumTv = (TextView) itemView.findViewById(R.id.tv_invoice_no);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
            }
        }

        public InvoicesAdapter(ArrayList<WmsDocumentHandoverInvoiceModel> myDataset) {
            list = myDataset;
        }

        @Override
        public InvoicesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_document_handover_item, parent, false);
            return new InvoicesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final InvoicesAdapter.DataObjectHolder holder, final int position) {
            WmsDocumentHandoverInvoiceModel model = list.get(position);

            if (model != null) {
                holder.partNumTv.setText(String.valueOf(model.getInvoiceNo()));
                holder.checkBox.setChecked(mChecked.get(position));
            }
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        mChecked.put(position, isChecked);
                        if (isAllValuesChecked()) {
                            selectAllCb.setChecked(true);
                        }
                    } else {
                        mChecked.delete(position);
                        selectAllCb.setChecked(false);
                    }
                    invoicesRv.post(new Runnable() {
                        @Override
                        public void run() {
                            invoicesAdapter.notifyDataSetChanged();
                        }
                    });
                }
            });

            holder.partNumTv.setTag(position);
            holder.checkBox.setTag(position);
        }

        @Override
        public int getItemCount() {
            count = list.size();
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<WmsDocumentHandoverInvoiceModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(WmsDocumentHandoverInvoiceModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    protected boolean isAllValuesChecked() {
        for (int i = 0; i < count; i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.SUPERVISOR_SIGNATURE_CODE:
                    String s = data.getStringExtra(Constants.SIGNATURE);
                    if (Utils.isValidString(s)) {
                        supervisorSignBase64 = s;
                        supervisorSignatureBtn.setBackgroundResource(R.drawable.btn_bg_green);
                    }
                    break;
                case Constants.DRIVER_SIGNATURE_CODE:
                    String str = data.getStringExtra(Constants.SIGNATURE);
                    if (Utils.isValidString(str)) {
                        driverSignBase64 = str;
                        driverSignatureBtn.setBackgroundResource(R.drawable.btn_bg_green);
                    }
                    break;
                default:
                    break;
            }
        }
    }

}
