package com.apptmyz.wms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.CustomerMasterModelNew;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.PartMasterModel;
import com.apptmyz.wms.data.PickListIdsModel;
import com.apptmyz.wms.data.PickListIdsResponse;
import com.apptmyz.wms.data.PutAwayData;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class PickByCustomerOrPartActivity extends BaseActivity {

    TextView customerLabelTv, partLabelTv;
    AutoCompleteTextView customerSp, partSp, pickListIdSp;
    Button goBtn, submitBtn;
    RadioGroup pickyByRg;
    private Context context;
    private DataSource dataSource;
    private String type, pickListId;
    private int customerId, partId, data;
    private List<PickListIdsModel> pickListIds ;
    private List<PickListIdsModel> selectedPickListIds = new ArrayList<>();
    private RecyclerView picklistRV;
    private SparseBooleanArray mChecked = new SparseBooleanArray();
    private CheckBox pickAllCb;
    private LinearLayoutManager completedLayoutManager;
    private PickListAdapter adapter;
    private ArrayList<PutAwayData> groupData = new ArrayList<>();
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_pick_by_customer_or_part, frameLayout);
        context = PickByCustomerOrPartActivity.this;
        dataSource = new DataSource(context);
        titleTv.setText("Pick List");
        pickyByRg = (RadioGroup) findViewById(R.id.radioGroup);
        customerLabelTv = (TextView) findViewById(R.id.tvSelectCustLabel);
        partLabelTv = (TextView) findViewById(R.id.tvSelectPartLabel);
        customerSp = (AutoCompleteTextView) findViewById(R.id.customerSp);
        partSp = (AutoCompleteTextView) findViewById(R.id.partSp);
        pickListIdSp = (AutoCompleteTextView) findViewById(R.id.pickListIdSp);
        goBtn = (Button) findViewById(R.id.btn_go);
        goBtn.setOnClickListener(listener);
        submitBtn = findViewById(R.id.btn_submit);
        submitBtn.setOnClickListener(listener);

        picklistRV =  findViewById(R.id.rv_picklist);
        pickAllCb = findViewById(R.id.cb_pick_all);
        pickAllCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemCount = count;
                for (int i = 0; i < itemCount; i++) {
                    if(adapter != null && pickListIds != null) {
                        PickListIdsModel model = pickListIds.get(i);
                        if(model.isPickallFlag()) {
                            mChecked.put(i, pickAllCb.isChecked());
                            if(pickAllCb.isChecked()) {
                                selectedPickListIds.add(model);
                            } else {
                                selectedPickListIds.remove(model);
                            }
                        }
                    }
                }
                if(adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
        new GetPickListIdsTask().execute();

        pickyByRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_customer) {
                    customerLabelTv.setVisibility(View.VISIBLE);
                    customerSp.setVisibility(View.VISIBLE);
                    partLabelTv.setVisibility(View.GONE);
                    partSp.setVisibility(View.GONE);
                } else {
                    customerLabelTv.setVisibility(View.GONE);
                    customerSp.setVisibility(View.GONE);
                    partLabelTv.setVisibility(View.VISIBLE);
                    partSp.setVisibility(View.VISIBLE);
                }
            }
        });
        customerSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    customerSp.showDropDown();

            }
        });

        customerSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                customerSp.showDropDown();
                return false;
            }
        });

        pickListIdSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    pickListIdSp.showDropDown();

            }
        });

        pickListIdSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                pickListIdSp.showDropDown();
                return false;
            }
        });

        partSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    partSp.showDropDown();

            }
        });

        partSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                partSp.showDropDown();
                return false;
            }
        });

        customerSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CustomerMasterModelNew item = (CustomerMasterModelNew) adapterView.getItemAtPosition(i);
                customerId = item.getId();
            }
        });

        pickListIdSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pickListId = (String) adapterView.getItemAtPosition(i);
            }
        });

        partSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PartMasterModel item = (PartMasterModel)adapterView.getItemAtPosition(i);
                partId = item.getPartId();
            }
        });

    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_go:
                    validateAndGoNext();
                    break;
                case R.id.btn_submit:
                    if(selectedPickListIds != null && selectedPickListIds.isEmpty()) {
                        Utils.showSimpleAlert(context, "", "Select atleast one pick list Id");
                        return;
                    }
                    new SubmitPickListIdsTask().execute();
                    break;
                default:
                    break;
            }
        }
    };

    class GetPickListIdsTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                progressDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getPickListIdsUrlV2().replace(" ", "%20");
                Utils.logD("Log 1");
                PickListIdsResponse response = (PickListIdsResponse) HttpRequest
                        .getInputStreamFromUrl(url, PickListIdsResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        pickListIds = (ArrayList<PickListIdsModel>) response.getData();
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (showErrorDialog()) {
                setPickListIdsData();
            }
            super.onPostExecute(result);
        }
    }


    private void validateAndGoNext() {
        if(Utils.isValidString(pickListId)) {
            type = "PL";
            Intent intent = new Intent(context, PickListActivity.class);
            intent.putExtra("type", type);
            intent.putExtra("data", pickListId);
            startActivity(intent);
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select a Pick List ID");
        }
    }

    ProgressDialog progressDialog;

    class SubmitPickListIdsTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                progressDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitPickListIds().replace(" ", "%20");
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(selectedPickListIds);
                GeneralResponse response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (showErrorDialog()) {
            }
            new GetPickListIdsTask().execute();
            mChecked.clear();
            super.onPostExecute(result);
        }
    }

    private void setPickListIdsData() {
        if (Utils.isValidArrayList((ArrayList<?>) pickListIds)) {
            adapter = new PickListAdapter((ArrayList<PickListIdsModel>) pickListIds);
            completedLayoutManager = new LinearLayoutManager(this);
            picklistRV.setLayoutManager(completedLayoutManager);
            picklistRV.setAdapter(adapter);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PickByCustomerOrPartActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    protected boolean isAllValuesChecked() {
        for (int i = 0; i < count; i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }

    protected boolean isAllPickListSelected() {
        for (int i = 0; i < count; i++) {
            if(adapter != null && pickListIds != null) {
                PickListIdsModel model = pickListIds.get(i);
                if(model.isPickallFlag()) {
                    if (!mChecked.get(i)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    class PickListAdapter extends RecyclerView.Adapter<PickListAdapter.DataObjectHolder> {
        private ArrayList<PickListIdsModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView pickListNumber;
            CheckBox checkBox;
            LinearLayout itemLayout;

            public DataObjectHolder(View itemView) {
                super(itemView);
                itemLayout = itemView.findViewById(R.id.ll_picklist_item);
                pickListNumber = (TextView) itemView.findViewById(R.id.tv_picklist_id);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
                itemLayout.setOnClickListener(null);
            }

            @Override
            public void onClick(View v) {
                validateAndGoNext();
            }
        }

        public PickListAdapter(ArrayList<PickListIdsModel> myDataset) {
            list = myDataset;
        }

        @Override
        public PickListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                                               int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_picklist_id_item, parent, false);
            return new PickListAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final PickListAdapter.DataObjectHolder holder, final int position) {
            PickListIdsModel model = list.get(position);
            holder.checkBox.setOnCheckedChangeListener(null);
            if (model != null) {
                holder.pickListNumber.setText(model.getPicklistNo());
                if(model.isPickallFlag()) {
                    holder.checkBox.setVisibility(View.VISIBLE);
                } else {
                    holder.checkBox.setVisibility(View.GONE);
                }
                holder.checkBox.setChecked(mChecked.get(position));
                holder.checkBox.setTag(position);
            }

            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pickListId = model.getPicklistNo();
                    validateAndGoNext();
                }
            });
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.getTag() != null) {
                        int pos = (int) buttonView.getTag();

                        if (isChecked) {
                            mChecked.put(pos, isChecked);
                            selectedPickListIds.add(list.get(pos));
                            if(isAllPickListSelected()) {
                                pickAllCb.setChecked(isChecked);
                            }
                        } else {
                            selectedPickListIds.remove(list.get(pos));
                            mChecked.delete(pos);
                            if(!isAllPickListSelected()) {
                                pickAllCb.setChecked(isChecked);
                            }
                        }
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            count = list.size();
            return list.size();
        }

        public void update(ArrayList<PickListIdsModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PickListIdsModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }
}
