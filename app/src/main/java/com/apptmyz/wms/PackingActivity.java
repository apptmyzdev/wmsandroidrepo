package com.apptmyz.wms;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.CustomerMasterModelNew;
import com.apptmyz.wms.data.PackingModel;
import com.apptmyz.wms.data.PackingOrderNoModel;
import com.apptmyz.wms.data.PackingOrderNoResponse;
import com.apptmyz.wms.data.PackingResponse;
import com.apptmyz.wms.data.SubProjectMasterModel;
import com.apptmyz.wms.data.SubprojectResponse;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class PackingActivity extends BaseActivity {
    private Spinner invoiceNumSp;
    private AutoCompleteTextView subProjectSp;
    private TextView shipAddressTv, noOfMasterBoxesTv;
    private Button goBtn, printLabelsBtn, addMasterCartonBtn;
    private ImageView refreshIv;
    private RecyclerView partsRv;
    private PartsAdapter adapter;
    private Context context;
    private DataSource dataSource;
    private List<SubProjectMasterModel> subprojects;
    private Gson gson = new Gson();
    private int subProjectId;
    private String orderNum, subProjectName, supplierName;
    private ArrayList<PackingModel> partsList;
    private ArrayList<PackingOrderNoModel> invoices = new ArrayList<>();
    private ArrayAdapter<PackingOrderNoModel> invoicesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = PackingActivity.this;
        getLayoutInflater().inflate(R.layout.activity_packing, frameLayout);
        noOfMasterBoxesTv = (TextView) findViewById(R.id.no_of_master_boxes);
        initializeViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    private void initializeViews() {
        titleTv.setText("Packing");
        dataSource = new DataSource(context);
        subProjectSp = (AutoCompleteTextView) findViewById(R.id.spSubproject);
        invoiceNumSp = (Spinner) findViewById(R.id.spInvoiceNumber);
        shipAddressTv = (TextView) findViewById(R.id.tvShipAddress);
        goBtn = (Button) findViewById(R.id.btn_go);
        addMasterCartonBtn = (Button) findViewById(R.id.btn_signature);
        printLabelsBtn = (Button) findViewById(R.id.btn_print_labels);

        goBtn.setOnClickListener(listener);
        addMasterCartonBtn.setOnClickListener(listener);
        printLabelsBtn.setOnClickListener(listener);

        partsRv = (RecyclerView) findViewById(R.id.rv_parts);
        partsRv.setLayoutManager(new LinearLayoutManager(context));
        adapter = new PartsAdapter((ArrayList<PackingModel>) partsList);
        partsRv.setAdapter(adapter);
        loadSubprojects();
        subProjectSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    subProjectSp.showDropDown();
            }
        });

        subProjectSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                subProjectSp.showDropDown();
                return false;
            }
        });

        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);
        subProjectSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SubProjectMasterModel item = (SubProjectMasterModel) adapterView.getItemAtPosition(i);
                subProjectId = item.getSubProjectId();
                subProjectName = item.getName();
                loadInvoices();
            }
        });

        invoiceNumSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                orderNum = invoices.get(i).getOrderNo();
                supplierName = invoices.get(i).getSupplierName();
                shipAddressTv.setText(invoices.get(i).getSupplierAddress());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_refresh:
                    refresh();
                    break;
                case R.id.btn_go:
                    validateAndGo();
                    break;
                case R.id.btn_print_labels:
                    validateAndPrintLabels();
                    break;
                case R.id.btn_signature:
                    validateAndAddMasterCarton();
                    break;
                default:
                    break;
            }
        }
    };

    private void refresh() {
        if (subProjectId != 0) {
            if (Utils.isValidString(orderNum)) {
                if (Utils.getConnectivityStatus(context)) {
                    new PackingDataTask().execute();
                }
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select Valid Invoice");
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select a Subproject");
        }
    }


    private void validateAndGo() {
        if (subProjectId != 0) {
            if (Utils.isValidString(orderNum)) {
                partsList = dataSource.packingParts.getPackingData(subProjectId, orderNum);
                if (Utils.isValidArrayList(partsList)) {
                    setData();
                } else {
                    if (Utils.getConnectivityStatus(context))
                        new PackingDataTask().execute();
                }
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select an Invoice");
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select a Subproject");
        }
    }

    private void validateAndPrintLabels() {

    }

    private void validateAndAddMasterCarton() {
        if (Utils.isValidArrayList(partsList)) {
            Intent intent = new Intent(context, PackingDetailActivity.class);
            intent.putExtra("subProjectId", subProjectId);
            intent.putExtra("subProjectName", subProjectName);
            intent.putExtra("supplierName", supplierName);
            intent.putExtra("orderNum", orderNum);
            intent.putExtra("shipAddress", shipAddressTv.getText().toString().trim());
            startActivity(intent);
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "There's no Data");
        }
    }

    private void loadSubprojects() {
        subprojects = dataSource.subprojects.getSubprojectsData();
        if (Utils.isValidArrayList((ArrayList<?>) subprojects)) {
            setSubprojectsData();
            subProjectId = subprojects.get(0).getSubProjectId();
            subProjectName = subprojects.get(0).getName();
            loadInvoices();
        } else {
            if (Utils.getConnectivityStatus(context))
                new SubprojectsTask().execute();
        }
    }

    private void loadInvoices() {
//        invoices = dataSource.invoices.getInvoicesData(subProjectId);
//        if (Utils.isValidArrayList(invoices)) {
//            setInvoicesData();
//        } else
        if (Utils.getConnectivityStatus(context)) {
            new InvoicesTask(subProjectId).execute();
        }
    }

    ProgressDialog packingDialog;

    class PackingDataTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                packingDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPackingDataUrl(subProjectId, orderNum);

                Utils.logD("Log 1");
                PackingResponse response = (PackingResponse) HttpRequest
                        .getInputStreamFromUrl(url, PackingResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        dataSource.packingParts.deletePackingData(subProjectId, orderNum);
                        ArrayList<PackingModel> data = (ArrayList<PackingModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            partsList = data;
                            dataSource.packingParts.insertPackingData(partsList, subProjectId, orderNum);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && packingDialog != null && packingDialog.isShowing()) {
                packingDialog.dismiss();
            }
            if (showErrorDialog()) {
                setData();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog subprojectsDialog;

    class SubprojectsTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                subprojectsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPickListSubprojectsUrl();

                Utils.logD("Log 1");
                SubprojectResponse response = (SubprojectResponse) HttpRequest
                        .getInputStreamFromUrl(url, SubprojectResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<SubProjectMasterModel> data = (ArrayList<SubProjectMasterModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            subprojects = data;
                            dataSource.subprojects.insertSubprojectData(subprojects);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && subprojectsDialog != null && subprojectsDialog.isShowing()) {
                subprojectsDialog.dismiss();
            }
            if (showErrorDialog()) {
                setSubprojectsData();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog invoicesDialog;

    class InvoicesTask extends AsyncTask<String, String, Object> {
        int subProjectId;

        InvoicesTask(int subProjectId) {
            this.subProjectId = subProjectId;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                invoicesDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getInvoicesUrl(subProjectId);

                Utils.logD("Log 1");
                PackingOrderNoResponse response = (PackingOrderNoResponse) HttpRequest
                        .getInputStreamFromUrl(url, PackingOrderNoResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<PackingOrderNoModel> data = (ArrayList<PackingOrderNoModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            invoices.clear();
                            invoices.addAll(data);
                            dataSource.invoices.clearAll();
                            dataSource.invoices.insertInvoicesData(invoices, subProjectId);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && invoicesDialog != null && invoicesDialog.isShowing()) {
                invoicesDialog.dismiss();
            }
            if (showErrorDialog()) {
                setInvoicesData();
            }
            super.onPostExecute(result);
        }
    }

    private void setSubprojectsData() {
        subprojects = dataSource.subprojects.getSubprojectsData();
        if (Utils.isValidArrayList((ArrayList<?>) subprojects)) {
            ArrayAdapter<SubProjectMasterModel> subProjectAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, subprojects);
            subProjectSp.setAdapter(subProjectAdapter);
        }
    }

    private void setInvoicesData() {
        invoices.clear();
        invoices.addAll(dataSource.invoices.getInvoicesData(subProjectId));
        if (Utils.isValidArrayList((ArrayList<?>) invoices)) {
            if(invoicesAdapter == null) {
                invoicesAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, invoices);
                invoiceNumSp.setAdapter(invoicesAdapter);
            }
            invoicesAdapter.notifyDataSetChanged();
        } else {
            if(invoicesAdapter != null) {
                invoicesAdapter.notifyDataSetChanged();
                shipAddressTv.setText("");
            }
        }
    }

    private void setData() {
        partsList = dataSource.packingParts.getPackingData(subProjectId, orderNum);
        adapter = new PartsAdapter((ArrayList<PackingModel>) partsList);
        partsRv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        noOfMasterBoxesTv.setText(String.valueOf(dataSource.packingParts.getNoOfMasterBoxes(subProjectId, orderNum)));
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PackingActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    class PartsAdapter extends RecyclerView.Adapter<PartsAdapter.DataObjectHolder> {
        private ArrayList<PackingModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView partNumTv, quantityTv, packedCartonsTv, remainingCartonsTv;

            public DataObjectHolder(View itemView) {
                super(itemView);

                partNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                quantityTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                packedCartonsTv = (TextView) itemView.findViewById(R.id.tv_packed_primary_cartons);
                remainingCartonsTv = (TextView) itemView.findViewById(R.id.tv_remaining_primary_cartons);
            }
        }

        public PartsAdapter(ArrayList<PackingModel> myDataset) {
            list = myDataset;
        }

        @Override
        public PartsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_packing_item, parent, false);
            return new PartsAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final PartsAdapter.DataObjectHolder holder, final int position) {
            PackingModel model = list.get(position);


            if (model != null) {
                holder.partNumTv.setText(String.valueOf(model.getPartNum()));
                holder.quantityTv.setText(String.valueOf(model.getQuantity()));
                holder.packedCartonsTv.setText(String.valueOf(model.getPrimarCartonsPacked()));
                holder.remainingCartonsTv.setText(String.valueOf(model.getRemainingPrimaryCartons()));

                if (model.getPickStatus() == 1) {
                    holder.itemView.setBackground(getResources().getDrawable(R.drawable.table_middle_green_bg));
                } else if (model.getPickStatus() == 0) {
                    holder.itemView.setBackground(getResources().getDrawable(R.drawable.table_middle_black));
                } else {
                    holder.itemView.setBackground(getResources().getDrawable(R.drawable.table_middle_orange_bg));
                }
            }

            holder.partNumTv.setTag(position);
            holder.quantityTv.setTag(position);
            holder.packedCartonsTv.setTag(position);
            holder.remainingCartonsTv.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<PackingModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PackingModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

}
