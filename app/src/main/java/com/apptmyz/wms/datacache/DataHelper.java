package com.apptmyz.wms.datacache;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.apptmyz.wms.util.Utils;

import java.util.ArrayList;

public class DataHelper extends SQLiteOpenHelper {
    public final static int DATABASE_VERSION = 2;
    public final static String DATABASE_NAME = "wms";

    private static DataHelper instance;

    public DataHelper(Context context) {
        super(context, DataHelper.DATABASE_NAME, null,
                DataHelper.DATABASE_VERSION);
    }

    public static synchronized DataHelper getHelper(Context context) {
        if (instance == null)
            instance = new DataHelper(context);

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DataHelper.SHARED_PREF_DATABASE_CREATE);
        database.execSQL(DataHelper.PUT_AWAY_DATABASE_CREATE_IF_NOT_EXISTS);
        database.execSQL(DataHelper.UNLOADING_PARTS_DATABASE_CREATE);
        Utils.logD(DataHelper.UNLOADING_PARTS_DATABASE_CREATE);
        database.execSQL(DataHelper.VENDOR_MASTER_DATABASE_CREATE);
        database.execSQL(DataHelper.LOADING_PARTS_DATABASE_CREATE);
        database.execSQL(DataHelper.LOADING_DATABASE_CREATE);
        database.execSQL(DataHelper.SUBPROJECT_DATABASE_CREATE);
        database.execSQL(DataHelper.NODE_DATABASE_CREATE);
        database.execSQL(DataHelper.PICKLIST_PARTS_CREATE_DATABASE);
        database.execSQL(DataHelper.EXCEPTION_CREATE_DATABASE);
        database.execSQL(DataHelper.INBOUND_INSPECTION_CREATE_DATABASE);
        database.execSQL(DataHelper.UNPACKING_CREATE_DATABASE);
        database.execSQL(DataHelper.UNPACKING_COMPLETED_CREATE_DATABASE);
        database.execSQL(DataHelper.PACKING_CREATE_DATABASE);
        database.execSQL(DataHelper.INVOICES_CREATE_DATABASE);
        database.execSQL(DataHelper.UNPACKING_BOX_CREATE_DATABASE);
        database.execSQL(DataHelper.BIN_CREATE_DATABASE);
        database.execSQL(DataHelper.PART_CREATE_DATABASE);
        database.execSQL(DataHelper.SKIP_BIN_REASON_DATABASE_CREATE);
        database.execSQL(DataHelper.EXCEPTION_TYPE_DATABASE_CREATE);
        database.execSQL(DataHelper.PICKING_EXCEPTION_REASONS_DATABASE_CREATE);
        database.execSQL(DataHelper.CUSTOMERS_CREATE_DATABASE);
        database.execSQL(DataHelper.VEHICLE_CREATE_DATABASE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * ************* SHARED_PREFERENCES **************
     **/
    public static final String SHARED_PREF_TABLE_NAME = "sharedpreferences";
    public final static String SHARED_PREF_KEY_ID = "SHARED_PREF_KEY_ID";
    public static final String SHARED_PREF_COLUMN_KEY = "SHARED_KEY";
    public static final String SHARED_PREF_COLUMN_VALUE = "SHARED_VALUE";

    public static final String SHARED_PREF_DATABASE_CREATE = "create table if not exists "
            + SHARED_PREF_TABLE_NAME + "(" + SHARED_PREF_KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + SHARED_PREF_COLUMN_KEY
            + " text not null, " + SHARED_PREF_COLUMN_VALUE
            + " text not null);";

    public static final String SHARED_PREF_DATABASE_CREATE_IF_NOT_EXISTS = "create table if not exists "
            + SHARED_PREF_TABLE_NAME + "(" + SHARED_PREF_KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + SHARED_PREF_COLUMN_KEY
            + " text not null, " + SHARED_PREF_COLUMN_VALUE
            + " text not null);";

    public static final String[] SHARED_PREF_COLUMNS = {
            SHARED_PREF_COLUMN_KEY, SHARED_PREF_COLUMN_VALUE};

    /**
     * *************** PUT AWAY DATA ************
     **/
    public static final String PUT_AWAY_TABLE_NAME = "putAwayData";

    public static final String PUT_AWAY_PART_KEY_ID = "PUT_AWAY_PART_KEY_ID";
    public static final String PUT_AWAY_ID = "PUT_AWAY_ID";
    public static final String PUT_AWAY_PART_ID = "PUT_AWAY_PART_ID";
    public static final String PUT_AWAY_PART_NUMBER = "PUT_AWAY_PART_NUMBER";
    public static final String PUT_AWAY_TOTAL_BOXES = "PUT_AWAY_TOTAL_BOXES";
    public static final String PUT_AWAY_TOTAL_QTY = "PUT_AWAY_TOTAL_QTY";
    public static final String PUT_AWAY_VEHICLE_NUMBER = "PUT_AWAY_VEHICLE_NUMBER";
    public static final String PUT_AWAY_INWARD_NUMBER = "PUT_AWAY_INWARD_NUMBER";
    public static final String PUT_AWAY_INVOICES = "PUT_AWAY_INVOICES";
    public static final String PUT_AWAY_STATUS = "PUT_AWAY_STATUS";
    public static final String PUT_AWAY_BIN_NUMBER = "PUT_AWAY_BIN_NUMBER";
    public static final String PUT_AWAY_TO_BE_SCANNED_BOXES = "PUT_AWAY_TO_BE_SCANNED_BOXES";
    public static final String PUT_AWAY_DATA_TYPE = "PUT_AWAY_DATA_TYPE";
    public static final String PUT_AWAY_DA_NUM = "PUT_AWAY_DA_NUM";
    public static final String PUT_AWAY_CUSTOMER_CODE = "PUT_AWAY_CUSTOMER_CODE";
    public static final String PUT_AWAY_SUBPROJECT = "PUT_AWAY_SUBPROJECT";
    public static final String PUT_AWAY_BATCH_NO = "PUT_AWAY_BATCH_NO";
    public static final String PUT_AWAY_DATABASE_CREATE_IF_NOT_EXISTS = "create table if not exists "
            + PUT_AWAY_TABLE_NAME + "(" + PUT_AWAY_PART_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PUT_AWAY_ID + " INTEGER, "
            + PUT_AWAY_PART_ID + " INTEGER, "
            + PUT_AWAY_PART_NUMBER + " TEXT, "
            + PUT_AWAY_DATA_TYPE + " TEXT, "
            + PUT_AWAY_DA_NUM + " TEXT, "
            + PUT_AWAY_TOTAL_BOXES + " INTEGER, " + PUT_AWAY_TOTAL_QTY + " INTEGER, "
            + PUT_AWAY_VEHICLE_NUMBER + " TEXT, "
            + PUT_AWAY_INWARD_NUMBER + " TEXT, "
            + PUT_AWAY_INVOICES + " TEXT, "
            + PUT_AWAY_STATUS + " INTEGER, "
            + PUT_AWAY_CUSTOMER_CODE + " TEXT, "
            + PUT_AWAY_SUBPROJECT + " TEXT, "
            + PUT_AWAY_BATCH_NO+" TEXT,"
            + PUT_AWAY_BIN_NUMBER + " TEXT, " + PUT_AWAY_TO_BE_SCANNED_BOXES + " INTEGER);";

    /********************** Unloading Table ***************/

    public static final String UNLOADING_PARTS_TABLE_NAME = "UNLOADING_PARTS_TABLE_NAME";
    public static final String UNLOADING_PARTS_COLUMN_ID = "UNLOADING_PARTS_COLUMN_ID";
    public static final String UNLOADING_PARTS_VEHICLE_NO = "UNLOADING_PARTS_VEHICLE_NO";
    public static final String UNLOADING_PARTS_COLUMN_PART_NO = "UNLOADING_PARTS_COLUMN_PART_NO";
    public static final String UNLOADING_PARTS_COLUMN_DA_NO = "DA_NO";
    public static final String UNLOADING_PARTS_COLUMN_DATATYPE = "DATA_TYPE";
    public static final String UNLOADING_PARTS_COLUMN_PART_ID = "UNLOADING_PARTS_COLUMN_PART_ID";
    public static final String UNLOADING_PARTS_COLUMN_TOTAL_BOXES = "UNLOADING_PARTS_COLUMN_TOTAL_BOXES";
    public static final String UNLOADING_PARTS_COLUMN_TOTAL_QTY = "UNLOADING_PARTS_COLUMN_TOTAL_QTY";
    public static final String UNLOADING_PARTS_COLUMN_INVOICE_NO = "UNLOADING_PARTS_COLUMN_INVOICE_NO";
    public static final String UNLOADING_PARTS_COLUMN_BOX_COUNT = "UNLOADING_PARTS_COLUMN_BOX_COUNT";
    public static final String UNLOADING_PARTS_COLUMN_QTY_INVOICE = "UNLOADING_PARTS_COLUMN_QTY_INVOICE";
    public static final String UNLOADING_PARTS_COLUMN_QTY_SCANNED = "UNLOADING_PARTS_COLUMN_QTY_SCANNED";
    public static final String UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT = "UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT";
    public static final String UNLOADING_PARTS_COLUMN_STATUS = "UNLOADING_PARTS_COLUMN_STATUS";
    public static final String UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED = "UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED";
    public static final String UNLOADING_PARTS_COLUMN_SCAN_TIME = "UNLOADING_PARTS_COLUMN_SCAN_TIME";
    public static final String UNLOADING_PARTS_COLUMN_IS_EXCEPTION = "UNLOADING_PARTS_COLUMN_IS_EXCEPTION";
    public static final String UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE = "UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE";
    public static final String UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION = "UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION";
    public static final String UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES = "UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES";
    public static final String UNLOADING_PARTS_INVOICE_BARCODE = "UNLOADING_PARTS_INVOICE_BARCODE";
    public static final String UNLOADING_PARTS_INVOICE_SCANNED_COUNT = "UNLOADING_PARTS_INVOICE_SCANNED_COUNT";
    public static final String UNLOADING_PARTS_MASTER_BOX_NUM = "UNLOADING_PARTS_MASTER_BOX_NUM";

    public static final String UNLOADING_PARTS_INVOICES_LIST = "UNLOADING_PARTS_INVOICES_LIST";
    public static final String UNLOADING_PARTS_BARCODE_LENGTH = "UNLOADING_PARTS_BARCODE_LENGTH";
    public static final String UNLOADING_PARTS_BARCODE_MIN_LENGTH = "UNLOADING_PARTS_BARCODE_MIN_LENGTH";
    public static final String UNLOADING_PARTS_BARCODE_MAX_LENGTH = "UNLOADING_PARTS_BARCODE_MAX_LENGTH";
    public static final String UNLOADING_PARTS_INWARD_NO = "UNLOADING_PARTS_INWARD_NO";

    public static final String UNLOADING_PARTS_DATABASE_CREATE = "create table "
            + UNLOADING_PARTS_TABLE_NAME + "(" + UNLOADING_PARTS_COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + UNLOADING_PARTS_VEHICLE_NO + " TEXT, "
            + UNLOADING_PARTS_COLUMN_DA_NO + " TEXT, "
            + UNLOADING_PARTS_COLUMN_DATATYPE + " TEXT, "
            + UNLOADING_PARTS_COLUMN_INVOICE_NO + " TEXT, "
            + UNLOADING_PARTS_COLUMN_PART_NO + " TEXT, "
            + UNLOADING_PARTS_COLUMN_PART_ID + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_TOTAL_BOXES + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_TOTAL_QTY + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_BOX_COUNT + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_QTY_INVOICE + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_QTY_SCANNED + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_STATUS + " TEXT, "
            + UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED + " TEXT, "
            + UNLOADING_PARTS_COLUMN_SCAN_TIME + " TEXT, "
            + UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " TEXT, "
            + UNLOADING_PARTS_INWARD_NO + " TEXT, "
            + UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE + " TEXT, "
            + UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION + " TEXT, "
            + UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES + " TEXT, "
            + UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT + " INTEGER, "
            + UNLOADING_PARTS_INVOICE_BARCODE + " TEXT, "
            + UNLOADING_PARTS_MASTER_BOX_NUM + " TEXT, "
            + UNLOADING_PARTS_INVOICES_LIST + " TEXT, "
            + UNLOADING_PARTS_BARCODE_LENGTH + " INTEGER, "
            + UNLOADING_PARTS_BARCODE_MIN_LENGTH + " INTEGER, "
            + UNLOADING_PARTS_BARCODE_MAX_LENGTH + " INTEGER, "
            + UNLOADING_PARTS_INVOICE_SCANNED_COUNT + " INTEGER)";

    /********************Vendor Master ***********************/


    public static final String VENDOR_MASTER_TABLE_NAME = "VENDOR_MASTER_TABLE_NAME";
    public static final String VENDOR_MASTER_COLUMN_ID = "VENDOR_MASTER_COLUMN_ID";
    public static final String VENDOR_MASTER_COLUMN_VENDOR_ID = "VENDOR_MASTER_COLUMN_VENDOR_ID";
    public static final String VENDOR_MASTER_COLUMN_VENDOR_CODE = "VENDOR_MASTER_COLUMN_CODE";
    public static final String VENDOR_MASTER_COLUMN_VENDOR_NAME = "VENDOR_MASTER_COLUMN_NAME";
    public static final String VENDOR_MASTER_COLUMN_VENDOR_TYPE = "VENDOR_MASTER_COLUMN_TYPE";
    public static final String VENDOR_MASTER_COLUMN_ACTIVE_FLAG = "VENDOR_MASTER_COLUMN_ACTIVE_FLAG";


    public static final String VENDOR_MASTER_DATABASE_CREATE = "create table "
            + VENDOR_MASTER_TABLE_NAME + "(" + VENDOR_MASTER_COLUMN_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + VENDOR_MASTER_COLUMN_VENDOR_ID + " INTEGER, "
            + VENDOR_MASTER_COLUMN_VENDOR_CODE + " TEXT, "
            + VENDOR_MASTER_COLUMN_VENDOR_NAME + " TEXT, "
            + VENDOR_MASTER_COLUMN_VENDOR_TYPE + " TEXT, "
            + VENDOR_MASTER_COLUMN_ACTIVE_FLAG + " TEXT) ";

    /*
     * Loading Latest Table
     * */

    public static final String LOADING_LATEST_TABLE_NAME = "loading";

    public static final String LOADING_PART_NUMBER = "LOADING_PART_NUMBER";
    public static final String LOADING_KEY_ID = "LOADING_KEY_ID";
    public static final String LOADING_PART_ID = "LOADING_PART_ID";
    public static final String LOADING_TOTAL_BOXES = "LOADING_TOTAL_BOXES";
    public static final String LOADING_SCANNED_BOXES = "LOADING_SCANNED_BOXES";
    public static final String LOADING_SCANNED_TIME = "LOADING_SCANNED_TIME";
    public static final String LOADING_IS_PICKING_COMPLETE = "LOADING_IS_PICKING_COMPLETE";
    public static final String LOADING_TC_NUMBER = "LOADING_TC_NUMBER";
    public static final String LOADING_STATUS = "LOADING_STATUS";
    public static final String LOADING_DA_NUMBER = "LOADING_DA_NUMBER";
    public static final String LOADING_INVOICE_NUMBER = "LOADING_INVOICE_NUMBER";
    public static final String LOADING_MASTER_BOX_NUMBER = "LOADING_MASTER_BOX_NUMBER";
    public static final String LOADING_BARCODE = "LOADING_BARCODE";
    public static final String LOADING_DATATYPE = "LOADING_DATATYPE";
    public static final String LOADING_INVOICES_LIST = "LOADING_INVOICES_LIST";
    public static final String LOADING_BARCODE_LENGTH = "LOADING_BARCODE_LENGTH";
    public static final String LOADING_BARCODE_MIN_LENGTH = "LOADING_BARCODE_MIN_LENGTH";
    public static final String LOADING_BARCODE_MAX_LENGTH = "LOADING_BARCODE_MAX_LENGTH";
    public static final String LOADING_INVOICE_SCANNED_COUNT = "LOADING_INVOICE_SCANNED_COUNT";

    public static final String LOADING_DATABASE_CREATE = "CREATE TABLE " + LOADING_LATEST_TABLE_NAME + " ("
            + LOADING_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + LOADING_PART_ID + " INTEGER, "
            + LOADING_DA_NUMBER + " TEXT, " + LOADING_DATATYPE + " TEXT, " + LOADING_INVOICE_NUMBER + " TEXT, "
            + LOADING_MASTER_BOX_NUMBER + " TEXT, " + LOADING_BARCODE + " TEXT, "
            + LOADING_INVOICES_LIST + " TEXT, "
            + LOADING_TC_NUMBER + " TEXT, " + LOADING_STATUS + " INTEGER, "
            + LOADING_PART_NUMBER + " TEXT, " + LOADING_TOTAL_BOXES + " INTEGER, " + LOADING_SCANNED_BOXES + " INTEGER, "
            + LOADING_SCANNED_TIME + " TEXT, "
            + LOADING_IS_PICKING_COMPLETE + " INTEGER,"
            + LOADING_BARCODE_LENGTH + " INTEGER, " + LOADING_BARCODE_MIN_LENGTH + " INTEGER, "
            + LOADING_BARCODE_MAX_LENGTH + " INTEGER"
            + ");";

    /***************************** Loading sheet***************************/

    public static final String LOADING_PARTS_TABLE_NAME = "LOADING_PARTS_TABLE_NAME";
    public static final String LOADING_PARTS_COLUMN_ID = "LOADING_PARTS_COLUMN_ID";
    public static final String LOADING_PARTS_VEHICLE_NO = "LOADING_PARTS_VEHICLE_NO";
    public static final String LOADING_PARTS_COLUMN_PART_NO = "LOADING_PARTS_COLUMN_PART_NO";
    public static final String LOADING_PARTS_COLUMN_PART_ID = "LOADING_PARTS_COLUMN_PART_ID";
    public static final String LOADING_PARTS_COLUMN_TOTAL_BOXES = "LOADING_PARTS_COLUMN_TOTAL_BOXES";
    public static final String LOADING_PARTS_COLUMN_TOTAL_QTY = "LOADING_PARTS_COLUMN_TOTAL_QTY";
    public static final String LOADING_PARTS_COLUMN_BIN_NO = "LOADING_PARTS_COLUMN_BIN_NO";
    public static final String LOADING_PARTS_COLUMN_BIN_ID = "LOADING_PARTS_COLUMN_BIN_ID";
    public static final String LOADING_PARTS_COLUMN_SCAN_TIME = "LOADING_PARTS_COLUMN_SCAN_TIME"; // not using these columns for now
    public static final String LOADING_PARTS_COLUMN_XD_FLAG = "LOADING_PARTS_COLUMN_XD_FLAG";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_NEXT_DEST = "LOADING_PARTS_COLUMN_NEXT_DEST";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_EXCEP_TYPE = "LOADING_PARTS_COLUMN_EXCEP_TYPE";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_EXCEP_DESC = "LOADING_PARTS_COLUMN_EXCEP_DESC";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_EXCEP_IMAGE = "LOADING_PARTS_COLUMN_EXCEP_IMAGE";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_QTY_SCANNED = "LOADING_PARTS_COLUMN_QTY_SCANNED";
    public static final String LOADING_PARTS_COLUMN_STATUS = "LOADING_PARTS_COLUMN_STATUS";

    public static final String LOADING_PARTS_DATABASE_CREATE = "create table "
            + LOADING_PARTS_TABLE_NAME + "(" + LOADING_PARTS_COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + LOADING_PARTS_VEHICLE_NO + " TEXT, "
            + LOADING_PARTS_COLUMN_PART_NO + " TEXT, "
            + LOADING_PARTS_COLUMN_PART_ID + " INTEGER, "
            + LOADING_PARTS_COLUMN_TOTAL_BOXES + " INTEGER, "
            + LOADING_PARTS_COLUMN_TOTAL_QTY + " INTEGER, "
            + LOADING_PARTS_COLUMN_BIN_NO + " TEXT, "
            + LOADING_PARTS_COLUMN_BIN_ID + " INTEGER, "
            + LOADING_PARTS_COLUMN_SCAN_TIME + " TEXT, "
            + LOADING_PARTS_COLUMN_XD_FLAG + " INTEGER, "
            + LOADING_PARTS_COLUMN_NEXT_DEST + " TEXT, "
            + LOADING_PARTS_COLUMN_EXCEP_TYPE + " TEXT, "
            + LOADING_PARTS_COLUMN_EXCEP_DESC + " TEXT, "
            + LOADING_PARTS_COLUMN_EXCEP_IMAGE + " TEXT, "
            + LOADING_PARTS_COLUMN_QTY_SCANNED + " INTEGER, "
            + LOADING_PARTS_COLUMN_STATUS + " TEXT) ";


    /*
     * Subprojects Table
     * */

    public static final String SUBPROJECTS_TABLE_NAME = "subprojects";

    public static final String SUBPROJECT_KEY_ID = "SUBPROJECT_KEY_ID";
    public static final String SUBPROJECT_ID = "SUBPROJECT_ID";
    public static final String SUBPROJECT_NAME = "SUBPROJECT_NAME";
    public static final String SUBPROJECT_PROJECT_ID = "SUBPROJECT_PROJECT_ID";
    public static final String SUBPROJECT_PROJECT_NAME = "SUBPROJECT_PROJECT_NAME";
    public static final String SUBPROJECT_PROJECT_TYPE_ID = "SUBPROJECT_PROJECT_TYPE_ID";
    public static final String SUBPROJECT_PROJECT_TYPE = "SUBPROJECT_PROJECT_TYPE";
    public static final String SUBPROJECT_LOCATION = "SUBPROJECT_LOCATION";
    public static final String SUBPROJECT_REVENUE_TYPE = "SUBPROJECT_REVENUE_TYPE";
    public static final String SUBPROJECT_REVENUE_DETAILS = "SUBPROJECT_REVENUE_DETAILS";
    public static final String SUBPROJECT_STATE_CODE = "SUBPROJECT_STATE_CODE";
    public static final String SUBPROJECT_STATE = "SUBPROJECT_STATE";
    public static final String SUBPROJECT_CITY = "SUBPROJECT_CITY";
    public static final String SUBPROJECT_USER_ID = "SUBPROJECT_USER_ID";
    public static final String SUBPROJECT_CREATEDBY = "SUBPROJECT_CREATEDBY";
    public static final String SUBPROJECT_CREATED_TIMESTAMP = "SUBPROJECT_CREATED_TIMESTAMP";
    public static final String SUBPROJECT_UPDATEDBY = "SUBPROJECT_UPDATEDBY";
    public static final String SUBPROJECT_UPDATED_TIMESTAMP = "SUBPROJECT_UPDATED_TIMESTAMP";
    public static final String SUBPROJECT_BRANCH = "SUBPROJECT_BRANCH";
    public static final String SUBPROJECT_BRANCH_NAME = "SUBPROJECT_BRANCH_NAME";
    public static final String SUBPROJECT_SAAC_CODE = "SUBPROJECT_SAAC_CODE";
    public static final String SUBPROJECT_DESCRIPTION = "SUBPROJECT_DESCRIPTION";
    public static final String SUBPROJECT_DISCOUNT_PERCENT = "SUBPROJECT_DISCOUNT_PERCENT";
    public static final String SUBPROJECT_REVENUE_ID = "SUBPROJECT_REVENUE_ID";
    public static final String SUBPROJECT_CUSTOMER_NAME = "SUBPROJECT_CUSTOMER_NAME";
    public static final String SUBPROJECT_REVENUE_TYPE_NAME = "SUBPROJECT_REVENUE_TYPE_NAME";
    public static final String SUBPROJECT_CUST_BILL_ADDR = "SUBPROJECT_CUST_BILL_ADDR";
    public static final String SUBPROJECT_TAX_DETAILS = "SUBPROJECT_TAX_DETAILS";
    public static final String SUBPROJECT_COLLECTION_MODEL = "SUBPROJECT_COLLECTION_MODEL";
    public static final String SUBPROJECT_FUEL_CHARGE_DETAILS = "SUBPROJECT_FUEL_CHARGE_DETAILS";
    public static final String SUBPROJECT_PLANT_ID = "SUBPROJECT_PLANT_ID";
    public static final String SUBPROJECT_CUST_ADDR = "SUBPROJECT_CUST_ADDR";
    public static final String SUBPROJECT_ROUTE_LIST = "SUBPROJECT_ROUTE_LIST";

    public static final String SUBPROJECT_DATABASE_CREATE = "CREATE TABLE "
            + SUBPROJECTS_TABLE_NAME + "(" + SUBPROJECT_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SUBPROJECT_ID + " INTEGER, " + SUBPROJECT_NAME + " TEXT, " + SUBPROJECT_PROJECT_ID + " INTEGER, "
            + SUBPROJECT_PROJECT_NAME + " TEXT, " + SUBPROJECT_PROJECT_TYPE_ID + " INTEGER, " + SUBPROJECT_PROJECT_TYPE + " TEXT, "
            + SUBPROJECT_LOCATION + " TEXT, " + SUBPROJECT_REVENUE_TYPE + " TEXT, " + SUBPROJECT_REVENUE_DETAILS + " TEXT, "
            + SUBPROJECT_STATE_CODE + " TEXT, " + SUBPROJECT_STATE + " TEXT, " + SUBPROJECT_CITY + " TEXT, "
            + SUBPROJECT_USER_ID + " INTEGER, " + SUBPROJECT_CREATEDBY + " TEXT, " + SUBPROJECT_CREATED_TIMESTAMP + " TEXT,"
            + SUBPROJECT_UPDATEDBY + " TEXT, " + SUBPROJECT_UPDATED_TIMESTAMP + " TEXT, " + SUBPROJECT_BRANCH + " INTEGER, "
            + SUBPROJECT_BRANCH_NAME + " TEXT, " + SUBPROJECT_SAAC_CODE + " TEXT, " + SUBPROJECT_DESCRIPTION + " TEXT, "
            + SUBPROJECT_DISCOUNT_PERCENT + " REAL, " + SUBPROJECT_REVENUE_ID + " INTEGER, " + SUBPROJECT_CUSTOMER_NAME + " TEXT, "
            + SUBPROJECT_REVENUE_TYPE_NAME + " TEXT, " + SUBPROJECT_CUST_BILL_ADDR + " TEXT, " + SUBPROJECT_TAX_DETAILS + " TEXT, "
            + SUBPROJECT_COLLECTION_MODEL + " TEXT, " + SUBPROJECT_FUEL_CHARGE_DETAILS + " TEXT, " + SUBPROJECT_PLANT_ID + " INTEGER, "
            + SUBPROJECT_CUST_ADDR + " TEXT, " + SUBPROJECT_ROUTE_LIST + " TEXT);";

    /*
     *  NODE TABLE
     * */

    public static final String NODE_TABLE_NAME = "nodes";

    public static final String NODE_SUBPROJECT_ID = "NODE_SUBPROJECT_ID";
    public static final String NODE_KEY_ID = "NODE_KEY_ID";
    public static final String NODE_ID = "NODE_ID";
    public static final String DOCKET_NODE_ID = "DOCKET_NODE_ID";
    public static final String NODE_TYPE_ID = "NODE_TYPE_ID";
    public static final String NODE_TYPE = "NODE_TYPE";
    public static final String NODE_NAME = "NODE_NAME";
    public static final String NODE_CUST_CODE = "NODE_CUST_CODE";
    public static final String NODE_LAT = "NODE_LAT";
    public static final String NODE_LNG = "NODE_LNG";
    public static final String NODE_ADDRESS1 = "NODE_ADDRESS1";
    public static final String NODE_ADDRESS2 = "NODE_ADDRESS2";
    public static final String NODE_ADDRESS3 = "NODE_ADDRESS3";
    public static final String NODE_STATE = "NODE_STATE";
    public static final String NODE_STATE_NAME = "NODE_STATE_NAME";
    public static final String NODE_CITY = "NODE_CITY";
    public static final String NODE_PINCODE = "NODE_PINCODE";
    public static final String NODE_MOBILE = "NODE_MOBILE";
    public static final String NODE_EMAIL = "NODE_EMAIL";
    public static final String NODE_CREATED_BY = "NODE_CREATED_BY";
    public static final String NODE_CREATED_TIMESTAMP = "NODE_CREATED_TIMESTAMP";
    public static final String NODE_UPDATED_BY = "NODE_UPDATED_BY";
    public static final String NODE_UPDATED_TIMESTAMP = "NODE_UPDATED_TIMESTAMP";
    public static final String NODE_SUBPROJECT_MODELS = "NODE_SUBPROJECT_MODELS";
    public static final String NODE_SEQUENCE = "NODE_SEQUENCE";
    public static final String NODE_DLY_STATUS = "NODE_DLY_STATUS";
    public static final String NODE_IS_CAPTURED = "NODE_IS_CAPTURED";

    public static final String NODE_DATABASE_CREATE = "CREATE TABLE " + NODE_TABLE_NAME
            + "(" + NODE_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NODE_SUBPROJECT_ID + " INTEGER, " + NODE_ID + " INTEGER, "
            + DOCKET_NODE_ID + " INTEGER, " + NODE_TYPE_ID + " INTEGER, " + NODE_TYPE + " TEXT, "
            + NODE_NAME + " TEXT, " + NODE_CUST_CODE + " TEXT, " + NODE_LAT + " REAL, " + NODE_LNG + " REAL, "
            + NODE_ADDRESS1 + " TEXT, " + NODE_ADDRESS2 + " TEXT, " + NODE_ADDRESS3 + " TEXT, " + NODE_STATE + " TEXT, "
            + NODE_STATE_NAME + " TEXT, " + NODE_CITY + " TEXT, " + NODE_PINCODE + " INTEGER, " + NODE_MOBILE + " TEXT, "
            + NODE_EMAIL + " TEXT, " + NODE_CREATED_BY + " TEXT, " + NODE_CREATED_TIMESTAMP + " TEXT, "
            + NODE_UPDATED_BY + " TEXT, " + NODE_UPDATED_TIMESTAMP + " TEXT, " + NODE_SUBPROJECT_MODELS + " TEXT, "
            + NODE_SEQUENCE + " INTEGER, " + NODE_DLY_STATUS + " INTEGER, " + NODE_IS_CAPTURED + " TEXT);";

    /*
     * BINS table */
    public static final String BIN_TABLE_NAME = "bins";

    public static final String BIN_KEY_ID = "BIN_KEY_ID";
    public static final String BIN_ID = "BIN_ID";
    public static final String BIN_NUMBER = "BIN_NUMBER";
    public static final String BIN_AISLE = "BIN_AISLE";

    public static final String BIN_CREATE_DATABASE = "CREATE TABLE " + BIN_TABLE_NAME + "("
            + BIN_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + BIN_ID + " INTEGER, "
            + BIN_NUMBER + " TEXT, " + BIN_AISLE + " TEXT);";

    /*
     * PARTS table */
    public static final String PART_TABLE_NAME = "parts";

    public static final String PART_KEY_ID = "PART_KEY_ID";
    public static final String PART_ID = "PART_ID";
    public static final String PART_NUMBER = "PART_NUMBER";

    public static final String PART_CREATE_DATABASE = "CREATE TABLE " + PART_TABLE_NAME + "("
            + PART_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PART_ID + " INTEGER, "
            + PART_NUMBER + " TEXT);";

    /*
     * SKIP BIN REASONS TABLE*/

    public static final String SKIP_BIN_REASONS_TABLE_NAME = "skipBinReasons";
    public static final String SKIP_BIN_REASON_KEY_ID = "SKIP_BIN_REASON_KEY_ID";
    public static final String SKIP_BIN_REASON_ID = "SKIP_BIN_REASON_ID";
    public static final String SKIP_BIN_REASON_NAME = "SKIP_BIN_REASON_NAME";

    public static final String SKIP_BIN_REASON_DATABASE_CREATE = "CREATE TABLE " + SKIP_BIN_REASONS_TABLE_NAME + "("
            + SKIP_BIN_REASON_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + SKIP_BIN_REASON_ID + " INTEGER, "
            + SKIP_BIN_REASON_NAME + " TEXT);";

    /*
     * * CUSTOMERS TABLE
     * */
    public static final String CUSTOMERS_TABLE_NAME = "customers";

    public static final String CUSTOMERS_KEY_ID = "CUSTOMERS_KEY_ID";
    public static final String CUSTOMERS_ID = "CUSTOMERS_ID";
    public static final String CUSTOMERS_VALUE = "CUSTOMERS_VALUE";
    public static final String CUSTOMERS_TYPE = "CUSTOMERS_TYPE";

    public static final String CUSTOMERS_CREATE_DATABASE = "CREATE TABLE " + CUSTOMERS_TABLE_NAME + "("
            + CUSTOMERS_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CUSTOMERS_ID + " INTEGER, " + CUSTOMERS_TYPE + " TEXT, "
            + CUSTOMERS_VALUE + " TEXT);";

    /**
     * VEHICLE TABLE
     */
    public static final String VEHICLES_TABLE_NAME = "vehicles";

    public static final String VEHICLE_KEY_ID = "VEHICLE_KEY_ID";
    public static final String VEHICLE_ID = "VEHICLE_ID";
    public static final String VEHICLE_VENDOR_ID = "VEHICLE_VENDOR_ID";
    public static final String VEHICLE_VENDOR_NAME = "VEHICLE_VENDOR_NAME";
    public static final String VEHICLE_NUMBER = "VEHICLE_NUMBER";
    public static final String VEHICLE_TYPE_ID = "VEHICLE_TYPE_ID";
    public static final String VEHICLE_TYPE = "VEHICLE_TYPE";
    public static final String VEHICLE_SUBPROJECT_ID = "VEHICLE_SUBPROJECT_ID";
    public static final String VEHICLE_SUBPROJECT = "VEHICLE_SUBPROJECT";
    public static final String VEHICLE_ROUTE_ID = "VEHICLE_ROUTE_ID";
    public static final String VEHICLE_ROUTE = "VEHICLE_ROUTE";
    public static final String VEHICLE_USERID = "VEHICLE_USERID";
    public static final String VEHICLE_CREATED_BY = "VEHICLE_CREATED_BY";
    public static final String VEHICLE_CREATED_TIMESTAMP = "VEHICLE_CREATED_TIMESTAMP";
    public static final String VEHICLE_UPDATED_BY = "VEHICLE_UPDATED_BY";
    public static final String VEHICLE_UPDATED_TIMESTAMP = "VEHICLE_UPDATED_TIMESTAMP";
    public static final String VEHICLE_ACTIVE_FLAG = "VEHICLE_ACTIVE_FLAG";

    public static final String VEHICLE_CREATE_DATABASE = "CREATE TABLE " + VEHICLES_TABLE_NAME + "("
            + VEHICLE_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + VEHICLE_ID + " INTEGER, "
            + VEHICLE_VENDOR_ID + " INTEGER, " + VEHICLE_VENDOR_NAME + " TEXT, " + VEHICLE_NUMBER + " TEXT, "
            + VEHICLE_TYPE_ID + " INTEGER, " + VEHICLE_TYPE + " TEXT, " + VEHICLE_SUBPROJECT_ID + " INTEGER, "
            + VEHICLE_SUBPROJECT + " TEXT, " + VEHICLE_ROUTE_ID + " INTEGER, " + VEHICLE_ROUTE + " TEXT, "
            + VEHICLE_USERID + " INTGER, " + VEHICLE_CREATED_BY + " TEXT, " + VEHICLE_CREATED_TIMESTAMP + " TEXT, "
            + VEHICLE_UPDATED_BY + " TEXT, " + VEHICLE_UPDATED_TIMESTAMP + " TEXT, " + VEHICLE_ACTIVE_FLAG + " INTEGER);";
    /*
     * EXCEPTION TYPES TABLE*/

    public static final String EXCEPTION_TYPES_TABLE_NAME = "exceptionTypes";
    public static final String EXCEPTION_TYPE_KEY_ID = "EXCEPTION_TYPE_KEY_ID";
    public static final String EXCEPTION_TYPE_ID = "EXCEPTION_TYPE_ID";
    public static final String EXCEPTION_TYPE_NAME = "EXCEPTION_TYPE_NAME";

    public static final String EXCEPTION_TYPE_DATABASE_CREATE = "CREATE TABLE " + EXCEPTION_TYPES_TABLE_NAME + "("
            + EXCEPTION_TYPE_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + EXCEPTION_TYPE_ID + " INTEGER, "
            + EXCEPTION_TYPE_NAME + " TEXT);";

    /*
     * PICKING EXCEPTION REASONS TABLE*/

    public static final String PICKING_EXCEPTION_REASONS_TABLE_NAME = "pickingExcepReasons";
    public static final String PICKING_EXCEPTION_REASON_KEY_ID = "PICKING_EXCEPTION_REASON_KEY_ID";
    public static final String PICKING_EXCEPTION_REASON_ID = "PICKING_EXCEPTION_REASON_ID";
    public static final String PICKING_EXCEPTION_REASON_NAME = "PICKING_EXCEPTION_REASON_NAME";

    public static final String PICKING_EXCEPTION_REASONS_DATABASE_CREATE = "CREATE TABLE " + PICKING_EXCEPTION_REASONS_TABLE_NAME + "("
            + PICKING_EXCEPTION_REASON_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PICKING_EXCEPTION_REASON_ID + " INTEGER, "
            + PICKING_EXCEPTION_REASON_NAME + " TEXT);";

    /*
     * PICK LIST PARTS TABLE
     * */

    public static final String PICKLIST_PARTS_TABLE_NAME = "picklistparts";

    public static final String PICKLIST_PART_KEY_ID = "PICKLIST_PART_KEY_ID";
    public static final String PICKLIST_UNIQUE_ID = "PICKLIST_UNIQUE_ID";
    public static final String PICKLIST_PART_SUBPROJECT_ID = "PICKLIST_PART_SUBPROJECT_ID";
    public static final String PICKLIST_PART_CUSTOMER_ID = "PICKLIST_PART_CUSTOMER_ID";
    public static final String PICKLIST_PART_NODE_ID = "PICKLIST_PART_NODE_ID";
    public static final String PICKLIST_PART_NO = "PICKLIST_PART_NO";
    public static final String PICKLIST_PART_ID = "PICKLIST_PART_ID";
    public static final String PICKLIST_TOTAL_BOXES = "PICKLIST_TOTAL_BOXES";
    public static final String PICKLIST_TOTAL_QTY = "PICKLIST_TOTAL_QTY";
    public static final String PICKLIST_BIN_NO = "PICKLIST_BIN_NO";
    public static final String PICKLIST_SCAN_TIME = "PICKLIST_SCAN_TIME";
    public static final String PICKLIST_XDFLAG = "PICKLIST_XDFLAG";
    public static final String PICKLIST_NEXT_DEST = "PICKLIST_NEXT_DEST";
    public static final String PICKLIST_EXCEP_TYPE = "PICKLIST_EXCEP_TYPE";
    public static final String PICKLIST_EXCEPT_DESC = "PICKLIST_EXCEPT_DESC";
    public static final String PICKLIST_EXCEP_IMAGE = "PICKLIST_EXCEP_IMAGE";
    public static final String PICKLIST_INVOICE_LIST = "PICKLIST_INVOICE_LIST";
    public static final String PICKLIST_IS_PICKED = "PICKLIST_IS_PICKED";
    public static final String PICKLIST_ORDER_NO = "PICKLIST_ORDER_NO";
    public static final String PICKLIST_BIN_ID = "PICKLIST_BIN_ID";
    public static final String PICKLIST_TYPE = "PICKLIST_TYPE";
    public static final String PICKLIST_ID = "PICKLIST_ID";
    public static final String PICKLIST_DATA_ID = "PICKLIST_DATA_ID";

    public static final String PICKLIST_DATATYPE = "PICKLIST_DATATYPE";
    public static final String PICKLIST_DA_NUMBER = "PICKLIST_DA_NUMBER";
    public static final String PICKLIST_SUPPLIER = "PICKLIST_SUPPLIER";
    public static final String PICKLIST_CUST_PART_NUM = "PICKLIST_CUST_PART_NUM";
    public static final String PICKLIST_CUST_CODE = "PICKLIST_CUST_CODE";

    public static final String PICKLIST_BARCODE_LENGTH = "PICKLIST_BARCODE_LENGTH";
    public static final String PICKLIST_BARCODE_MIN_LENGTH = "PICKLIST_BARCODE_MIN_LENGTH";
    public static final String PICKLIST_BARCODE_MAX_LENGTH = "PICKLIST_BARCODE_MAX_LENGTH";
    public static final String PICKLIST_BATCH_NO="PICKLIST_BATCH_NO";
    public static final String PICKLIST_PARTS_CREATE_DATABASE = "CREATE TABLE " + PICKLIST_PARTS_TABLE_NAME + "("
            + PICKLIST_PART_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PICKLIST_UNIQUE_ID + " INTEGER, "
            + PICKLIST_PART_SUBPROJECT_ID + " INTEGER, " + PICKLIST_PART_NODE_ID + " INTEGER, "
            + PICKLIST_PART_NO + " TEXT, "
            + PICKLIST_PART_ID + " INTEGER, " + PICKLIST_TOTAL_BOXES + " INTEGER, " + PICKLIST_BIN_NO + " TEXT, "
            + PICKLIST_TOTAL_QTY + " INTEGER, " + PICKLIST_SCAN_TIME + " TEXT, " + PICKLIST_XDFLAG + " INTEGER, "
            + PICKLIST_NEXT_DEST + " TEXT, " + PICKLIST_EXCEP_TYPE + " INTEGER, " + PICKLIST_EXCEPT_DESC + " TEXT, "
            + PICKLIST_EXCEP_IMAGE + " TEXT, " + PICKLIST_INVOICE_LIST + " TEXT, " + PICKLIST_IS_PICKED + " INTEGER, "
            + PICKLIST_ORDER_NO + " TEXT, " + PICKLIST_BIN_ID + " INTEGER, " + PICKLIST_PART_CUSTOMER_ID + " INTEGER, "
            + PICKLIST_TYPE + " TEXT, "
            + PICKLIST_DATA_ID + " TEXT, "
            + PICKLIST_DATATYPE + " TEXT, " + PICKLIST_DA_NUMBER + " TEXT, " + PICKLIST_SUPPLIER + " TEXT, "
            + PICKLIST_CUST_PART_NUM + " TEXT, " + PICKLIST_CUST_CODE + " TEXT, "
            + PICKLIST_BARCODE_LENGTH + " INTEGER, "
            + PICKLIST_BARCODE_MIN_LENGTH + " INTEGER, "
            + PICKLIST_BARCODE_MAX_LENGTH + " INTEGER, "
            + PICKLIST_BATCH_NO+" TEXT,"
            + PICKLIST_ID + " INTEGER);";

    /********* EXCEPTIONS DATA **************/

    public static final String EXCEPTION_TABLE_NAME = "exceptions";

    public static final String EXCEPTION_KEY_ID = "EXCEPTION_KEY_ID";
    public static final String EXCEPTION_REASON = "EXCEPTION_REASON";
    public static final String EXCEPTION_TRUCK_IMAGE = "EXCEPTION_TRUCK_IMAGE";
    public static final String EXCEPTION_VEHICLE_NUM = "EXCEPTION_VEHICLE_NUM";
    public static final String EXCEPTION_PART_NUM = "EXCEPTION_PART_NUM";
    public static final String EXCEPTION_COUNT = "EXCEPTION_COUNT";
    public static final String EXCEPTION_SCAN_TIME = "EXCEPTION_SCAN_TIME";
    public static final String EXCEPTION_DATATYPE = "EXCEPTION_DATATYPE";

    public static final String EXCEPTION_CREATE_DATABASE = "CREATE TABLE " + EXCEPTION_TABLE_NAME + "("
            + EXCEPTION_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EXCEPTION_PART_NUM + " TEXT, " + EXCEPTION_VEHICLE_NUM + " TEXT, "
            + EXCEPTION_TYPE_ID + " INTEGER, " + EXCEPTION_COUNT + " INTEGER, " + EXCEPTION_REASON + " TEXT, "
            + EXCEPTION_TRUCK_IMAGE + " TEXT, "
            + EXCEPTION_DATATYPE + " TEXT, "
            + EXCEPTION_SCAN_TIME + " TEXT)";

    /***********  INBOUND INSPECTION DATA TABLE  *************/

    public static final String INBOUND_DATA_TABLE_NAME = "inboundInspection";

    public static final String INBOUND_KEY_ID = "INBOUND_KEY_ID";
    public static final String INBOUND_PART_ID = "INBOUND_PART_ID";
    public static final String INBOUND_PART_NUM = "INBOUND_PART_NUM";
    public static final String INBOUND_NO_OF_MASTER_CARTONS = "INBOUND_NO_OF_MASTER_CARTONS";
    public static final String INBOUND_NO_OF_EXCEPTIONS = "INBOUND_NO_OF_EXCEPTIONS";
    public static final String INBOUND_STATUS = "INBOUND_STATUS";
    public static final String INBOUND_VEHICLE_NUMBER = "INBOUND_VEHICLE_NUMBER";
    public static final String INBOUND_INWARD_NUMBER = "INBOUND_INWARD_NUMBER";
    public static final String INBOUND_QUANTITY = "INBOUND_QUANTITY";
    public static final String INBOUND_DATATYPE = "INBOUND_DATATYPE";
    public static final String INBOUND_DA_NO = "INBOUND_DA_NO";
    public static final String INBOUND_INVOICE_NO = "INBOUND_INVOICE_NO";
    public static final String INBOUND_MASTER_BOX_NO = "INBOUND_MASTER_BOX_NO";
    public static final String INBOUND_NO_INVOICE = "INBOUND_NO_INVOICE";
    public static final String INBOUND_NO_MASTER_BOX = "INBOUND_NO_MASTER_BOX";

    public static final String INBOUND_INSPECTION_CREATE_DATABASE = "CREATE TABLE " + INBOUND_DATA_TABLE_NAME + "("
            + INBOUND_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + INBOUND_PART_ID + " INTEGER, "
            + INBOUND_PART_NUM + " TEXT, " + INBOUND_NO_OF_MASTER_CARTONS + " TEXT, " + INBOUND_NO_OF_EXCEPTIONS + " INTEGER, "
            + INBOUND_STATUS + " INTEGER, " + INBOUND_VEHICLE_NUMBER + " TEXT, "
            + INBOUND_DATATYPE + " TEXT, "
            + INBOUND_DA_NO + " TEXT, " + INBOUND_INVOICE_NO + " TEXT, " + INBOUND_MASTER_BOX_NO + " TEXT, "
            + INBOUND_NO_INVOICE + " INTEGER, " + INBOUND_NO_MASTER_BOX + " INTEGER, "
            + INBOUND_INWARD_NUMBER + " TEXT, " + INBOUND_QUANTITY + " INTEGER);";

    /*********** UNPACKING Box TABLE  *************/
    public static final String UNPACKING_BOX_TABLE_NAME = "unpackingBoxes";

    public static final String UNPACKING_BOX_KEY_ID = "UNPACKING_BOX_KEY_ID";
    public static final String UNPACKING_QUANTITY = "UNPACKING_QUANTITY";
    public static final String UNPACKING_STATUS = "UNPACKING_STATUS";
    public static final String UNPACKING_VEHICLE_NUMBER = "UNPACKING_VEHICLE_NUMBER";
    public static final String UNPACKING_INWARD_NUMBER = "UNPACKING_INWARD_NUMBER";
    public static final String UNPACKING_BOX_INVOICE_NUMBER = "UNPACKING_INVOICE_NUMBER";
    public static final String UNPACKING_MASTER_BOX_NUMBER = "UNPACKING_MASTER_BOX_NUMBER";
    public static final String UNPACKING_BOX_ID = "UNPACKING_BOX_ID";
    public static final String UNPACKING_BOX_STATUS = "UNPACKING_BOX_STATUS";

    public static final String UNPACKING_BOX_CREATE_DATABASE = "CREATE TABLE " + UNPACKING_BOX_TABLE_NAME + "("
            + UNPACKING_BOX_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UNPACKING_QUANTITY + " INTEGER, "
            + UNPACKING_BOX_ID + " INTEGER, "
            + UNPACKING_BOX_STATUS + " INTEGER, "
            + UNPACKING_BOX_INVOICE_NUMBER + " TEXT, "
            + UNPACKING_STATUS + " INTEGER, " + UNPACKING_VEHICLE_NUMBER + " TEXT, "
            + UNPACKING_INWARD_NUMBER + " TEXT, " + UNPACKING_MASTER_BOX_NUMBER + " TEXT);";

    /*********** UNPACKING DATA TABLE  *************/
    public static final String UNPACKING_TABLE_NAME = "unpackingParts";

    public static final String UNPACKING_KEY_ID = "UNPACKING_KEY_ID";
    public static final String UNPACKING_PART_ID = "UNPACKING_PART_ID";
    public static final String UNPACKING_PART_NUM = "UNPACKING_PART_NUM";
    public static final String UNPACKING_IS_SELECTED = "UNPACKING_IS_SELECTED";
    public static final String UNPACKING_INVOICE_NUMBER = "UNPACKING_INVOICE_NUMBER";
    public static final String UNPACKING_PRIMARY_CARTONS = "UNPACKING_PRIMARY_CARTONS";
    public static final String UNPACKING_PRIMARY_CARTONS_CONTAINED = "UNPACKING_PRIMARY_CARTONS_CONTAINED";
    public static final String UNPACKING_NO_OF_EXCEPTIONS = "UNPACKING_NO_OF_EXCEPTIONS";
    public static final String UNPACKING_IS_ACTED_UPON = "UNPACKING_IS_ACTED_UPON";
    public static final String UNPACKING_PART_STATUS = "UNPACKING_PART_STATUS";

    public static final String UNPACKING_CREATE_DATABASE = "CREATE TABLE " + UNPACKING_TABLE_NAME + "("
            + UNPACKING_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UNPACKING_PART_ID + " INTEGER, "
            + UNPACKING_PART_NUM + " TEXT, " + UNPACKING_QUANTITY + " INTEGER, "
            + UNPACKING_IS_SELECTED + " INTEGER, "
            + UNPACKING_PRIMARY_CARTONS + " INTEGER, "
            + UNPACKING_INVOICE_NUMBER + " TEXT, "
            + UNPACKING_PRIMARY_CARTONS_CONTAINED + " INTEGER, " + UNPACKING_NO_OF_EXCEPTIONS + " INTEGER, "
            + UNPACKING_STATUS + " INTEGER, " + UNPACKING_IS_ACTED_UPON + " INTEGER, " + UNPACKING_VEHICLE_NUMBER + " TEXT, "
            + UNPACKING_INWARD_NUMBER + " TEXT, " + UNPACKING_MASTER_BOX_NUMBER + " TEXT, "
            + UNPACKING_PART_STATUS + " INTEGER);";

    /**
     * UNPACKING COMPLETED TABLE NAME
     *****/
    public static final String UNPACKING_COMPLETED_TABLE_NAME = "unpackingCompletedParts";

    public static final String UNPACKING_COMPLETED_KEY_ID = "UNPACKING_COMPLETED_KEY_ID";
    public static final String UNPACKING_COMPLETED_PART_ID = "UNPACKING_COMPLETED_PART_ID";
    public static final String UNPACKING_COMPLETED_PART_NUM = "UNPACKING_COMPLETED_PART_NUM";
    public static final String UNPACKING_COMPLETED_IS_SELECTED = "UNPACKING_COMPLETED_IS_SELECTED";
    public static final String UNPACKING_COMPLETED_INVOICE_NUMBER = "UNPACKING_COMPLETED_INVOICE_NUMBER";
    public static final String UNPACKING_COMPLETED_PRIMARY_CARTONS = "UNPACKING_COMPLETED_PRIMARY_CARTONS";
    public static final String UNPACKING_COMPLETED_PRIMARY_CARTONS_CONTAINED = "UNPACKING_COMPLETED_PRIMARY_CARTONS_CONTAINED";
    public static final String UNPACKING_COMPLETED_NO_OF_EXCEPTIONS = "UNPACKING_COMPLETED_NO_OF_EXCEPTIONS";
    public static final String UNPACKING_COMPLETED_IS_ACTED_UPON = "UNPACKING_COMPLETED_IS_ACTED_UPON";
    public static final String UNPACKING_COMPLETED_PART_STATUS = "UNPACKING_COMPLETED_PART_STATUS";

    public static final String UNPACKING_COMPLETED_CREATE_DATABASE = "CREATE TABLE " + UNPACKING_COMPLETED_TABLE_NAME + "("
            + UNPACKING_COMPLETED_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UNPACKING_COMPLETED_PART_ID + " INTEGER, "
            + UNPACKING_COMPLETED_PART_NUM + " TEXT, " + UNPACKING_QUANTITY + " INTEGER, "
            + UNPACKING_COMPLETED_IS_SELECTED + " INTEGER, "
            + UNPACKING_COMPLETED_PRIMARY_CARTONS + " INTEGER, "
            + UNPACKING_COMPLETED_INVOICE_NUMBER + " TEXT, "
            + UNPACKING_COMPLETED_PRIMARY_CARTONS_CONTAINED + " INTEGER, " + UNPACKING_COMPLETED_NO_OF_EXCEPTIONS + " INTEGER, "
            + UNPACKING_STATUS + " INTEGER, " + UNPACKING_COMPLETED_IS_ACTED_UPON + " INTEGER, " + UNPACKING_VEHICLE_NUMBER + " TEXT, "
            + UNPACKING_INWARD_NUMBER + " TEXT, " + UNPACKING_MASTER_BOX_NUMBER + " TEXT, "
            + UNPACKING_COMPLETED_PART_STATUS + " INTEGER);";

    /************************ Packing data table ****************/
    public static final String PACKING_TABLE_NAME = "packing";

    public static final String PACKING_KEY_ID = "PACKING_KEY_ID";
    public static final String PACKING_PART_ID = "PACKING_PART_ID";
    public static final String PACKING_PART_NUM = "PACKING_PART_NUM";
    public static final String PACKING_QUANTITY = "PACKING_QUANTITY";
    public static final String PACKING_PACKED_PRIMARY_CARTONS = "PACKING_PACKED_PRIMARY_CARTONS";
    public static final String PACKING_REMAINING_PRIMARY_CARTONS = "PACKING_REMAINING_PRIMARY_CARTONS";
    public static final String PACKING_INVOICE_NUMBER = "PACKING_INVOICE_NUMBER";
    public static final String PACKING_INVOICE_ID = "PACKING_INVOICE_ID";
    public static final String PACKING_SUBPROJECT_ID = "PACKING_SUBPROJECT_ID";
    public static final String PACKING_SUBPROJECT_NAME = "PACKING_SUBPROJECT_NAME";
    public static final String PACKING_SHIP_ADDRESS = "PACKING_SHIP_ADDRESS";
    public static final String PACKING_PICK_STATUS = "PACKING_PICK_STATUS";
    public static final String PACKING_WEIGHT = "PACKING_WEIGHT";
    public static final String PACKING_NO_OF_MASTER_BOXES = "PACKING_NO_OF_MASTER_BOXES";

    public static final String PACKING_CREATE_DATABASE = "CREATE TABLE " + PACKING_TABLE_NAME + "("
            + PACKING_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PACKING_PART_ID + " INTEGER, "
            + PACKING_PART_NUM + " TEXT, " + PACKING_QUANTITY + " INTEGER, " + PACKING_PACKED_PRIMARY_CARTONS + " INTEGER, "
            + PACKING_REMAINING_PRIMARY_CARTONS + " INTEGER, " + PACKING_INVOICE_NUMBER + " TEXT, " + PACKING_INVOICE_ID +
            " INTEGER, " + PACKING_SUBPROJECT_ID + " INTEGER, " + PACKING_SUBPROJECT_NAME + " TEXT, " + PACKING_SHIP_ADDRESS
            + " TEXT, " + PACKING_NO_OF_MASTER_BOXES + " INTEGER, "
            + PACKING_WEIGHT + " REAL, "
            + PACKING_PICK_STATUS + " INTEGER);";

    /*********** invoices data table ***************/
    public static final String INVOICES_TABLE_NAME = "invoices";
    public static final String INVOICES_KEY_ID = "INVOICES_KEY_ID";
    public static final String INVOICE_SUBPROJECT_ID = "INVOICE_SUBPROJECT_ID";
    public static final String INVOICES_ORDER_NUM = "INVOICES_ORDER_NUM";
    public static final String INVOICES_SUPPLIER_NAME = "INVOICES_SUPPLIER_NAME";
    public static final String INVOICES_SUPPLIER_ADDRESS = "INVOICES_SUPPLIER_ADDRESS";

    public static final String INVOICES_CREATE_DATABASE = "CREATE TABLE " + INVOICES_TABLE_NAME +
            "(" + INVOICES_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + INVOICE_SUBPROJECT_ID + " INTEGER, "
            + INVOICES_ORDER_NUM + " TEXT, "
            + INVOICES_SUPPLIER_NAME + " TEXT, " + INVOICES_SUPPLIER_ADDRESS + " TEXT);";

    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"message"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try {
            String maxQuery = Query;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        } catch (Exception ex) {
            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }
    }

}
