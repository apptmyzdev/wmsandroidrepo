package com.apptmyz.wms.datacache;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.util.Log;

import com.apptmyz.wms.InvoicesScreen;
import com.apptmyz.wms.LoadingScanningLatestScreen;
import com.apptmyz.wms.LoadingScanningScreen;
import com.apptmyz.wms.PartsScanningScreen;
import com.apptmyz.wms.R;
import com.apptmyz.wms.data.BinMasterModel;
import com.apptmyz.wms.data.CustomerMasterModel;
import com.apptmyz.wms.data.CustomerMasterModelNew;
import com.apptmyz.wms.data.ExceptionModel;
import com.apptmyz.wms.data.InspectionPart;
import com.apptmyz.wms.data.InvoiceModel;
import com.apptmyz.wms.data.LoadingResponse;
import com.apptmyz.wms.data.LoadingResponseLatest;
import com.apptmyz.wms.data.LoadingaPartsModel;
import com.apptmyz.wms.data.NewLoadingPartsModel;
import com.apptmyz.wms.data.NewProductModel;
import com.apptmyz.wms.data.NodeMasterModel;
import com.apptmyz.wms.data.PackingModel;
import com.apptmyz.wms.data.PackingOrderNoModel;
import com.apptmyz.wms.data.PartMasterModel;
import com.apptmyz.wms.data.ProductModel;
import com.apptmyz.wms.data.PutAwayData;
import com.apptmyz.wms.data.SharedPreferenceItem;
import com.apptmyz.wms.data.SkipBinReasonModel;
import com.apptmyz.wms.data.SubProjectMasterModel;
import com.apptmyz.wms.data.UnloadingModel;
import com.apptmyz.wms.data.UnloadingResponse;
import com.apptmyz.wms.data.UnpackingBox;
import com.apptmyz.wms.data.UnpackingPart;
import com.apptmyz.wms.data.VehicleMasterModel;
import com.apptmyz.wms.data.VendorMasterModel;
import com.apptmyz.wms.data.WmsLoadingModel;
import com.apptmyz.wms.data.WmsPartInvoiceModel;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataSource {
    private SQLiteDatabase database;
    private DataHelper dbHelper;
    private Context mContext;
    private Gson gson;
    public LoadingParts loadingParts;
    public LoadingPartsNew loadingData;
    public VendorMaster vendorMaster;
    public UnloadingParts unloadingParts;
    public ShardPreferences sharedPreferences;
    public SkipBinReasons skipBinReasons;
    public Bins bins;
    public Parts parts;
    public PutAway putAway;
    public Subprojects subprojects;
    public Nodes nodes;
    public PickListParts pickListParts;
    public ExceptionTypes exceptionTypes;
    public PickingExcepReasons pickingExcepReasons;
    public Exceptions exceptions;
    public Customers customers;
    public Vehicles vehicles;
    public InboundInspection inboundInspection;
    public UnpackingParts unpackingParts;
    public UnpackingCompletedParts unpackingCompletedParts;
    public UnpackingBoxes unpackingBoxes;
    public PackingParts packingParts;
    public Invoices invoices;

    public DataSource(Context context) {
        try {
            mContext = context;
            dbHelper = DataHelper.getHelper(context);
            sharedPreferences = new ShardPreferences();
            unloadingParts = new UnloadingParts();
            putAway = new PutAway();
            parts = new Parts();
            loadingData = new LoadingPartsNew();
            exceptionTypes = new ExceptionTypes();
            pickingExcepReasons = new PickingExcepReasons();
            vendorMaster = new VendorMaster();
            loadingParts = new LoadingParts();
            subprojects = new Subprojects();
            skipBinReasons = new SkipBinReasons();
            bins = new Bins();
            nodes = new Nodes();
            pickListParts = new PickListParts();
            gson = new Gson();
            exceptions = new Exceptions();
            customers = new Customers();
            vehicles = new Vehicles();
            inboundInspection = new InboundInspection();
            unpackingParts = new UnpackingParts();
            unpackingCompletedParts = new UnpackingCompletedParts();
            unpackingBoxes = new UnpackingBoxes();
            packingParts = new PackingParts();
            invoices = new Invoices();
        } catch (SQLiteException e) {
            Utils.logE(e.toString());
        } catch (VerifyError e) {
            Utils.logE(e.toString());
        }
    }

    public void dropTables() {
        // TODO: 14/01/21  Drop the tables that are irrelevant after the logout
    }

    @SuppressLint("NewApi")
    public void open() throws SQLException {
        if ((database != null && !database.isOpen()) || database == null) {
            try {
                database = dbHelper.getWritableDatabase();
            } catch (Exception e) {
                Utils.logE(e.toString());
            }
        }
    }

    synchronized public void openRead() throws SQLException {
        if ((database != null && !database.isOpen()) || database == null) {
            database = dbHelper.getReadableDatabase();
        }
    }

    public void close() {
        if ((database != null && database.isOpen())) {
//            dbHelper.close();
        }
    }

    // ************** SHARED_PREFERENCES **************//

    public class ShardPreferences {

        public void set(String key, String value) {
            open();
            try {
                if (!Utils.isValidString(value))
                    value = "";

                if (Utils.isValidString(key)) {
                    int id = exists(key);
                    if (id == -1) {
                        create(key, value);
                    } else {
                        update(key, value);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void update(String key, String value) {
            open();
            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.SHARED_PREF_COLUMN_VALUE, value);

                if (database.update(DataHelper.SHARED_PREF_TABLE_NAME, values,
                        DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key + "'",
                        null) > 0) {
                } else {
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }

        }

        public void create(String key, String value) {
            open();
            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.SHARED_PREF_COLUMN_KEY, key);
                values.put(DataHelper.SHARED_PREF_COLUMN_VALUE, value);

                if (database.insert(DataHelper.SHARED_PREF_TABLE_NAME, null,
                        values) > 0) {
                } else {
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void delete(String key) {
            open();
            try {
                if (database.delete(DataHelper.SHARED_PREF_TABLE_NAME,
                        DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key + "'",
                        null) > 0) {
                } else {
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public List<SharedPreferenceItem> getAll() {
            openRead();
            List<SharedPreferenceItem> items = new ArrayList<SharedPreferenceItem>();
            Cursor cursor = null;
            try {

                cursor = database.query(DataHelper.SHARED_PREF_TABLE_NAME,
                        DataHelper.SHARED_PREF_COLUMNS, null, null, null, null,
                        null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    SharedPreferenceItem item = cursorToItem(cursor);
                    items.add(item);
                    cursor.moveToNext();
                }

            } catch (Exception e) {
                Utils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return items;
        }

        public String getValue(String key) {
            openRead();

            Cursor cursor = null;
            String value = "";

            try {
                String selectQuery = "SELECT  "
                        + DataHelper.SHARED_PREF_COLUMN_VALUE + " FROM "
                        + DataHelper.SHARED_PREF_TABLE_NAME + " WHERE "
                        + DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key
                        + "'";

                cursor = database.rawQuery(selectQuery, null);

                if (cursor.moveToFirst()) {
                    value = cursor.getString(0);
                }

            } catch (Exception e) {
                Utils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return value;

        }

        public int exists(String key) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.SHARED_PREF_KEY_ID
                        + " FROM " + DataHelper.SHARED_PREF_TABLE_NAME
                        + " WHERE " + DataHelper.SHARED_PREF_COLUMN_KEY
                        + " = '" + key + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        private SharedPreferenceItem cursorToItem(Cursor cursor) {
            return new SharedPreferenceItem(cursor.getString(0),
                    cursor.getString(1));
        }
    }

    public class PutAway {
        public int exists(int dataId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PUT_AWAY_PART_KEY_ID
                        + " FROM " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " WHERE " + DataHelper.PUT_AWAY_ID
                        + " = " + dataId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public void delete(int id) {
            openRead();
            try {
                String updateSql = "DELETE FROM " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " WHERE "
                        + DataHelper.PUT_AWAY_ID + " = " + id;

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateAsPartialScan(int dataId, int noOfBoxesToScan) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " SET " + DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES + " = " + noOfBoxesToScan
                        + " WHERE " + DataHelper.PUT_AWAY_ID + " = " + dataId;

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateAsSubmitted(int dataId) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " SET " + DataHelper.PUT_AWAY_STATUS + " = " + 1
                        + " WHERE " + DataHelper.PUT_AWAY_ID + " = " + dataId;

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public long insertPutAwayData(List<PutAwayData> putAwayDataList, int status) {
            open();
            long retVal = -1;
            try {
                for (PutAwayData putAwayData : putAwayDataList) {
                    int id = exists(putAwayData.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PUT_AWAY_PART_ID, putAwayData.getPartId());
                        values.put(DataHelper.PUT_AWAY_PART_NUMBER, putAwayData.getPartNo());
                        values.put(DataHelper.PUT_AWAY_STATUS, status);
                        values.put(DataHelper.PUT_AWAY_CUSTOMER_CODE, putAwayData.getCustomerCode());
                        if (status == 0)
                            values.put(DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES, putAwayData.getTotalBoxes());
                        else
                            values.put(DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES, 0);
                        values.put(DataHelper.PUT_AWAY_VEHICLE_NUMBER, putAwayData.getVehNo());
                        values.put(DataHelper.PUT_AWAY_TOTAL_BOXES, putAwayData.getTotalBoxes());
                        values.put(DataHelper.PUT_AWAY_TOTAL_QTY, putAwayData.getTotalQty());
                        values.put(DataHelper.PUT_AWAY_BIN_NUMBER, putAwayData.getBinNumber());
                        values.put(DataHelper.PUT_AWAY_INWARD_NUMBER, putAwayData.getInwardNo());
                        values.put(DataHelper.PUT_AWAY_ID, putAwayData.getId());
                        values.put(DataHelper.PUT_AWAY_DA_NUM, putAwayData.getDaNo());
                        values.put(DataHelper.PUT_AWAY_DATA_TYPE, putAwayData.getDataType());
                        values.put(DataHelper.PUT_AWAY_SUBPROJECT, putAwayData.getSubproject());
                        values.put(DataHelper.PUT_AWAY_BATCH_NO,putAwayData.getBatchNo());
                        String jsonStr = gson.toJson(putAwayData.getInvoiceList());
                        values.put(DataHelper.PUT_AWAY_INVOICES, jsonStr);

                        retVal = database.insert(DataHelper.PUT_AWAY_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<PutAwayData> getPutAwayData(int status) {
            ArrayList<PutAwayData> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PUT_AWAY_TABLE_NAME + " WHERE "
                    + DataHelper.PUT_AWAY_STATUS + " = " + status;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partIdIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_PART_ID);
                int partNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_PART_NUMBER);
                int invoicesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_INVOICES);
                int vehicleNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_VEHICLE_NUMBER);
                int totalBoxesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TOTAL_BOXES);
                int totalQtyIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TOTAL_QTY);
                int statusIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_STATUS);
                int binNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_BIN_NUMBER);
                int tobeScannedBoxesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES);
                int inwardNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_INWARD_NUMBER);
                int idIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_ID);
                int daNoIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_DA_NUM);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_DATA_TYPE);
                int customerCodeIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_CUSTOMER_CODE);
                int subprojectIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_SUBPROJECT);
                int batchNoIdx=cursor.getColumnIndex(DataHelper.PUT_AWAY_BATCH_NO);
                do {
                    PutAwayData model = new PutAwayData();
                    model.setPartNo(cursor.getString(partNumIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    String invoicesStr = cursor.getString(invoicesIdx);
                    Type token = new TypeToken<List<InvoiceModel>>() {
                    }.getType();
                    model.setInvoiceList((List<InvoiceModel>) gson.fromJson(invoicesStr, token));
                    model.setVehNo(cursor.getString(vehicleNumIdx));
                    model.setTotalBoxes(cursor.getInt(totalBoxesIdx));
                    model.setTotalQty(cursor.getInt(totalQtyIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setBinNumber(cursor.getString(binNumIdx));
                    model.setNoOfBoxesToScan(cursor.getInt(tobeScannedBoxesIdx));
                    model.setInwardNo(cursor.getString(inwardNumIdx));
                    model.setId(cursor.getInt(idIdx));
                    model.setDaNo(cursor.getString(daNoIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));
                    model.setCustomerCode(cursor.getString(customerCodeIdx));
                    model.setSubproject(cursor.getString(subprojectIdx));
                    model.setBatchNo(cursor.getString(batchNoIdx));
                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PUT_AWAY_TABLE_NAME;
            database.execSQL(query);

            close();
        }

        public void deletePendingData() {
            open();
            String query = "DELETE FROM " + DataHelper.PUT_AWAY_TABLE_NAME + " WHERE "
                    + DataHelper.PUT_AWAY_STATUS + " = 0";
            database.execSQL(query);

            close();
        }

        public void deleteCompletedData() {
            open();
            String query = "DELETE FROM " + DataHelper.PUT_AWAY_TABLE_NAME + " WHERE "
                    + DataHelper.PUT_AWAY_STATUS + " = 1";
            database.execSQL(query);

            close();
        }

    }

    public class ExceptionTypes {
        public long saveExceptionTypes(List<SkipBinReasonModel> skipBinReasonModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (SkipBinReasonModel skipBinReasonModel : skipBinReasonModels) {
                    id = exists(skipBinReasonModel.getReasonId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.EXCEPTION_TYPE_ID, skipBinReasonModel.getReasonId());
                        values.put(DataHelper.EXCEPTION_TYPE_NAME, skipBinReasonModel.getReason());

                        retVal = database.insert(DataHelper.EXCEPTION_TYPES_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int reasonId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.EXCEPTION_TYPE_KEY_ID
                        + " FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME
                        + " WHERE " + DataHelper.EXCEPTION_TYPE_ID
                        + " = " + reasonId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<SkipBinReasonModel> getExceptionTypes() {
            ArrayList<SkipBinReasonModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_NAME);

                do {
                    SkipBinReasonModel model = new SkipBinReasonModel();
                    model.setReasonId(cursor.getInt(reasonIdIdx));
                    model.setReason(cursor.getString(reasonIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public SkipBinReasonModel getException(int id) {
            SkipBinReasonModel model = null;

            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_TYPE_ID + " = " + id;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_NAME);
                model = new SkipBinReasonModel();
                model.setReasonId(cursor.getInt(reasonIdIdx));
                model.setReason(cursor.getString(reasonIdx));
            }
            if (cursor != null)
                cursor.close();
            close();

            return model;
        }


        public String getExceptionId(String desc) {
            String depsId = "";

            openRead();

            String sql = "SELECT " + DataHelper.EXCEPTION_TYPE_ID + " FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME
                    + " WHERE " + DataHelper.EXCEPTION_TYPE_NAME + " = '" + desc + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                depsId = cursor.getString(0);
            }

            cursor.close();
            close();

            return depsId;
        }


        public ArrayList<String> getExceptionList() {
            ArrayList<String> codes = new ArrayList<String>();

            openRead();

            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int descId = cursor
                        .getColumnIndex(DataHelper.EXCEPTION_TYPE_NAME);
                do {
                    codes.add(cursor.getString(descId));
                } while (cursor.moveToNext());
            }

            cursor.close();
            close();

            return codes;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }


    public class PickingExcepReasons {
        public long saveReasons(List<SkipBinReasonModel> skipBinReasonModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (SkipBinReasonModel skipBinReasonModel : skipBinReasonModels) {
                    id = exists(skipBinReasonModel.getReasonId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PICKING_EXCEPTION_REASON_ID, skipBinReasonModel.getReasonId());
                        values.put(DataHelper.PICKING_EXCEPTION_REASON_NAME, skipBinReasonModel.getReason());

                        retVal = database.insert(DataHelper.PICKING_EXCEPTION_REASONS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int reasonId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PICKING_EXCEPTION_REASON_KEY_ID
                        + " FROM " + DataHelper.PICKING_EXCEPTION_REASONS_TABLE_NAME
                        + " WHERE " + DataHelper.PICKING_EXCEPTION_REASON_ID
                        + " = " + reasonId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<SkipBinReasonModel> getReasons() {
            ArrayList<SkipBinReasonModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PICKING_EXCEPTION_REASONS_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.PICKING_EXCEPTION_REASON_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.PICKING_EXCEPTION_REASON_NAME);

                do {
                    SkipBinReasonModel model = new SkipBinReasonModel();
                    model.setReasonId(cursor.getInt(reasonIdIdx));
                    model.setReason(cursor.getString(reasonIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public SkipBinReasonModel getReason(int id) {
            SkipBinReasonModel model = null;

            openRead();
            String sql = "SELECT * FROM " + DataHelper.PICKING_EXCEPTION_REASONS_TABLE_NAME + " WHERE "
                    + DataHelper.PICKING_EXCEPTION_REASON_ID + " = " + id;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.PICKING_EXCEPTION_REASON_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.PICKING_EXCEPTION_REASON_NAME);
                model = new SkipBinReasonModel();
                model.setReasonId(cursor.getInt(reasonIdIdx));
                model.setReason(cursor.getString(reasonIdx));
            }
            if (cursor != null)
                cursor.close();
            close();

            return model;
        }


        public String getReasonId(String desc) {
            String depsId = "";

            openRead();

            String sql = "SELECT " + DataHelper.PICKING_EXCEPTION_REASON_ID + " FROM " + DataHelper.PICKING_EXCEPTION_REASONS_TABLE_NAME
                    + " WHERE " + DataHelper.PICKING_EXCEPTION_REASON_NAME + " = '" + desc + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                depsId = cursor.getString(0);
            }

            cursor.close();
            close();

            return depsId;
        }


        public ArrayList<String> getReasonsList() {
            ArrayList<String> codes = new ArrayList<String>();

            openRead();

            String sql = "SELECT * FROM " + DataHelper.PICKING_EXCEPTION_REASONS_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int descId = cursor
                        .getColumnIndex(DataHelper.PICKING_EXCEPTION_REASON_NAME);
                do {
                    codes.add(cursor.getString(descId));
                } while (cursor.moveToNext());
            }

            cursor.close();
            close();

            return codes;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PICKING_EXCEPTION_REASONS_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Vehicles {
        public long saveVehicles(List<VehicleMasterModel> vehicleMasterModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (VehicleMasterModel vehicleMasterModel : vehicleMasterModels) {
                    id = exists(vehicleMasterModel.getVehicleId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.VEHICLE_ID, vehicleMasterModel.getVehicleId());
                        values.put(DataHelper.VEHICLE_VENDOR_ID, vehicleMasterModel.getVendorId());
                        values.put(DataHelper.VEHICLE_VENDOR_NAME, vehicleMasterModel.getVendorName());
                        values.put(DataHelper.VEHICLE_NUMBER, vehicleMasterModel.getVehicleNo());
                        values.put(DataHelper.VEHICLE_TYPE_ID, vehicleMasterModel.getVehicleTypeId());
                        values.put(DataHelper.VEHICLE_TYPE, vehicleMasterModel.getVehicleType());
                        values.put(DataHelper.VEHICLE_SUBPROJECT_ID, vehicleMasterModel.getSubProjectId());
                        values.put(DataHelper.VEHICLE_SUBPROJECT, vehicleMasterModel.getSubProject());
                        values.put(DataHelper.VEHICLE_ROUTE_ID, vehicleMasterModel.getRouteId());
                        values.put(DataHelper.VEHICLE_ROUTE, vehicleMasterModel.getRoute());
                        values.put(DataHelper.VEHICLE_USERID, vehicleMasterModel.getUserId());
                        values.put(DataHelper.VEHICLE_CREATED_BY, vehicleMasterModel.getCreatedBy());
                        values.put(DataHelper.VEHICLE_CREATED_TIMESTAMP, vehicleMasterModel.getCreatedTimestamp());
                        values.put(DataHelper.VEHICLE_UPDATED_BY, vehicleMasterModel.getUpdatedBy());
                        values.put(DataHelper.VEHICLE_UPDATED_TIMESTAMP, vehicleMasterModel.getUpdatedTimestamp());
                        values.put(DataHelper.VEHICLE_ACTIVE_FLAG, vehicleMasterModel.isActiveFlag() ? 1 : 0);

                        retVal = database.insert(DataHelper.VEHICLES_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int vehicleId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.VEHICLE_KEY_ID
                        + " FROM " + DataHelper.VEHICLES_TABLE_NAME
                        + " WHERE " + DataHelper.VEHICLE_ID
                        + " = " + vehicleId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<VehicleMasterModel> getVehicles() {
            ArrayList<VehicleMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.VEHICLES_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int vehicleIdIdx = cursor.getColumnIndex(DataHelper.VEHICLE_ID);
                int vendorIdIdx = cursor.getColumnIndex(DataHelper.VEHICLE_VENDOR_ID);
                int vendorNameIdx = cursor.getColumnIndex(DataHelper.VEHICLE_VENDOR_NAME);
                int vehNoIdx = cursor.getColumnIndex(DataHelper.VEHICLE_NUMBER);
                int vehicleTypeIdIdx = cursor.getColumnIndex(DataHelper.VEHICLE_TYPE_ID);
                int vehicleTypeIdx = cursor.getColumnIndex(DataHelper.VEHICLE_TYPE);
                int subprojectIdIdx = cursor.getColumnIndex(DataHelper.VEHICLE_SUBPROJECT_ID);
                int subprojectIdx = cursor.getColumnIndex(DataHelper.VEHICLE_SUBPROJECT);
                int routeIdIdx = cursor.getColumnIndex(DataHelper.VEHICLE_ROUTE_ID);
                int routeIdx = cursor.getColumnIndex(DataHelper.VEHICLE_ROUTE);
                int userIdIdx = cursor.getColumnIndex(DataHelper.VEHICLE_USERID);
                int createdByIdx = cursor.getColumnIndex(DataHelper.VEHICLE_CREATED_BY);
                int createdTimeIdx = cursor.getColumnIndex(DataHelper.VEHICLE_CREATED_TIMESTAMP);
                int updatedByIdx = cursor.getColumnIndex(DataHelper.VEHICLE_UPDATED_BY);
                int updatedTimeStampIdx = cursor.getColumnIndex(DataHelper.VEHICLE_UPDATED_TIMESTAMP);
                int activeFlagIdx = cursor.getColumnIndex(DataHelper.VEHICLE_ACTIVE_FLAG);

                do {
                    VehicleMasterModel model = new VehicleMasterModel();

                    model.setVehicleId(cursor.getInt(vehicleIdIdx));
                    model.setVehicleNo(cursor.getString(vehNoIdx));
                    model.setVendorId(cursor.getInt(vendorIdIdx));
                    model.setVendorName(cursor.getString(vendorNameIdx));
                    model.setVehicleType(cursor.getString(vehicleTypeIdx));
                    model.setVehicleTypeId(cursor.getInt(vehicleTypeIdIdx));
                    model.setSubProjectId(cursor.getInt(subprojectIdIdx));
                    model.setSubProject(cursor.getString(subprojectIdx));
                    model.setRouteId(cursor.getInt(routeIdIdx));
                    model.setRoute(cursor.getString(routeIdx));
                    model.setUserId(cursor.getInt(userIdIdx));
                    model.setCreatedBy(cursor.getString(createdByIdx));
                    model.setCreatedTimestamp(cursor.getString(createdTimeIdx));
                    model.setUpdatedBy(cursor.getString(updatedByIdx));
                    model.setUpdatedTimestamp(cursor.getString(updatedTimeStampIdx));
                    model.setActiveFlag(cursor.getInt(activeFlagIdx) == 0 ? false : true);

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.VEHICLES_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Customers {
        public long saveCustomers(List<CustomerMasterModelNew> customerMasterModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (CustomerMasterModelNew customerMasterModel : customerMasterModels) {
                    id = exists(customerMasterModel.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.CUSTOMERS_ID, customerMasterModel.getId());
                        values.put(DataHelper.CUSTOMERS_VALUE, customerMasterModel.getValue());
                        values.put(DataHelper.CUSTOMERS_TYPE, customerMasterModel.getType());

                        retVal = database.insert(DataHelper.CUSTOMERS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int custId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.CUSTOMERS_KEY_ID
                        + " FROM " + DataHelper.CUSTOMERS_TABLE_NAME
                        + " WHERE " + DataHelper.CUSTOMERS_ID
                        + " = " + custId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<CustomerMasterModelNew> getCustomers() {
            ArrayList<CustomerMasterModelNew> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.CUSTOMERS_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int custIdIdx = cursor.getColumnIndex(DataHelper.CUSTOMERS_ID);
                int custValueIdx = cursor.getColumnIndex(DataHelper.CUSTOMERS_VALUE);
                int custTypeIdx = cursor.getColumnIndex(DataHelper.CUSTOMERS_TYPE);
                do {
                    CustomerMasterModelNew model = new CustomerMasterModelNew();

                    model.setId(cursor.getInt(custIdIdx));
                    model.setType(cursor.getString(custTypeIdx));
                    model.setValue(cursor.getString(custValueIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.CUSTOMERS_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class SkipBinReasons {
        public long saveReasons(List<SkipBinReasonModel> skipBinReasonModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (SkipBinReasonModel skipBinReasonModel : skipBinReasonModels) {
                    id = exists(skipBinReasonModel.getReasonId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.SKIP_BIN_REASON_ID, skipBinReasonModel.getReasonId());
                        values.put(DataHelper.SKIP_BIN_REASON_NAME, skipBinReasonModel.getReason());

                        retVal = database.insert(DataHelper.SKIP_BIN_REASONS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int reasonId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.SKIP_BIN_REASON_KEY_ID
                        + " FROM " + DataHelper.SKIP_BIN_REASONS_TABLE_NAME
                        + " WHERE " + DataHelper.SKIP_BIN_REASON_ID
                        + " = " + reasonId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<SkipBinReasonModel> getSkipBinReasons() {
            ArrayList<SkipBinReasonModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.SKIP_BIN_REASONS_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.SKIP_BIN_REASON_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.SKIP_BIN_REASON_NAME);

                do {
                    SkipBinReasonModel model = new SkipBinReasonModel();
                    model.setReasonId(cursor.getInt(reasonIdIdx));
                    model.setReason(cursor.getString(reasonIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.SKIP_BIN_REASONS_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Parts {
        public int matchesData(String scannedValue) {
            openRead();
            int matchingValues = 0;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  COUNT(*) FROM " + DataHelper.PART_TABLE_NAME
                        + " WHERE " + DataHelper.PART_NUMBER
                        + " LIKE '" + scannedValue + "%'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    matchingValues = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return matchingValues;
        }

        public long saveParts(List<PartMasterModel> partMasterModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (PartMasterModel partMasterModel : partMasterModels) {
                    id = exists(partMasterModel.getPartId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PART_ID, partMasterModel.getPartId());
                        values.put(DataHelper.PART_NUMBER, partMasterModel.getPartNo());

                        retVal = database.insert(DataHelper.PART_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int partId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PART_KEY_ID
                        + " FROM " + DataHelper.PART_TABLE_NAME
                        + " WHERE " + DataHelper.PART_ID
                        + " = " + partId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public int exists(String partNumber) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PART_KEY_ID
                        + " FROM " + DataHelper.PART_TABLE_NAME
                        + " WHERE " + DataHelper.PART_NUMBER
                        + " = '" + partNumber + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<PartMasterModel> getPartsData() {
            ArrayList<PartMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PART_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partIdIdx = cursor.getColumnIndex(DataHelper.PART_ID);
                int partNoIdx = cursor.getColumnIndex(DataHelper.PART_NUMBER);

                do {
                    PartMasterModel model = new PartMasterModel();
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setPartNo(cursor.getString(partNoIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PART_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Bins {
        public int matchesData(String scannedValue) {
            openRead();
            int matchingValues = 0;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  COUNT(*) FROM " + DataHelper.BIN_TABLE_NAME
                        + " WHERE " + DataHelper.BIN_NUMBER
                        + " LIKE '" + scannedValue + "%'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    matchingValues = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return matchingValues;
        }

        public long saveBins(List<BinMasterModel> binMasterModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (BinMasterModel binMasterModel : binMasterModels) {
                    id = exists(binMasterModel.getBinId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.BIN_ID, binMasterModel.getBinId());
                        values.put(DataHelper.BIN_NUMBER, binMasterModel.getBinNo());
                        values.put(DataHelper.BIN_AISLE, binMasterModel.getAisle());

                        retVal = database.insert(DataHelper.BIN_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int binId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.BIN_KEY_ID
                        + " FROM " + DataHelper.BIN_TABLE_NAME
                        + " WHERE " + DataHelper.BIN_ID
                        + " = " + binId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public int exists(String binNumber) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.BIN_KEY_ID
                        + " FROM " + DataHelper.BIN_TABLE_NAME
                        + " WHERE " + DataHelper.BIN_NUMBER
                        + " = '" + binNumber + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public BinMasterModel getBin(String binNumber) {
            BinMasterModel model = new BinMasterModel();

            openRead();
            String sql = "SELECT * FROM " + DataHelper.BIN_TABLE_NAME + " WHERE "
                    + DataHelper.BIN_NUMBER + " = '" + binNumber + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int binIdIdx = cursor.getColumnIndex(DataHelper.BIN_ID);
                int binNoIdx = cursor.getColumnIndex(DataHelper.BIN_NUMBER);
                int binAisleIdx = cursor.getColumnIndex(DataHelper.BIN_AISLE);

                do {
                    model.setBinId(cursor.getInt(binIdIdx));
                    model.setBinNo(cursor.getString(binNoIdx));
                    model.setAisle(cursor.getString(binAisleIdx));
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return model;
        }

        public ArrayList<BinMasterModel> getBinsData() {
            ArrayList<BinMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.BIN_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int binIdIdx = cursor.getColumnIndex(DataHelper.BIN_ID);
                int binNoIdx = cursor.getColumnIndex(DataHelper.BIN_NUMBER);
                int binAisleIdx = cursor.getColumnIndex(DataHelper.BIN_AISLE);

                do {
                    BinMasterModel model = new BinMasterModel();
                    model.setBinId(cursor.getInt(binIdIdx));
                    model.setBinNo(cursor.getString(binNoIdx));
                    model.setAisle(cursor.getString(binAisleIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.BIN_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Subprojects {
        public long insertSubprojectData(List<SubProjectMasterModel> subProjectMasterModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (SubProjectMasterModel subProjectMasterModel : subProjectMasterModels) {
                    id = exists(subProjectMasterModel.getSubProjectId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.SUBPROJECT_ID, subProjectMasterModel.getSubProjectId());
                        values.put(DataHelper.SUBPROJECT_NAME, subProjectMasterModel.getName());

                        values.put(DataHelper.SUBPROJECT_PROJECT_ID, subProjectMasterModel.getProjectId());
                        values.put(DataHelper.SUBPROJECT_PROJECT_NAME, subProjectMasterModel.getProjectName());
                        values.put(DataHelper.SUBPROJECT_PLANT_ID, subProjectMasterModel.getPlantId());
                        values.put(DataHelper.SUBPROJECT_PROJECT_TYPE, subProjectMasterModel.getProjectType());
                        values.put(DataHelper.SUBPROJECT_PROJECT_TYPE_ID, subProjectMasterModel.getProjectTypeId());
                        values.put(DataHelper.SUBPROJECT_LOCATION, subProjectMasterModel.getLocation());
                        values.put(DataHelper.SUBPROJECT_REVENUE_TYPE, subProjectMasterModel.getRevenueType());
                        values.put(DataHelper.SUBPROJECT_REVENUE_DETAILS, subProjectMasterModel.getRevenueDetails());
                        values.put(DataHelper.SUBPROJECT_REVENUE_TYPE_NAME, subProjectMasterModel.getRevenueTypeName());
                        values.put(DataHelper.SUBPROJECT_STATE_CODE, subProjectMasterModel.getStateCode());
                        values.put(DataHelper.SUBPROJECT_STATE, subProjectMasterModel.getState());
                        values.put(DataHelper.SUBPROJECT_CITY, subProjectMasterModel.getCity());
                        values.put(DataHelper.SUBPROJECT_USER_ID, subProjectMasterModel.getUserId());
                        values.put(DataHelper.SUBPROJECT_CREATEDBY, subProjectMasterModel.getCreatedBy());
                        values.put(DataHelper.SUBPROJECT_CREATED_TIMESTAMP, subProjectMasterModel.getCreatedTimestamp());
                        values.put(DataHelper.SUBPROJECT_UPDATEDBY, subProjectMasterModel.getUpdatedBy());
                        values.put(DataHelper.SUBPROJECT_UPDATED_TIMESTAMP, subProjectMasterModel.getUpdatedTimestamp());
                        values.put(DataHelper.SUBPROJECT_BRANCH, subProjectMasterModel.getBranch());
                        values.put(DataHelper.SUBPROJECT_BRANCH_NAME, subProjectMasterModel.getBranchName());
                        values.put(DataHelper.SUBPROJECT_SAAC_CODE, subProjectMasterModel.getSaacCode());
                        values.put(DataHelper.SUBPROJECT_DESCRIPTION, subProjectMasterModel.getDescription());
                        values.put(DataHelper.SUBPROJECT_DISCOUNT_PERCENT, subProjectMasterModel.getDiscountPercent());
                        values.put(DataHelper.SUBPROJECT_CUSTOMER_NAME, subProjectMasterModel.getCustomerName());
                        values.put(DataHelper.SUBPROJECT_CUST_ADDR, subProjectMasterModel.getCustAddr());

                        retVal = database.insert(DataHelper.SUBPROJECTS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int subprojectId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.SUBPROJECT_KEY_ID
                        + " FROM " + DataHelper.SUBPROJECTS_TABLE_NAME
                        + " WHERE " + DataHelper.SUBPROJECT_ID
                        + " = " + subprojectId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<SubProjectMasterModel> getSubprojectsData() {
            ArrayList<SubProjectMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.SUBPROJECTS_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int subprojectIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_ID);
                int nameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_NAME);
                int projectIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PROJECT_ID);
                int projectNameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PROJECT_NAME);
                int projectTypeIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PROJECT_TYPE);
                int projectTypeIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PROJECT_TYPE_ID);
                int locationIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_LOCATION);
                int revenueTypeIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_REVENUE_TYPE);
                int revenueDetailsIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_REVENUE_DETAILS);
                int stateCodeIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_STATE_CODE);
                int stateIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_STATE);
                int cityIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_CITY);
                int userIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_USER_ID);
                int createdByIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_CREATEDBY);
                int createdTimestampIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_CREATED_TIMESTAMP);
                int updatedByIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_UPDATEDBY);
                int updatedTimestampIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_UPDATED_TIMESTAMP);
                int branchIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_BRANCH);
                int branchNameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_BRANCH_NAME);
                int saacCodeIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_SAAC_CODE);
                int descriptionIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_DESCRIPTION);

                int discountPercentIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_DISCOUNT_PERCENT);
                int revenueIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_REVENUE_ID);
                int custNameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_CUSTOMER_NAME);

                int revenueTypeNameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_REVENUE_TYPE_NAME);
                int subprojectPlantIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PLANT_ID);

                do {
                    SubProjectMasterModel model = new SubProjectMasterModel();
                    model.setSubProjectId(cursor.getInt(subprojectIdIdx));
                    model.setProjectName(cursor.getString(projectNameIdx));
                    model.setProjectId(cursor.getInt(projectIdIdx));
                    model.setName(cursor.getString(nameIdx));
                    model.setProjectType(cursor.getString(projectTypeIdx));
                    model.setProjectTypeId(cursor.getInt(projectTypeIdIdx));
                    model.setState(cursor.getString(stateIdx));
                    model.setLocation(cursor.getString(locationIdx));
                    model.setRevenueType(cursor.getInt(revenueTypeIdx));
                    model.setRevenueDetails(cursor.getString(revenueDetailsIdx));
                    model.setState(cursor.getString(stateIdx));
                    model.setStateCode(cursor.getString(stateCodeIdx));

                    model.setCity(cursor.getString(cityIdx));
                    model.setUserId(cursor.getInt(userIdIdx));
                    model.setCreatedBy(cursor.getString(createdByIdx));
                    model.setCreatedTimestamp(cursor.getString(createdTimestampIdx));
                    model.setUpdatedBy(cursor.getString(updatedByIdx));
                    model.setUpdatedTimestamp(cursor.getString(updatedTimestampIdx));
                    model.setBranch(cursor.getInt(branchIdx));
                    model.setBranchName(cursor.getString(branchNameIdx));
                    model.setSaacCode(cursor.getString(saacCodeIdx));

                    model.setDescription(cursor.getString(descriptionIdx));
                    model.setDiscountPercent(cursor.getDouble(discountPercentIdx));
                    model.setRevenueId(cursor.getInt(revenueIdIdx));
                    model.setCustomerName(cursor.getString(custNameIdx));
                    model.setRevenueTypeName(cursor.getString(revenueTypeNameIdx));
                    model.setPlantId(cursor.getInt(subprojectPlantIdIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.SUBPROJECTS_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class UnpackingBoxes {
        public UnpackingBox getBox(String vehicleNum, String inwardNum, String masterboxNum, String invoiceNum) {
            String sql = "SELECT * FROM "
                    + DataHelper.UNPACKING_BOX_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterboxNum + "' AND "
                    + DataHelper.UNPACKING_BOX_INVOICE_NUMBER + " = '" + invoiceNum + "'";
            UnpackingBox unpackingBox = null;
            openRead();
            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int qtyIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_QUANTITY);

                int statusIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNumIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNumIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int boxIdIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_BOX_ID);

                int boxStatusIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_BOX_STATUS);

                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);
                int invoiceNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_BOX_INVOICE_NUMBER);

                unpackingBox = new UnpackingBox();
                unpackingBox.setMasterBoxNumber(cursor.getString(masterBoxNumIdx));

                String vehNumber = cursor.getString(vehicleNumIdx);
                if (Utils.isValidString(vehNumber))
                    unpackingBox.setVehicleNumber(vehNumber);

                unpackingBox.setInwardNumber(cursor.getString(inwardNumIdx));
                unpackingBox.setBoxStatus(cursor.getInt(boxStatusIdx));
                unpackingBox.setBoxId(cursor.getInt(boxIdIdx));
                unpackingBox.setQuantity(cursor.getInt(qtyIdx));
                unpackingBox.setStatus(cursor.getInt(statusIdx));
                unpackingBox.setInvoiceNum(cursor.getString(invoiceNumIdx));
            }
            if (cursor != null)
                cursor.close();

            return unpackingBox;
        }

        public void updateStatus(String vehicleNumber, String inwardNumber, String masterBoxNumber, int boxStatus, String invoiceNum) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNPACKING_BOX_TABLE_NAME
                        + " SET " + DataHelper.UNPACKING_BOX_STATUS + " = " + boxStatus
                        + " WHERE "
                        + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNumber
                        + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_MASTER_BOX_NUMBER + "= '" + masterBoxNumber + "' AND "
                        + DataHelper.UNPACKING_BOX_INVOICE_NUMBER + " = '" + invoiceNum + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void deleteUnpackingBoxData(String vehicleNo, String inwardNo, String invoiceNum) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.UNPACKING_BOX_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNo + "' AND "
                        + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNo + "' AND "
                        + DataHelper.UNPACKING_BOX_INVOICE_NUMBER + " = '" + invoiceNum + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(String masterBoxNumber, String vehicleNumber, String inwardNumber, String invoiceNumber) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.UNPACKING_BOX_KEY_ID
                        + " FROM " + DataHelper.UNPACKING_BOX_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_MASTER_BOX_NUMBER
                        + " = '" + masterBoxNumber + "' AND " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNumber + "' AND "
                        + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_BOX_INVOICE_NUMBER + " = '" + invoiceNumber + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertUnpackingBoxData(List<UnpackingBox> unpackingBoxes, String vehicleNum, String inwardNum, String invoiceNum) {
            open();

            long retVal = -1, id = -1;
            try {
                for (UnpackingBox unpackingBox : unpackingBoxes) {
                    id = exists(unpackingBox.getMasterBoxNumber(), vehicleNum, inwardNum, invoiceNum);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.UNPACKING_STATUS, unpackingBox.getStatus());
                        values.put(DataHelper.UNPACKING_QUANTITY, unpackingBox.getQuantity());
                        values.put(DataHelper.UNPACKING_MASTER_BOX_NUMBER, unpackingBox.getMasterBoxNumber());
                        values.put(DataHelper.UNPACKING_VEHICLE_NUMBER, vehicleNum);
                        values.put(DataHelper.UNPACKING_INWARD_NUMBER, inwardNum);
                        values.put(DataHelper.UNPACKING_BOX_INVOICE_NUMBER, invoiceNum);
                        values.put(DataHelper.UNPACKING_BOX_ID, unpackingBox.getBoxId());
                        values.put(DataHelper.UNPACKING_BOX_STATUS, unpackingBox.getBoxStatus());

                        retVal = database.insert(DataHelper.UNPACKING_BOX_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<UnpackingBox> getUnpackingBoxData(String vehicleNum, String inwardNum, String invoiceNum) {
            ArrayList<UnpackingBox> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNPACKING_BOX_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.UNPACKING_BOX_INVOICE_NUMBER + " = '" + invoiceNum + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int statusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.UNPACKING_QUANTITY);
                int boxIdIdx = cursor.getColumnIndex(DataHelper.UNPACKING_BOX_ID);
                int boxStatusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_BOX_STATUS);
                int invoiceNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_BOX_INVOICE_NUMBER);

                do {
                    UnpackingBox model = new UnpackingBox();
                    model.setVehicleNumber(cursor.getString(vehicleNoIdx));
                    model.setInwardNumber(cursor.getString(inwardNoIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setMasterBoxNumber(cursor.getString(masterBoxNumIdx));
                    model.setQuantity(cursor.getInt(qtyIdx));
                    model.setBoxId(cursor.getInt(boxIdIdx));
                    model.setBoxStatus(cursor.getInt(boxStatusIdx));
                    model.setInvoiceNum(cursor.getString(invoiceNumIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.UNPACKING_BOX_TABLE_NAME;
            database.execSQL(query);
            close();
        }
    }

    public class Invoices {
        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.INVOICES_TABLE_NAME;
            database.execSQL(query);
            close();
        }

        public int exists(int subProjectId, String orderNum) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.INVOICES_KEY_ID
                        + " FROM " + DataHelper.INVOICES_TABLE_NAME
                        + " WHERE "
                        + DataHelper.INVOICE_SUBPROJECT_ID + " = " + subProjectId + " AND "
                        + DataHelper.INVOICES_ORDER_NUM + " = '" + orderNum + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertInvoicesData(List<PackingOrderNoModel> packingOrderNoModels, int subprojectId) {
            open();

            long retVal = -1, id = -1;
            try {
                for (PackingOrderNoModel orderNoModel : packingOrderNoModels) {
                    id = exists(subprojectId, orderNoModel.getOrderNo());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.INVOICE_SUBPROJECT_ID, subprojectId);
                        values.put(DataHelper.INVOICES_ORDER_NUM, orderNoModel.getOrderNo());
                        values.put(DataHelper.INVOICES_SUPPLIER_ADDRESS, orderNoModel.getSupplierAddress());
                        values.put(DataHelper.INVOICES_SUPPLIER_NAME, orderNoModel.getSupplierName());

                        retVal = database.insert(DataHelper.INVOICES_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<PackingOrderNoModel> getInvoicesData(int subprojectId) {
            ArrayList<PackingOrderNoModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.INVOICES_TABLE_NAME + " WHERE "
                    + DataHelper.INVOICE_SUBPROJECT_ID + " = " + subprojectId;

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int subprojectIdIdx = cursor.getColumnIndex(DataHelper.INVOICE_SUBPROJECT_ID);
                int supplierNameIdx = cursor.getColumnIndex(DataHelper.INVOICES_SUPPLIER_NAME);
                int orderNumIdx = cursor.getColumnIndex(DataHelper.INVOICES_ORDER_NUM);
                int supplierAddressIdx = cursor.getColumnIndex(DataHelper.INVOICES_SUPPLIER_ADDRESS);

                do {
                    PackingOrderNoModel model = new PackingOrderNoModel();
                    model.setOrderNo(cursor.getString(orderNumIdx));
                    model.setSupplierAddress(cursor.getString(supplierAddressIdx));
                    model.setSupplierName(cursor.getString(supplierNameIdx));
                    model.setSubProjectId(cursor.getInt(subprojectIdIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }
    }

    public class PackingParts {
        public void updatePackingCount(int partId, int subprojectId, String orderNum, int packedCartons,
                                       int remainingCartons, int noOfMasterBoxes) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PACKING_TABLE_NAME
                        + " SET " + DataHelper.PACKING_PACKED_PRIMARY_CARTONS + " = " + packedCartons
                        + ", " + DataHelper.PACKING_REMAINING_PRIMARY_CARTONS + " = " + remainingCartons
                        + ", " + DataHelper.PACKING_NO_OF_MASTER_BOXES + " = " + noOfMasterBoxes
                        + " WHERE " + DataHelper.PACKING_PART_ID + " = '" + partId + "' AND "
                        + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum
                        + "' AND " + DataHelper.PACKING_SUBPROJECT_ID + " = '" + subprojectId + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void deletePackingData(int subProjectId, String orderNum) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.PACKING_TABLE_NAME
                        + " WHERE " + DataHelper.PACKING_SUBPROJECT_ID + " = "
                        + subProjectId + " AND " + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(int partId, int subProjectId, String orderNum) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PACKING_KEY_ID
                        + " FROM " + DataHelper.PACKING_TABLE_NAME
                        + " WHERE " + DataHelper.PACKING_PART_ID
                        + " = " + partId + " AND " + DataHelper.PACKING_SUBPROJECT_ID + " = "
                        + subProjectId + " AND "
                        + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertPackingData(List<PackingModel> packingModels, int subprojectId, String orderNum) {
            open();

            long retVal = -1, id = -1;
            try {
                for (PackingModel packingModel : packingModels) {
                    id = exists(packingModel.getPartId(), subprojectId, orderNum);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PACKING_PART_ID, packingModel.getPartId());
                        values.put(DataHelper.PACKING_PART_NUM, packingModel.getPartNum());
                        values.put(DataHelper.PACKING_QUANTITY, packingModel.getQuantity());
                        values.put(DataHelper.PACKING_PACKED_PRIMARY_CARTONS, packingModel.getPrimarCartonsPacked());
                        values.put(DataHelper.PACKING_REMAINING_PRIMARY_CARTONS, packingModel.getRemainingPrimaryCartons());
                        values.put(DataHelper.PACKING_PICK_STATUS, packingModel.getPickStatus());
                        values.put(DataHelper.PACKING_NO_OF_MASTER_BOXES, packingModel.getNoOfMasterBoxes());
                        values.put(DataHelper.PACKING_SHIP_ADDRESS, packingModel.getShipToAddress());
                        values.put(DataHelper.PACKING_SUBPROJECT_ID, subprojectId);
                        values.put(DataHelper.PACKING_INVOICE_NUMBER, orderNum);
                        values.put(DataHelper.PACKING_WEIGHT, packingModel.getWeight());
                        values.put(DataHelper.PACKING_SUBPROJECT_NAME, packingModel.getSubProjectName());

                        retVal = database.insert(DataHelper.PACKING_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int getNoOfMasterBoxes(int subprojectId, String orderNum) {
            int count = 0;
            int total = 0;

            openRead();
            Cursor cursor = null;
            String query = "SELECT SUM(" + DataHelper.PACKING_NO_OF_MASTER_BOXES + ") FROM " + DataHelper.PACKING_TABLE_NAME + " WHERE "
                    + DataHelper.PACKING_SUBPROJECT_ID + " = " + subprojectId + " AND "
                    + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum + "'";
            String rows = "SELECT COUNT(" + DataHelper.PACKING_NO_OF_MASTER_BOXES + ") FROM " + DataHelper.PACKING_TABLE_NAME + " WHERE "
                    + DataHelper.PACKING_SUBPROJECT_ID + " = " + subprojectId + " AND "
                    + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum + "'";
            Utils.logD("SQL Query " + query);
            try {
                cursor = database.rawQuery(query, null);
                if (cursor.moveToNext())
                    count = cursor.getInt(0);
                cursor = database.rawQuery(rows, null);
                if (cursor.moveToNext())
                    total = cursor.getInt(0);
                if(total != 0) {
                    count = count/total;
                    Utils.logD("Pending Count : " + count);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return count;
        }

        public ArrayList<PackingModel> getPackingData(int subprojectId, String orderNum) {
            ArrayList<PackingModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PACKING_TABLE_NAME + " WHERE "
                    + DataHelper.PACKING_SUBPROJECT_ID + " = " + subprojectId + " AND "
                    + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum + "'";

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.PACKING_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.PACKING_PART_ID);
                int primaryCartonsPackedIdx = cursor.getColumnIndex(DataHelper.PACKING_PACKED_PRIMARY_CARTONS);
                int primaryCartonsRemainingIdx = cursor.getColumnIndex(DataHelper.PACKING_REMAINING_PRIMARY_CARTONS);
                int quantityIdx = cursor.getColumnIndex(DataHelper.PACKING_QUANTITY);
                int pickStatusIdx = cursor.getColumnIndex(DataHelper.PACKING_PICK_STATUS);
                int invoiceNoIdx = cursor.getColumnIndex(DataHelper.PACKING_INVOICE_NUMBER);
                int invoiceIdIdx = cursor.getColumnIndex(DataHelper.PACKING_INVOICE_ID);
                int subprojectIdIdx = cursor.getColumnIndex(DataHelper.PACKING_SUBPROJECT_ID);
                int subProjectNameIdx = cursor.getColumnIndex(DataHelper.PACKING_SUBPROJECT_NAME);
                int noOfMasterBoxesIdx = cursor.getColumnIndex(DataHelper.PACKING_NO_OF_MASTER_BOXES);
                int weightIdx = cursor.getColumnIndex(DataHelper.PACKING_WEIGHT);

                do {
                    PackingModel model = new PackingModel();
                    model.setPartNum(cursor.getString(partNoIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setPrimarCartonsPacked(cursor.getInt(primaryCartonsPackedIdx));
                    model.setRemainingPrimaryCartons(cursor.getInt(primaryCartonsRemainingIdx));
                    model.setPickStatus(cursor.getInt(pickStatusIdx));
                    model.setQuantity(cursor.getInt(quantityIdx));
                    model.setInvoiceNum(cursor.getString(invoiceNoIdx));
                    model.setInvoiceId(cursor.getInt(invoiceIdIdx));
                    model.setSubProjectId(cursor.getInt(subprojectIdIdx));
                    model.setSubProjectName(cursor.getString(subProjectNameIdx));
                    model.setNoOfMasterBoxes(cursor.getInt(noOfMasterBoxesIdx));
                    model.setWeight(cursor.getDouble(weightIdx));

                    models.add(model);
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PACKING_TABLE_NAME;
            database.execSQL(query);
            close();
        }
    }

    public class UnpackingCompletedParts {
        public void updateStatus(int partId, String vehicleNumber, String inwardNumber, String invoiceNumber, int exceptionCount,
                                 int goodBoxes, String masterBoxNumber) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNPACKING_COMPLETED_TABLE_NAME
                        + " SET " + DataHelper.UNPACKING_COMPLETED_NO_OF_EXCEPTIONS + " = " + exceptionCount + ", "
                        + DataHelper.UNPACKING_COMPLETED_PRIMARY_CARTONS_CONTAINED + " = " + goodBoxes
                        + " WHERE "
                        + DataHelper.UNPACKING_COMPLETED_PART_ID + " = " + partId + " AND "
                        + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNumber
                        + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_COMPLETED_INVOICE_NUMBER + " = '" + invoiceNumber + "' AND "
                        + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterBoxNumber + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void deleteUnpackingData(String vehicleNo, String inwardNo, String invoiceNum) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.UNPACKING_COMPLETED_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNo + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNo + "' AND "
                        + DataHelper.UNPACKING_COMPLETED_INVOICE_NUMBER + " = '" + invoiceNum + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(int partId, String vehicleNumber, String inwardNumber, String invoiceNum, String masterBoxNumber) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.UNPACKING_COMPLETED_KEY_ID
                        + " FROM " + DataHelper.UNPACKING_COMPLETED_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_COMPLETED_PART_ID
                        + " = " + partId + " AND " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNumber + "' AND "
                        + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_COMPLETED_INVOICE_NUMBER + " = '" + invoiceNum + "' AND "
                        + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterBoxNumber + "'";
                Utils.logD(selectQuery);
                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertUnpackingData(UnpackingPart unpackingPart, String vehicleNum,
                                        String inwardNum, String invoiceNum, String masterBoxNumber, int status) {
            open();

            long retVal = -1, id = -1;
            try {
                id = exists(unpackingPart.getPartId(), vehicleNum, inwardNum, invoiceNum, masterBoxNumber);
                if (id == -1) {
                    ContentValues values = new ContentValues();
                    values.put(DataHelper.UNPACKING_COMPLETED_PART_ID, unpackingPart.getPartId());
                    values.put(DataHelper.UNPACKING_COMPLETED_PART_NUM, unpackingPart.getPartNo());
                    values.put(DataHelper.UNPACKING_QUANTITY, unpackingPart.getQty());
                    values.put(DataHelper.UNPACKING_COMPLETED_PRIMARY_CARTONS, unpackingPart.getNoOfPrimaryCartons());
                    values.put(DataHelper.UNPACKING_COMPLETED_PRIMARY_CARTONS_CONTAINED, unpackingPart.getNoOfPrimaryCartonsContained());
                    values.put(DataHelper.UNPACKING_COMPLETED_NO_OF_EXCEPTIONS, unpackingPart.getNoOfExceptions());
                    values.put(DataHelper.UNPACKING_MASTER_BOX_NUMBER, unpackingPart.getMasterBoxNum());
                    values.put(DataHelper.UNPACKING_COMPLETED_IS_ACTED_UPON, unpackingPart.getIsActedUpon());
                    values.put(DataHelper.UNPACKING_STATUS, unpackingPart.getStatus());
                    values.put(DataHelper.UNPACKING_VEHICLE_NUMBER, vehicleNum);
                    values.put(DataHelper.UNPACKING_INWARD_NUMBER, inwardNum);
                    values.put(DataHelper.UNPACKING_COMPLETED_INVOICE_NUMBER, invoiceNum);
                    values.put(DataHelper.UNPACKING_COMPLETED_IS_SELECTED, unpackingPart.getIsSelected());
                    values.put(DataHelper.UNPACKING_COMPLETED_PART_STATUS, status);

                    retVal = database.insert(DataHelper.UNPACKING_COMPLETED_TABLE_NAME, null,
                            values);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public UnpackingPart getUnpackingPartData(String vehicleNum, String inwardNum, String invoiceNum, String masterBoxNumber, int partId) {
            UnpackingPart model = null;
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNPACKING_COMPLETED_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.UNPACKING_COMPLETED_INVOICE_NUMBER + " = '" + invoiceNum + "' AND "
                    + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterBoxNumber + "' AND "
                    + DataHelper.UNPACKING_COMPLETED_PART_ID + " = " + partId;

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_PART_ID);
                int primaryCartonsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_PRIMARY_CARTONS);
                int primaryCartonsContainedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_PRIMARY_CARTONS_CONTAINED);
                int exceptionsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_NO_OF_EXCEPTIONS);
                int statusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int isActedUponIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_IS_ACTED_UPON);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.UNPACKING_QUANTITY);
                int isSelectedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_IS_SELECTED);
                int invoiceNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_INVOICE_NUMBER);

                model = new UnpackingPart();
                model.setPartNo(cursor.getString(partNoIdx));
                model.setVehicleNumber(vehicleNum);
                model.setInwardNumber(inwardNum);
                model.setNoOfExceptions(cursor.getInt(exceptionsIdx));
                model.setPartId(cursor.getInt(partIdIdx));
                model.setStatus(cursor.getInt(statusIdx));
                model.setNoOfPrimaryCartons(cursor.getInt(primaryCartonsIdx));
                model.setNoOfPrimaryCartonsContained(cursor.getInt(primaryCartonsContainedIdx));
                model.setIsActedUpon(cursor.getInt(isActedUponIdx));
                model.setMasterBoxNum(cursor.getString(masterBoxNumIdx));
                model.setQty(cursor.getInt(qtyIdx));
                model.setIsSelected(cursor.getInt(isSelectedIdx));
                model.setInvoiceNo(cursor.getString(invoiceNumIdx));
            }

            if (cursor != null)
                cursor.close();
            close();
            return model;
        }

        public ArrayList<UnpackingPart> getUnpackingData(String vehicleNum, String inwardNum, String invoiceNum, int status, String masterBoxNumber) {
            ArrayList<UnpackingPart> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNPACKING_COMPLETED_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.UNPACKING_COMPLETED_INVOICE_NUMBER + " = '" + invoiceNum + "'";

            if (status == 1) {
                sql += " AND " + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterBoxNumber + "'";
            }

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_PART_ID);
                int primaryCartonsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_PRIMARY_CARTONS);
                int primaryCartonsContainedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_PRIMARY_CARTONS_CONTAINED);
                int exceptionsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_NO_OF_EXCEPTIONS);
                int statusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int isActedUponIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_IS_ACTED_UPON);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.UNPACKING_QUANTITY);
                int isSelectedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_IS_SELECTED);
                int invoiceNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_COMPLETED_INVOICE_NUMBER);

                do {
                    UnpackingPart model = new UnpackingPart();
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setVehicleNumber(vehicleNum);
                    model.setInwardNumber(inwardNum);
                    model.setNoOfExceptions(cursor.getInt(exceptionsIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setNoOfPrimaryCartons(cursor.getInt(primaryCartonsIdx));
                    model.setNoOfPrimaryCartonsContained(cursor.getInt(primaryCartonsContainedIdx));
                    model.setIsActedUpon(cursor.getInt(isActedUponIdx));
                    model.setMasterBoxNum(cursor.getString(masterBoxNumIdx));
                    model.setQty(cursor.getInt(qtyIdx));
                    model.setIsSelected(cursor.getInt(isSelectedIdx));
                    model.setInvoiceNo(cursor.getString(invoiceNumIdx));
                    if (status == 0) {
                        if (model.getQty() - (model.getNoOfPrimaryCartonsContained() + model.getNoOfExceptions()) != 0) {
                            models.add(model);
                        }
                    } else {
                        if (model.getNoOfPrimaryCartonsContained() + model.getNoOfExceptions() != 0) {
                            models.add(model);
                        }
                    }
                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.UNPACKING_COMPLETED_TABLE_NAME;
            database.execSQL(query);
            close();
        }
    }

    public class UnpackingParts {

        public void updateStatus(int partId, String vehicleNumber, String inwardNumber, String invoiceNumber, int exceptionCount,
                                 int goodBoxes, String masterBoxNumber) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNPACKING_TABLE_NAME
                        + " SET " + DataHelper.UNPACKING_NO_OF_EXCEPTIONS + " = " + exceptionCount + ", "
                        + DataHelper.UNPACKING_PRIMARY_CARTONS_CONTAINED + " = " + goodBoxes + ", "
                        + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterBoxNumber
                        + "' WHERE "
                        + DataHelper.UNPACKING_PART_ID + " = " + partId + " AND "
                        + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNumber
                        + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_INVOICE_NUMBER + " = '" + invoiceNumber + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void deleteUnpackingData(String vehicleNo, String inwardNo, String invoiceNum) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.UNPACKING_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNo + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNo + "' AND "
                        + DataHelper.UNPACKING_INVOICE_NUMBER + " = '" + invoiceNum + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(int partId, String vehicleNumber, String inwardNumber, String invoiceNum) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.UNPACKING_KEY_ID
                        + " FROM " + DataHelper.UNPACKING_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_PART_ID
                        + " = " + partId + " AND " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNumber + "' AND "
                        + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_INVOICE_NUMBER + " = '" + invoiceNum + "'";
                Utils.logD(selectQuery);
                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertUnpackingData(List<com.apptmyz.wms.data.UnpackingPart> unpackingPartParts, String vehicleNum,
                                        String inwardNum, String invoiceNum, int status) {
            open();

            long retVal = -1, id = -1;
            try {
                for (com.apptmyz.wms.data.UnpackingPart unpackingPart : unpackingPartParts) {
                    id = exists(unpackingPart.getPartId(), vehicleNum, inwardNum, invoiceNum);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.UNPACKING_PART_ID, unpackingPart.getPartId());
                        values.put(DataHelper.UNPACKING_PART_NUM, unpackingPart.getPartNo());
                        values.put(DataHelper.UNPACKING_QUANTITY, unpackingPart.getQty());
                        values.put(DataHelper.UNPACKING_PRIMARY_CARTONS, unpackingPart.getNoOfPrimaryCartons());
                        values.put(DataHelper.UNPACKING_PRIMARY_CARTONS_CONTAINED, unpackingPart.getNoOfPrimaryCartonsContained());
                        values.put(DataHelper.UNPACKING_NO_OF_EXCEPTIONS, unpackingPart.getNoOfExceptions());
                        values.put(DataHelper.UNPACKING_MASTER_BOX_NUMBER, unpackingPart.getMasterBoxNum());
                        values.put(DataHelper.UNPACKING_IS_ACTED_UPON, unpackingPart.getIsActedUpon());
                        values.put(DataHelper.UNPACKING_STATUS, unpackingPart.getStatus());
                        values.put(DataHelper.UNPACKING_VEHICLE_NUMBER, vehicleNum);
                        values.put(DataHelper.UNPACKING_INWARD_NUMBER, inwardNum);
                        values.put(DataHelper.UNPACKING_INVOICE_NUMBER, invoiceNum);
                        values.put(DataHelper.UNPACKING_IS_SELECTED, unpackingPart.getIsSelected());
                        values.put(DataHelper.UNPACKING_PART_STATUS, status);

                        retVal = database.insert(DataHelper.UNPACKING_TABLE_NAME, null,
                                values);
                    } else {
                        UnpackingPart part = getUnpackingPartData(vehicleNum, inwardNum, invoiceNum, unpackingPart.getPartId());
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.UNPACKING_QUANTITY, part.getQty() + unpackingPart.getQty());
                        values.put(DataHelper.UNPACKING_PRIMARY_CARTONS_CONTAINED, part.getNoOfPrimaryCartonsContained() + unpackingPart.getNoOfPrimaryCartonsContained());
                        values.put(DataHelper.UNPACKING_NO_OF_EXCEPTIONS, part.getNoOfExceptions() + unpackingPart.getNoOfExceptions());

                        retVal = database.update(DataHelper.UNPACKING_TABLE_NAME, values,
                                DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                                        + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                                        + DataHelper.UNPACKING_INVOICE_NUMBER + " = '" + invoiceNum + "' AND "
                                        + DataHelper.UNPACKING_PART_ID + " = " + part.getPartId(),
                                null);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public UnpackingPart getUnpackingPartData(String vehicleNum, String inwardNum, String invoiceNum, int partId) {
            UnpackingPart model = null;
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNPACKING_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.UNPACKING_INVOICE_NUMBER + " = '" + invoiceNum + "' AND "
                    + DataHelper.UNPACKING_PART_ID + " = " + partId;

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PART_ID);
                int primaryCartonsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PRIMARY_CARTONS);
                int primaryCartonsContainedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PRIMARY_CARTONS_CONTAINED);
                int exceptionsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_NO_OF_EXCEPTIONS);
                int statusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int isActedUponIdx = cursor.getColumnIndex(DataHelper.UNPACKING_IS_ACTED_UPON);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.UNPACKING_QUANTITY);
                int isSelectedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_IS_SELECTED);
                int invoiceNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INVOICE_NUMBER);

                model = new UnpackingPart();
                model.setPartNo(cursor.getString(partNoIdx));
                model.setVehicleNumber(vehicleNum);
                model.setInwardNumber(inwardNum);
                model.setNoOfExceptions(cursor.getInt(exceptionsIdx));
                model.setPartId(cursor.getInt(partIdIdx));
                model.setStatus(cursor.getInt(statusIdx));
                model.setNoOfPrimaryCartons(cursor.getInt(primaryCartonsIdx));
                model.setNoOfPrimaryCartonsContained(cursor.getInt(primaryCartonsContainedIdx));
                model.setIsActedUpon(cursor.getInt(isActedUponIdx));
                model.setMasterBoxNum(cursor.getString(masterBoxNumIdx));
                model.setQty(cursor.getInt(qtyIdx));
                model.setIsSelected(cursor.getInt(isSelectedIdx));
                model.setInvoiceNo(cursor.getString(invoiceNumIdx));
            }

            if (cursor != null)
                cursor.close();
            close();
            return model;
        }

        public ArrayList<UnpackingPart> getUnpackingData(String vehicleNum, String inwardNum, String invoiceNum, int status, String masterBoxNumber) {
            ArrayList<UnpackingPart> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNPACKING_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.UNPACKING_INVOICE_NUMBER + " = '" + invoiceNum + "'";

            if (status == 1) {
                sql += " AND " + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterBoxNumber + "'";
            }

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PART_ID);
                int primaryCartonsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PRIMARY_CARTONS);
                int primaryCartonsContainedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PRIMARY_CARTONS_CONTAINED);
                int exceptionsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_NO_OF_EXCEPTIONS);
                int statusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int isActedUponIdx = cursor.getColumnIndex(DataHelper.UNPACKING_IS_ACTED_UPON);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.UNPACKING_QUANTITY);
                int isSelectedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_IS_SELECTED);
                int invoiceNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INVOICE_NUMBER);

                do {
                    UnpackingPart model = new UnpackingPart();
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setVehicleNumber(vehicleNum);
                    model.setInwardNumber(inwardNum);
                    model.setNoOfExceptions(cursor.getInt(exceptionsIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setNoOfPrimaryCartons(cursor.getInt(primaryCartonsIdx));
                    model.setNoOfPrimaryCartonsContained(cursor.getInt(primaryCartonsContainedIdx));
                    model.setIsActedUpon(cursor.getInt(isActedUponIdx));
                    model.setMasterBoxNum(cursor.getString(masterBoxNumIdx));
                    model.setQty(cursor.getInt(qtyIdx));
                    model.setIsSelected(cursor.getInt(isSelectedIdx));
                    model.setInvoiceNo(cursor.getString(invoiceNumIdx));
                    if (status == 0) {
                        if (model.getQty() - (model.getNoOfPrimaryCartonsContained() + model.getNoOfExceptions()) > 0) {
                            models.add(model);
                        }
                    } else {
                        if (model.getNoOfPrimaryCartonsContained() + model.getNoOfExceptions() > 0) {
                            models.add(model);
                        }
                    }
                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.UNPACKING_TABLE_NAME;
            database.execSQL(query);
            close();
        }
    }

    public class InboundInspection {
        public void updateStatus(String dataType, String number, String vehicleNumber, String inwardNumber) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.INBOUND_DATA_TABLE_NAME
                        + " SET " + DataHelper.INBOUND_STATUS + " = " + 1
                        + " WHERE "
                        + DataHelper.INBOUND_VEHICLE_NUMBER + " = '" + vehicleNumber
                        + "' AND " + DataHelper.INBOUND_INWARD_NUMBER + " = '" + inwardNumber + "' AND ";
                if (dataType.equals("DA")) {
                    updateSql += DataHelper.INBOUND_DA_NO + " = '" + number + "'";
                } else if (dataType.equals("IN")) {
                    updateSql += DataHelper.INBOUND_INVOICE_NO + " = '" + number + "'";
                } else if (dataType.equals("P")) {
                    updateSql += DataHelper.INBOUND_PART_NUM + " = '" + number + "'";
                } else if (dataType.equals("MB")) {
                    updateSql += DataHelper.INBOUND_MASTER_BOX_NO + " = '" + number + "'";
                }
                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void deleteInspectionData(String vehicleNo, String inwardNo) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.INBOUND_DATA_TABLE_NAME
                        + " WHERE " + DataHelper.INBOUND_VEHICLE_NUMBER + " = '"
                        + vehicleNo + "' AND " + DataHelper.INBOUND_INWARD_NUMBER + " = '" + inwardNo + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(String dataType, String number, String vehicleNumber, String inwardNumber) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.INBOUND_KEY_ID
                        + " FROM " + DataHelper.INBOUND_DATA_TABLE_NAME
                        + " WHERE "
                        + DataHelper.INBOUND_VEHICLE_NUMBER + " = '"
                        + vehicleNumber.toUpperCase() + "' AND "
                        + DataHelper.INBOUND_INWARD_NUMBER + " = '" + inwardNumber + "' AND ";

                if (dataType.equals("DA")) {
                    selectQuery += DataHelper.INBOUND_DA_NO + " = '" + number + "'";
                } else if (dataType.equals("IN")) {
                    selectQuery += DataHelper.INBOUND_INVOICE_NO + " = '" + number + "'";
                } else if (dataType.equals("P")) {
                    selectQuery += DataHelper.INBOUND_PART_NUM + " = '" + number + "'";
                } else if (dataType.equals("MB")) {
                    selectQuery += DataHelper.INBOUND_MASTER_BOX_NO + " = '" + number + "'";
                }

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public long insertInboundInspectionData(List<InspectionPart> inspectionParts, String dataType, String vehicleNum, String inwardNum) {
            open();

            long retVal = -1, id = -1;
            try {
                for (InspectionPart inspectionPart : inspectionParts) {
                    id = exists(inspectionPart.getDataType(), Utils.getNumber(inspectionPart), vehicleNum, inwardNum);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.INBOUND_PART_ID, inspectionPart.getPartId());
                        values.put(DataHelper.INBOUND_PART_NUM, inspectionPart.getPartNo());
                        values.put(DataHelper.INBOUND_QUANTITY, inspectionPart.getQty());
                        values.put(DataHelper.INBOUND_NO_OF_MASTER_CARTONS, inspectionPart.getNoOfMasterCartons());
                        values.put(DataHelper.INBOUND_NO_OF_EXCEPTIONS, inspectionPart.getNoOfExceptions());
                        values.put(DataHelper.INBOUND_STATUS, inspectionPart.getStatus());
                        values.put(DataHelper.INBOUND_VEHICLE_NUMBER, vehicleNum);
                        values.put(DataHelper.INBOUND_INWARD_NUMBER, inwardNum);
                        values.put(DataHelper.INBOUND_DATATYPE, dataType);
                        values.put(DataHelper.INBOUND_DA_NO, inspectionPart.getDaNo());
                        values.put(DataHelper.INBOUND_INVOICE_NO, inspectionPart.getInvoiceNo());
                        values.put(DataHelper.INBOUND_MASTER_BOX_NO, inspectionPart.getMasterBoxNo());

                        retVal = database.insert(DataHelper.INBOUND_DATA_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<InspectionPart> getInboundInspectionData(String vehicleNum, String inwardNum) {
            ArrayList<InspectionPart> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.INBOUND_DATA_TABLE_NAME
                    + " WHERE "
                    + DataHelper.INBOUND_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.INBOUND_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.INBOUND_STATUS + " = 0";

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.INBOUND_PART_ID);
                int masterCartonsIdx = cursor.getColumnIndex(DataHelper.INBOUND_NO_OF_MASTER_CARTONS);
                int exceptionsIdx = cursor.getColumnIndex(DataHelper.INBOUND_NO_OF_EXCEPTIONS);
                int statusIdx = cursor.getColumnIndex(DataHelper.INBOUND_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_INWARD_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.INBOUND_QUANTITY);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_DATATYPE);
                int daNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_DA_NO);
                int invoiceNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_INVOICE_NO);
                int masterBoxNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_MASTER_BOX_NO);

                do {
                    InspectionPart model = new InspectionPart();
                    model.setPartNo(cursor.getString(partNoIdx));
                    if (Utils.isValidString(cursor.getString(vehicleNoIdx)))
                        model.setVehicleNumber(cursor.getString(vehicleNoIdx).toUpperCase());
                    model.setInwardNumber(cursor.getString(inwardNoIdx));
                    model.setNoOfExceptions(cursor.getInt(exceptionsIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setNoOfMasterCartons(cursor.getInt(masterCartonsIdx));
                    model.setQty(cursor.getInt(qtyIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));
                    model.setDaNo(cursor.getString(daNoIdx));
                    model.setInvoiceNo(cursor.getString(invoiceNoIdx));
                    model.setMasterBoxNo(cursor.getString(masterBoxNoIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.INBOUND_DATA_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Exceptions {
        public void deleteExceptionsdata(String vehicleNo) {
            open();
            try {

                String deleteSql = "DELETE  FROM " + DataHelper.EXCEPTION_TABLE_NAME
                        + " WHERE " + DataHelper.EXCEPTION_VEHICLE_NUM + " = '"
                        + vehicleNo + "'";
                database.execSQL(deleteSql);

            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public long insertException(Context context, String dataType, ExceptionModel exceptionModel, String partNum, String vehicleNum) {
            open();
            long retVal = -1;
            PartsScanningScreen partsScanningScreen = (PartsScanningScreen) context;

            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.EXCEPTION_TYPE_ID, exceptionModel.getExcepType());
                values.put(DataHelper.EXCEPTION_COUNT, exceptionModel.getExceptionCount());
                values.put(DataHelper.EXCEPTION_PART_NUM, partNum);
                values.put(DataHelper.EXCEPTION_VEHICLE_NUM, vehicleNum);
                values.put(DataHelper.EXCEPTION_REASON, exceptionModel.getExcepDesc());
                values.put(DataHelper.EXCEPTION_SCAN_TIME, exceptionModel.getScanTime());
                values.put(DataHelper.EXCEPTION_TRUCK_IMAGE, gson.toJson(exceptionModel.getImgBase64s()));
                values.put(DataHelper.EXCEPTION_DATATYPE, dataType);

                retVal = database.insert(DataHelper.EXCEPTION_TABLE_NAME, null,
                        values);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            partsScanningScreen.resetAllFragmentData();
            return retVal;
        }

        public int getProductExceptionCount(String partNo, String dataType, String vehicleNo) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_PART_NUM + " = '" + partNo + "' AND "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " = " + 12;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public int getDamagedExceptionCount(String partNo, String dataType, String vehicleNo) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_PART_NUM + " = '" + partNo + "' AND "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " = " + 13;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public int getShortageCount(String number, String dataType, String vehicleNo) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_PART_NUM + " = '" + number + "' AND "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " == " + 11;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }


        public int getExceptionCount(String number, String dataType, String vehicleNo) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_PART_NUM + " = '" + number + "' AND "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "'"
                    + " AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " != " + 10;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public int getExtrasCount(String partNo, String dataType, String vehicleNo) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_PART_NUM + " = '" + partNo + "' AND "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " == " + 10;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public ArrayList<ExceptionModel> getExceptions(String vehicleNum) {
            ArrayList<ExceptionModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNum + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PART_NUM);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_VEHICLE_NUM);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_SCAN_TIME);
                int exceptionCountIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_COUNT);
                int exceptionTypeIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int exceptionReasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_REASON);
                int imageIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TRUCK_IMAGE);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_DATATYPE);

                do {
                    ExceptionModel model = new ExceptionModel();
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setVehicleNum(cursor.getString(vehicleNoIdx));
                    model.setExcepDesc(cursor.getString(exceptionReasonIdx));
                    model.setExceptionCount(cursor.getInt(exceptionCountIdx));
                    String base64sStr = cursor.getString(imageIdx);
                    Type token = new TypeToken<List<String>>() {
                    }.getType();
                    model.setImgBase64s((List<String>) gson.fromJson(base64sStr, token));
                    model.setExcepType(cursor.getInt(exceptionTypeIdIdx));
                    model.setScanTime(cursor.getString(scanTimeIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public ArrayList<ExceptionModel> getExceptions(String partNo, String dataType, String vehicleNum) {
            ArrayList<ExceptionModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_PART_NUM + " = '" + partNo + "' AND "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNum + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PART_NUM);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_VEHICLE_NUM);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_SCAN_TIME);
                int exceptionCountIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_COUNT);
                int exceptionTypeIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int exceptionReasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_REASON);
                int imageIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TRUCK_IMAGE);

                do {
                    ExceptionModel model = new ExceptionModel();
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setVehicleNum(cursor.getString(vehicleNoIdx));
                    model.setExcepDesc(cursor.getString(exceptionReasonIdx));
                    model.setExceptionCount(cursor.getInt(exceptionCountIdx));
                    String base64sStr = cursor.getString(imageIdx);
                    Type token = new TypeToken<List<String>>() {
                    }.getType();
                    model.setImgBase64s((List<String>) gson.fromJson(base64sStr, token));
                    model.setExcepType(cursor.getInt(exceptionTypeIdIdx));
                    model.setScanTime(cursor.getString(scanTimeIdx));
                    model.setDataType(dataType);

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public ArrayList<ExceptionModel> getExceptions() {
            ArrayList<ExceptionModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PART_NUM);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_VEHICLE_NUM);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_SCAN_TIME);
                int exceptionCountIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_COUNT);
                int exceptionTypeIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int exceptionReasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_REASON);
                int imageIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TRUCK_IMAGE);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_DATATYPE);

                do {
                    ExceptionModel model = new ExceptionModel();
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setVehicleNum(cursor.getString(vehicleNoIdx));
                    model.setExcepDesc(cursor.getString(exceptionReasonIdx));
                    model.setExceptionCount(cursor.getInt(exceptionCountIdx));
                    String base64sStr = cursor.getString(imageIdx);
                    Type token = new TypeToken<List<String>>() {
                    }.getType();
                    model.setImgBase64s((List<String>) gson.fromJson(base64sStr, token));
                    model.setExcepType(cursor.getInt(exceptionTypeIdIdx));
                    model.setScanTime(cursor.getString(scanTimeIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.EXCEPTION_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Nodes {
        public long insertNodeData(List<NodeMasterModel> nodeMasterModels, int subprojectId) {
            open();
            long retVal = -1, id = -1;
            try {
                for (NodeMasterModel nodeMasterModel : nodeMasterModels) {
                    id = exists(subprojectId, nodeMasterModel.getNodeId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.NODE_SUBPROJECT_ID, subprojectId);
                        values.put(DataHelper.NODE_ID, nodeMasterModel.getNodeId());

                        values.put(DataHelper.NODE_NAME, nodeMasterModel.getNodeName());
                        values.put(DataHelper.NODE_TYPE, nodeMasterModel.getNodeType());
                        values.put(DataHelper.NODE_TYPE_ID, nodeMasterModel.getNodeTypeId());
                        values.put(DataHelper.NODE_CUST_CODE, nodeMasterModel.getCustCode());
                        values.put(DataHelper.NODE_LAT, nodeMasterModel.getLat());
                        values.put(DataHelper.NODE_LNG, nodeMasterModel.getLng());
                        values.put(DataHelper.NODE_ADDRESS1, nodeMasterModel.getAddress1());
                        values.put(DataHelper.NODE_ADDRESS2, nodeMasterModel.getAddress2());
                        values.put(DataHelper.NODE_ADDRESS3, nodeMasterModel.getAddress3());
                        values.put(DataHelper.NODE_CITY, nodeMasterModel.getCity());
                        values.put(DataHelper.NODE_STATE, nodeMasterModel.getState());
                        values.put(DataHelper.NODE_STATE_NAME, nodeMasterModel.getStateName());
                        values.put(DataHelper.NODE_PINCODE, nodeMasterModel.getPincode());
                        values.put(DataHelper.NODE_MOBILE, nodeMasterModel.getMobileNo());
                        values.put(DataHelper.NODE_EMAIL, nodeMasterModel.getEmailId());
                        values.put(DataHelper.NODE_CREATED_BY, nodeMasterModel.getCreatedBy());
                        values.put(DataHelper.NODE_CREATED_TIMESTAMP, nodeMasterModel.getCreatedTimestamp());
                        values.put(DataHelper.NODE_UPDATED_BY, nodeMasterModel.getUpdatedBy());
                        values.put(DataHelper.NODE_UPDATED_TIMESTAMP, nodeMasterModel.getUpdatedTimestamp());
                        values.put(DataHelper.NODE_SEQUENCE, nodeMasterModel.getSequence());
                        values.put(DataHelper.NODE_IS_CAPTURED, nodeMasterModel.isCaptured());
                        values.put(DataHelper.NODE_DLY_STATUS, nodeMasterModel.getDlyStatus());

                        retVal = database.insert(DataHelper.NODE_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int subprojectId, int nodeId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.NODE_KEY_ID
                        + " FROM " + DataHelper.NODE_TABLE_NAME
                        + " WHERE " + DataHelper.NODE_SUBPROJECT_ID
                        + " = " + subprojectId + " AND " + DataHelper.NODE_ID + " = " + nodeId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<NodeMasterModel> getNodesData(int subprojectId) {
            ArrayList<NodeMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.NODE_TABLE_NAME + " WHERE " + DataHelper.NODE_SUBPROJECT_ID
                    + " = " + subprojectId;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int docketNodeIdIdx = cursor.getColumnIndex(DataHelper.DOCKET_NODE_ID);
                int nodeIdIdx = cursor.getColumnIndex(DataHelper.NODE_ID);
                int nodeTypeIdIdx = cursor.getColumnIndex(DataHelper.NODE_TYPE_ID);
                int nodeTypeIdx = cursor.getColumnIndex(DataHelper.NODE_TYPE);
                int nodeNameIdx = cursor.getColumnIndex(DataHelper.NODE_NAME);
                int custCodeIdx = cursor.getColumnIndex(DataHelper.NODE_CUST_CODE);
                int latIdx = cursor.getColumnIndex(DataHelper.NODE_LAT);
                int lngIdx = cursor.getColumnIndex(DataHelper.NODE_LNG);
                int addres1Idx = cursor.getColumnIndex(DataHelper.NODE_ADDRESS1);
                int addres2Idx = cursor.getColumnIndex(DataHelper.NODE_ADDRESS2);
                int addres3Idx = cursor.getColumnIndex(DataHelper.NODE_ADDRESS3);
                int stateIdx = cursor.getColumnIndex(DataHelper.NODE_STATE);
                int stateNameIdx = cursor.getColumnIndex(DataHelper.NODE_STATE_NAME);
                int cityIdx = cursor.getColumnIndex(DataHelper.NODE_CITY);
                int pincodeIdx = cursor.getColumnIndex(DataHelper.NODE_PINCODE);
                int mobileIdx = cursor.getColumnIndex(DataHelper.NODE_MOBILE);
                int emailIdx = cursor.getColumnIndex(DataHelper.NODE_EMAIL);
                int createdByIdx = cursor.getColumnIndex(DataHelper.NODE_CREATED_BY);
                int createdTimestampIdx = cursor.getColumnIndex(DataHelper.NODE_CREATED_TIMESTAMP);
                int updatedByIdx = cursor.getColumnIndex(DataHelper.NODE_UPDATED_BY);
                int updatedTimestampIdx = cursor.getColumnIndex(DataHelper.NODE_UPDATED_TIMESTAMP);

                int sequenceIdx = cursor.getColumnIndex(DataHelper.NODE_SEQUENCE);
                int dlystatusIdx = cursor.getColumnIndex(DataHelper.NODE_DLY_STATUS);
                int isCapturedIdx = cursor.getColumnIndex(DataHelper.NODE_IS_CAPTURED);

                do {
                    NodeMasterModel model = new NodeMasterModel();
                    model.setDktNodeId(cursor.getInt(docketNodeIdIdx));
                    model.setNodeId(cursor.getInt(nodeIdIdx));
                    model.setNodeName(cursor.getString(nodeNameIdx));
                    model.setAddress1(cursor.getString(addres1Idx));
                    model.setAddress2(cursor.getString(addres2Idx));
                    model.setAddress3(cursor.getString(addres3Idx));
                    model.setState(cursor.getString(stateIdx));
                    model.setStateName(cursor.getString(stateNameIdx));
                    model.setPincode(cursor.getInt(pincodeIdx));
                    model.setMobileNo(cursor.getString(mobileIdx));
                    model.setEmailId(cursor.getString(emailIdx));
                    String isCapturedStr = cursor.getString(isCapturedIdx);
                    if (Utils.isValidString(isCapturedStr) && isCapturedStr.equals("true"))
                        model.setCaptured(true);

                    model.setCity(cursor.getString(cityIdx));
                    model.setLat(cursor.getDouble(latIdx));
                    model.setLng(cursor.getDouble(lngIdx));
                    model.setCreatedBy(cursor.getString(createdByIdx));
                    model.setCreatedTimestamp(cursor.getString(createdTimestampIdx));
                    model.setUpdatedBy(cursor.getString(updatedByIdx));
                    model.setUpdatedTimestamp(cursor.getString(updatedTimestampIdx));
                    model.setCustCode(cursor.getString(custCodeIdx));
                    model.setDlyStatus(cursor.getInt(dlystatusIdx));
                    model.setSequence(cursor.getInt(sequenceIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.NODE_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class PickListParts {
        public void updateAsSubmitted(String type, String data, int id) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PICKLIST_PARTS_TABLE_NAME
                        + " SET " + DataHelper.PICKLIST_IS_PICKED + " = " + 1
                        + " WHERE " + DataHelper.PICKLIST_TYPE + " = '" + type + "'";
                updateSql += " AND "
                        + DataHelper.PICKLIST_DATA_ID + " = '" + data + "'";
                updateSql += " AND " + DataHelper.PICKLIST_UNIQUE_ID
                        + " = " + id;

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateBoxCount(int boxCount, String type, String data, int id) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PICKLIST_PARTS_TABLE_NAME
                        + " SET " + DataHelper.PICKLIST_TOTAL_BOXES + " = " + boxCount
                        + " WHERE " + DataHelper.PICKLIST_TYPE + " = '" + type + "'";
                updateSql += " AND "
                        + DataHelper.PICKLIST_DATA_ID + " = '" + data + "'";
                updateSql += " AND " + DataHelper.PICKLIST_UNIQUE_ID
                        + " = " + id;

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public long insertPickListData(List<WmsPartInvoiceModel> wmsPartInvoiceModels, String type, String data) {
            open();
            long retVal = -1, id = -1;
            try {
                for (WmsPartInvoiceModel wmsPartInvoiceModel : wmsPartInvoiceModels) {
                    id = exists(wmsPartInvoiceModel.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PICKLIST_UNIQUE_ID, wmsPartInvoiceModel.getId());
                        values.put(DataHelper.PICKLIST_DATA_ID, data);
                        values.put(DataHelper.PICKLIST_TYPE, type);
                        values.put(DataHelper.PICKLIST_PART_ID, wmsPartInvoiceModel.getPartId());
                        values.put(DataHelper.PICKLIST_PART_NO, wmsPartInvoiceModel.getPartNo());
                        values.put(DataHelper.PICKLIST_IS_PICKED, wmsPartInvoiceModel.getIsPicked());
                        values.put(DataHelper.PICKLIST_TOTAL_BOXES, wmsPartInvoiceModel.getTotalBoxes());
                        values.put(DataHelper.PICKLIST_TOTAL_QTY, wmsPartInvoiceModel.getTotalQty());
                        values.put(DataHelper.PICKLIST_BIN_NO, wmsPartInvoiceModel.getBinNo());
                        values.put(DataHelper.PICKLIST_SCAN_TIME, wmsPartInvoiceModel.getScanTime());
                        values.put(DataHelper.PICKLIST_XDFLAG, wmsPartInvoiceModel.getXdFlag());
                        values.put(DataHelper.PICKLIST_NEXT_DEST, wmsPartInvoiceModel.getNextDest());
                        values.put(DataHelper.PICKLIST_EXCEP_TYPE, wmsPartInvoiceModel.getExcepType());
                        values.put(DataHelper.PICKLIST_ORDER_NO, wmsPartInvoiceModel.getOrderNo());
                        values.put(DataHelper.PICKLIST_BIN_ID, wmsPartInvoiceModel.getBinId());
                        values.put(DataHelper.PICKLIST_INVOICE_LIST, gson.toJson(wmsPartInvoiceModel.getInvoiceList()));
                        values.put(DataHelper.PICKLIST_ID, wmsPartInvoiceModel.getId());
                        values.put(DataHelper.PICKLIST_DATATYPE, wmsPartInvoiceModel.getDataType());
                        values.put(DataHelper.PICKLIST_DA_NUMBER, wmsPartInvoiceModel.getDaNo());
                        values.put(DataHelper.PICKLIST_CUST_CODE, wmsPartInvoiceModel.getCustCode());
                        values.put(DataHelper.PICKLIST_CUST_PART_NUM, wmsPartInvoiceModel.getCustPartNo());
                        values.put(DataHelper.PICKLIST_SUPPLIER, wmsPartInvoiceModel.getSupplier());
                        values.put(DataHelper.PICKLIST_BARCODE_LENGTH, wmsPartInvoiceModel.getBarcodeLength());
                        values.put(DataHelper.PICKLIST_BARCODE_MIN_LENGTH, wmsPartInvoiceModel.getBarcodeMinLength());
                        values.put(DataHelper.PICKLIST_BARCODE_MAX_LENGTH, wmsPartInvoiceModel.getBarcodeMaxLength());
                        values.put(DataHelper.PICKLIST_BATCH_NO, wmsPartInvoiceModel.getBatchNo());

                        retVal = database.insert(DataHelper.PICKLIST_PARTS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int uniqueId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PICKLIST_PART_KEY_ID
                        + " FROM " + DataHelper.PICKLIST_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.PICKLIST_UNIQUE_ID + " = " + uniqueId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<WmsPartInvoiceModel> getPickListData(String type, String data, int status) {
            ArrayList<WmsPartInvoiceModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PICKLIST_PARTS_TABLE_NAME;
            sql += " WHERE " + DataHelper.PICKLIST_DATA_ID + " = '" + data + "'";

            sql += " AND " + DataHelper.PICKLIST_IS_PICKED + " = " + status;

            Utils.logD(sql);
            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int uniqueIdIdx = cursor.getColumnIndex(DataHelper.PICKLIST_UNIQUE_ID);
                int partIdIdx = cursor.getColumnIndex(DataHelper.PICKLIST_PART_ID);
                int partNumIdx = cursor.getColumnIndex(DataHelper.PICKLIST_PART_NO);
                int totalBoxesIdx = cursor.getColumnIndex(DataHelper.PICKLIST_TOTAL_BOXES);
                int totalQtyIdx = cursor.getColumnIndex(DataHelper.PICKLIST_TOTAL_QTY);
                int isPickedIdx = cursor.getColumnIndex(DataHelper.PICKLIST_IS_PICKED);
                int binNumIdx = cursor.getColumnIndex(DataHelper.PICKLIST_BIN_NO);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.PICKLIST_SCAN_TIME);
                int xdFlagIdx = cursor.getColumnIndex(DataHelper.PICKLIST_XDFLAG);
                int nextDestIdx = cursor.getColumnIndex(DataHelper.PICKLIST_NEXT_DEST);
                int excepTypeIdx = cursor.getColumnIndex(DataHelper.PICKLIST_EXCEP_TYPE);
                int orderNoIdx = cursor.getColumnIndex(DataHelper.PICKLIST_ORDER_NO);
                int binIdIdx = cursor.getColumnIndex(DataHelper.PICKLIST_BIN_ID);
                int invoicesIdx = cursor.getColumnIndex(DataHelper.PICKLIST_INVOICE_LIST);
                int idIdx = cursor.getColumnIndex(DataHelper.PICKLIST_ID);
                int datatypeIdx = cursor.getColumnIndex(DataHelper.PICKLIST_DATATYPE);
                int supplierIdx = cursor.getColumnIndex(DataHelper.PICKLIST_SUPPLIER);
                int custPartNumIdx = cursor.getColumnIndex(DataHelper.PICKLIST_CUST_PART_NUM);
                int custCodeIdx = cursor.getColumnIndex(DataHelper.PICKLIST_CUST_CODE);
                int daNumIdx = cursor.getColumnIndex(DataHelper.PICKLIST_DA_NUMBER);
                int barcodeLengthIdx = cursor.getColumnIndex(DataHelper.PICKLIST_BARCODE_LENGTH);
                int barcodeMaxLengthIdx = cursor.getColumnIndex(DataHelper.PICKLIST_BARCODE_MAX_LENGTH);
                int barcodeMinLengthIdx = cursor.getColumnIndex(DataHelper.PICKLIST_BARCODE_MIN_LENGTH);
                int batchnoIdx=cursor.getColumnIndex(DataHelper.PICKLIST_BATCH_NO);

                do {
                    WmsPartInvoiceModel model = new WmsPartInvoiceModel();
                    model.setId(cursor.getInt(uniqueIdIdx));
                    model.setPartNo(cursor.getString(partNumIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setTotalBoxes(cursor.getInt(totalBoxesIdx));
                    model.setTotalQty(cursor.getInt(totalQtyIdx));
                    model.setIsPicked(cursor.getInt(isPickedIdx));
                    model.setBinNo(cursor.getString(binNumIdx));
                    model.setScanTime(cursor.getString(scanTimeIdx));
                    model.setXdFlag(cursor.getInt(xdFlagIdx));
                    model.setNextDest(cursor.getString(nextDestIdx));
                    model.setExcepType(cursor.getInt(excepTypeIdx));
                    model.setOrderNo(cursor.getString(orderNoIdx));
                    model.setBinId(cursor.getInt(binIdIdx));
                    model.setBatchNo(cursor.getString(batchnoIdx));
                    String invoicesStr = cursor.getString(invoicesIdx);
                    Type token = new TypeToken<List<InvoiceModel>>() {
                    }.getType();
                    model.setInvoiceList((List<InvoiceModel>) gson.fromJson(invoicesStr, token));
                    model.setId(cursor.getInt(idIdx));
                    model.setSupplier(cursor.getString(supplierIdx));
                    model.setDataType(cursor.getString(datatypeIdx));
                    model.setCustPartNo(cursor.getString(custPartNumIdx));
                    model.setCustCode(cursor.getString(custCodeIdx));
                    model.setDaNo(cursor.getString(daNumIdx));
                    model.setBarcodeLength(cursor.getInt(barcodeLengthIdx));
                    model.setBarcodeMaxLength(cursor.getInt(barcodeMaxLengthIdx));
                    model.setBarcodeMinLength(cursor.getInt(barcodeMinLengthIdx));
                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void delete(String type, String data) {
            open();
            String query = "DELETE FROM " + DataHelper.PICKLIST_PARTS_TABLE_NAME;
            query += " WHERE " + DataHelper.PICKLIST_DATA_ID + " = '" + data + "'";
            query += " AND " + DataHelper.PICKLIST_IS_PICKED + " = " + 0;
            database.execSQL(query);

            close();
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PICKLIST_PARTS_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    /********************** Unloading parts ****************/

    public class UnloadingParts {
        private Context context;
        private PartsScanningScreen partsScanningScreen;
        private ProgressDialog pDialog;
        private boolean isProgress = false;
        private UnloadingResponse unloadingResponse;
        private String vehicleNo;

        private void showProgressDialog() {
            if (pDialog == null) {
                pDialog = new ProgressDialog(context);
                pDialog.setMessage(context
                        .getString(R.string.progress_dialog_title));
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        public void updateExcepType(String excepType, String barcode) {
            String scanTime = Utils.getScanTime();
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " SET "
                        + DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE + " = '" + excepType + "', "
                        + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " = '" + Constants.TRUE + "'"
                        + " WHERE " + DataHelper.UNLOADING_PARTS_INVOICE_BARCODE + " ='" + barcode + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateScanCount(Context context, String barcode, int scannedCount, int scannedInvoiceQty) {
            String scanTime = Utils.getScanTime();
            PartsScanningScreen partsScanningScreen = (PartsScanningScreen) context;
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " SET "
                        + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1, "
                        + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " = '" + Constants.FALSE + "' , "
                        + DataHelper.UNLOADING_PARTS_INVOICE_SCANNED_COUNT + " = " + scannedInvoiceQty + " , "
                        + DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED + " = " + scannedCount + ", "
                        + DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME + " = '" + scanTime + "'"
                        + " WHERE " + DataHelper.UNLOADING_PARTS_INVOICE_BARCODE + " ='" + barcode + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            partsScanningScreen.resetAllFragmentData();
        }

        public int exists(String number, String dataType) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.UNLOADING_PARTS_COLUMN_ID
                        + " FROM " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE
                        + " = '" + dataType + "' AND ";

                if (dataType.equals("DA")) {
                    selectQuery += DataHelper.UNLOADING_PARTS_COLUMN_DA_NO + " = '" + number + "'";
                } else if (dataType.equals("IN")) {
                    selectQuery += DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO + " = '" + number + "'";
                } else if (dataType.equals("P")) {
                    selectQuery += DataHelper.UNLOADING_PARTS_COLUMN_PART_NO + " = '" + number + "'";
                } else if (dataType.equals("MB")) {
                    selectQuery += DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM + " = '" + number + "'";
                }

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public void saveData(Context mContext, UnloadingResponse response, String vehicleNumber) {
            if (!isProgress) {
                isProgress = true;
                context = mContext;
                partsScanningScreen = (PartsScanningScreen) context;
                unloadingResponse = response;
                vehicleNo = vehicleNumber;
                new Save().execute();
            }
        }

        public boolean getPartsData(String vehicleNo) {
            Boolean isExists = false;
            Cursor cursor = null;
            openRead();
            String selectQuery = "SELECT * " + "FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehicleNo
                    + "'";
            Utils.logD(selectQuery);
            try {
                cursor = database.rawQuery(selectQuery, null);

                if (cursor.getCount() > 0)
                    isExists = true;
                else isExists = false;

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
            }

            return isExists;
        }

        public long cacheSheetData(Context mContext,
                                   UnloadingResponse response, String vehicleNo) {


            int numberOfRecords = 0;
            boolean isExists = false;

            try {

                open();
                database.beginTransaction();

                Cursor cursor = null;
                HashMap<String, Integer> sheetDataCached = new HashMap<String, Integer>();
                HashMap<String, Integer> sortOrder = new HashMap<String, Integer>();


                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                ContentValues values = new ContentValues();
                for (ProductModel data : response.getData().getPartList()) {
                    String dataType = response.getData().getDataType();
                    String number = Utils.getNumber(data, dataType);
                    long retVal = -1;
                    if (Utils.isValidString(number)) {
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_DA_NO, data.getDaNo());
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE, response.getData().getDataType());
                        values.put(DataHelper.UNLOADING_PARTS_BARCODE_LENGTH, response.getData().getBarcodeLength());
                        values.put(DataHelper.UNLOADING_PARTS_BARCODE_MIN_LENGTH, response.getData().getBarcodeMinLength());
                        values.put(DataHelper.UNLOADING_PARTS_BARCODE_MAX_LENGTH, response.getData().getBarcodeMaxLength());
                        String inwardNo = response.getData().getInwardNo();
                        if (Utils.isValidString(inwardNo))
                            values.put(DataHelper.UNLOADING_PARTS_INWARD_NO, inwardNo);
                        else if (Utils.isValidString(Globals.inwardNo))
                            values.put(DataHelper.UNLOADING_PARTS_INWARD_NO, Globals.inwardNo);

                        values.put(DataHelper.UNLOADING_PARTS_VEHICLE_NO,
                                vehicleNo);

                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_PART_NO,
                                data.getPartNo());

                        values.put(DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM, data.getMasterBoxNo());
                        values.put(DataHelper.UNLOADING_PARTS_INVOICES_LIST, gson.toJson(data.getInvoiceList()));

                        int partId = data.getPartId();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_PART_ID,
                                partId);

                        int tot_boxes = data.getTotalBoxes();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES, tot_boxes);

                        int tot_qty = data.getTotalQty();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY, tot_qty);


                        String invoiceNo = data.getInvoiceNo();
                        if (Utils.isValidString(invoiceNo))
                            values.put(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO, invoiceNo);


                        int box_count = data.getTotalBoxes();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_BOX_COUNT, box_count);

                        int invoice_box_count = data.getTotalQty();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_QTY_INVOICE, invoice_box_count);

                        if (Utils.isValidString(data.getBarcode())) {
                            values.put(DataHelper.UNLOADING_PARTS_INVOICE_BARCODE, data.getBarcode().toLowerCase());
                        }

                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED, 0); //for now the scanned count is 0 in future the count should come from server if any boxes are scanned

                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_STATUS, 0);

                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED, Constants.FALSE); //by default the scan completed is false

                        retVal = database.insert(
                                DataHelper.UNLOADING_PARTS_TABLE_NAME, null, values);
                        if (retVal > -1)
                            numberOfRecords++;
                    }
                }
                database.setTransactionSuccessful();
                database.endTransaction();

                Utils.logD("Records : " + numberOfRecords);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                close();
            }
            return numberOfRecords;
        }

        public void deleteUnloadingdata(String vehicleNo) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '"
                        + vehicleNo + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public double getScannedQty(String vehNo) {
            openRead();
            int wt = 0;
            String sql = "SELECT SUM(" + DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY
                    + ") FROM " + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' GROUP BY " + DataHelper.UNLOADING_PARTS_COLUMN_PART_ID;
            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                wt = cursor.getInt(0);
            }

            Utils.logD(sql);
            Utils.logD("Total Wt 2 : " + wt);
            if (cursor != null)
                cursor.close();
            close();
            return wt;
        }

        public List<NewProductModel> getScannedParts() {
            openRead();
            String vehNo = "";
            if (Utils.isValidString(Globals.vehicleNo))
                vehNo = Globals.vehicleNo;

            Utils.logD("Scan Data Start");

            String allDataSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo + "'";
            ArrayList<NewProductModel> scannedParts = new ArrayList<NewProductModel>();

            Cursor allCursor = database.rawQuery(allDataSql, null);
            scannedParts = getUnloadingData(allCursor);

            Utils.logD("Scan Data End");
            close();
            return scannedParts;
        }

        /* *
          filterType :
          1 -> Part
          2 -> DA Number
          3 -> Invoice
          4 -> Master Box
         */
        public void getFilteredScanConsList(Context context, int tag, int filterType, String filterStr, boolean isInvoice) {
            InvoicesScreen invoicesScreen = null;
            PartsScanningScreen scanScreen = null;
            if (isInvoice)
                invoicesScreen = (InvoicesScreen) context;
            else
                scanScreen = (PartsScanningScreen) context;

            openRead();
            String vehNo = "";
            if (Utils.isValidString(Globals.vehicleNo))
                vehNo = Globals.vehicleNo;

            Utils.logD("Scan Data Start");

            String filterSql = "";
            if (Utils.isValidString(filterStr)) {
                if (filterType == 1) {
                    filterSql += " AND " + DataHelper.UNLOADING_PARTS_COLUMN_PART_NO + " LIKE '%" + filterStr + "%'";
                } else if (filterType == 2) {
                    filterSql += " AND " + DataHelper.UNLOADING_PARTS_COLUMN_DA_NO + " LIKE '%" + filterStr + "%'";
                } else if (filterType == 3) {
                    filterSql += " AND " + DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO + " LIKE '%" + filterStr + "%'";
                } else if (filterType == 4) {
                    filterSql += " AND " + DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM + " LIKE '%" + filterStr + "%'";
                }
            }

            String allDataSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo + "'" + filterSql;

            String toBeScannedSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' " + filterSql;

            String exceptionSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND " + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " ='"
                    + Constants.TRUE + "'" + filterSql;


            String scanInProgressSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND "
                    + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1" + filterSql;

            ArrayList<NewProductModel> toBeScannedDockets = new ArrayList<NewProductModel>();
            ArrayList<NewProductModel> scanInProDockets = new ArrayList<NewProductModel>();
            ArrayList<ExceptionModel> exceptionDockets = new ArrayList<>();

            if (tag == -1) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getUnloadingData(toBeScannedCursor);
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
                scanInProDockets = getUnloadingData(scanInProCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.toBeScannedTag) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getUnloadingData(toBeScannedCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
            } else if (tag == Constants.scanInProgressTag) {
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
                scanInProDockets = getUnloadingData(scanInProCursor);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.exceptionTag) {
                Utils.logD(exceptionSql);
                exceptionDockets = exceptions.getExceptions(vehNo);
                scanScreen.exceptionFragment.reloadData(exceptionDockets);
            } else if (tag == Constants.invoicesTag) {
                Cursor allCursor = database.rawQuery(allDataSql, null);
                scanInProDockets = getUnloadingData(allCursor);
                invoicesScreen.resetData(scanInProDockets);
            }

            Utils.logD("Scan Data End");
            close();
        }

        public void getScanConsList(Context context, int tag, boolean isInvoice) {
            InvoicesScreen invoicesScreen = null;
            PartsScanningScreen scanScreen = null;
            if (isInvoice)
                invoicesScreen = (InvoicesScreen) context;
            else
                scanScreen = (PartsScanningScreen) context;

            openRead();
            String vehNo = "";
            if (Utils.isValidString(Globals.vehicleNo))
                vehNo = Globals.vehicleNo;


            Utils.logD("Scan Data Start");

            String allDataSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo + "'";

            String toBeScannedSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' ";

            String exceptionSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND " + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " ='"
                    + Constants.TRUE + "'";

            String scanInProgressSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND "
                    + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1";

            ArrayList<NewProductModel> toBeScannedDockets = new ArrayList<NewProductModel>();
            ArrayList<NewProductModel> scanInProDockets = new ArrayList<NewProductModel>();
            ArrayList<ExceptionModel> exceptionDockets = new ArrayList<>();

            if (tag == -1) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getUnloadingData(toBeScannedCursor);
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
                scanInProDockets = getUnloadingData(scanInProCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.toBeScannedTag) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getUnloadingData(toBeScannedCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
            } else if (tag == Constants.scanInProgressTag) {
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
                scanInProDockets = getUnloadingData(scanInProCursor);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.exceptionTag) {
                Utils.logD(exceptionSql);
                exceptionDockets = exceptions.getExceptions(vehNo);
                scanScreen.exceptionFragment.reloadData(exceptionDockets);
            } else if (tag == Constants.invoicesTag) {
                Cursor allCursor = database.rawQuery(allDataSql, null);
                scanInProDockets = getUnloadingData(allCursor);
                invoicesScreen.resetData(scanInProDockets);
            }

            Utils.logD("Scan Data End");
            close();
        }

        public NewProductModel getUnloadingData(String number, String dataType) {
            String sql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + " FROM " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                    + " WHERE " + DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE
                    + " = '" + dataType + "' AND ";

            if (dataType.equals("DA")) {
                sql += DataHelper.UNLOADING_PARTS_COLUMN_DA_NO + " = '" + number + "'";
            } else if (dataType.equals("IN")) {
                sql += DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO + " = '" + number + "'";
            } else if (dataType.equals("P")) {
                sql += DataHelper.UNLOADING_PARTS_COLUMN_PART_NO + " = '" + number + "'";
            } else if (dataType.equals("MB")) {
                sql += DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM + " = '" + number + "'";
            }

            NewProductModel newProductModel = null;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int colId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_ID);
                int vehNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_VEHICLE_NO);
                int partNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_NO);
                int partId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_ID);
                int totBoxes = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM);
                int totQty = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY);
                int invoiceNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO);
                int boxCount = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_BOX_COUNT);
                int qtyInvoice = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_INVOICE);

                int qtyScanned = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED);
                int exceptionCountIdx = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT);
                int status = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_STATUS);
                int scanTime = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME);

                int exceptionType = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE);

                int exceptionDesc = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION);

                int isException = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION);

                int images = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES);

                int scanCompleted = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED);
                int barcodeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INVOICE_BARCODE);
                int daNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DA_NO);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE);
                int barcodeLengthIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_LENGTH);
                int barcodeMinLenIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_MIN_LENGTH);
                int barcodeMaxLenIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_MAX_LENGTH);
                int inwardNumIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INWARD_NO);
                int invoicesIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INVOICES_LIST);

                newProductModel = new NewProductModel();
                int id = cursor.getInt(colId);
                newProductModel.setId(id);

                String vehNumber = cursor.getString(vehNo);
                if (Utils.isValidString(vehNumber))
                    newProductModel.setVehicleNo(vehNumber);

                newProductModel.setMasterBoxNo(cursor.getString(masterBoxNumIdx));

                String partNum = cursor.getString(partNo);
                if (Utils.isValidString(partNum))
                    newProductModel.setPartNo(partNum);
                else
                    newProductModel.setPartNo("");

                String inwardNo = cursor.getString(inwardNumIdx);
                Globals.inwardNo = inwardNo;
                newProductModel.setInwardNo(inwardNo);

                newProductModel.setDaNo(cursor.getString(daNoIdx));
                newProductModel.setDataType(cursor.getString(dataTypeIdx));

                Globals.barcodeLength = cursor.getInt(barcodeLengthIdx);
                Globals.barcodeMinLength = cursor.getInt(barcodeMinLenIdx);
                Globals.barcodeMaxLength = cursor.getInt(barcodeMaxLenIdx);

                newProductModel.setBarcodeLength(Globals.barcodeLength);
                newProductModel.setBarcodeMaxLength(Globals.barcodeMaxLength);
                newProductModel.setBarcodeMinLength(Globals.barcodeMinLength);

                int partid = cursor.getInt(partId);
                newProductModel.setPartId(partid);

                int tot_boxes = cursor.getInt(totBoxes);
                newProductModel.setTotalBoxes(tot_boxes);

                int tot_qty = cursor.getInt(totQty);
                newProductModel.setTotalQty(tot_qty);

                int qty_scanned = cursor.getInt(qtyScanned);
                newProductModel.setScannedCount(qty_scanned);

                int exceptionCount = cursor.getInt(exceptionCountIdx);
                newProductModel.setExceptionCount(exceptionCount);

                String scanned_status = cursor.getString(status);
                newProductModel.setScanned(scanned_status);

                String scan_time = cursor.getString(scanTime);
                if (Utils.isValidString(scan_time))
                    newProductModel.setScanTime(scan_time);

                String invoicesStr = cursor.getString(invoicesIdx);
                Type token = new TypeToken<List<InvoiceModel>>() {
                }.getType();
                newProductModel.setInvoiceList((List<InvoiceModel>) gson.fromJson(invoicesStr, token));

                InvoiceModel invoiceModel = new InvoiceModel();

                int box_count = cursor.getInt(boxCount);
                invoiceModel.setNoOfBoxes(box_count);

                int qty_invoice = cursor.getInt(qtyInvoice);
                invoiceModel.setTotalQty(qty_invoice);

                String invoice = cursor.getString(invoiceNo);
                if (Utils.isValidString(invoice)) {
                    invoiceModel.setInvoiceNo(invoice);
                    newProductModel.setInvoiceNo(invoice);
                }

                invoiceModel.setBarcode(cursor.getString(barcodeIdx));
                newProductModel.setBarcode(cursor.getString(barcodeIdx));

                String exType = cursor.getString(exceptionType);
                if (Utils.isValidString(exType))
                    newProductModel.setExceptionType(exType);

                String exDesc = cursor.getString(exceptionDesc);
                if (Utils.isValidString(exDesc))
                    newProductModel.setExceptionDesc(exDesc);

                String isEx = cursor.getString(isException);
                if (Utils.isValidString(isEx))
                    newProductModel.setIsException(isEx);

                String exImage = cursor.getString(images);
                if (Utils.isValidString(exImage))
                    newProductModel.setImages(exImage);


                String isScanCompleted = cursor.getString(scanCompleted);
                if (Utils.isValidString(isScanCompleted))
                    newProductModel.setIsScanCompleted(isScanCompleted);

            }
            if (cursor != null)
                cursor.close();

            return newProductModel;
        }

        public NewProductModel getUnloadingData(String barcode) {
            String sql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_INVOICE_BARCODE + " = '" + barcode + "'";
            NewProductModel newProductModel = null;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int colId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_ID);
                int vehNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_VEHICLE_NO);
                int partNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_NO);
                int partId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_ID);
                int totBoxes = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES);
                int totQty = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY);
                int invoiceNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO);
                int boxCount = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_BOX_COUNT);
                int qtyInvoice = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_INVOICE);

                int qtyScanned = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED);
                int exceptionCountIdx = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT);
                int status = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_STATUS);
                int scanTime = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME);

                int exceptionType = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE);

                int exceptionDesc = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION);

                int isException = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION);

                int images = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES);

                int scanCompleted = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED);
                int barcodeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INVOICE_BARCODE);
                int daNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DA_NO);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE);
                int barcodeLengthIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_LENGTH);
                int barcodeMinLenIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_MIN_LENGTH);
                int barcodeMaxLenIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_MAX_LENGTH);
                int inwardNumIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INWARD_NO);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM);

                newProductModel = new NewProductModel();
                int id = cursor.getInt(colId);
                newProductModel.setId(id);

                newProductModel.setMasterBoxNo(cursor.getString(masterBoxNumIdx));

                String vehNumber = cursor.getString(vehNo);
                if (Utils.isValidString(vehNumber))
                    newProductModel.setVehicleNo(vehNumber);

                String partNum = cursor.getString(partNo);
                if (Utils.isValidString(partNum))
                    newProductModel.setPartNo(partNum);
                else
                    newProductModel.setPartNo("");

                String inwardNo = cursor.getString(inwardNumIdx);
                Globals.inwardNo = inwardNo;
                newProductModel.setInwardNo(inwardNo);

                newProductModel.setDaNo(cursor.getString(daNoIdx));
                newProductModel.setDataType(cursor.getString(dataTypeIdx));

                Globals.barcodeLength = cursor.getInt(barcodeLengthIdx);
                Globals.barcodeMinLength = cursor.getInt(barcodeMinLenIdx);
                Globals.barcodeMaxLength = cursor.getInt(barcodeMaxLenIdx);

                newProductModel.setBarcodeLength(Globals.barcodeLength);
                newProductModel.setBarcodeMaxLength(Globals.barcodeMaxLength);
                newProductModel.setBarcodeMinLength(Globals.barcodeMinLength);

                int partid = cursor.getInt(partId);
                newProductModel.setPartId(partid);

                int tot_boxes = cursor.getInt(totBoxes);
                newProductModel.setTotalBoxes(tot_boxes);

                int tot_qty = cursor.getInt(totQty);
                newProductModel.setTotalQty(tot_qty);

                int qty_scanned = cursor.getInt(qtyScanned);
                newProductModel.setScannedCount(qty_scanned);

                int exceptionCount = cursor.getInt(exceptionCountIdx);
                newProductModel.setExceptionCount(exceptionCount);

                String scanned_status = cursor.getString(status);
                newProductModel.setScanned(scanned_status);

                String scan_time = cursor.getString(scanTime);
                if (Utils.isValidString(scan_time))
                    newProductModel.setScanTime(scan_time);

                InvoiceModel invoiceModel = new InvoiceModel();

                int box_count = cursor.getInt(boxCount);
                invoiceModel.setNoOfBoxes(box_count);

                int qty_invoice = cursor.getInt(qtyInvoice);
                invoiceModel.setTotalQty(qty_invoice);

                String invoice = cursor.getString(invoiceNo);
                if (Utils.isValidString(invoice)) {
                    invoiceModel.setInvoiceNo(invoice);
                    newProductModel.setInvoiceNo(invoice);
                }

                invoiceModel.setBarcode(cursor.getString(barcodeIdx));
                newProductModel.setBarcode(cursor.getString(barcodeIdx));

                String exType = cursor.getString(exceptionType);
                if (Utils.isValidString(exType))
                    newProductModel.setExceptionType(exType);

                String exDesc = cursor.getString(exceptionDesc);
                if (Utils.isValidString(exDesc))
                    newProductModel.setExceptionDesc(exDesc);

                String isEx = cursor.getString(isException);
                if (Utils.isValidString(isEx))
                    newProductModel.setIsException(isEx);

                String exImage = cursor.getString(images);
                if (Utils.isValidString(exImage))
                    newProductModel.setImages(exImage);


                String isScanCompleted = cursor.getString(scanCompleted);
                if (Utils.isValidString(isScanCompleted))
                    newProductModel.setIsScanCompleted(isScanCompleted);

            }
            if (cursor != null)
                cursor.close();

            return newProductModel;
        }

        public ArrayList<NewProductModel> getUnloadingData(Cursor cursor) {
            ArrayList<NewProductModel> productList = new ArrayList<NewProductModel>();

            if (cursor.moveToFirst()) {
                int colId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_ID);
                int vehNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_VEHICLE_NO);
                int partNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_NO);
                int partId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_ID);
                int totBoxes = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES);
                int totQty = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY);
                int invoiceNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO);
                int boxCount = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_BOX_COUNT);
                int qtyInvoice = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_INVOICE);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM);

                int qtyScanned = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED);
                int exceptionCountIdx = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT);
                int status = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_STATUS);
                int scanTime = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME);

                int exceptionType = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE);

                int exceptionDesc = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION);

                int isException = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION);

                int images = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES);

                int scanCompleted = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED);

                int invoiceNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO);
                int daNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DA_NO);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE);
                int barcodeLengthIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_LENGTH);
                int barcodeMinLenIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_MIN_LENGTH);
                int barcodeMaxLenIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_BARCODE_MAX_LENGTH);
                int inwardNumIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INWARD_NO);
                int barcodeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INVOICE_BARCODE);
                int invoicesIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INVOICES_LIST);

                do {
                    NewProductModel newProductModel = new NewProductModel();
                    int id = cursor.getInt(colId);
                    newProductModel.setId(id);

                    String vehNumber = cursor.getString(vehNo);
                    if (Utils.isValidString(vehNumber))
                        newProductModel.setVehicleNo(vehNumber);

                    String partNum = cursor.getString(partNo);
                    if (Utils.isValidString(partNum))
                        newProductModel.setPartNo(partNum);
                    else
                        newProductModel.setPartNo("");

                    String inwardNo = cursor.getString(inwardNumIdx);
                    Globals.inwardNo = inwardNo;
                    newProductModel.setInwardNo(inwardNo);

                    newProductModel.setMasterBoxNo(cursor.getString(masterBoxNumIdx));

                    String invoicesStr = cursor.getString(invoicesIdx);
                    Type token = new TypeToken<List<InvoiceModel>>() {
                    }.getType();
                    newProductModel.setInvoiceList((List<InvoiceModel>) gson.fromJson(invoicesStr, token));

                    int partid = cursor.getInt(partId);
                    newProductModel.setPartId(partid);

                    newProductModel.setInvoiceNo(cursor.getString(invoiceNoIdx));
                    newProductModel.setDaNo(cursor.getString(daNoIdx));
                    newProductModel.setDataType(cursor.getString(dataTypeIdx));

                    Globals.barcodeLength = cursor.getInt(barcodeLengthIdx);
                    Globals.barcodeMinLength = cursor.getInt(barcodeMinLenIdx);
                    Globals.barcodeMaxLength = cursor.getInt(barcodeMaxLenIdx);

                    newProductModel.setBarcodeLength(Globals.barcodeLength);
                    newProductModel.setBarcodeMaxLength(Globals.barcodeMaxLength);
                    newProductModel.setBarcodeMinLength(Globals.barcodeMinLength);

                    int tot_boxes = cursor.getInt(totBoxes);
                    newProductModel.setTotalBoxes(tot_boxes);

                    int tot_qty = cursor.getInt(totQty);
                    newProductModel.setTotalQty(tot_qty);

                    int qty_scanned = cursor.getInt(qtyScanned);
                    newProductModel.setScannedCount(qty_scanned);

                    int exceptionCount = cursor.getInt(exceptionCountIdx);
                    newProductModel.setExceptionCount(exceptionCount);

                    String scanned_status = cursor.getString(status);
                    newProductModel.setScanned(scanned_status);

                    newProductModel.setBarcode(cursor.getString(barcodeIdx));

                    String scan_time = cursor.getString(scanTime);
                    if (Utils.isValidString(scan_time))
                        newProductModel.setScanTime(scan_time);

                    InvoiceModel invoiceModel = new InvoiceModel();

                    int box_count = cursor.getInt(boxCount);
                    invoiceModel.setNoOfBoxes(box_count);

                    int qty_invoice = cursor.getInt(qtyInvoice);
                    invoiceModel.setTotalQty(qty_invoice);

                    String invoice = cursor.getString(invoiceNo);
                    if (Utils.isValidString(invoice))
                        invoiceModel.setInvoiceNo(invoice);

                    String exType = cursor.getString(exceptionType);
                    if (Utils.isValidString(exType))
                        newProductModel.setExceptionType(exType);

                    String exDesc = cursor.getString(exceptionDesc);
                    if (Utils.isValidString(exDesc))
                        newProductModel.setExceptionDesc(exDesc);

                    String isEx = cursor.getString(isException);
                    if (Utils.isValidString(isEx))
                        newProductModel.setIsException(isEx);


                    String exImage = cursor.getString(images);
                    if (Utils.isValidString(exImage))
                        newProductModel.setImages(exImage);


                    String isScanCompleted = cursor.getString(scanCompleted);
                    if (Utils.isValidString(isScanCompleted))
                        newProductModel.setIsScanCompleted(isScanCompleted);


//                    List<InvoiceModel> pList = new ArrayList<InvoiceModel>();
//                    pList.add(invoiceModel);

//                    if (productList.size() > 0
//                            && productList.get(productList.size() - 1).getPartNo()
//                            .equals(partNum)) {
//                        List<InvoiceModel> pkts = productList.get(productList.size() - 1)
//                                .getInvoiceList();
//                        pkts.add(invoiceModel);
//                        productList.get(productList.size() - 1).setInvoiceList(pkts);
//                    } else {
//                        newProductModel.setInvoiceList(pList);
                    productList.add(newProductModel);
//                    }

                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();

            Utils.logD(String.valueOf(productList.size()));

            return productList;
        }

        public void updatePartScan(Context context, String vehicleNo, int qtyScanned, String dataType, String number) {
            String scanTime = Utils.getScanTime();
            PartsScanningScreen partsScanningScreen = (PartsScanningScreen) context;
            openRead();
            try {

                String updateSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " SET " + DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED + " = " + qtyScanned
                        + " , " + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1 ,"
                        + DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME + " = '" + scanTime
                        + "' WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " ='" + vehicleNo + "' AND ";
                if (dataType.equals("P")) {
                    updateSql += DataHelper.UNLOADING_PARTS_COLUMN_PART_NO + " ='" + number + "'";
                } else if (dataType.equals("DA")) {
                    updateSql += DataHelper.UNLOADING_PARTS_COLUMN_DA_NO + " ='" + number + "'";
                } else if (dataType.equals("IN")) {
                    updateSql += DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO + " ='" + number + "'";
                } else if (dataType.equals("MB")) {
                    updateSql += DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM + " ='" + number + "'";
                }

                Utils.logD(updateSql);
                database.execSQL(updateSql);

                String getDataSql = "SELECT " + DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED + " , " +
                        DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES + " FROM "
                        + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " ='" + vehicleNo + "' AND ";
                if (dataType.equals("P")) {
                    getDataSql += DataHelper.UNLOADING_PARTS_COLUMN_PART_NO + " ='" + number + "'";
                } else if (dataType.equals("DA")) {
                    getDataSql += DataHelper.UNLOADING_PARTS_COLUMN_DA_NO + " ='" + number + "'";
                } else if (dataType.equals("IN")) {
                    getDataSql += DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO + " ='" + number + "'";
                } else if (dataType.equals("MB")) {
                    getDataSql += DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM + " ='" + number + "'";
                }
                Utils.logD(getDataSql);

                Cursor cursor = database.rawQuery(getDataSql, null);

                int totalQty = 0, scannedQty = 0;

                if (cursor.moveToFirst()) {
                    scannedQty = cursor.getInt(0);
                    totalQty = cursor.getInt(1);
                }

                if (scannedQty > 0 && scannedQty == totalQty) {
                    String updateScanStatusSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                            + " SET " + DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED + " = '" + Constants.TRUE + "' "
                            + "' WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " ='" + vehicleNo + "' AND ";
                    if (dataType.equals("P")) {
                        updateScanStatusSql += DataHelper.UNLOADING_PARTS_COLUMN_PART_NO + " ='" + number + "'";
                    } else if (dataType.equals("DA")) {
                        updateScanStatusSql += DataHelper.UNLOADING_PARTS_COLUMN_DA_NO + " ='" + number + "'";
                    } else if (dataType.equals("IN")) {
                        updateScanStatusSql += DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO + " ='" + number + "'";
                    } else if (dataType.equals("MB")) {
                        updateScanStatusSql += DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM + " ='" + number + "'";
                    }

                    Utils.logD(updateScanStatusSql);
                    database.execSQL(updateScanStatusSql);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            partsScanningScreen.resetAllFragmentData();
        }


        public void recordException(Context context, String vehicleNo, String partNo, String excepDesc, String excepType, int exceptionCount, String image) {
            String scanTime = Utils.getScanTime();
            PartsScanningScreen partsScanningScreen = (PartsScanningScreen) context;
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " SET "
                        + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1, "
                        + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " = '" + Constants.TRUE + "' , "
                        + DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE + " = '" + excepType + "' , "
                        + DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION + " = '" + excepDesc + "' , "
                        + DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES + " = '" + image + "' , "
                        + DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME + " = '" + scanTime + "' , "
                        + DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT + " = " + exceptionCount
                        + " WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " ='" + vehicleNo + "' AND "
                        + DataHelper.UNLOADING_PARTS_COLUMN_PART_NO + " ='" + partNo + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);


            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            partsScanningScreen.resetAllFragmentData();
        }


        public class Save extends AsyncTask<Object, Object, Object> {
            @Override
            protected void onPreExecute() {
                showProgressDialog();
                Utils.logD("Start saving the data");
            }

            @Override
            protected List<ProductModel> doInBackground(Object... arg0) {

                try {
                    if (unloadingResponse.getData() != null)
                        sharedPreferences.set(Constants.DATA_TYPE, unloadingResponse.getData().getDataType());
                    long c = cacheSheetData(context, unloadingResponse, vehicleNo);
                    Utils.logD("Count : " + c);
                } catch (Exception e) {
                    Utils.logE(e.toString());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object result) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                    partsScanningScreen.resetAllFragmentData();
                    pDialog = null;
                    isProgress = false;
                    Utils.logD("Loaded data");
                }
                super.onPostExecute(result);
            }
        }
    }

    /******************** Vendor Master **************************/

    public class VendorMaster {


        public void truncateTable() {
            open();
            if (database != null) {
                String query = "DELETE FROM " + DataHelper.VENDOR_MASTER_TABLE_NAME;
                database.execSQL(query);
            }
            close();
        }

        public void clearAll() {
            open();
            if (database != null) {
                String query = "DELETE FROM " + DataHelper.VENDOR_MASTER_TABLE_NAME;
                database.execSQL(query);
            }
            close();
        }


        public int saveVendorMaster(List<VendorMasterModel> masterList) {
            int numberOfRecords = 0;
            clearAll();

            open();
            database.beginTransaction();

            ContentValues values = new ContentValues();
            for (VendorMasterModel b : masterList) {
                String code = b.getVendorCode();
                if (Utils.isValidString(code))
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_CODE, code);
                else
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_CODE, "");

                String type = b.getVendorType();
                if (Utils.isValidString(type))
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_TYPE, type);
                else
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_TYPE, "");

                String name = b.getVendorName();
                if (Utils.isValidString(name))
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_NAME, name);
                else
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_NAME, "");

                boolean active = b.isActiveFlag();
                values.put(DataHelper.VENDOR_MASTER_COLUMN_ACTIVE_FLAG, active);

                int id = b.getVendorId();
                values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_ID, id);


                long retVal = database.insert(DataHelper.VENDOR_MASTER_TABLE_NAME, null, values);
                if (retVal > -1)
                    numberOfRecords++;
            }

            database.setTransactionSuccessful();
            database.endTransaction();
            close();
            Utils.logD("Vendor master records : " + numberOfRecords);
            return numberOfRecords;
        }


        public List<VendorMasterModel> getAll() {
            openRead();
            List<VendorMasterModel> items = new ArrayList<VendorMasterModel>();
            Cursor cursor = null;
            String sql;
            try {
                sql = "SELECT *" + " FROM " + DataHelper.VENDOR_MASTER_TABLE_NAME;
                cursor = database.rawQuery(sql, null);
                Log.e("query", sql);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    VendorMasterModel masterModel = new VendorMasterModel();
                    int id = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_ID);
                    int code = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_CODE);
                    int name = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_NAME);
                    int type = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_TYPE);
                    int active = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_ACTIVE_FLAG);


                    int vendor_id = cursor.getInt(id);
                    masterModel.setVendorId(vendor_id);


                    String vendor_code = cursor.getString(code);
                    if (Utils.isValidString(vendor_code))
                        masterModel.setVendorCode(vendor_code);

                    String vendor_name = cursor.getString(name);
                    if (Utils.isValidString(vendor_name))
                        masterModel.setVendorName(vendor_name);


                    String vendor_type = cursor.getString(type);
                    if (Utils.isValidString(vendor_type))
                        masterModel.setVendorType(vendor_type);


                    String is_active = cursor.getString(active);
                    masterModel.setActiveFlag(Boolean.parseBoolean(is_active));

                    items.add(masterModel);
                    cursor.moveToNext();
                }

            } catch (Exception e) {
                Utils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return items;
        }
    }

    public class LoadingPartsNew {

        private Context context;
        private LoadingScanningLatestScreen loadingScanningScreen;
        private ProgressDialog pDialog;
        private boolean isProgress = false;
        private LoadingResponseLatest loadingResponse;
        private String tcNumber;

        public NewProductModel getLoadingData(String barcodeStr) {
            String sql = "SELECT * FROM "
                    + DataHelper.LOADING_LATEST_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_BARCODE + " = '" + barcodeStr + "'";
            NewProductModel loadingaPartsModel = null;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int tcNumber = cursor
                        .getColumnIndex(DataHelper.LOADING_TC_NUMBER);
                int partNo = cursor
                        .getColumnIndex(DataHelper.LOADING_PART_NUMBER);
                int partId = cursor
                        .getColumnIndex(DataHelper.LOADING_PART_ID);
                int totBoxes = cursor
                        .getColumnIndex(DataHelper.LOADING_TOTAL_BOXES);
                int scannedBoxes = cursor
                        .getColumnIndex(DataHelper.LOADING_SCANNED_BOXES);
                int status = cursor
                        .getColumnIndex(DataHelper.LOADING_STATUS);
                int scanTime = cursor
                        .getColumnIndex(DataHelper.LOADING_SCANNED_TIME);
                int isPickingComplete = cursor
                        .getColumnIndex(DataHelper.LOADING_IS_PICKING_COMPLETE);
                int daNo = cursor.getColumnIndex(DataHelper.LOADING_DA_NUMBER);
                int invoiceNo = cursor.getColumnIndex(DataHelper.LOADING_INVOICE_NUMBER);
                int invoiceList = cursor.getColumnIndex(DataHelper.LOADING_INVOICES_LIST);
                int masterBoxNo = cursor.getColumnIndex(DataHelper.LOADING_MASTER_BOX_NUMBER);
                int dataType = cursor.getColumnIndex(DataHelper.LOADING_DATATYPE);
                int barcode = cursor.getColumnIndex(DataHelper.LOADING_BARCODE);
                int barcodeLen = cursor.getColumnIndex(DataHelper.LOADING_BARCODE_LENGTH);
                int barcodeMinLen = cursor.getColumnIndex(DataHelper.LOADING_BARCODE_MIN_LENGTH);
                int barcodeMaxLen = cursor.getColumnIndex(DataHelper.LOADING_BARCODE_MAX_LENGTH);

                loadingaPartsModel = new NewProductModel();

                int id = cursor.getInt(partId);
                loadingaPartsModel.setPartId(id);

                String partNum = cursor.getString(partNo);
                if (Utils.isValidString(partNum))
                    loadingaPartsModel.setPartNo(partNum);
                else
                    loadingaPartsModel.setPartNo("");

                int tot_boxes = cursor.getInt(totBoxes);
                loadingaPartsModel.setTotalBoxes(tot_boxes);

                int qty_scanned = cursor.getInt(scannedBoxes);
                loadingaPartsModel.setScannedBoxes(qty_scanned);

                loadingaPartsModel.setInvoiceNo(cursor.getString(invoiceNo));
                loadingaPartsModel.setDaNo(cursor.getString(daNo));
                loadingaPartsModel.setDataType(cursor.getString(dataType));
                loadingaPartsModel.setMasterBoxNo(cursor.getString(masterBoxNo));
                loadingaPartsModel.setBarcode(cursor.getString(barcode));
                loadingaPartsModel.setBarcodeLength(cursor.getInt(barcodeLen));
                loadingaPartsModel.setBarcodeMinLength(cursor.getInt(barcodeMinLen));
                loadingaPartsModel.setBarcodeMaxLength(cursor.getInt(barcodeMaxLen));
                loadingaPartsModel.setPickingComplete(cursor.getInt(isPickingComplete));

                Globals.loaidngBarcodeLength = cursor.getInt(barcodeLen);
                Globals.loaidngBarcodeMinLength = cursor.getInt(barcodeMinLen);
                Globals.loaidngBarcodeMaxLength = cursor.getInt(barcodeMaxLen);

                loadingaPartsModel.setBarcodeLength(Globals.loaidngBarcodeLength);
                loadingaPartsModel.setBarcodeMaxLength(Globals.loaidngBarcodeMaxLength);
                loadingaPartsModel.setBarcodeMinLength(Globals.loaidngBarcodeMinLength);
            }
            if (cursor != null)
                cursor.close();

            return loadingaPartsModel;
        }

        private void showProgressDialog() {
            if (pDialog == null) {
                pDialog = new ProgressDialog(context);
                pDialog.setMessage(context
                        .getString(R.string.progress_dialog_title));
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        public void saveData(Context mContext, LoadingResponseLatest response, String tcNumber) {
            if (!isProgress) {
                isProgress = true;
                context = mContext;
                loadingScanningScreen = (LoadingScanningLatestScreen) context;
                loadingResponse = response;
                this.tcNumber = tcNumber;
                new Save().execute();
            }
        }

        public boolean getPartsData(String tcNumber) {
            Boolean isExists = false;
            Cursor cursor = null;
            openRead();
            String selectQuery = "SELECT * " + "FROM "
                    + DataHelper.LOADING_LATEST_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_TC_NUMBER + " = '" + tcNumber
                    + "'";
            Utils.logD(selectQuery);
            try {
                cursor = database.rawQuery(selectQuery, null);

                if (cursor.getCount() > 0)
                    isExists = true;
                else isExists = false;

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
            }

            return isExists;
        }

        public void updateScanCount(Context context, String barcode, int scannedCount, int scannedInvoiceQty) {
            String scanTime = Utils.getScanTime();
            LoadingScanningLatestScreen loadingScanningLatestScreen = (LoadingScanningLatestScreen) context;
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.LOADING_LATEST_TABLE_NAME
                        + " SET "
                        + DataHelper.LOADING_STATUS + " = 1, "
                        + DataHelper.LOADING_SCANNED_BOXES + " = " + scannedCount + ", "
                        + DataHelper.LOADING_SCANNED_TIME + " = '" + scanTime + "'"
                        + " WHERE " + DataHelper.LOADING_BARCODE + " ='" + barcode + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            loadingScanningLatestScreen.resetAllFragmentData();
        }

        public long cacheSheetData(Context mContext,
                                   LoadingResponseLatest response, String tcNumber) {
            int numberOfRecords = 0;
            boolean isExists = false;

            try {
                open();
                database.beginTransaction();

                Cursor cursor = null;
                HashMap<String, Integer> sheetDataCached = new HashMap<String, Integer>();
                HashMap<String, Integer> sortOrder = new HashMap<String, Integer>();


                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                ContentValues values = new ContentValues();
                UnloadingModel model = response.getData();
                for (ProductModel data : model.getPartList()) {
                    String dataType = response.getData().getDataType();
                    String number = Utils.getNumber(data, dataType);
                    if (Utils.isValidString(number)) {
                        values.put(DataHelper.LOADING_INVOICE_NUMBER, data.getInvoiceNo());
                        values.put(DataHelper.LOADING_MASTER_BOX_NUMBER, data.getMasterBoxNo());
                        values.put(DataHelper.LOADING_INVOICES_LIST, gson.toJson(data.getInvoiceList()));
                        values.put(DataHelper.LOADING_DA_NUMBER, data.getDaNo());
                        values.put(DataHelper.LOADING_DATATYPE, response.getData().getDataType());
                        values.put(DataHelper.LOADING_BARCODE_LENGTH, response.getData().getBarcodeLength());
                        Globals.loaidngBarcodeLength = response.getData().getBarcodeLength();
                        values.put(DataHelper.LOADING_BARCODE_MIN_LENGTH, response.getData().getBarcodeMinLength());
                        Globals.loaidngBarcodeMinLength = response.getData().getBarcodeMinLength();
                        values.put(DataHelper.LOADING_BARCODE_MAX_LENGTH, response.getData().getBarcodeMaxLength());
                        Globals.loaidngBarcodeMaxLength = response.getData().getBarcodeMaxLength();
                        if (Utils.isValidString(data.getBarcode())) {
                            values.put(DataHelper.LOADING_BARCODE, data.getBarcode().toLowerCase());
                        }

                        values.put(DataHelper.LOADING_TC_NUMBER,
                                tcNumber);

                        values.put(DataHelper.LOADING_PART_NUMBER,
                                data.getPartNo());

                        int partId = data.getPartId();
                        values.put(DataHelper.LOADING_PART_ID,
                                partId);

                        int tot_boxes = data.getTotalBoxes();
                        values.put(DataHelper.LOADING_TOTAL_BOXES, tot_boxes);
                        values.put(DataHelper.LOADING_IS_PICKING_COMPLETE, data.getPickingComplete());

                        values.put(DataHelper.LOADING_STATUS, 0);
                        values.put(DataHelper.LOADING_SCANNED_BOXES, 0);

                        long retVal = database.insert(
                                DataHelper.LOADING_LATEST_TABLE_NAME, null, values);
                        if (retVal > -1)
                            numberOfRecords++;
                    }
                }
                database.setTransactionSuccessful();
                database.endTransaction();

                Utils.logD("Recordssss : " + numberOfRecords);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                close();
            }
            return numberOfRecords;

        }

        public void deleteLoadingData(String tcNumber) {
            open();
            try {

                String deleteSql = "DELETE  FROM " + DataHelper.LOADING_LATEST_TABLE_NAME
                        + " WHERE " + DataHelper.LOADING_TC_NUMBER + " = '"
                        + tcNumber + "'";
                database.execSQL(deleteSql);
                Utils.logD(deleteSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(int partId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.LOADING_KEY_ID
                        + " FROM " + DataHelper.LOADING_LATEST_TABLE_NAME
                        + " WHERE " + DataHelper.LOADING_PART_ID
                        + " = " + partId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public double getScannedQty(String tcNumber) {
            openRead();
            int wt = 0;
            String sql = "SELECT SUM(" + DataHelper.LOADING_SCANNED_BOXES
                    + ") FROM " + DataHelper.LOADING_LATEST_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_TC_NUMBER + " = '" + tcNumber
                    + "' GROUP BY " + DataHelper.LOADING_PART_ID;
            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                wt = cursor.getInt(0);
            }

            Utils.logD(sql);
            Utils.logD("Total Wt 2 : " + wt);
            if (cursor != null)
                cursor.close();
            close();
            return wt;
        }

        public List<NewProductModel> getScannedParts() {
            openRead();
            String tcNumber = "";
            if (Utils.isValidString(Globals.tcNumber))
                tcNumber = Globals.tcNumber;

            Utils.logD("Scan Data Start");

            String allDataSql = "SELECT * FROM "
                    + DataHelper.LOADING_LATEST_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_TC_NUMBER + " = '" + tcNumber + "'";
            ArrayList<NewProductModel> scannedParts = new ArrayList<>();

            Cursor allCursor = database.rawQuery(allDataSql, null);
            scannedParts = getLoadingData(allCursor);

            Utils.logD("Scan Data End");

            close();

            return scannedParts;
        }

        public void getScanConsList(Context context, int tag, boolean isInvoice) {
            InvoicesScreen invoicesScreen = null;
            LoadingScanningLatestScreen scanScreen = null;
            if (isInvoice)
                invoicesScreen = (InvoicesScreen) context;
            else
                scanScreen = (LoadingScanningLatestScreen) context;

            openRead();
            String tcNumber = "";
            if (Utils.isValidString(Globals.tcNumber))
                tcNumber = Globals.tcNumber;

            Utils.logD("Scan Data Start");

            String allDataSql = "SELECT * FROM "
                    + DataHelper.LOADING_LATEST_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_TC_NUMBER + " = '" + tcNumber + "'";

            String toBeScannedSql = "SELECT * FROM "
                    + DataHelper.LOADING_LATEST_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_TC_NUMBER + " = '" + tcNumber
                    + "' AND " + DataHelper.LOADING_SCANNED_BOXES + " < " + DataHelper.LOADING_TOTAL_BOXES;

            String scanInProgressSql = "SELECT * FROM "
                    + DataHelper.LOADING_LATEST_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_TC_NUMBER + " = '" + tcNumber
                    + "' AND "
                    + DataHelper.LOADING_STATUS + " = 1";
            ArrayList<NewProductModel> toBeScannedDockets = new ArrayList<>();
            ArrayList<NewProductModel> scanInProDockets = new ArrayList<>();

            if (tag == -1) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getLoadingData(toBeScannedCursor);
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
                scanInProDockets = getLoadingData(scanInProCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.toBeScannedTag) {
                Utils.logD(toBeScannedSql);
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getLoadingData(toBeScannedCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
            } else if (tag == Constants.scanInProgressTag) {
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
                scanInProDockets = getLoadingData(scanInProCursor);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.invoicesTag) {
//                Cursor allCursor = database.rawQuery(allDataSql, null);
//                scanInProDockets = getLoadingData(allCursor);
//                invoicesScreen.resetData(scanInProDockets);
            }
            Utils.logD("Scan Data End");
            close();
        }

        public ArrayList<NewProductModel> getLoadingData(Cursor cursor) {
            ArrayList<NewProductModel> loadingList = new ArrayList<>();

            Utils.logD(String.valueOf(cursor.getCount()));
            if (cursor.moveToFirst()) {
                int tcNumber = cursor
                        .getColumnIndex(DataHelper.LOADING_TC_NUMBER);
                int partNo = cursor
                        .getColumnIndex(DataHelper.LOADING_PART_NUMBER);
                int partId = cursor
                        .getColumnIndex(DataHelper.LOADING_PART_ID);
                int totBoxes = cursor
                        .getColumnIndex(DataHelper.LOADING_TOTAL_BOXES);
                int scannedBoxes = cursor
                        .getColumnIndex(DataHelper.LOADING_SCANNED_BOXES);
                int status = cursor
                        .getColumnIndex(DataHelper.LOADING_STATUS);
                int scanTime = cursor
                        .getColumnIndex(DataHelper.LOADING_SCANNED_TIME);
                int isPickingComplete = cursor
                        .getColumnIndex(DataHelper.LOADING_IS_PICKING_COMPLETE);
                int daNo = cursor.getColumnIndex(DataHelper.LOADING_DA_NUMBER);
                int invoiceNo = cursor.getColumnIndex(DataHelper.LOADING_INVOICE_NUMBER);
                int invoiceList = cursor.getColumnIndex(DataHelper.LOADING_INVOICES_LIST);
                int masterBoxNo = cursor.getColumnIndex(DataHelper.LOADING_MASTER_BOX_NUMBER);
                int dataType = cursor.getColumnIndex(DataHelper.LOADING_DATATYPE);
                int barcode = cursor.getColumnIndex(DataHelper.LOADING_BARCODE);
                int barcodeLen = cursor.getColumnIndex(DataHelper.LOADING_BARCODE_LENGTH);
                int barcodeMinLen = cursor.getColumnIndex(DataHelper.LOADING_BARCODE_MIN_LENGTH);
                int barcodeMaxLen = cursor.getColumnIndex(DataHelper.LOADING_BARCODE_MAX_LENGTH);
                int invoiceListIdx = cursor.getColumnIndex(DataHelper.LOADING_INVOICES_LIST);

                do {
                    NewProductModel loadingaPartsModel = new NewProductModel();

                    int id = cursor.getInt(partId);
                    loadingaPartsModel.setPartId(id);

                    String partNum = cursor.getString(partNo);
                    if (Utils.isValidString(partNum))
                        loadingaPartsModel.setPartNo(partNum);
                    else
                        loadingaPartsModel.setPartNo("");

                    int tot_boxes = cursor.getInt(totBoxes);
                    loadingaPartsModel.setTotalBoxes(tot_boxes);

                    Globals.loadingDataType = cursor.getString(dataType);
                    Globals.loaidngBarcodeLength = cursor.getInt(barcodeLen);
                    Globals.loaidngBarcodeMaxLength = cursor.getInt(barcodeMaxLen);
                    Globals.loaidngBarcodeMinLength = cursor.getInt(barcodeMinLen);

                    int qty_scanned = cursor.getInt(scannedBoxes);
                    loadingaPartsModel.setScannedBoxes(qty_scanned);

                    loadingaPartsModel.setInvoiceNo(cursor.getString(invoiceNo));
                    loadingaPartsModel.setDaNo(cursor.getString(daNo));
                    loadingaPartsModel.setDataType(cursor.getString(dataType));
                    loadingaPartsModel.setMasterBoxNo(cursor.getString(masterBoxNo));
                    loadingaPartsModel.setBarcode(cursor.getString(barcode));
                    loadingaPartsModel.setBarcodeLength(cursor.getInt(barcodeLen));
                    loadingaPartsModel.setBarcodeMinLength(cursor.getInt(barcodeMinLen));
                    loadingaPartsModel.setBarcodeMaxLength(cursor.getInt(barcodeMaxLen));
                    String invoicesListStr = cursor.getString(invoiceListIdx);
                    Type token = new TypeToken<List<InvoiceModel>>() {
                    }.getType();
                    loadingaPartsModel.setInvoiceList(gson.fromJson(invoicesListStr, token));

                    loadingaPartsModel.setPickingComplete(cursor.getInt(isPickingComplete));
                    loadingList.add(loadingaPartsModel);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();

            Utils.logD(String.valueOf(loadingList.size()));

            return loadingList;
        }

        public void updatePartScan(Context context, String tcNumber, int qtyScanned, String dataType, String number) {
            String scanTime = Utils.getScanTimeLoading();
            LoadingScanningLatestScreen loadingScanningScreen = (LoadingScanningLatestScreen) context;
            openRead();

            try {
                String updateSql = "UPDATE " + DataHelper.LOADING_LATEST_TABLE_NAME
                        + " SET " + DataHelper.LOADING_SCANNED_BOXES + " = " + qtyScanned
                        + " , " + DataHelper.LOADING_STATUS + " = 1 ,"
                        + DataHelper.LOADING_SCANNED_TIME + " = '" + scanTime
                        + "' WHERE " + DataHelper.LOADING_TC_NUMBER + " ='" + tcNumber + "' AND ";
                if (dataType.equals("P")) {
                    updateSql += DataHelper.LOADING_PART_NUMBER + " ='" + number + "'";
                } else if (dataType.equals("DA")) {
                    updateSql += DataHelper.LOADING_DA_NUMBER + " ='" + number + "'";
                } else if (dataType.equals("IN")) {
                    updateSql += DataHelper.LOADING_INVOICE_NUMBER + " ='" + number + "'";
                } else if (dataType.equals("MB")) {
                    updateSql += DataHelper.LOADING_MASTER_BOX_NUMBER + " ='" + number + "'";
                }

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            loadingScanningScreen.resetAllFragmentData();
        }

        public class Save extends AsyncTask<Object, Object, Object> {
            @Override
            protected void onPreExecute() {
                showProgressDialog();
                Utils.logD("Start saving the data");
            }

            @Override
            protected List<ProductModel> doInBackground(Object... arg0) {
                try {
                    long c = cacheSheetData(context, loadingResponse, tcNumber);
                    Utils.logD("Count : " + c);
                } catch (Exception e) {
                    Utils.logE(e.toString());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object result) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                    loadingScanningScreen.resetAllFragmentData();
                    pDialog = null;
                    isProgress = false;
                    Utils.logD("Loaded data");
                }
                super.onPostExecute(result);
            }
        }
    }

    public class LoadingParts {
        private Context context;
        private LoadingScanningScreen loadingScanningScreen;
        private ProgressDialog pDialog;
        private boolean isProgress = false;
        private LoadingResponse loadingResponse;
        private String vehicleNo;

        private void showProgressDialog() {
            if (pDialog == null) {
                pDialog = new ProgressDialog(context);
                pDialog.setMessage(context
                        .getString(R.string.progress_dialog_title));
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        public void saveData(Context mContext, LoadingResponse response, String vehicleNumber) {
            if (!isProgress) {
                isProgress = true;
                context = mContext;

                loadingScanningScreen = (LoadingScanningScreen) context;

                loadingResponse = response;
                vehicleNo = vehicleNumber;
                new Save().execute();
            }
        }

        public boolean getPartsData(String vehicleNo) {
            Boolean isExists = false;
            Cursor cursor = null;
            openRead();
            String selectQuery = "SELECT * " + "FROM "
                    + DataHelper.LOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_PARTS_VEHICLE_NO + " = '" + vehicleNo
                    + "'";
            Utils.logD(selectQuery);
            try {
                cursor = database.rawQuery(selectQuery, null);

                if (cursor.getCount() > 0)
                    isExists = true;
                else isExists = false;

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
            }

            return isExists;
        }

        public long cacheSheetData(Context mContext,
                                   LoadingResponse response, String vehicleNo) {


            int numberOfRecords = 0;
            boolean isExists = false;

            try {

                open();
                database.beginTransaction();

                Cursor cursor = null;
                HashMap<String, Integer> sheetDataCached = new HashMap<String, Integer>();
                HashMap<String, Integer> sortOrder = new HashMap<String, Integer>();


                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                ContentValues values = new ContentValues();
                for (LoadingaPartsModel data : response.getData()) {

                    String partNo = data.getPartNo();
                    if (Utils.isValidString(partNo)) {
                        values.put(DataHelper.LOADING_PARTS_VEHICLE_NO,
                                vehicleNo);

                        values.put(DataHelper.LOADING_PARTS_COLUMN_PART_NO,
                                partNo);

                        int partId = data.getPartId();
                        values.put(DataHelper.LOADING_PARTS_COLUMN_PART_ID,
                                partId);

                        int tot_boxes = data.getTotalBoxes();
                        values.put(DataHelper.LOADING_PARTS_COLUMN_TOTAL_BOXES, tot_boxes);

                        int tot_qty = data.getTotalQty();
                        values.put(DataHelper.LOADING_PARTS_COLUMN_TOTAL_QTY, tot_qty);


                        String binNo = data.getBinNo();
                        if (Utils.isValidString(binNo))
                            values.put(DataHelper.LOADING_PARTS_COLUMN_BIN_NO, binNo);
                        else
                            values.put(DataHelper.LOADING_PARTS_COLUMN_BIN_NO, "");

                        String binId = data.getBinId();
                        if (Utils.isValidString(binId))
                            values.put(DataHelper.LOADING_PARTS_COLUMN_BIN_ID, binId);
                        else
                            values.put(DataHelper.LOADING_PARTS_COLUMN_BIN_ID, "");

                        values.put(DataHelper.LOADING_PARTS_COLUMN_QTY_SCANNED, 0); //for now the scanned count is 0 in future the count should come from server if any boxes are scanned

                        values.put(DataHelper.LOADING_PARTS_COLUMN_STATUS, 0);

                        long retVal = database.insert(
                                DataHelper.LOADING_PARTS_TABLE_NAME, null, values);
                        if (retVal > -1)
                            numberOfRecords++;


                    }
                }
                database.setTransactionSuccessful();
                database.endTransaction();

                Utils.logD("Recordssss : " + numberOfRecords);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                close();
            }
            return numberOfRecords;
        }

        public void deleteLoadingData(String vehicleNo) {
            open();
            try {

                String deleteSql = "DELETE  FROM " + DataHelper.LOADING_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.LOADING_PARTS_VEHICLE_NO + " = '"
                        + vehicleNo + "'";
                database.execSQL(deleteSql);

                Utils.logD(deleteSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public double getScannedQty(String vehNo) {
            openRead();
            int wt = 0;
            String sql = "SELECT SUM(" + DataHelper.LOADING_SCANNED_BOXES
                    + ") FROM " + DataHelper.LOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' GROUP BY " + DataHelper.LOADING_PARTS_COLUMN_PART_ID;


            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                wt = cursor.getInt(0);
            }

            Utils.logD(sql);
            Utils.logD("Total Wt 2 : " + wt);
            if (cursor != null)
                cursor.close();
            close();
            return wt;
        }

        public List<NewLoadingPartsModel> getScannedParts() {
            openRead();
            String vehNo = "";
            if (Utils.isValidString(Globals.vehicleNo))
                vehNo = Globals.vehicleNo;


            Utils.logD("Scan Data Start");

            String allDataSql = "SELECT * FROM "
                    + DataHelper.LOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_PARTS_VEHICLE_NO + " = '" + vehNo + "'";
            ArrayList<NewLoadingPartsModel> scannedParts = new ArrayList<NewLoadingPartsModel>();

            Cursor allCursor = database.rawQuery(allDataSql, null);
            scannedParts = getLoadingData(allCursor);

            Utils.logD("Scan Data End");

            close();

            return scannedParts;
        }

        public void getScanConsList(Context context, int tag, boolean isInvoice) {
            InvoicesScreen invoicesScreen = null;
            LoadingScanningScreen scanScreen = null;
            if (isInvoice)
                invoicesScreen = (InvoicesScreen) context;
            else
                scanScreen = (LoadingScanningScreen) context;

            openRead();
            String vehNo = "";
            if (Utils.isValidString(Globals.vehicleNo))
                vehNo = Globals.vehicleNo;


            Utils.logD("Scan Data Start");

            String allDataSql = "SELECT * FROM "
                    + DataHelper.LOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_PARTS_VEHICLE_NO + " = '" + vehNo + "'";

            String toBeScannedSql = "SELECT * FROM "
                    + DataHelper.LOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' ";

            String scanInProgressSql = "SELECT * FROM "
                    + DataHelper.LOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.LOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND "
                    + DataHelper.LOADING_PARTS_COLUMN_STATUS + " = 1";


            ArrayList<NewLoadingPartsModel> toBeScannedDockets = new ArrayList<NewLoadingPartsModel>();
            ArrayList<NewLoadingPartsModel> scanInProDockets = new ArrayList<NewLoadingPartsModel>();

            Utils.logD("Scan Data End");

            close();

        }

        public ArrayList<NewLoadingPartsModel> getLoadingData(Cursor cursor) {


            ArrayList<NewLoadingPartsModel> loadingList = new ArrayList<NewLoadingPartsModel>();

            Utils.logD(String.valueOf(cursor.getCount()));
            if (cursor.moveToFirst()) {
                int colId = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_ID);
                int vehNo = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_VEHICLE_NO);
                int partNo = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_PART_NO);
                int partId = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_PART_ID);
                int totBoxes = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_TOTAL_BOXES);
                int totQty = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_TOTAL_QTY);

                int binId = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_BIN_ID);
                int binNo = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_BIN_NO);

                int qtyScanned = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_QTY_SCANNED);
                int status = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_STATUS);
                int scanTime = cursor
                        .getColumnIndex(DataHelper.LOADING_PARTS_COLUMN_SCAN_TIME);


                do {
                    NewLoadingPartsModel loadingaPartsModel = new NewLoadingPartsModel();

                    int id = cursor.getInt(colId);
                    loadingaPartsModel.setId(id);

                    String vehNumber = cursor.getString(vehNo);
                    if (Utils.isValidString(vehNumber))
                        loadingaPartsModel.setVehNo(vehNumber);

                    String partNum = cursor.getString(partNo);
                    if (Utils.isValidString(partNum))
                        loadingaPartsModel.setPartNo(partNum);
                    else
                        loadingaPartsModel.setPartNo("");

                    int partid = cursor.getInt(partId);
                    loadingaPartsModel.setPartId(partid);

                    int tot_boxes = cursor.getInt(totBoxes);
                    loadingaPartsModel.setTotalBoxes(tot_boxes);

                    int tot_qty = cursor.getInt(totQty);
                    loadingaPartsModel.setTotalQty(tot_qty);

                    int qty_scanned = cursor.getInt(qtyScanned);
                    loadingaPartsModel.setScannedCount(qty_scanned);

                    String scanned_status = cursor.getString(status);
                    loadingaPartsModel.setIsScanned((scanned_status));

                    String scan_time = cursor.getString(scanTime);
                    if (Utils.isValidString(scan_time))
                        loadingaPartsModel.setScanTime(scan_time);

                    String bin_no = cursor.getString(binNo);
                    if (Utils.isValidString(bin_no))
                        loadingaPartsModel.setBinNo(bin_no);

                    String bin_id = cursor.getString(binId);
                    if (Utils.isValidString(bin_id))
                        loadingaPartsModel.setBinId(bin_id);


                    loadingList.add(loadingaPartsModel);


                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();

            Utils.logD(String.valueOf(loadingList.size()));

            return loadingList;
        }

        public void updatePartScan(Context context, String vehicleNo, int qtyScanned, String partNo) {
            String scanTime = Utils.getScanTimeLoading();
            LoadingScanningScreen loadingScanningScreen = (LoadingScanningScreen) context;
            openRead();
            try {

                String updateSql = "UPDATE " + DataHelper.LOADING_PARTS_TABLE_NAME
                        + " SET " + DataHelper.LOADING_PARTS_COLUMN_QTY_SCANNED + " = " + qtyScanned
                        + " , " + DataHelper.LOADING_PARTS_COLUMN_STATUS + " = 1 ,"
                        + DataHelper.LOADING_PARTS_COLUMN_SCAN_TIME + " = '" + scanTime
                        + "' WHERE " + DataHelper.LOADING_PARTS_VEHICLE_NO + " ='" + vehicleNo + "' AND "
                        + DataHelper.LOADING_PARTS_COLUMN_PART_NO + " ='" + partNo + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);


            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            loadingScanningScreen.resetAllFragmentData();
        }

        public class Save extends AsyncTask<Object, Object, Object> {
            @Override
            protected void onPreExecute() {
                showProgressDialog();
                Utils.logD("Start saving the data");
            }

            @Override
            protected List<ProductModel> doInBackground(Object... arg0) {

                try {
                    long c = cacheSheetData(context, loadingResponse, vehicleNo);
                    Utils.logD("Count : " + c);
                } catch (Exception e) {
                    Utils.logE(e.toString());
                }
                return null;

            }

            @Override
            protected void onPostExecute(Object result) {

                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();

                    loadingScanningScreen.resetAllFragmentData();
                    pDialog = null;
                    isProgress = false;

                    Utils.logD("Loaded data");
                }

                super.onPostExecute(result);
            }
        }

    }


}
