package com.apptmyz.wms.custom;

public interface CounterListener {
    void onIncrementClick(String value);
    void onDecrementClick(String value);
    void onEditValue(String value);
}
