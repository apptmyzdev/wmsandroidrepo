package com.apptmyz.wms;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptmyz.wms.custom.BitmapScalingUtil;
import com.apptmyz.wms.custom.CameraActivity;
import com.apptmyz.wms.custom.CounterView;
import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.ExceptionModel;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.InvoiceModel;
import com.apptmyz.wms.data.NewProductModel;
import com.apptmyz.wms.data.TruckImageModel;
import com.apptmyz.wms.data.UnloadingResponse;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.fileutils.ImageLoader;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.ScanUtils;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.apptmyz.wms.wmsfragments.ExceptionFragment;
import com.apptmyz.wms.wmsfragments.ScanInProgress;
import com.apptmyz.wms.wmsfragments.ToBeScanned;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PartsScanningScreen extends Activity {

    public final int TAKE_PHOTO = 1;
    public final int TAKE_STACKING_PHOTO = 2;
    public ToBeScanned toBeScanned;
    public ScanInProgress scanInProgress;
    public ExceptionFragment exceptionFragment;
    private Context context;
    private DataSource dataSource;
    private List<NewProductModel> newProductModelList;
    private RadioGroup radioGroup;
    private EditText searchView;
    private ImageView closeIv;
    private FragmentManager fm;
    private Button vehicleNoBtn;
    private TextView weightTv, percentTv;
    private RelativeLayout boxCountPopup, reasonLayout;
    private EditText boxCountEt, scannedBoxEt;
    private NewProductModel selectedProductModel = new NewProductModel();
    private TextView productNoTv, scannedCountTv;
    private Button boxSubmitBtn, addBtn, subtractBtn, reconcileBtn, manualBtn, autoBtn;
    private CounterView counterView, exceptionCv;
    private TextView partNoTv;
    private EditText reasonsEt;
    private Button captureImageBtn, sumbitBtn, submitReasonBtn, dismissExceptionBtn;
    private RadioGroup exceptionRg;
    private RadioButton exceptionRb, extraRb, shortRb;
    private RelativeLayout exceptionPopup, searchRl;
    private ImageView exceptionIv;
    private String truckImageFileName, stackingImgFileName, barcodeNum, requiredBarcodeText;
    private EditText exceptionTypeEt, extraReasonEt;
    private LinearLayout exceptionLl, reasonLl, goodLl;
    private RelativeLayout exceptionCountLayout;
    private int maxCount = -1,
            minCount = -1;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private int isManual;
    private TimerTask timerScanTask;
    private ImageView proceedIv;

    private ArrayList<Bitmap> imgBitmaps = new ArrayList<>();
    private ArrayList<String> imgFileNames;
    private Button captureNewBtn, cancelImgViewBtn;
    private RecyclerView imagesRv;
    private ConstraintLayout imagesView;
    private ImagesAdapter imagesAdapter;
    private int imagePosition = -1, stackingImagePosition = -1;
    private boolean isSearch;
    private String searchQuery = "";

    private RelativeLayout imageLayout;
    private RecyclerView stackingImagesRv;
    private Button sendBtn, captureBtn;
    private ImagesAdapter stackingImagesAdapter;
    private ArrayList<Bitmap> stackingImgBitmaps = new ArrayList<>();
    private ArrayList<String> stackingImgFileNames;
    private ImageView truckIv;
    private TextView msgTv, countTv;
    private Gson gson = new Gson();
    private boolean isStackingImg = true;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_capture_image:
                    stackingImgFileName = "";
                    stackingImagePosition = -1;
                    if (Utils.isValidArrayList(stackingImgFileNames) && stackingImgFileNames.size() == 3) {
                        Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                    } else {
                        takePicture(context, TAKE_STACKING_PHOTO);
                    }
                    break;
                case R.id.btn_send_image:
                    if (Utils.isValidArrayList(stackingImgFileNames)) {
                        Utils.logE("in LT image");

                        String text = "";

                        String truck = Globals.vehicleNo;
                        if (Utils.isValidString(truck)) {
                            text = text + "\n" + "Veh#  : " + truck;
                        }

                        String s = dataSource.sharedPreferences
                                .getValue(Constants.IMEI);
                        if (Utils.isValidString(s))
                            text = text + "\n" + "T-ID# : " + s;

                        String user = dataSource.sharedPreferences
                                .getValue(Constants.USERNAME_PREF);
                        if (Utils.isValidString(user))
                            text = text + "\n" + "U-ID# : " + user;

                        text = text + "\n" + "I-DT# : "
                                + Utils.getITime(Long.valueOf(stackingImgFileName));

                        imageLayout.setVisibility(View.GONE);
                        TruckImageModel model = new TruckImageModel();

                        model.setVehTxnDate(Utils.getScanTimeLoading());
                        model.setLoadUnloadFlag(Constants.UNLOADING_STARTED);

                        model.setVehicleNo(Globals.vehicleNo);

                        ImageLoader loader = new ImageLoader(context, Constants.TRUCK);
                        try {
                            String base64 = loader.fileCache.getBase64StringToName(
                                    stackingImgFileName, text, context);
                            if (Utils.isValidArrayList(stackingImgBitmaps)) {
                                List<String> list = getBase64s(stackingImgBitmaps);
                                model.setImages(list);
                                Utils.logD(model.toString());
                                new SendStackingImage().execute(model);
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utils.logE("in else ");
                        Utils.showSimpleAlert(
                                context,
                                getString(R.string.alert_dialog_title),
                                "Please capture stacking image");
                    }
                    break;
                case R.id.btn_captue_new:
                    if (Utils.isValidArrayList(imgBitmaps) && imgBitmaps.size() == 3) {
                        Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                    } else {
                        imagePosition = -1;
                        takePicture(context, TAKE_PHOTO);
                    }
                    break;
                case R.id.btn_cancel_imgs:
                    if (imagesAlert != null && imagesAlert.isShowing())
                        imagesAlert.dismiss();
                    break;

                case R.id.btn_submit_reason:
                    String reasonExtra = extraReasonEt.getText().toString().trim();
                    if (Utils.isValidString(reasonExtra)) {
                        reasonLayout.setVisibility(View.GONE);
                        markAsExtra(reasonExtra);
                    } else {
                        Utils.showSimpleAlert(context, "Alert", "Please Enter Valid Reason");
                    }
                    break;
                case R.id.btn_auto:
                    isManual = 0;
                    scannedBoxEt.requestFocus();
                    break;
                case R.id.iv_proceed:
                    barcodeNum = scannedBoxEt.getText().toString().trim();
                    updateBoxScan();
                    break;
                case R.id.btn_manual:
                    isManual = 1;
                    scannedBoxEt.requestFocus();
                    break;
                case R.id.substract_btn:
                    String count1 = boxCountEt.getText().toString();
                    if (Utils.isValidString(count1)) {
                        int i1 = Integer.parseInt(count1) - 1;
                        if (i1 >= minCount) {
                            boxCountEt.setText(Integer.toString(i1));
                        }
                    }
                    break;

                case R.id.add_btn:
                    String count2 = boxCountEt.getText().toString();
                    if (Utils.isValidString(count2)) {
                        int i2 = Integer.parseInt(count2) + 1;
                        if (maxCount > 0 && i2 <= maxCount) {
                            boxCountEt.setText(Integer.toString(i2));
                        }
                    }
                    break;

                case R.id.btn_reconcile:
                    Intent intent = new Intent(context, PartsReconcileScreen.class);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.btn_dismiss:
                    if (exceptionAlert != null && exceptionAlert.isShowing())
                        exceptionAlert.dismiss();
                    break;

                case R.id.btn_send:
                    if (imagesAlert != null && imagesAlert.isShowing())
                        imagesAlert.dismiss();
                    String reason = reasonsEt.getText().toString();
                    String excepDesc = exceptionTypeEt.getText().toString();
                    String exceptionType = "";
                    if (exceptionRg.getCheckedRadioButtonId() == R.id.rb_capture_exception) {
                        exceptionType = dataSource.exceptionTypes.getExceptionId(excepDesc);
                    } else if (exceptionRg.getCheckedRadioButtonId() == R.id.rb_short) {
                        exceptionType = Constants.SHORT;
                    }
                    int exceptionCount = exceptionCv.getValue();
                    int maxLimit = exceptionCv.getmMaxLimit();
                    if (exceptionRg.getCheckedRadioButtonId() == R.id.rb_good) {
                        int count3 = counterView.getValue();
                        if (count3 != 0) {
                            if (count3 >= minCount && count3 <= maxCount) {
                                String partNo = partNoTv.getText().toString();
                                dataSource.unloadingParts.updatePartScan(context, Globals.vehicleNo, selectedProductModel.getScannedCount() + (count3), selectedProductModel.getDataType(),
                                        partNo);
                                exceptionRg.check(R.id.rb_good);
                                exceptionPopup.setVisibility(View.GONE);
                                if (exceptionAlert != null && exceptionAlert.isShowing())
                                    exceptionAlert.dismiss();
                                Utils.dissmissKeyboard(boxCountEt);
                                // TODO: 21/01/21 streaming update
                            } else {
                                Utils.showToast(context, getString(R.string.alert_box));
                            }
                        } else {
                            Utils.showToast(context, getString(R.string.alert_box));
                        }
                    } else {
                        if (Utils.isValidString(reason)) {
                            if (Utils.isValidString(exceptionType)) {
                                if (exceptionRg.getCheckedRadioButtonId() == R.id.rb_capture_exception) {
                                    int excepTypeVal = Integer.parseInt(exceptionType);
                                    if (exceptionType.equals(Constants.EXTRA) || Utils.isValidArrayList(imgFileNames)) {
                                        if (exceptionCount != 0 && (exceptionType.equals(Constants.EXTRA) || exceptionCount <= maxLimit)) {
                                            ExceptionModel exceptionModel = new ExceptionModel();
                                            exceptionModel.setScanTime(Utils.getScanTime());
                                            exceptionModel.setExcepType(excepTypeVal);
                                            exceptionModel.setExcepDesc(reason);
                                            exceptionModel.setImgBase64s(imgFileNames);
                                            exceptionModel.setExceptionCount(exceptionCount);

                                            dataSource.exceptions.insertException(context, selectedProductModel.getDataType(), exceptionModel, Globals.partNo, Globals.vehicleNo);
                                            exceptionIv.setImageBitmap(null);
                                            exceptionTypeEt.setText(null);
                                            exceptionRg.check(R.id.rb_good);
                                            exceptionPopup.setVisibility(View.GONE);
                                            if (exceptionAlert != null && exceptionAlert.isShowing())
                                                exceptionAlert.dismiss();
                                            imgBitmaps.clear();
                                            // TODO: 21/01/21 streaming update
                                        } else {
                                            Utils.showToast(context, "Please Enter Valid Exception Count");
                                        }
                                    } else {
                                        Utils.showToast(context, "Please capture image");
                                    }
                                } else {
                                    if (exceptionCount != 0 && exceptionCount <= maxLimit) {
                                        ExceptionModel exceptionModel = new ExceptionModel();
                                        exceptionModel.setScanTime(Utils.getScanTime());
                                        exceptionModel.setExcepType(Integer.parseInt(exceptionType));
                                        exceptionModel.setExcepDesc(reason);
                                        exceptionModel.setExceptionCount(exceptionCount);

                                        dataSource.exceptions.insertException(context, selectedProductModel.getDataType(), exceptionModel, Globals.partNo, Globals.vehicleNo);
                                        exceptionRg.check(R.id.rb_good);
                                        exceptionPopup.setVisibility(View.GONE);
                                        if (exceptionAlert != null && exceptionAlert.isShowing())
                                            exceptionAlert.dismiss();
                                        // TODO: 21/01/21 streaming update
                                    } else {
                                        Utils.showToast(context, "Please Enter Valid Short Count");
                                    }
                                }
                            } else {
                                Utils.showToast(context, "Please select exception type");
                            }
                        } else {
                            Utils.showToast(context, "Please enter valid reason");
                        }
                    }
                    break;

                case R.id.btn_captureimage:
                    truckImageFileName = "";
                    if (!Utils.isValidArrayList(imgBitmaps)) {
                        imagePosition = -1;
                        takePicture(context, TAKE_PHOTO);
                    } else {
                        if (imagesAlert == null || !imagesAlert.isShowing()) {
                            showImagesDialog(imgBitmaps);
                        }
                    }
                    break;

                case R.id.et_exception_type:
                    List<String> depsList = new ArrayList<>();
                    List<String> exceptionsList = dataSource.exceptionTypes.getExceptionList();
                    if (selectedProductModel.getDataType().equalsIgnoreCase("MB")) {
                        for (String exception : exceptionsList) {
                            if (!exception.equalsIgnoreCase("Extra")) {
                                depsList.add(exception);
                            }
                        }
                    } else {
                        depsList.addAll(dataSource.exceptionTypes.getExceptionList());
                    }
                    String[] a = new String[depsList.size()];
                    a = depsList.toArray(a);
                    showExceptionType(exceptionTypeEt, a);
                    break;
            }
        }
    };

    private List<String> getBase64s(List<Bitmap> bitmaps) {
        List<String> list = new ArrayList<>();
        for (Bitmap bitmap : bitmaps) {
            String base64 = getBase64(bitmap);
            list.add(base64);
        }
        return list;
    }

    private String getBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70,
                byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        encoded = encoded.replace("\n", "");
        return encoded;
    }

    private LinearLayout toBeScannedLayout, scanInProgressLayout,
            scanCompletedLayout, exceptionLayout, topFrags;

    private RadioGroup.OnCheckedChangeListener changeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
            resetAllFragmentData();
            switch (i) {
                case R.id.rb_tobescanned:
                    showView(toBeScannedLayout);
                    hideView(scanInProgressLayout);
                    hideView(scanCompletedLayout);
                    hideView(exceptionLayout);
                    break;

                case R.id.rb_recentscanned:
                    hideView(toBeScannedLayout);
                    showView(scanInProgressLayout);
                    hideView(scanCompletedLayout);
                    hideView(exceptionLayout);
                    break;

                case R.id.rb_scancompleted:
                    hideView(toBeScannedLayout);
                    hideView(scanInProgressLayout);
                    showView(scanCompletedLayout);
                    hideView(exceptionLayout);
                    break;

                case R.id.rb_exception:
                    hideView(toBeScannedLayout);
                    hideView(scanInProgressLayout);
                    hideView(scanCompletedLayout);
                    showView(exceptionLayout);
                    break;

                case R.id.rb_capture_exception:
                    captureImageBtn.setVisibility(View.VISIBLE);
                    exceptionIv.setVisibility(View.VISIBLE);
                    exceptionLl.setVisibility(View.VISIBLE);
                    exceptionCountLayout.setVisibility(View.VISIBLE);
                    goodLl.setVisibility(View.GONE);
                    reasonLl.setVisibility(View.VISIBLE);
                    break;

                case R.id.rb_extra:
                    captureImageBtn.setVisibility(View.GONE);
                    exceptionIv.setVisibility(View.GONE);
                    exceptionLl.setVisibility(View.GONE);
                    exceptionCountLayout.setVisibility(View.GONE);
                    goodLl.setVisibility(View.GONE);
                    reasonLl.setVisibility(View.VISIBLE);
                    break;

                case R.id.rb_short:
                    captureImageBtn.setVisibility(View.GONE);
                    exceptionIv.setVisibility(View.GONE);
                    exceptionLl.setVisibility(View.GONE);
                    exceptionCountLayout.setVisibility(View.VISIBLE);
                    goodLl.setVisibility(View.GONE);
                    reasonLl.setVisibility(View.VISIBLE);
                    break;
                case R.id.rb_good:
                    captureImageBtn.setVisibility(View.GONE);
                    exceptionIv.setVisibility(View.GONE);
                    exceptionLl.setVisibility(View.GONE);
                    exceptionCountLayout.setVisibility(View.GONE);
                    reasonLl.setVisibility(View.GONE);
                    goodLl.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
    };

    private void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    private void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.parts_scanning_screen);

        context = PartsScanningScreen.this;

        dataSource = new DataSource(context);
        searchRl = (RelativeLayout) findViewById(R.id.searchRl);
        closeIv = (ImageView) findViewById(R.id.iv_close_icon);
        searchView = (EditText) findViewById(R.id.searchView);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.setText(null);
                searchView.clearFocus();
                toggleKeyboard(PartsScanningScreen.this);
            }
        });

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isSearch = true;
                searchQuery = charSequence.toString().toUpperCase();
                resetAllFragmentData();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        searchRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.requestFocus();
                toggleKeyboard(PartsScanningScreen.this);
            }
        });

        fm = getFragmentManager();

        imagesRv = (RecyclerView) findViewById(R.id.rv_exception_images);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));
        imagesView = (ConstraintLayout) findViewById(R.id.layout_image_capture);
        captureNewBtn = (Button) findViewById(R.id.btn_captue_new);
        cancelImgViewBtn = (Button) findViewById(R.id.btn_cancel_imgs);
        cancelImgViewBtn.setOnClickListener(listener);
        captureNewBtn.setOnClickListener(listener);

        imagesAdapter = new ImagesAdapter(imgBitmaps);
        imagesRv.setAdapter(imagesAdapter);

        extraReasonEt = (EditText) findViewById(R.id.reason_extra_et);
        submitReasonBtn = (Button) findViewById(R.id.btn_submit_reason);
        submitReasonBtn.setOnClickListener(listener);
        reasonLayout = (RelativeLayout) findViewById(R.id.layout_reason_extra);

        toBeScannedLayout = (LinearLayout) findViewById(R.id.tobe_scanned_layout);
        scanInProgressLayout = (LinearLayout) findViewById(R.id.sca_inprogress_layout);
        scanCompletedLayout = (LinearLayout) findViewById(R.id.scan_completed_layout);
        exceptionLayout = (LinearLayout) findViewById(R.id.exception_layout);

        vehicleNoBtn = (Button) findViewById(R.id.btn_sheet);
        scannedBoxEt = (EditText) findViewById(R.id.scanned_pkg);
        scannedBoxEt.addTextChangedListener(scanWatcher);
        weightTv = (TextView) findViewById(R.id.tv_weight);
        setVehicleInwardNumbers();

        radioGroup = (RadioGroup) findViewById(R.id.rg_display_type);
        radioGroup.setOnCheckedChangeListener(changeListener);
        percentTv = (TextView) findViewById(R.id.tv_percentage);

        proceedIv = (ImageView) findViewById(R.id.iv_proceed);
        proceedIv.setOnClickListener(listener);

        boxCountEt = (EditText) findViewById(R.id.et_box_count);

        addBtn = (Button) findViewById(R.id.add_btn);
        addBtn.setOnClickListener(listener);

        subtractBtn = (Button) findViewById(R.id.substract_btn);
        subtractBtn.setOnClickListener(listener);

        reconcileBtn = (Button) findViewById(R.id.btn_reconcile);
        reconcileBtn.setOnClickListener(listener);

        manualBtn = (Button) findViewById(R.id.btn_manual);
        manualBtn.setOnClickListener(listener);

        autoBtn = (Button) findViewById(R.id.btn_auto);
        autoBtn.setOnClickListener(listener);

        exceptionRb = (RadioButton) findViewById(R.id.rb_capture_exception);
        shortRb = (RadioButton) findViewById(R.id.rb_short);
        extraRb = (RadioButton) findViewById(R.id.rb_extra);

        exceptionRg = (RadioGroup) findViewById(R.id.rg_exception);
        exceptionRg.setOnCheckedChangeListener(changeListener);

        captureImageBtn = (Button) findViewById(R.id.btn_captureimage);
        captureImageBtn.setOnClickListener(listener);

        sumbitBtn = (Button) findViewById(R.id.btn_send);
        sumbitBtn.setOnClickListener(listener);
        dismissExceptionBtn = (Button) findViewById(R.id.btn_dismiss);
        dismissExceptionBtn.setOnClickListener(listener);
        reasonsEt = (EditText) findViewById(R.id.reasons_et);
        partNoTv = (TextView) findViewById(R.id.part_no_tv);
        exceptionIv = (ImageView) findViewById(R.id.iv_exception_image);
        msgTv = (TextView) findViewById(R.id.tv_msg);
        countTv = (TextView) findViewById(R.id.tv_count);
        exceptionTypeEt = (EditText) findViewById(R.id.et_exception_type);
        exceptionTypeEt.setOnClickListener(listener);

        counterView = (CounterView) findViewById(R.id.counterView);
        exceptionCv = (CounterView) findViewById(R.id.exceptionCv);
        exceptionCv.setValue(0);

        exceptionPopup = (RelativeLayout) findViewById(R.id.layout_capture_exception_popup);

        exceptionLl = (LinearLayout) findViewById(R.id.exception_ll);
        reasonLl = (LinearLayout) findViewById(R.id.reason_ll);
        goodLl = (LinearLayout) findViewById(R.id.good_ll);
        scannedCountTv = (TextView) findViewById(R.id.tv_scanned_count);

        exceptionCountLayout = (RelativeLayout) findViewById(R.id.exceptionCountLayout);

        toBeScanned = (ToBeScanned) fm
                .findFragmentById(R.id.tobe_scanned_fragment);
        scanInProgress = (ScanInProgress) fm
                .findFragmentById(R.id.scan_inprogress_fragment);
        exceptionFragment = (ExceptionFragment) fm.findFragmentById(R.id.exception_fragment);

        imageLayout = (RelativeLayout) findViewById(R.id.layout_image_popup);

        captureBtn = (Button) findViewById(R.id.btn_capture_image);
        captureBtn.setOnClickListener(listener);

        sendBtn = (Button) findViewById(R.id.btn_send_image);
        sendBtn.setOnClickListener(listener);

        truckIv = (ImageView) findViewById(R.id.iv_truck);
        truckIv.setOnClickListener(listener);

        stackingImagesRv = (RecyclerView) findViewById(R.id.rv_images);
        stackingImagesRv.setLayoutManager(new LinearLayoutManager(context));

        if (Utils.isValidArrayList(stackingImgBitmaps)) {
            stackingImagesAdapter = new ImagesAdapter(stackingImgBitmaps);
            stackingImagesRv.setAdapter(stackingImagesAdapter);
        }

        msgTv.setText("Before Starting Unloading, Take Picture of shipment stacked inside vehicle, Keeping back doors Open");

        if (Utils.isValidString(Globals.vehicleNo)) {
            if (dataSource.unloadingParts.getPartsData(Globals.vehicleNo))
                resetAllFragmentData();
            else
                new UnloadingTask().execute(Globals.vehicleNo);
        }
    }

    private void toggleKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.toggleSoftInputFromWindow(
                view.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }

    private void setVehicleInwardNumbers() {
        vehicleNoBtn.setText(getString(R.string.vehicle_num) + " : ");
        if (Utils.isValidString(Globals.vehicleNo))
            vehicleNoBtn.append(Globals.vehicleNo);

        if (Utils.isValidString(Globals.inwardNo))
            weightTv.setText("Inward Num: " + Globals.inwardNo);
    }

    private AlertDialog exceptionAlert;

    private void showExceptionDialog() {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.capture_exception_popup, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        exceptionAlert = alertDialog.show();
        exceptionRb = (RadioButton) view.findViewById(R.id.rb_capture_exception);
        shortRb = (RadioButton) view.findViewById(R.id.rb_short);
        extraRb = (RadioButton) view.findViewById(R.id.rb_extra);

        exceptionRg = (RadioGroup) view.findViewById(R.id.rg_exception);
        exceptionRg.setOnCheckedChangeListener(changeListener);

        captureImageBtn = (Button) view.findViewById(R.id.btn_captureimage);
        captureImageBtn.setOnClickListener(listener);

        sumbitBtn = (Button) view.findViewById(R.id.btn_send);
        sumbitBtn.setOnClickListener(listener);
        dismissExceptionBtn = (Button) view.findViewById(R.id.btn_dismiss);
        dismissExceptionBtn.setOnClickListener(listener);
        reasonsEt = (EditText) view.findViewById(R.id.reasons_et);
        partNoTv = (TextView) view.findViewById(R.id.part_no_tv);
        exceptionIv = (ImageView) view.findViewById(R.id.iv_exception_image);
        msgTv = (TextView) view.findViewById(R.id.tv_msg);
        countTv = (TextView) view.findViewById(R.id.tv_count);
        exceptionTypeEt = (EditText) view.findViewById(R.id.et_exception_type);
        exceptionTypeEt.setOnClickListener(listener);

        counterView = (CounterView) view.findViewById(R.id.counterView);
        exceptionCv = (CounterView) view.findViewById(R.id.exceptionCv);
        exceptionCv.setValue(0);

        exceptionPopup = (RelativeLayout) view.findViewById(R.id.layout_capture_exception_popup);

        exceptionLl = (LinearLayout) view.findViewById(R.id.exception_ll);
        reasonLl = (LinearLayout) view.findViewById(R.id.reason_ll);
        goodLl = (LinearLayout) view.findViewById(R.id.good_ll);
        scannedCountTv = (TextView) view.findViewById(R.id.tv_scanned_count);

        exceptionCountLayout = (RelativeLayout) view.findViewById(R.id.exceptionCountLayout);

        sendBtn.setOnClickListener(listener);
        exceptionTypeEt.setOnClickListener(listener);
        captureImageBtn.setOnClickListener(listener);
    }

    ProgressDialog unloadingDialog;

    class UnloadingTask extends AsyncTask<String, Object, Object> {
        UnloadingResponse unloadingResponse;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                unloadingDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String vehNo = params[0];

                String url = WmsUtils.getUnloadingUrlTag(vehNo);

                unloadingResponse = (UnloadingResponse) HttpRequest
                        .getInputStreamFromUrl(url, UnloadingResponse.class, context);
                if (unloadingResponse != null) {
                    Utils.logD(unloadingResponse.toString());
                    if (unloadingResponse.getStatus()) {
                        Globals.unloadingResponse = unloadingResponse;
                    } else {
                        Globals.lastErrMsg = unloadingResponse.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && unloadingDialog != null && unloadingDialog.isShowing()) {
                unloadingDialog.dismiss();
            }
            if (showErrorDialog()) {
                if (unloadingResponse.getData().isImgFlag()) {
                    imageLayout.setVisibility(View.VISIBLE);
                } else {
                    if (unloadingResponse != null && Utils.isValidArrayList((ArrayList<?>) unloadingResponse.getData().getPartList())) {
                        Globals.inwardNo = unloadingResponse.getData().getInwardNo();
                        setVehicleInwardNumbers();
                        dataSource.unloadingParts.saveData(context, unloadingResponse, Globals.vehicleNo);
                    }
                }
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
        if (isLogout)
            dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

        try {
            if (!isFinishing()) {
                if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
                    errDlg = new CustomAlertDialog(PartsScanningScreen.this, Globals.lastErrMsg,
                            true, isLogout);
                    errDlg.setTitle(getString(R.string.alert_dialog_title));
                    errDlg.setCancelable(false);
                    Globals.lastErrMsg = "";
                    Utils.dismissProgressDialog();
                    isNotErr = false;
                    errDlg.show();
                }
            }
        } catch (Exception e) {
            Utils.logE(e.toString());
        }
        return isNotErr;
    }

    class SendStackingImage extends AsyncTask<TruckImageModel, Object, Object> {
        GeneralResponse response;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            Utils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(TruckImageModel... params) {

            try {
                Globals.lastErrMsg = "";

                TruckImageModel model = params[0];

                String url = WmsUtils.getImageSubmitUrlTag();

                String data = gson.toJson(model);

                response = (GeneralResponse) HttpRequest
                        .postData(url, data, GeneralResponse.class, context);
                if (response != null) {
                    if (response.isStatus()) {
                        Utils.logD(response.toString());
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Object result) {
            Utils.dismissProgressDialog();
            if (showErrorDialog()) {
                if (Globals.unloadingResponse != null && Utils.isValidArrayList((ArrayList<?>) Globals.unloadingResponse.getData().getPartList())) {
                    Globals.inwardNo = (String) response.getData();
                    setVehicleInwardNumbers();
                    dataSource.unloadingParts.saveData(context, Globals.unloadingResponse, Globals.vehicleNo);
                }
            }
            super.onPostExecute(result);
        }
    }

    private TextWatcher scanWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = scannedBoxEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue + " len: " + scannedValue.length());
                                    if (isManual == 0 && scannedValue.length() > 0) {
                                        barcodeNum = scannedValue;
                                        updateBoxScan();//it's auto scan
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }
                            }
                        });
                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }
        }
    };

    private void updateBoxScan() {
        boolean isExtra = false;
        if (Utils.isValidString(barcodeNum) && barcodeNum.length() >= Globals.barcodeLength && barcodeNum.length() >= Globals.barcodeMaxLength) {
            requiredBarcodeText = barcodeNum.substring(Globals.barcodeMinLength - 1, Globals.barcodeMaxLength);
            NewProductModel model = dataSource.unloadingParts.getUnloadingData(requiredBarcodeText);
            if (model != null) {
                Globals.partNo = Utils.getNumber(model);
                int invoiceScannedQty = 0;
                if (Utils.isValidArrayList((ArrayList<?>) model.getInvoiceList())) {
                    InvoiceModel invoice = model.getInvoiceList().get(0);
                    if (invoice != null) {
                        if (invoice.getNoOfBoxes() <= invoice.getScannedBoxes() || model.getScannedCount() >= model.getTotalBoxes()) {
                            isExtra = true;
                            scannedBoxesCount = model.getScannedCount();
                            invoiceScannedQtyCount = invoiceScannedQty;
                            showIsExtraPopup();
                        }
                        invoiceScannedQty = invoice.getScannedBoxes() + 1;
                    }
                } else {
                    if (model.getScannedCount() >= model.getTotalBoxes()) {
                        isExtra = true;
                        scannedBoxesCount = model.getScannedCount();
                        invoiceScannedQtyCount = invoiceScannedQty;
                        showIsExtraPopup();
                    }
                }
                scannedBoxEt.setText(null);
                if (!isExtra) {
                    dataSource.unloadingParts.updateScanCount(context, requiredBarcodeText, model.getScannedCount() + 1, invoiceScannedQty);
                }
            } else {
                scannedBoxEt.setText(null);
            }
        }
    }

    private void showIsExtraPopup() {
        String message = "All the boxes are Already Scanned. Do you want to mark this as an Extra?";
        isExtraDialog = new IsExtraDialog(message);
        isExtraDialog.setCancelable(false);
        isExtraDialog.show();
    }

    IsExtraDialog isExtraDialog = null;
    int scannedBoxesCount, invoiceScannedQtyCount;

    public class IsExtraDialog extends Dialog {
        private TextView messageTv;
        private Button yesBtn, noBtn;
        private String msg;

        public IsExtraDialog(String message) {
            super(PartsScanningScreen.this, R.style.CustomAlertDialog);
            msg = message;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_alert_dialog_2);

            messageTv = (TextView) findViewById(R.id.tv_error_message);
            messageTv.setText(msg);

            yesBtn = (Button) findViewById(R.id.btn_go);
            noBtn = (Button) findViewById(R.id.btn_cancel);
            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isExtraDialog.dismiss();
                    reasonLayout.setVisibility(View.VISIBLE);
                }
            });
            noBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
        }
    }

    private void markAsExtra(String reason) {
        dataSource.unloadingParts.updateExcepType(Constants.EXTRA, requiredBarcodeText);
        ExceptionModel exceptionModel = new ExceptionModel();
        exceptionModel.setScanTime(Utils.getScanTime());
        exceptionModel.setExcepDesc(reason);
        exceptionModel.setExcepType(Integer.parseInt(Constants.EXTRA));
        exceptionModel.setExceptionCount(1);
        String dataType = dataSource.sharedPreferences.getValue(Constants.DATA_TYPE);
        dataSource.exceptions.insertException(context, dataType, exceptionModel, Globals.partNo, Globals.vehicleNo);
        dataSource.unloadingParts.updateScanCount(context, requiredBarcodeText, scannedBoxesCount, invoiceScannedQtyCount);
    }

    public void resetAllFragmentData() {
        Utils.dismissProgressDialog();
        int id = radioGroup.getCheckedRadioButtonId();
        int tag = -1;

        switch (id) {
            case R.id.rb_tobescanned:
                tag = Constants.toBeScannedTag;
                break;

            case R.id.rb_recentscanned:
                tag = Constants.scanInProgressTag;
                break;

            case R.id.rb_scancompleted:
                tag = Constants.scanCompletedTag;
                break;

            case R.id.rb_exception:
                tag = Constants.exceptionTag;
                break;

            default:
                break;
        }

        if (isSearch) {
            int filterType = 1;
            String dataType = dataSource.sharedPreferences.getValue(Constants.DATA_TYPE);
            if (dataType.equalsIgnoreCase("P")) {
                filterType = 1;
            } else if (dataType.equalsIgnoreCase("DA")) {
                filterType = 2;
            } else if (dataType.equalsIgnoreCase("IN")) {
                filterType = 3;
            } else if (dataType.equalsIgnoreCase("MB")) {
                filterType = 4;
            }
            dataSource.unloadingParts.getFilteredScanConsList(context, tag, filterType, searchQuery, false);
        } else {
            dataSource.unloadingParts.getScanConsList(context, tag, false);
        }
        setVehicleInwardNumbers();
    }

    public void showExceptionPopup(NewProductModel model) {
        int scannedCount = model.getScannedCount();
        String productNo = Utils.getNumber(model);
        int totPackets = model.getTotalBoxes();

        int exceptionCount = dataSource.exceptions.getExceptionCount(productNo, model.getDataType(), model.getVehicleNo());

        scannedCountTv.setText(Integer.toString(scannedCount));
        maxCount = totPackets - (scannedCount + exceptionCount);
        minCount = 1;
        counterView.setmMaxLimit(maxCount);
        counterView.setmMinLimit(scannedCount == 0 ? 1 : scannedCount);
        counterView.setValue(minCount);

        selectedProductModel = model;
        showExceptionDialog();
        int count = dataSource.exceptions.getExceptionCount(Utils.getNumber(model), model.getDataType(), model.getVehicleNo());
        int extrasCount = dataSource.exceptions.getExtrasCount(Utils.getNumber(model), model.getDataType(), model.getVehicleNo());
        countTv.setText((scannedCount + count + extrasCount) + "/" + totPackets);
        reasonsEt.setText("");
        partNoTv.setText(Utils.getNumber(model));
        exceptionCv.setValue(0);
        exceptionCv.setmMaxLimit(model.getTotalBoxes() - (model.getScannedCount() + exceptionCount));
        exceptionCv.setmMinLimit(1);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (exceptionPopup.getVisibility() == View.VISIBLE) {
                exceptionRg.check(R.id.rb_good);
                if (exceptionAlert != null && exceptionAlert.isShowing())
                    exceptionAlert.dismiss();
                return false;
            } else
                goBack();
        }
        return true;
    }

    private void goBack() {
        finish();
    }

    private void takePicture(Context context, int requestCode) {
        Utils.logD("*");
        Intent intent = new Intent(context, CameraActivity.class);
        startActivityForResult(intent, requestCode);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        switch (requestCode) {
            case TAKE_STACKING_PHOTO:
                try {
                    Utils.logD("**");
                    Uri uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
                    String imageUri = uri.toString();
                    bitmap = BitmapScalingUtil.bitmapFromUri(context,
                            Uri.parse(imageUri));
                    if (bitmap != null) {
                        int w = bitmap.getWidth();
                        int h = bitmap.getHeight();
                        Matrix mat = new Matrix();
                        if (w > h)
                            mat.postRotate(90);

                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                        Globals.bitmap = bitmap;

                        String diff = dataSource.sharedPreferences
                                .getValue(Constants.TIME_DIFF);
                        long d = 0;
                        if (Utils.isValidString(diff))
                            d = Long.valueOf(diff);

                        addStackingImage(bitmap, Utils.getImageTime(d), false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case TAKE_PHOTO:
                try {
                    Utils.logD("**");

                    Uri uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
                    String imageUri = uri.toString();
                    bitmap = BitmapScalingUtil.bitmapFromUri(context,
                            Uri.parse(imageUri));
                    if (bitmap != null) {
                        int w = bitmap.getWidth();
                        int h = bitmap.getHeight();
                        Matrix mat = new Matrix();
                        if (w > h)
                            mat.postRotate(90);

                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                        Globals.bitmap = bitmap;

                        String diff = dataSource.sharedPreferences
                                .getValue(Constants.TIME_DIFF);
                        long d = 0;
                        if (Utils.isValidString(diff))
                            d = Long.valueOf(diff);

                        addImage(bitmap, Utils.getImageTime(d), false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void addStackingImage(Bitmap bitmap, String imageFileName, boolean isCrop) {
        Utils.logD("***");

        if (truckIv != null && bitmap != null) {
            Utils.logD("*****");
            ImageLoader imageLoader = new ImageLoader(context, Constants.TRUCK);
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                stackingImgFileName = imageFileName;
                if (!Utils.isValidArrayList(stackingImgBitmaps)) {
                    stackingImgBitmaps = new ArrayList<>();
                    stackingImgFileNames = new ArrayList<>();
                    Utils.enableView(sendBtn);
                }
                if (stackingImagePosition == -1) {
                    stackingImgBitmaps.add(bitmap);
                    stackingImgFileNames.add(imageFileName);
                } else {
                    stackingImgBitmaps.set(stackingImagePosition, bitmap);
                    stackingImgFileNames.set(stackingImagePosition, imageFileName);
                }
                isStackingImg = true;
                stackingImagesAdapter = new ImagesAdapter(stackingImgBitmaps);
                stackingImagesRv.setAdapter(stackingImagesAdapter);
                stackingImagesAdapter.notifyDataSetChanged();
            }
        }
    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop) {
        Utils.logD("***");
        if (exceptionIv != null && bitmap != null) {
            Utils.logD("*****");
            ImageLoader imageLoader = ScanUtils.getImageLoader(context);
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                truckImageFileName = imageFileName;
                if (!Utils.isValidArrayList(imgBitmaps)) {
                    imgBitmaps = new ArrayList<>();
                    imgFileNames = new ArrayList<>();
                }
                if (imagePosition == -1) {
                    imgBitmaps.add(bitmap);
                    imgFileNames.add(imageFileName);
                } else {
                    imgBitmaps.set(imagePosition, bitmap);
                    imgFileNames.set(imagePosition, imageFileName);
                }
                isStackingImg = false;
                imagesRv.setLayoutManager(new LinearLayoutManager(context));
                imagesRv.setAdapter(imagesAdapter);
                imagesAdapter.notifyDataSetChanged();

                if (imagePosition == -1 && (imagesAlert == null || !imagesAlert.isShowing())) {
                    showImagesDialog(imgBitmaps);
                }
            }
        }
    }

    private AlertDialog imagesAlert;

    private void showImagesDialog(ArrayList<Bitmap> imageBitmaps) {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_capture_image, null, false);
        view.setVisibility(View.VISIBLE);
        imagesRv = (RecyclerView) view.findViewById(R.id.rv_exception_images);
        captureNewBtn = (Button) view.findViewById(R.id.btn_captue_new);
        cancelImgViewBtn = (Button) view.findViewById(R.id.btn_cancel_imgs);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));
        if (Utils.isValidArrayList(imageBitmaps)) {
            imagesAdapter = new ImagesAdapter(imageBitmaps);
            imagesRv.setAdapter(imagesAdapter);
        }
        cancelImgViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagesAlert.dismiss();
                if (imagesAlert != null && imagesAlert.isShowing())
                    imagesAlert.dismiss();
            }
        });
        captureNewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isValidArrayList(imageBitmaps) && imageBitmaps.size() == 3) {
                    Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                } else {
                    imagePosition = -1;
                    takePicture(context, TAKE_PHOTO);
                }
            }
        });
        alertDialog.setView(view);
        imagesAlert = alertDialog.show();
    }

    private void showExceptionType(final EditText exceptionTv,
                                   final String arr[]) {
        int selectedPos = -1;
        String text = URLDecoder
                .decode(exceptionTv.getText().toString().trim());
        if (Utils.isValidString(text)) {
            for (int i = 0; i < arr.length; i++) {
                if (text.equalsIgnoreCase(arr[i].toString())) {
                    selectedPos = i;
                    break;
                }
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setSingleChoiceItems(arr, selectedPos,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        dialog.dismiss();
                        exceptionTv.setText(arr[pos]);
                        exceptionTypeEt.setText(arr[pos]);
                        if (arr[pos].equals("Extra")) {
                            captureImageBtn.setVisibility(View.GONE);
                        } else {
                            captureImageBtn.setVisibility(View.VISIBLE);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_btn),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        }).show();

    }

    class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.DataObjectHolder> {
        private ArrayList<Bitmap> bitmaps;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            ImageView imageView, deleteIv;
            Button viewBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.iv_image);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                viewBtn = (Button) itemView.findViewById(R.id.btn_view);
                viewBtn.setOnClickListener(this);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (v.getId()) {
                    case R.id.btn_view:
                        int pos = (int) v.getTag();
                        if (isStackingImg) {
                            showImagePreview(stackingImgBitmaps.get(pos), pos);
                        } else {
                            showImagePreview(imgBitmaps.get(pos), pos);
                        }
                        break;
                    case R.id.iv_delete:
                        if (isStackingImg) {
                            stackingImgBitmaps.remove((int) v.getTag());
                        } else {
                            imgBitmaps.remove((int) v.getTag());
                        }
                        notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        }

        public ImagesAdapter(ArrayList<Bitmap> myDataset) {
            bitmaps = myDataset;
        }

        @Override
        public ImagesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_image_item, parent, false);
            return new ImagesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ImagesAdapter.DataObjectHolder holder, final int position) {
            Bitmap bitmap = bitmaps.get(position);
            if (bitmap != null) {
                holder.imageView.setImageBitmap(bitmap);

                holder.imageView.setTag(position);
                holder.deleteIv.setTag(position);
                holder.viewBtn.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            int count = bitmaps.size();
            return bitmaps.size();
        }

        public void update(ArrayList<Bitmap> data) {
            bitmaps.clear();
            bitmaps.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(Bitmap dataObj, int index) {
            bitmaps.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            bitmaps.remove(index);
            notifyItemRemoved(index);
        }
    }

    private void showImagePreview(Bitmap bitmap, int position) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Image Preview");
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_pod, null);
        ImageView docketIv;
        Button cancelBtn, reCaptureBtn;
        docketIv = (ImageView) view.findViewById(R.id.iv_pod);
        cancelBtn = (Button) view.findViewById(R.id.btn_crop);
        cancelBtn.setText("Cancel");
        reCaptureBtn = (Button) view.findViewById(R.id.btn_recapture);

        alertDialog.setView(view);
        byte[] decodedString = Base64.decode(getBase64(bitmap), Base64.NO_WRAP);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        decodedByte.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        docketIv.setImageBitmap(decodedByte);
        final AlertDialog alert = alertDialog.show();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        reCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                if (isStackingImg) {
                    stackingImagePosition = position;
                    takePicture(context, TAKE_STACKING_PHOTO);
                } else {
                    imagePosition = position;
                    takePicture(context, TAKE_PHOTO);
                }
            }
        });
    }
}