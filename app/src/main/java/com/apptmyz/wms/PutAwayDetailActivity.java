package com.apptmyz.wms;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.wms.custom.CounterView;
import com.apptmyz.wms.custom.CustomAlertDialog;
import com.apptmyz.wms.data.Bin;
import com.apptmyz.wms.data.CreateGrnInput;
import com.apptmyz.wms.data.CreateGrnPartJson;
import com.apptmyz.wms.data.GeneralResponse;
import com.apptmyz.wms.data.PutAwayData;
import com.apptmyz.wms.data.PutAwayResponse;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.HttpRequest;
import com.apptmyz.wms.util.Utils;
import com.apptmyz.wms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PutAwayDetailActivity extends BaseActivity {
    public static int BOXES_MAX_COUNT = 0;

    TextView titleTv, productNumTv, qtyTv;
    CounterView counterView;
    TextInputEditText binNumberEt;
    Button addBinButton, confirmBtn;
    ImageView scanBinIv;
    Context context;
    RecyclerView binsRv;
    LinearLayoutManager layoutManager;
    Gson gson = new Gson();
    PutAwayData bin = null;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan, quantity;
    private TimerTask timerScanTask;
    private DataSource dataSource;
    private String vehicleNum, inwardNum;
    private int partId, id;
    private boolean isBinInvalid = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_put_away_detail, frameLayout);

        context = PutAwayDetailActivity.this;
        dataSource = new DataSource(context);

        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Put Away");

        binsRv = (RecyclerView) findViewById(R.id.rv_bins);
        layoutManager = new LinearLayoutManager(this);
        binsRv.setLayoutManager(layoutManager);
        binsRv.setNestedScrollingEnabled(false);
        binAdapter = new BinAdapter(bins);

        productNumTv = (TextView) findViewById(R.id.tv_product_number);
        qtyTv = (TextView) findViewById(R.id.tv_qty);

        counterView = (CounterView) findViewById(R.id.counterView);
        binNumberEt = (TextInputEditText) findViewById(R.id.input_bin_number);
        binNumberEt.addTextChangedListener(watcher);
        scanBinIv = (ImageView) findViewById(R.id.iv_scan_bin);
        scanBinIv.setOnClickListener(listener);

        addBinButton = (Button) findViewById(R.id.btn_add_bin);
        confirmBtn = (Button) findViewById(R.id.btn_confirm);
        addBinButton.setOnClickListener(listener);
        confirmBtn.setOnClickListener(listener);

        if (getIntent().getExtras() != null) {
            vehicleNum = getIntent().getStringExtra("vehicleNum");
            inwardNum = getIntent().getStringExtra("inwardNum");
            String binStr = getIntent().getStringExtra("bin");
            bin = gson.fromJson(binStr, PutAwayData.class);
            id = bin.getId();
            partId = bin.getPartId();
            quantity = bin.getNoOfBoxesToScan();
            counterView.setValue(quantity);
            counterView.setmMaxLimit(quantity);
            BOXES_MAX_COUNT = quantity;
        }

        productNumTv.setText(bin.getPartNo());
        if (bin.getDataType().equals("P")) {
            productNumTv.setText(bin.getPartNo());
        } else {
            productNumTv.setText(bin.getDaNo());
        }
        qtyTv.setText(String.valueOf(quantity));
    }

    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binNumberEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() >= 2) {
                                        binNumber = scannedValue;
                                        int matchCount = dataSource.bins.matchesData(scannedValue);
                                        isBinInvalid = true;
                                        if (matchCount == 0) {
                                            isBinInvalid = false;
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN number doesn't exist");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = null;
            switch (view.getId()) {
                case R.id.iv_scan_bin:
                    binNumberEt.setText("");
                    binNumberEt.requestFocus();
                    break;
                case R.id.btn_add_bin:
                    String msg = validateBinDetails();
                    if (!Utils.isValidString(msg)) {
                        bins.add(bin);
                        bin = null;
                        binAdapter = new BinAdapter(bins);
                        binsRv.setAdapter(binAdapter);
                        binAdapter.notifyDataSetChanged();
                        binNumberEt.setText("");
                        counterView.setValue(BOXES_MAX_COUNT - addedBoxesCount);
                        counterView.setmMaxLimit(BOXES_MAX_COUNT - addedBoxesCount);
                    } else {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), msg);
                    }
                    break;
                case R.id.btn_confirm:
                    if (!Utils.isValidArrayList(bins)) {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Scan the BINs for submission");
                        return;
                    }
                    if (addedBoxesCount > BOXES_MAX_COUNT) {
                        reason = "";
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "You are trying to add more than the actual No. of Boxes");
                    } else {
                        reason = "";
                        submit();
                    }
                    break;
                default:
                    break;
            }
            if (intent != null)
                startActivity(intent);
        }
    };

    private CreateGrnInput generateSubmitJson() {
        CreateGrnInput input = new CreateGrnInput();
        input.setReason(reason);
        List<CreateGrnPartJson> partList = new ArrayList<>();
        for (PutAwayData data : bins) {
            CreateGrnPartJson model = new CreateGrnPartJson(data.getId(), data.getPartNo(), data.getPartId(), data.getTotalBoxes(),
                    data.getBinNumber(), data.getScanTime(), data.getVehNo(), data.getInwardNo(), data.getDataType(), data.getDaNo(), data.getGrnBoxes());
            partList.add(model);
        }
        input.setPartList(partList);

        return input;
    }

    private String reason;

    private void submit() {
        if (Utils.getConnectivityStatus(context)) {
            CreateGrnInput input = generateSubmitJson();
            new SubmitPutAway(input).execute();
        }
    }

    private String binNumber;
    private int addedBoxesCount;
    BinAdapter binAdapter;
    private ArrayList<PutAwayData> bins = new ArrayList<>();

    private String validateBinDetails() {
        String message = "";
        binNumber = (Utils.isValidString(binNumber)) ? binNumber : binNumberEt.getText().toString().trim();

        if (Utils.isValidString(binNumber)) {
            if (isBinInvalid) {
                int boxesCount = counterView.getValue();
                if (boxesCount != 0) {
                    if (boxesCount > BOXES_MAX_COUNT - addedBoxesCount) {
                        message = "You are trying to add more boxes than required";
                    } else {
                        addedBoxesCount += boxesCount;
                        bin = new PutAwayData();
                        bin.setId(id);
                        bin.setVehNo(vehicleNum);
                        bin.setInwardNo(inwardNum);
                        bin.setBinNumber(binNumberEt.getText().toString().trim());
                        bin.setTotalBoxes(boxesCount);
                        bin.setPartId(partId);
                        bin.setPartNo(productNumTv.getText().toString().trim());
                        bin.setScanTime(Utils.getTime(new Date(), getString(R.string.server_date_format)));
                    }
                } else {
                    message = "Please Scan Atleast one Product";
                }
            } else {
                message = "BIN Number doesn't exist";
            }
        } else {
            message = "Please Scan the Bin";
        }
        return message;
    }

    class BinAdapter extends RecyclerView.Adapter<BinAdapter.DataObjectHolder> {
        private ArrayList<PutAwayData> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView binNumTv, productNumTv, noOfBoxesTv;
            ImageView deleteIv;

            public DataObjectHolder(View itemView) {
                super(itemView);

                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_boxes_count);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.iv_delete) {
                    int position = (int) view.getTag();
                    PutAwayData deleteBin = bins.get(position);
                    int value = counterView.getValue();
                    counterView.setValue(value + deleteBin.getTotalBoxes());
                    addedBoxesCount -= deleteBin.getTotalBoxes();
                    bins.remove(position);
                    notifyDataSetChanged();
                }
            }
        }

        public BinAdapter(ArrayList<PutAwayData> myDataset) {
            list = myDataset;
        }

        @Override
        public BinAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_putaway_bin_item, parent, false);
            return new BinAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final BinAdapter.DataObjectHolder holder, final int position) {
            PutAwayData model = list.get(position);
            if (model != null) {
                holder.binNumTv.setText(model.getBinNumber());
                holder.productNumTv.setText(model.getPartNo());
                holder.noOfBoxesTv.setText(String.valueOf(model.getTotalBoxes()));

                holder.binNumTv.setTag(position);
                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.deleteIv.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<PutAwayData> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PutAwayData dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    ProgressDialog submitDialog;

    class SubmitPutAway extends AsyncTask<String, String, Object> {
        CreateGrnInput input;
        GeneralResponse response;

        SubmitPutAway(CreateGrnInput input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitPutAwayUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input, CreateGrnInput.class);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        if (BOXES_MAX_COUNT - addedBoxesCount == 0) {
                            dataSource.putAway.delete(id); //updateAsSubmitted
                        } else
                            dataSource.putAway.updateAsPartialScan(id, BOXES_MAX_COUNT - addedBoxesCount);
                    }
                    Utils.showToast(context, response.getMessage());
                }
                if (BOXES_MAX_COUNT - addedBoxesCount == 0) {
                    if (Utils.getConnectivityStatus(context)) {
                        new PutAwayCompletedDataTask().execute();
                    }
                } else {
                    Intent intent = new Intent(context, PutAwayActivity.class);
                    startActivity(intent);
                }
            }
            super.onPostExecute(result);
        }

    }

    private ProgressDialog putAwayCompletedDialog;

    class PutAwayCompletedDataTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                putAwayCompletedDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPutAwayCompletedUrl();

                Utils.logD("Log 1");
                PutAwayResponse response = (PutAwayResponse) HttpRequest
                        .getInputStreamFromUrl(url, PutAwayResponse.class,
                                context);

                if (response != null) {
                    dataSource.putAway.deleteCompletedData();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<PutAwayData> data = (ArrayList<PutAwayData>) response.getData();
                        if (Utils.isValidArrayList(data)) {
                            dataSource.putAway.insertPutAwayData(data, 1);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && putAwayCompletedDialog != null && putAwayCompletedDialog.isShowing()) {
                putAwayCompletedDialog.dismiss();
            }
            Intent intent = new Intent(context, PutAwayActivity.class);
            startActivity(intent);
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PutAwayDetailActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }
}
