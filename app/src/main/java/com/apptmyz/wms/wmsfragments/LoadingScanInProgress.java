package com.apptmyz.wms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.wms.LoadingScanningLatestScreen;
import com.apptmyz.wms.LoadingScanningScreen;
import com.apptmyz.wms.PartsScanningScreen;
import com.apptmyz.wms.R;
import com.apptmyz.wms.data.NewCon;
import com.apptmyz.wms.data.NewLoadingPartsModel;
import com.apptmyz.wms.data.NewPacket;
import com.apptmyz.wms.data.NewProductModel;
import com.apptmyz.wms.data.WmsLoadingModel;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class LoadingScanInProgress extends Fragment {
    public ArrayList<String> recentScannedList = new ArrayList<String>();
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    private View parentView;
    private ListView recentScannedListView;
    private TextView labelTv;
    private Context context;
    private LoadingScanningLatestScreen scanningScreen;
    private ListView toBeScannedLv;
    private MyAdapter adapter;
    private DataSource dataSource;

    private Button scanBtn;
    private boolean isRBChanged = false;
    public List<NewProductModel> list = new ArrayList<>();


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity.getApplicationContext();
        this.scanningScreen = (LoadingScanningLatestScreen) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.wms_scan_in_progress, container,
                false);

        dataSource = new DataSource(context);

        toBeScannedLv = (ListView) parentView
                .findViewById(R.id.lv_scan_in_progress);




        labelTv = (TextView) parentView.findViewById(R.id.tv_label);
        labelTv.setText(getString(R.string.to_be_scanned));

        adapter = new MyAdapter(context,R.layout.con_view,list);
        toBeScannedLv.setAdapter(adapter);

        return parentView;
    }

    public class MyAdapter extends ArrayAdapter<NewProductModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public MyAdapter(Context context, int layoutId, List<NewProductModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.partNoTv = (TextView) convertView.findViewById(R.id.tv_con_number);
            holder.boxCountTv = (TextView) convertView.findViewById(R.id.tv_badge);
            holder.grpIv = (ImageView) convertView.findViewById(R.id.iv_group_indicator);
            holder.conLayout = (RelativeLayout) convertView.findViewById(R.id.layout_con);

            holder.grpIv.setVisibility(View.GONE);

            NewProductModel loadingPartsModel = getItem(position);
            if (loadingPartsModel.getPickingComplete() == 1) {
                holder.conLayout.setBackground(getResources().getDrawable(R.drawable.docket_bg_green));
            } else {
                holder.conLayout.setBackground(getResources().getDrawable(R.drawable.docket_bg));

            }

            String dest = Utils.getNumber(loadingPartsModel);
            if (Utils.isValidString(dest))
                holder.partNoTv.setText(dest);
            else
                holder.partNoTv.setText("");

            holder.partNoTv.setTextColor(getResources().getColor(R.color.white));

            String totQty = String.valueOf(loadingPartsModel.getTotalBoxes());
            String scannedQty = String.valueOf(loadingPartsModel.getScannedBoxes());
            holder.boxCountTv.setText(scannedQty + "/" + totQty);

            return convertView;
        }

    }

    public class ViewHolder {
        public TextView partNoTv, boxCountTv;
        public ImageView grpIv;
        public RelativeLayout conLayout;
    }


    private void clearList() {
        if (list != null)
            list.clear();
        else
            list = new ArrayList<NewProductModel>();

        refresh();
    }

    public void reloadData(List<NewProductModel> data) {
        clearList();

        for (NewProductModel d : data)
            list.add(d);

        Utils.logD(list.toString());

        refresh();
    }

    private void refresh() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }
}