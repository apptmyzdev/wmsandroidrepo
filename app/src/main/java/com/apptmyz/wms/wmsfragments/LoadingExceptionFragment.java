package com.apptmyz.wms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptmyz.wms.R;
import com.apptmyz.wms.data.NewCon;
import com.apptmyz.wms.data.NewPacket;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.ScanUtils;
import com.apptmyz.wms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class LoadingExceptionFragment extends Fragment {
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    private View parentView;
    private ExpandableListAdapter adapter;
    private Context context;
    private ExpandableListView excepExpLv;
    private TextView labelTv;
    private DataSource dataSource;
    private View.OnClickListener groupClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            int groupPosition = (Integer) v.getTag();
            if (excepExpLv.isGroupExpanded(groupPosition))
                excepExpLv.collapseGroup(groupPosition);
            else
                excepExpLv.expandGroup(groupPosition);

        }
    };
    private OnChildClickListener onChildClick = new OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v,
                                    int groupPosition, int childPosition, long id) {

            String selectedConNo = groupData.get(groupPosition).getConNo();
            int selectedPacketNo = childData.get(groupPosition)
                    .get(childPosition).getPktNo();
            NewPacket packet = childData.get(groupPosition).get(childPosition);
            boolean isShortOrExtra = false;
            if (packet != null) {
                if (packet.getExcepType().equalsIgnoreCase(Constants.SHORT_CODE) || packet.getExcepType().equalsIgnoreCase(Constants.EXTRA_CODE))
                    isShortOrExtra = true;
            }

            return false;
        }

    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.to_be_scanned, container, false);

        dataSource = new DataSource(context);

        excepExpLv = (ExpandableListView) parentView
                .findViewById(R.id.elv_to_be_scanned);
        labelTv = (TextView) parentView.findViewById(R.id.tv_label);
        labelTv.setText(getString(R.string.exception));

        excepExpLv.setOnChildClickListener(onChildClick);
        adapter = new ExpandableListAdapter(context);
        excepExpLv.setAdapter(adapter);
        return parentView;
    }

    private int getNoOfScannedPkts(int groupPosition) {

        int noOfScannedPackets = 0;
        for (NewPacket scanPacket : childData.get(groupPosition)) {
            if (scanPacket.isScanned() && scanPacket.isExcep())
                noOfScannedPackets++;
        }

        return noOfScannedPackets;

    }

    private boolean shortsRecNotFitting(NewCon con) {
        boolean isRec = false;
        String conNumber = con.getConNo();
        NewCon newCon = con;  //dataSource.consData.getCompleteConData(Globals.selectedSheet.getSheetNo(), conNumber);
        for (NewPacket p : newCon.getPackets()) {
            if (!p.isScanned()) {
                if (Utils.isValidString(p.getExcepType())
                        && p.getExcepType().equalsIgnoreCase(
                        Constants.SHORT_CODE)
                        && Utils.isValidString(p.getExcpDtls()) && p.getExcpDtls().equalsIgnoreCase(Constants.NOT_FITTING_IN_VEHICLE)) {
                    isRec = true;
                } else {
                    isRec = false;
                    return isRec;
                }
            }
        }
        return isRec;
    }

    private void clearList() {

        if (groupData != null)
            groupData.clear();
        else
            groupData = new ArrayList<NewCon>();

        if (childData != null)
            childData.clear();
        else
            childData = new ArrayList<ArrayList<NewPacket>>();
    }

    public void resetData(ArrayList<NewCon> cons) {

        clearList();

        if (Utils.isValidArrayList(cons)) {
            for (NewCon con : cons) {
                Utils.logD(con.toString());
                if (!con.getConNo().equalsIgnoreCase(
                        Constants.INVALID_DKT_NUMBER)) {
                    groupData.add(con);
                    ArrayList<NewPacket> packets = new ArrayList<NewPacket>();
                    packets.addAll(con.getPackets());
                    childData.add(packets);
                }
            }
        }

        refresh();
    }

    public void refresh() {
        if (adapter != null)
            adapter.notifyDataSetChanged();

        if (Constants.isAlwaysExpandable) {
            for (int groupPosition = 0; groupPosition < groupData.size(); groupPosition++) {
                try {
                    excepExpLv.collapseGroup(groupPosition);
                    excepExpLv.expandGroup(groupPosition);

                } catch (Exception e) {
                }
            }
        }
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater inflater;
        private View.OnLongClickListener groupLongClickListener = new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                int groupPosition = (Integer) v.getTag();
                NewCon con = (NewCon) getGroup(groupPosition);
                if (shortsRecNotFitting(con)) {
//                    scanningScreen.removeShortReasonsPopUp(true, con.getConNo());
//                } else {
//                    int pkgs = con.getNumPkgsSheet();
//                    long count = con.getScannedPktCount();
//                    if (count < pkgs) {
//                        scanningScreen.showReasonsPopUp(true, con.getConNo(), 0);
//                    }
                }

                return false;
            }
        };

        public ExpandableListAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.packet_view, parent, false);
            NewPacket obj = (NewPacket) getChild(groupPosition, childPosition);
            TextView itemTv = (TextView) v.findViewById(R.id.tv_pkt_no);
            if (itemTv != null)
                itemTv.setText(String.valueOf(obj.getPktNo()));

            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childData.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groupData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {

            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.con_view, parent, false);

            v.setTag(groupPosition);

            NewCon con = (NewCon) getGroup(groupPosition);

            ImageView groupIndicator = (ImageView) v
                    .findViewById(R.id.iv_group_indicator);
            if (excepExpLv.isGroupExpanded(groupPosition))
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_maximized);
            else
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_minimized);

            TextView conTv = (TextView) v.findViewById(R.id.tv_con_number);
            TextView badgeTv = (TextView) v.findViewById(R.id.tv_badge);

            ImageView flightIv = (ImageView) v.findViewById(R.id.iv_flight);
            if (Utils.isValidString(con.getProduct())
                    && con.getProduct().equalsIgnoreCase("02"))
                flightIv.setVisibility(View.VISIBLE);
            else
                flightIv.setVisibility(View.GONE);

            int noOfScannedPackets = getNoOfScannedPkts(groupPosition);
            int noOfPkgs = con.getNumPkgsSheet();

            conTv.setText(con.getConNo());
            badgeTv.setText(ScanUtils.getGroupCounterText(noOfScannedPackets,
                    noOfPkgs));

            v.setOnClickListener(groupClickListener);
            v.setOnLongClickListener(groupLongClickListener);

            return v;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }

    }

}