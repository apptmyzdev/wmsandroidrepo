package com.apptmyz.wms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.wms.InvoicesScreen;
import com.apptmyz.wms.PartsScanningScreen;
import com.apptmyz.wms.R;
import com.apptmyz.wms.data.ExceptionModel;
import com.apptmyz.wms.data.InvoiceModel;
import com.apptmyz.wms.data.NewCon;
import com.apptmyz.wms.data.NewPacket;
import com.apptmyz.wms.data.NewProductModel;
import com.apptmyz.wms.data.ScanConData;
import com.apptmyz.wms.datacache.DataSource;
import com.apptmyz.wms.util.Constants;
import com.apptmyz.wms.util.Globals;
import com.apptmyz.wms.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ToBeScanned extends Fragment { // implements  AdapterView.OnItemLongClickListener {
    public List<NewProductModel> groupData = new ArrayList<>();
    public ArrayList<ArrayList<InvoiceModel>> childData = new ArrayList<ArrayList<InvoiceModel>>();
    private View parentView;
    private MyAdapter adapter;
    private Context mContext;
    private PartsScanningScreen scanningScreen;
    private ListView toBeScannedLv;
    private TextView labelTv, packetsTotalTv;
    private DataSource dataSource;
    public List<NewProductModel> list = new ArrayList<NewProductModel>();
    private ExpandableListAdapter expandableListAdapter;

    ListView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            NewProductModel productModel = (NewProductModel) adapterView.getAdapter().getItem(i);
            scanningScreen.showExceptionPopup(productModel);
            Globals.partNo = Utils.getNumber(productModel);
        }
    };

    ListView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
            NewProductModel productModel = (NewProductModel) adapterView.getAdapter().getItem(i);
            Utils.logD(productModel.getIsScanned());
            return false;
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mContext = activity.getApplicationContext();
        this.scanningScreen = (PartsScanningScreen) activity;
    }

    private ExpandableListView invoicesELv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.wms_to_be_scanned, container, false);

        dataSource = new DataSource(mContext);

        toBeScannedLv = (ListView) parentView
                .findViewById(R.id.lv_to_be_scanned);
        toBeScannedLv.setOnItemClickListener(onItemClickListener);
        toBeScannedLv.setOnItemLongClickListener(onItemLongClickListener);
        labelTv = (TextView) parentView.findViewById(R.id.tv_label);
        labelTv.setText(getString(R.string.to_be_scanned));

        packetsTotalTv = (TextView) parentView
                .findViewById(R.id.tv_packets_total);

        adapter = new MyAdapter(mContext, R.layout.con_view, list);
        toBeScannedLv.setAdapter(adapter);

        invoicesELv = (ExpandableListView) parentView.findViewById(R.id.invoices_elv);
        expandableListAdapter = new ExpandableListAdapter(mContext);
        invoicesELv.setAdapter(expandableListAdapter);

        return parentView;
    }

    public class MyAdapter extends ArrayAdapter<NewProductModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public MyAdapter(Context context, int layoutId, List<NewProductModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.partNoTv = (TextView) convertView.findViewById(R.id.tv_con_number);
            holder.boxCountTv = (TextView) convertView.findViewById(R.id.tv_badge);
            holder.grpIv = (ImageView) convertView.findViewById(R.id.iv_group_indicator);
            holder.conLayout = (RelativeLayout) convertView.findViewById(R.id.layout_con);
            holder.grpIv.setVisibility(View.GONE);

            NewProductModel newProductModel = getItem(position);

            String dest = Utils.getNumber(newProductModel);
            if (Utils.isValidString(dest))
                holder.partNoTv.setText(dest);
            else
                holder.partNoTv.setText("");

            holder.partNoTv.setTextColor(getResources().getColor(R.color.white));

            String totQty = String.valueOf(newProductModel.getTotalBoxes());
            String scannedQty = String.valueOf(newProductModel.getScannedCount());

            int count = dataSource.exceptions.getExceptionCount(Utils.getNumber(newProductModel), newProductModel.getDataType(), newProductModel.getVehicleNo());
            int extrasCount = dataSource.exceptions.getExtrasCount(Utils.getNumber(newProductModel), newProductModel.getDataType(), newProductModel.getVehicleNo());
            int scannedCount = newProductModel.getScannedCount();
            holder.boxCountTv.setText((scannedCount + count + extrasCount) + "/" + totQty);

            if (newProductModel.getTotalBoxes() == scannedCount + count) {
                holder.conLayout.setBackground(getResources().getDrawable(R.drawable.docket_bg_green));
            } else {
                holder.conLayout.setBackground(getResources().getDrawable(R.drawable.docket_bg));
            }
            return convertView;
        }
    }

    public class ViewHolder {
        public TextView partNoTv, boxCountTv;
        public ImageView grpIv;
        public RelativeLayout conLayout;
    }

    private void clearList() {
        if (list != null)
            list.clear();
        else
            list = new ArrayList<NewProductModel>();

        refresh();
    }

    private void refresh() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    public void reloadData(ArrayList<NewProductModel> productList) {

        clearPPMList();

        if (Utils.isValidArrayList(productList)) {

            for (NewProductModel model : productList) {
                groupData.add(model);
                ArrayList<InvoiceModel> invoiceModels = new ArrayList<InvoiceModel>();
                if (Utils.isValidArrayList((ArrayList<?>) model.getInvoiceList()))
                    invoiceModels.addAll(model.getInvoiceList());
                childData.add(invoiceModels);
            }
        }

        refreshPPM();
    }

    private void clearPPMList() {

        if (groupData != null)
            groupData.clear();
        else
            groupData = new ArrayList<NewProductModel>();

        if (childData != null)
            childData.clear();
        else
            childData = new ArrayList<ArrayList<InvoiceModel>>();
    }

    public void refreshPPM() {
        if (expandableListAdapter != null)
            expandableListAdapter.notifyDataSetChanged();

        for (int groupPosition = 0; groupPosition < groupData.size(); groupPosition++) {
            try {
                invoicesELv.expandGroup(groupPosition);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater inflater;
        private View.OnClickListener groupClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                NewProductModel productModel = (NewProductModel) groupData.get(pos);
                scanningScreen.showExceptionPopup(productModel);
                Globals.partNo = Utils.getNumber(productModel);
            }
        };

        public ExpandableListAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            View v = inflater.inflate(R.layout.item_to_be_scanned_child, parent, false);

            v.setTag(R.string.group_pos, groupPosition);
            v.setTag(R.string.child_pos, childPosition);


            InvoiceModel invoiceModel = (InvoiceModel) getChild(groupPosition, childPosition);
            TextView custCodeTv = (TextView) v.findViewById(R.id.tv_cust_code);
            TextView partNoTv = (TextView) v.findViewById(R.id.tv_part_no);
            TextView partQtyTv = (TextView) v.findViewById(R.id.tv_part_qty);


            if (Utils.isValidString(invoiceModel.getCustCode()))
                custCodeTv.setText(invoiceModel.getCustCode());

            partQtyTv.setText(invoiceModel.getCustPartNo());

            partNoTv.setText(invoiceModel.getBatchNo());
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int gPos = (Integer) v.getTag(R.string.group_pos);
                    int cPos = (Integer) v.getTag(R.string.child_pos);

                    NewProductModel productModel = (NewProductModel) getGroup(gPos);
                    InvoiceModel invoiceModel1 = (InvoiceModel) getChild(gPos, cPos);
                }
            });

            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childData.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groupData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.con_view, parent, false);

            v.setTag(groupPosition);

            ImageView groupIndicator = (ImageView) v.findViewById(R.id.iv_group_indicator);
            groupIndicator.setVisibility(View.GONE);

            TextView partNoTv = (TextView) v.findViewById(R.id.tv_con_number);
            TextView boxCountTv = (TextView) v.findViewById(R.id.tv_badge);
            ImageView grpIv = (ImageView) v.findViewById(R.id.iv_group_indicator);
            RelativeLayout conLayout = (RelativeLayout) v.findViewById(R.id.layout_con);
            grpIv.setVisibility(View.GONE);

            NewProductModel newProductModel = (NewProductModel) getGroup(groupPosition);

            String dest = Utils.getNumber(newProductModel);
            if (Utils.isValidString(dest))
                partNoTv.setText(dest);
            else
                partNoTv.setText("");

            partNoTv.setTextColor(getResources().getColor(R.color.white));

            String totQty = String.valueOf(newProductModel.getTotalBoxes());
            String scannedQty = String.valueOf(newProductModel.getScannedCount());

            int count = dataSource.exceptions.getExceptionCount(Utils.getNumber(newProductModel), newProductModel.getDataType(), newProductModel.getVehicleNo());
            int extrasCount = dataSource.exceptions.getExtrasCount(Utils.getNumber(newProductModel), newProductModel.getDataType(), newProductModel.getVehicleNo());
            int scannedCount = newProductModel.getScannedCount();
            boxCountTv.setText((scannedCount + count + extrasCount) + "/" + totQty);

            if (newProductModel.getTotalBoxes() == scannedCount + count) {
                conLayout.setBackground(getResources().getDrawable(R.drawable.docket_bg_green));
            } else {
                conLayout.setBackground(getResources().getDrawable(R.drawable.docket_bg));
            }
            v.setOnClickListener(groupClickListener);

            return v;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }

    }

}
